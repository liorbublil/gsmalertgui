#ifndef COM_TRD_H
#define COM_TRD_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "StdAfx.h"

#define COM_TIME_OUT 2000

#define UM_COMMAND_ARRIVED WM_USER+1
#define UM_SEND_COMMAND WM_USER+2
#define UM_SENSORS_LIST_CHANGED WM_USER+3

#define COMMAND_MESSAGE_MODEM_RESPOND 0
#define COMMAND_MESSAGE_TIME_OUT 1
#define COMMAND_MESSAGE_UNIT_NOT_CONNECTED 2
#define COMMAND_MESSAGE_UNIT_FOUND 3

#define START_OF_MSG_COMMAND "msg"
#define END_OF_MSG_COMMAND "~~~\r"

#define PASS_MSG_COMMAND "#pass"

#define ENCR_MSG_COMMAND "#get_encr"
#define ENCR_MSG_RESPOND "#get_encr: "

#define DISCONNECT_MSG_COMMAND "#exit"

#define SET_PHONE_LIST_MSG_COMMAND "#set_phone_list"
#define SET_PHONE_LIST_MSG_RESPOND "#set_phone_list: "

#define PHONE_NUM_REQ_MSG_COMMAND "#get_phones"
#define PHONE_NUM_REQ_MSG_RESPOND "#get_phones: "

#define ADC_SET_MSG_COMMAND "#adc_set="
#define ADC_SET_MSG_RESPOND "#adc_set: "

#define ADC_GET_MSG_COMMAND "#adc_get="
#define ADC_GET_MSG_RESPOND "#adc_get: "

#define UNIT_VOLT_CLOUD_GET_MSG_COMMAND "#unit_volt_cloud_notify_get"
#define UNIT_VOLT_CLOUD_GET_MSG_RESPOND "#unit_volt_cloud_notify_get: "

#define UNIT_VOLT_CLOUD_SET_MSG_COMMAND "#unit_volt_cloud_notify_set="
#define UNIT_VOLT_CLOUD_SET_MSG_RESPOND "#unit_volt_cloud_notify_set: "


#define LEVEL_EVENT_GET_MSG_COMMAND "#level_events_get"
#define LEVEL_EVENT_GET_MSG_RESPOND "#level_events_get: "

#define LEVEL_EVENT_SET_MSG_COMMAND "#level_events_set="
#define LEVEL_EVENT_SET_MSG_RESPOND "#level_events_set: "

#define ONE_WIRE_SENSOR_GET_MSG_COMMAND "#1_wire_sensors_get"
#define ONE_WIRE_SENSOR_GET_MSG_RESPOND "#1_wire_sensors_get: "

#define ONE_WIRE_SENSOR_SET_MSG_COMMAND "#1_wire_sensors_set="
#define ONE_WIRE_SENSOR_SET_MSG_RESPOND "#1_wire_sensors_set: "

#define ONE_WIRE_SENSOR_MARK_MSG_COMMAND "#temp_sensor_mark="
#define ONE_WIRE_SENSOR_MARK_MSG_RESPOND "#temp_sensor_mark: "

#define ONE_WIRE_SENSOR_QUERY_MSG_COMMAND "#1_wire_sensors_query"
#define ONE_WIRE_SENSOR_QUERY_MSG_RESPOND "#1_wire_sensors_query: "

#define ADC_SENSOR_GET_MSG_COMMAND "#adc_sensors_get"
#define ADC_SENSOR_GET_MSG_RESPOND "#adc_sensors_get: "

#define ADC_SENSOR_SET_MSG_COMMAND "#adc_sensors_set="
#define ADC_SENSOR_SET_MSG_RESPOND "#adc_sensors_set: "

#define SET_UNIT_TIME_MSG_COMMAND "#set_clock"
#define SET_UNIT_TIME_MSG_RESPOND "#set_clock: "

#define CHANGE_PASS_MSG_COMMAND "#change_pass"
#define CHANGE_PASS_MSG_RESPOND "#change_pass: "

#define GET_UNIT_NAME_MSG_COMMAND "#get_unit_name"
#define GET_UNIT_NAME_MSG_RESPOND "#get_unit_name: "

#define SET_UNIT_NAME_MSG_COMMAND "#set_unit_name"
#define SET_UNIT_NAME_MSG_RESPOND "#set_unit_name: "

#define GET_GATE_DELAY_MSG_COMMAND "#get_gate_delay"
#define GET_GATE_DELAY_MSG_RESPOND "#get_gate_delay: "

#define SET_GATE_DELAY_MSG_COMMAND "#set_gate_delay"
#define SET_GATE_DELAY_MSG_RESPOND "#set_gate_delay: "

#define GET_SELF_SMS_MSG_COMMAND "#get_self_cu_sms"
#define GET_SELF_SMS_MSG_RESPOND "#get_self_cu_sms: "

#define SET_SELF_SMS_MSG_COMMAND "#set_self_cu_sms"
#define SET_SELF_SMS_MSG_RESPOND "#set_self_cu_sms: "

#define GET_BAND_SELECT_MSG_COMMAND "#get_band_select"
#define GET_BAND_SELECT_MSG_RESPOND "#get_band_select: "

#define SET_BAND_SELECT_MSG_COMMAND "#set_band_select"
#define SET_BAND_SELECT_MSG_RESPOND "#set_band_select: "

#define GET_VERSION_MSG_COMMAND "#get_version"
#define GET_VERSION_MSG_RESPOND "#get_version: "

#define GET_RECEPTION_MSG_COMMAND "#get_reception"
#define GET_RECEPTION_MSG_RESPOND "#get_reception: "

#define GET_INTERNET_PARAMS_MSG_COMMAND "#get_gprs_params"
#define GET_INTERNET_PARAMS_MSG_RESPOND "#get_gprs_params: "

#define SET_INTERNET_PARAMS_MSG_COMMAND "#set_gprs_params"
#define SET_INTERNET_PARAMS_MSG_RESPOND "#set_gprs_params: "


#define SET_ZONE_ACTIVE_MODE_MSG_COMMAND "#set_zone_activation_mode"
#define SET_ZONE_ACTIVE_MODE_MSG_RESPOND "#set_zone_activation_mode: "

#define GET_ZONE_ACTIVE_MODE_MSG_COMMAND "#get_zone_activation_mode"
#define GET_ZONE_ACTIVE_MODE_MSG_RESPOND "#get_zone_activation_mode: "

#define SET_INPUT_DEBOUNCE_MSG_COMMAND "#set_input_debounce"
#define SET_INPUT_DEBOUNCE_MSG_RESPOND "#set_input_debounce: "

#define GET_INPUT_DEBOUNCE_MSG_COMMAND "#get_input_debounce"
#define GET_INPUT_DEBOUNCE_MSG_RESPOND "#get_input_debounce: "

#define ZONES_REQ_MSG_COMMAND "#get_zones"
#define ZONES_REQ_MSG_RESPOND "#get_zones: "

#define SET_ZONES_MSG_COMMAND "#set_zones"
#define SET_ZONES_MSG_RESPOND "#set_zones: "

#define GET_ZONES_QUERY_MSG_COMMAND "#get_zones_snapshot"
#define GET_ZONES_QUERY_MSG_RESPOND "#get_zones_snapshot: "

#define SET_OUTPUTS_MSG_COMMAND "#set_outputs"
#define SET_OUTPUTS_MSG_RESPOND "#set_outputs: "

#define OUTPUTS_REQ_MSG_COMMAND "#get_outputs"
#define OUTPUTS_REQ_MSG_RESPOND "#get_outputs: "

#define GET_LOG_LEN_MSG_COMMAND "#get_log_len"
#define GET_LOG_LEN_MSG_RESPOND "#get_log_len: "

#define GET_LOG_DATA_MSG_COMMAND "#get_log_data"
#define GET_LOG_DATA_MSG_RESPOND "#get_log_data: "

#define DEL_LOG_FILES_MSG_COMMAND "#del_log"
#define DEL_LOG_FILES_MSG_RESPOND "#del_log: "

#define SET_REMINDER_MSG_COMMAND "#set_reminder"
#define SET_REMINDER_MSG_RESPOND "#set_reminder: "

#define GET_REMINDER_MSG_COMMAND "#get_reminder"
#define GET_REMINDER_MSG_RESPOND "#get_reminder: "

#define SET_TEST_ALARM_MSG_COMMAND "#set_test_alarm"
#define SET_TEST_ALARM_MSG_RESPOND "#set_test_alarm: "

#define GET_TEST_ALARM_MSG_COMMAND "#get_test_alarm"
#define GET_TEST_ALARM_MSG_RESPOND "#get_test_alarm: "

#define SET_SMS_TO_LOGGER_MSG_COMMAND "#sms_to_logger_set"
#define SET_SMS_TO_LOGGER_MSG_RESPOND "#sms_to_logger_set: "

#define GET_SMS_TO_LOGGER_MSG_COMMAND "#sms_to_logger_get"
#define GET_SMS_TO_LOGGER_MSG_RESPOND "#sms_to_logger_get: "


#define MODBUS_GET_MSG_COMMAND "#get_modbus_regs"
#define MODBUS_GET_MSG_RESPOND "#get_modbus_regs: "

#define MODBUS_SET_MSG_COMMAND "#set_modbus_regs="
#define MODBUS_SET_MSG_RESPOND "#set_modbus_regs: "

#define MODBUS_PORT_SET_MSG_COMMAND "#set_modbus_port_param="
#define MODBUS_PORT_SET_MSG_RESPOND "#set_modbus_port_param: "

#define MODBUS_PORT_GET_MSG_COMMAND "#get_modbus_port_param"
#define MODBUS_PORT_GET_MSG_RESPOND "#get_modbus_port_param: "

#define MODBUS_GET_RESULTS_MSG_COMMAND "#get_modbus_results"
#define MODBUS_GET_RESULTS_MSG_RESPOND "#get_modbus_results: "


#define WL_SENSORS_QUERY_SENSORS_MSG_COMMAND "#get_rf_data"
#define WL_SENSORS_QUERY_SENSORS_MSG_RESPOND "#get_rf_data: "

#define WL_SENSORS_CLEAR_SENSORS_MSG_COMMAND "#clear_rf_data"
#define WL_SENSORS_CLEAR_SENSORS_MSG_RESPOND "#clear_rf_data: "

#define WL_SENSORS_SET_MSG_COMMAND "#set_rf_params="
#define WL_SENSORS_SET_MSG_RESPOND "#set_rf_params: "

#define WL_SENSORS_GET_MSG_COMMAND "#get_rf_params"
#define WL_SENSORS_GET_MSG_RESPOND "#get_rf_params: "

#define RSSI_TO_CLOUD_GET_MSG_COMMAND "#rssi_cloud_notify_get"
#define RSSI_TO_CLOUD_GET_MSG_RESPOND "#rssi_cloud_notify_get: "

#define RSSI_TO_CLOUD_SET_MSG_COMMAND "#rssi_cloud_notify_set"
#define RSSI_TO_CLOUD_SET_MSG_RESPOND "#rssi_cloud_notify_set: "

#define LOG_ROW_MSG_RESPOND "LOG:"

#define ACK_RESPOND "ok"
#define ERROR_RESPOND "error"
#define BUSY_RESPOND "busy"
#define END_OF_BUSRT_RESPOND "\r\n"

#define MODEM_INIT_COMMAND "ATE0;V1;+CMEE=0;+IFC=0,0;+CBST=71,0,1\r"
#define MODEM_CHECK_CREG_COMMAND "AT+CREG?\r"
#define MODEM_DIAL_COMMAND "ATD"
#define MODEM_DISCONNECT_COMMAND "+++"
#define MODEM_HANGUP_COMMAND "ATH\r"


#define MODEM_CREG_RESPOND_REG "+CREG: 0,1"
#define MODEM_CREG_RESPOND_ROAMING "+CREG: 0,5"

#define MODEM_COMMAND_PREFIX "AT"

#define MODEM_ACK_RESPOND "OK"
#define MODEM_ERROR_RESPOND "ERROR"
#define MODEM_BUSY_RESPOND "BUSY"
#define MODEM_CONNECT_RESPOND "CONNECT"
#define MODEM_NO_CARRIER_RESPOND "NO CARRIER"
#define MODEM_END_OF_BUSRT_RESPOND "\r"
#define MODEM_END_OF_MSG_COMMAND "\r"

struct CComTrdVars
{
	HANDLE *l_hComPort;
	HWND m_hWnd;
};

struct CComTrdDataInVars
{
	int m_iDataLen;
	char *m_pData;
};

UINT ComThread(void *a_ComPortTrdVars);//The thread that listen to the rs232 and get the trigger
#endif