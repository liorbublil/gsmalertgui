#include "stdafx.h"
#include "DualSplitWnd.h"
#include "BarSplitWnd.h"
#include "MPIChildFrame.h"
#include "MainFrm.h"
#include "MPITabWnd.h"
#include "GsmAlert.h"
#include "GsmAlertDoc.h"
#include "MiscSettingsView.h"
#include "PhoneListView.h"
#include "LogView.h"
#include "ZoneListView.h"
#include "AdcView.h"
#include "LevelAlertView.h"
#include "ModbusView.h"
#include "WirelessSenseView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMPIChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CMPIChildFrame, CMDIChildWnd)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CMPIChildFrame)
	ON_WM_CLOSE()
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


CMPIChildFrame::CMPIChildFrame()
{
	m_enmMPIChildFrameType=enmMPIChildFrameTypeNoType;
	m_pLogView=NULL;
	m_pPhoneListView=NULL;
	m_pZoneListView=NULL;
	m_pMiscSettingsView=NULL;
}

CMPIChildFrame::~CMPIChildFrame()
{
	POSITION pos = m_listSplitters.GetHeadPosition();
	while (pos)
		delete m_listSplitters.GetNext(pos);
}

BOOL CMPIChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CMDIChildWnd::PreCreateWindow(cs))
		return FALSE;

	// disable closing or resizing the child window
	cs.style &= ~WS_SYSMENU;

	return TRUE;
}


#ifdef _DEBUG
void CMPIChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CMPIChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}
#endif //_DEBUG


void CMPIChildFrame::ActivateFrame(int nCmdShow) 
{
	CMDIChildWnd::ActivateFrame(SW_SHOWMAXIMIZED);
}

void CMPIChildFrame::OnClose() 
{
	// do nothing
}

void CMPIChildFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	CGsmAlertDoc *l_pDoc=(CGsmAlertDoc*)GetActiveDocument();
	GetMDIFrame()->OnUpdateFrameTitle(FALSE);

	// display child frame title instead of the document's title
	SetWindowText(l_pDoc->m_strUnitName);
}

BOOL CMPIChildFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	switch(m_enmMPIChildFrameType)
	{
	case enmMPIChildFrameTypeLog:
		return CreateStatusClient(pContext);
	case enmMPIChildFrameTypeSettings:
		return CreateSettingsClient(pContext);
	}
	return TRUE;
}

void CMPIChildFrame::OnUpdateFrameMenu(BOOL bActivate, CWnd* pActivateWnd, HMENU hMenuAlt)
{
	CMainFrame* pFrame = (CMainFrame*)GetMDIFrame();

	if (hMenuAlt == NULL)
		hMenuAlt = m_hMenuShared;

	// change the menu in the main frame's rebar control
	if (hMenuAlt != NULL && bActivate)
		pFrame->UpdateMenu(hMenuAlt);
	else if (hMenuAlt != NULL && !bActivate && pActivateWnd == NULL)
		pFrame->UpdateMenu(pFrame->m_hMenuDefault);
}

bool CMPIChildFrame::SetChildType(enmMPIChildFrameType a_enmNewType)
{
	if(m_enmMPIChildFrameType==enmMPIChildFrameTypeNoType)
	{
		m_enmMPIChildFrameType=a_enmNewType;
		return true;
	}
	else
	{
		return false;
	}
}

BOOL CMPIChildFrame::CreateStatusClient(CCreateContext* pContext)
{
	// get view's runtime class information
	CRuntimeClass* pViewClass = RUNTIME_CLASS(CLogView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	pContext->m_pNewViewClass = pViewClass;
	if((m_pLogView=(CLogView*)CreateView(pContext, AFX_IDW_PANE_FIRST))==NULL)
	{
		return FALSE;
	}
	return TRUE;
}

BOOL CMPIChildFrame::CreateSettingsClient(CCreateContext* pContext)
{
	int l_iIndex = 1;

	// create the tabbed splitter window...
	CMPITabWnd* pTabWnd = new CMPITabWnd;

	// ...as the child frame's child
	if (!pTabWnd->CreateStatic(this, 7, 1))
		return FALSE;

	// create the tab control and load tabs from toolbar resource
	int nTabs = pTabWnd->CreateTabs(IDR_SETTINGS_TABS);
	if (!nTabs)
		return FALSE;

	// create tabs content recursively

	CRuntimeClass* pViewClass;
	CView* pView;
	
	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CPhoneListView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0,0,0,0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CPhoneListView,pView);
	m_pPhoneListView=(CPhoneListView*)pView;
	++l_iIndex;

	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CZoneListView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0,0,0,0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CZoneListView,pView);
	m_pZoneListView=(CZoneListView*)pView;
	++l_iIndex;


	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CAdcView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0,0,0,0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CAdcView,pView);
	m_pAdcView=(CAdcView*)pView;
	++l_iIndex;

	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CWirelessSenseView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0, 0, 0, 0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CWirelessSenseView, pView);
	m_pWirelessSenseView = (CWirelessSenseView*)pView;
	++l_iIndex;

	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CModbusView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0, 0, 0, 0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CModbusView, pView);
	m_pModbusView= (CModbusView*)pView;
	++l_iIndex;


	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CLevelAlertView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0,0,0,0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CLevelAlertView,pView);
	m_pLevelAlertView=(CLevelAlertView*)pView;
	++l_iIndex;


	// get view's runtime class information
	pViewClass = RUNTIME_CLASS(CMiscSettingsView);
	ASSERT_POINTER(pViewClass, CRuntimeClass);
	// create view object and window
	pView = (CView*)pViewClass->CreateObject();
	if (!pView->Create(NULL, NULL, AFX_WS_DEFAULT_VIEW & ~WS_BORDER, CRect(0,0,0,0), pTabWnd, l_iIndex, pContext))
		return FALSE;
	ASSERT_KINDOF(CMiscSettingsView,pView);
	m_pMiscSettingsView=(CMiscSettingsView*)pView;
	++l_iIndex;


	// initialize - display the first page only
	pTabWnd->SetPage(0);
	// remember the created window
	m_listSplitters.AddTail(pTabWnd);

	return TRUE;
}

LRESULT CMPIChildFrame::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CMPIChildFrame::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		return 0;
	}
	else if((wParam==COMMAND_MESSAGE_UNIT_FOUND)||(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED))
	{
		INFO_LOG(g_DbLogger,CString("CMPIChildFrame::OnCommandArrived (wParam==COMMAND_MESSAGE_UNIT_FOUND)||(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)"));
		switch(m_enmMPIChildFrameType)
		{
		case enmMPIChildFrameTypeSettings:
			m_pPhoneListView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			m_pZoneListView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			m_pMiscSettingsView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			m_pAdcView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			m_pLevelAlertView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			m_pWirelessSenseView->PostMessage(UM_COMMAND_ARRIVED, wParam, lParam);
			m_pModbusView->PostMessage(UM_COMMAND_ARRIVED, wParam, lParam);
			break;
		case enmMPIChildFrameTypeLog:
			m_pLogView->PostMessage(UM_COMMAND_ARRIVED,wParam,lParam);
			break;
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CMPIChildFrame::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}
	
	CString *l_pMessage=(CString*)lParam;
	delete l_pMessage;
	INFO_LOG(g_DbLogger,CString("exit CMPIChildFrame::OnCommandArrived"));
	return 0;
}
