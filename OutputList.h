// OutputList.h: interface for the COutputList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUTPUTLIST_H__412E00E3_0A93_45C2_8D84_38F12A4A0465__INCLUDED_)
#define AFX_OUTPUTLIST_H__412E00E3_0A93_45C2_8D84_38F12A4A0465__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COutputListElement : public CObject
{
public:
	COutputListElement();
	~COutputListElement();
	int m_iLocInMem;
	int m_iOutputState;
	int m_iOutputStateParam;
	bool m_bInUnit;
	CString ElementToStr();
	void StrToElement(CString a_strData);
};

class COutputList : public CTypedPtrList< CObList, COutputListElement* > 
{
public:
	DECLARE_SERIAL( COutputList )

	COutputList();
	virtual ~COutputList();
	void Serialize( CArchive& archive );
	
	void DeleteAllList();
	COutputListElement* AddItemSorted(int a_iLocInMem,int a_iOutputState,int m_iOutputStateParam, bool a_bInUnit,bool a_bNoSearch);
	COutputListElement* GetItemByLocInMem(int a_iLocInMem);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_OUTPUTLIST_H__412E00E3_0A93_45C2_8D84_38F12A4A0465__INCLUDED_)