/*********************************************************
* Splitter Window Extension
* Version: 1.3
* Date: March 6, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* Copyright (C) 2002-03 by Michal Mecinski
*********************************************************/

#include "stdafx.h"
#include "DualSplitWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CDualSplitWnd, CSplitterWnd);

CDualSplitWnd::CDualSplitWnd()
{
	m_nPrev = -1;
	m_bChange = FALSE;
	m_dRatio = 0.5;
	SystemParametersInfo(SPI_GETDRAGFULLWINDOWS, 0, &m_bDragFull, 0);
}

CDualSplitWnd::~CDualSplitWnd()
{
}


BEGIN_MESSAGE_MAP(CDualSplitWnd, CSplitterWnd)
	//{{AFX_MSG_MAP(CDualSplitWnd)
	ON_WM_MOUSEMOVE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


void CDualSplitWnd::OnInvertTracker(const CRect& rect)
{
	if (!m_bDragFull)
		CSplitterWnd::OnInvertTracker(rect);
}

void CDualSplitWnd::SetSplitCursor(int ht)
{
	if (m_bDragFull)	// use system cursors
	{
		if (ht >= vSplitterBar1 && ht <= vSplitterBar15)
			SetCursor(LoadCursor(NULL, IDC_SIZENS));
		else if (ht >= hSplitterBar1 && ht <= hSplitterBar15)
			SetCursor(LoadCursor(NULL, IDC_SIZEWE));
		else if (ht >= splitterIntersection1 && ht <= splitterIntersection225)
			SetCursor(LoadCursor(NULL, IDC_SIZEALL));
		else
			SetCursor(LoadCursor(NULL, IDC_ARROW));
	}
	else
		CSplitterWnd::SetSplitCursor(ht);
}

void CDualSplitWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	// only static splitter supported
	ASSERT(!(GetStyle() & SPLS_DYNAMIC_SPLIT));

	if (!m_bDragFull)	// normal dragging
	{
		CSplitterWnd::OnMouseMove(nFlags, point);
		if (m_bTracking)
			m_bChange = TRUE;
		return;
	}

	if (m_bTracking && GetCapture() != this)
		StopTracking(FALSE);

	if (m_bTracking)
	{
		if (point.y < m_rectLimit.top)
			point.y = m_rectLimit.top;
		else if (point.y > m_rectLimit.bottom)
			point.y = m_rectLimit.bottom;
		if (point.x < m_rectLimit.left)
			point.x = m_rectLimit.left;
		else if (point.x > m_rectLimit.right)
			point.x = m_rectLimit.right;

		if (m_htTrack == vSplitterBar1)
		{
			if (m_pRowInfo[0].nCurSize != point.y - m_nTrackPos)
			{
				m_pRowInfo[0].nIdealSize = point.y - m_nTrackPos;
				RecalcLayout();
				RedrawWindow();
				m_bChange = TRUE;
			}
		}
		else if (m_htTrack == hSplitterBar1)
		{
			if (m_pColInfo[0].nIdealSize != point.x - m_nTrackPos)
			{
				m_pColInfo[0].nIdealSize = point.x - m_nTrackPos;
				RecalcLayout();
				RedrawWindow();
				m_bChange = TRUE;
			}
		}
	}
	else
	{
		int ht = HitTest(point);
		SetSplitCursor(ht);
	}
}

void CDualSplitWnd::OnSize(UINT nType, int cx, int cy) 
{
	if (!m_pColInfo)
		return;

	if (m_nCols==2 && m_nRows==1)	// vertical resize
	{
		int cxCur, cxMin;
		GetColumnInfo(0, cxCur, cxMin);

		if (m_bChange && m_nPrev>0)
		{
			m_dRatio = (double)cxCur / (double)m_nPrev;	// calc new ratio
			m_bChange = FALSE;
		}
		SetColumnInfo(0, (int)(cx * m_dRatio), cxMin);
		RecalcLayout();

		m_nPrev = cx;
	}
	else if (m_nCols==1 && m_nRows==2)	// horizontal resize
	{
		int cyCur, cyMin;
		GetRowInfo(0, cyCur, cyMin);

		if (m_bChange && m_nPrev>0)
		{
			m_dRatio = (double)cyCur / (double)m_nPrev;
			m_bChange = FALSE;
		}
		SetRowInfo(0, (int)(cy * m_dRatio), cyMin);
		RecalcLayout();

		m_nPrev = cy;
	}

	CSplitterWnd::OnSize(nType, cx, cy);
}


void CDualSplitWnd::StartTracking(int ht)
{
	if (!m_bDragFull)
	{
		CSplitterWnd::StartTracking(ht);
		return;
	}

	GetInsideRect(m_rectLimit);

	CPoint ptCursor;
	GetCursorPos(&ptCursor);
	ScreenToClient(&ptCursor);

	if (ht >= vSplitterBar1 && ht <= vSplitterBar15)
		m_nTrackPos = ptCursor.y - m_pRowInfo[0].nCurSize;
	else if (ht >= hSplitterBar1 && ht <= hSplitterBar15)
		m_nTrackPos = ptCursor.x - m_pColInfo[0].nCurSize;

	SetCapture();

	m_bTracking = TRUE;
	m_htTrack = ht;
	SetSplitCursor(ht);
}


void CDualSplitWnd::StopTracking(BOOL bAccept)
{
	if (!m_bDragFull)
		CSplitterWnd::StopTracking(bAccept);
	else
	{
		ReleaseCapture();
		m_bTracking = FALSE;
	}
}

