// OutputList.cpp: implementation of the COutputList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gsmalert.h"
#include "OutputList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COutputListElement::COutputListElement()
{
}

COutputListElement::~COutputListElement()
{
}

CString COutputListElement::ElementToStr()
{
	CString l_strData;
	l_strData.Format("%02d%09d",m_iOutputState,m_iOutputStateParam);

	return l_strData;
}

void COutputListElement::StrToElement(CString a_strData)
{
}

COutputList::COutputList()
{
}

COutputList::~COutputList()
{
	DeleteAllList();
}

void COutputList::DeleteAllList()
{
	COutputListElement *l_pElementToRemove;
	
	while(!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove=RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}

void COutputList::Serialize( CArchive& archive )
{
	COutputListElement *l_pListElement;
    //CObject::Serialize( archive );

    // now do the stuff for our specific class
    if( archive.IsStoring() )
	{
		POSITION l_Position;

        archive << GetCount();
		
		l_Position=GetHeadPosition();
		while(l_Position!=NULL)
		{
			l_pListElement=GetNext(l_Position);
			archive << l_pListElement->m_iLocInMem<< l_pListElement->m_iOutputState<<l_pListElement->m_iOutputStateParam<< l_pListElement->m_bInUnit;
		}
	}
    else
	{
		int l_iListCount;
		DeleteAllList();

		archive >> l_iListCount;
		for(int l_iIndex=0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement=new COutputListElement;
			archive >> l_pListElement->m_iLocInMem >>l_pListElement->m_iOutputState>> l_pListElement->m_iOutputStateParam>> l_pListElement->m_bInUnit;
			AddTail(l_pListElement);
		}
	}
}

COutputListElement* COutputList::AddItemSorted(int a_iLocInMem,int a_iOutputState,int a_iOutputStateParam, bool a_bInUnit,bool a_bNoSearch)
{
	COutputListElement *l_pListElement;
	bool l_bNew=false;
	if(a_bNoSearch)
	{
		l_bNew=true;
		l_pListElement=new COutputListElement;
	}
	else
	{
		if((l_pListElement=GetItemByLocInMem(a_iLocInMem))==NULL)
		{
			l_bNew=true;
			l_pListElement=new COutputListElement;
		}
	}
	
	l_pListElement->m_iLocInMem=a_iLocInMem;
	l_pListElement->m_iOutputState=a_iOutputState;
	l_pListElement->m_iOutputStateParam=a_iOutputStateParam;
	l_pListElement->m_bInUnit=a_bInUnit;
	if(l_bNew)
	{
		AddTail(l_pListElement);
	}
	return l_pListElement;
}

COutputListElement* COutputList::GetItemByLocInMem(int a_iLocInMem)
{
	POSITION l_Position;
	COutputListElement *l_pListElement;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iLocInMem==a_iLocInMem)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

int COutputList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	COutputListElement *l_pListElement;
	int l_iCounter=0;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_bInUnit==false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(COutputList,  CObList, 0)