// WirelessList.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "WirelessList.h"
#include "Language.h"

CString CWlSensorsListElement::SensorParamsToProtocol()
{
	CString l_strTmp,l_strSensorType;
	switch (m_iSensorType)
	{
	case SET_TAGSTICK_GEN:
		l_strSensorType = SENSOR_TYPE_GENERAL;
		break;
	case SET_TAGSTICK_REED:
		l_strSensorType = SENSOR_TYPE_REED;
		break;
	case SET_TAGSTICK_LIGHT:
		l_strSensorType = SENSOR_TYPE_LIGHT;
		break;
	case SET_TAGSTICK_TEMPERATURE:
		l_strSensorType = SENSOR_TYPE_TEMPERATURE;
		break;
	case SET_TAGSTICK_HUMIDITY:
		l_strSensorType = SENSOR_TYPE_HUMIDITY;
		break;
	}
	
	return l_strSensorType + CString(",") + m_strSensorParam;
}

//Convert protocol string to sensors paramters and return the index of the string that ends the current sensor
int CWlSensorsListElement::ProtocolToSensorParams(CString a_strProtoParams)
{
	CString l_strSensorType;
	int l_iIndexStrart,l_iIndexEnd, l_iNumOfCommas,l_iCommaCount;
	//Get the sensor type
	AfxExtractSubString(l_strSensorType, a_strProtoParams, 0, _T(','));

	if (l_strSensorType == SENSOR_TYPE_GENERAL)
	{
		l_iNumOfCommas = 2;
		m_iSensorType = SET_TAGSTICK_GEN;
	}
	else if (l_strSensorType == SENSOR_TYPE_REED)
	{
		m_iSensorType = SET_TAGSTICK_REED;
		l_iNumOfCommas = 3;
	}
	else if (l_strSensorType == SENSOR_TYPE_LIGHT)
	{
		m_iSensorType = SET_TAGSTICK_LIGHT;
		l_iNumOfCommas = 5;
	}
	else if (l_strSensorType == SENSOR_TYPE_TEMPERATURE)
	{
		m_iSensorType = SET_TAGSTICK_TEMPERATURE;
		l_iNumOfCommas = 6;
	}
	else if (l_strSensorType == SENSOR_TYPE_HUMIDITY)
	{
		m_iSensorType = SET_TAGSTICK_HUMIDITY;
		l_iNumOfCommas = 6;
	}
	else
	{
		m_iSensorType = SET_TAGSTICK_UNKNOWN;
		l_iIndexEnd = a_strProtoParams.GetLength();
		l_iNumOfCommas = 0;
	}

	l_iIndexStrart = a_strProtoParams.Find(",");
	for (l_iIndexEnd= l_iIndexStrart, l_iCommaCount = 0; l_iCommaCount < l_iNumOfCommas; l_iCommaCount++)
	{
		l_iIndexEnd = a_strProtoParams.Find(",", l_iIndexEnd+1);
		if (l_iIndexEnd == -1)
		{
			l_iIndexEnd = a_strProtoParams.GetLength();
			break;
		}
	}
	
	m_strSensorParam = a_strProtoParams.Mid(l_iIndexStrart+1, l_iIndexEnd - l_iIndexStrart-1);

	SensorParamToHuminRead();
	return l_iIndexEnd+1;
}

void CWlSensorsListElement::SensorParamToHuminRead()
{
	CString l_strParam1, l_strParam2, l_strParam3, l_strParam4, l_strParam5;

	switch (m_iSensorType)
	{
	case SET_TAGSTICK_GEN:
		{
			CString l_strKeepAliveInterval;
			AfxExtractSubString(l_strParam1, m_strSensorParam, 0, _T(','));
			AfxExtractSubString(l_strParam2, m_strSensorParam, 1, _T(','));
			l_strKeepAliveInterval = GetSecAsTimedStr(atoi((LPCSTR)l_strParam1));
			m_strSensorParamHuminRead.Format("%s=%s,%s=%s", GetResString(IDS_KEEP_ALIVE_INTERVAL), l_strKeepAliveInterval, GetResString(IDS_BIND_THING), l_strParam2);
		}
		break;
	case SET_TAGSTICK_REED:
		{
			AfxExtractSubString(l_strParam1, m_strSensorParam, 0, _T(','));
			AfxExtractSubString(l_strParam2, m_strSensorParam, 1, _T(','));

			int l_iReedReport = atoi((LPCSTR)l_strParam2);
			switch (l_iReedReport)
			{
			case REED_REPORT_HOLD:
				l_strParam2 = GetResString(IDS_REED_REPORT_HOLD);
				break;
			case REED_REPORT_HOLD_AND_RELEASE:
				l_strParam2 = GetResString(IDS_REED_REPORT_HOLD_AND_RELEASE);
				break;
			}

			m_strSensorParamHuminRead.Format("%s=%s, %s=%s", GetResString(IDS_CLOUD_KEY), l_strParam1, GetResString(IDS_REED_REPORT), l_strParam2);
		}
	break;
	case SET_TAGSTICK_LIGHT:
		{
			AfxExtractSubString(l_strParam1, m_strSensorParam, 0, _T(','));
			AfxExtractSubString(l_strParam2, m_strSensorParam, 1, _T(','));
			AfxExtractSubString(l_strParam3, m_strSensorParam, 2, _T(','));
			AfxExtractSubString(l_strParam4, m_strSensorParam, 3, _T(','));
			AfxExtractSubString(l_strParam5, m_strSensorParam, 4, _T(','));

			int l_iLightReport = atoi((LPCSTR)l_strParam3);
			switch (l_iLightReport)
			{
			case LIGHT_REPORT_TO_DARK:
				l_strParam3 = GetResString(IDS_LIGHT_REPORT_TO_DARK);
				break;
			case LIGHT_REPORT_TO_LIGHT:
				l_strParam3 = GetResString(IDS_LIGHT_REPORT_TO_LIGHT);
				break;
			case LIGHT_REPORT_BOTH:
				l_strParam3 = GetResString(IDS_LIGHT_REPORT_BOTH);
				break;
			}

			CString l_strSampleInterval= GetSecAsTimedStr(atoi((LPCSTR)l_strParam2));
			m_strSensorParamHuminRead.Format("%s=%s, %s=%s, %s=%s, %s=%s, %s=%s", GetResString(IDS_CLOUD_KEY), l_strParam1, GetResString(IDS_LIGHT_REPORT), l_strParam3, GetResString(IDS_SAMPLE_INTERVAL), l_strSampleInterval, GetResString(IDS_LIGHT_DETECT_TRESH), l_strParam4, GetResString(IDS_DARK_DETECT_TRESH), l_strParam5);
		}
		break;
	case SET_TAGSTICK_TEMPERATURE:
	case SET_TAGSTICK_HUMIDITY:
		{
			float l_fHigh, l_fLow, l_fGap;

			AfxExtractSubString(l_strParam1, m_strSensorParam, 0, _T(','));
			AfxExtractSubString(l_strParam2, m_strSensorParam, 1, _T(','));
			AfxExtractSubString(l_strParam3, m_strSensorParam, 2, _T(','));
			AfxExtractSubString(l_strParam4, m_strSensorParam, 3, _T(','));
			AfxExtractSubString(l_strParam5, m_strSensorParam, 5, _T(','));

			l_fHigh = atof((LPCSTR)l_strParam3) / 10.0;
			l_fLow = atof((LPCSTR)l_strParam4) / 10.0;
			l_fGap = atof((LPCSTR)l_strParam5) / 10.0;

			CString l_strSampleInterval = GetSecAsTimedStr(atoi((LPCSTR)l_strParam2));
			m_strSensorParamHuminRead.Format("%s=%s, %s=%s, %s=%.01f, %s=%.01f, %s=%.01f", GetResString(IDS_CLOUD_KEY), l_strParam1, GetResString(IDS_SAMPLE_INTERVAL), l_strSampleInterval, GetResString(IDS_HIGH_LEVEL), l_fHigh, GetResString(IDS_LOW_LEVEL), l_fLow, GetResString(IDS_GAP), l_fGap);
		}
		break;
	}
}

CString CWlSensorsListElement::GetSensorParams(int a_iParamNum)
{
	CString l_strTmp;
	AfxExtractSubString(l_strTmp, m_strSensorParam, a_iParamNum-1, _T(','));
	return l_strTmp;
}

void CWlSensorsList::ProtocolToSensors(CString a_strProtoParams)
{
	CWlSensorsListElement *l_pListElement;
	int l_iNextSensor;
	
	//Clear the sensors list
	DeleteAllList();

	
	while(a_strProtoParams.GetLength())
	{
		l_pListElement = new CWlSensorsListElement;
		AddTail(l_pListElement);
		l_iNextSensor = l_pListElement->ProtocolToSensorParams(a_strProtoParams);
		a_strProtoParams = a_strProtoParams.Mid(l_iNextSensor);
	}
}

void CWlSensorsList::Serialize(CArchive& archive)
{
	CWlSensorsListElement *l_pListElement;
	//CObject::Serialize( archive );

	// now do the stuff for our specific class
	if (archive.IsStoring())
	{
		POSITION l_Position;

		archive << GetCount();

		l_Position = GetHeadPosition();
		while (l_Position != NULL)
		{
			l_pListElement = GetNext(l_Position);
			archive << l_pListElement->m_iSensorType << l_pListElement->m_strSensorParam << l_pListElement->m_strSensorParamHuminRead;
		}
	}
	else
	{
		int l_iListCount;
		int l_iZoneType, l_iTagType, l_iBoolInUnit;

		DeleteAllList();

		archive >> l_iListCount;
		for (int l_iIndex = 0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement = new CWlSensorsListElement;

			archive >> l_iTagType>> l_pListElement->m_strSensorParam >> l_pListElement->m_strSensorParamHuminRead;
			l_pListElement->m_iSensorType = (enmTagSensorType)l_iTagType;
			AddTail(l_pListElement);
		}
	}
}

void CWlSensorsList::DeleteAllList()
{
	CWlSensorsListElement *l_pElementToRemove;

	while (!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove = RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}

CWlSensorsListElement* CWlSensorsList::AddItem(enmTagSensorType a_iSensorType, CString a_strSensorParam)
{
	CWlSensorsListElement *l_pListElement;
	bool l_bNew = false;
	if ((l_pListElement = GetItemBySensorType(a_iSensorType)) == NULL)
	{
		l_bNew = true;
		l_pListElement = new CWlSensorsListElement;
	}

	//Copy all the data to the list element
	l_pListElement->m_iSensorType = a_iSensorType;
	l_pListElement->m_strSensorParam = a_strSensorParam;
	l_pListElement->SensorParamToHuminRead();

	if (l_bNew)
	{
		AddTail(l_pListElement);
	}
	return l_pListElement;
}

CWlSensorsListElement* CWlSensorsList::GetItemBySensorType(enmTagSensorType a_iSensorType)
{
	POSITION l_Position;
	CWlSensorsListElement *l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_iSensorType == a_iSensorType)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

void CWlSensorsList::DeleteItem(enmTagSensorType a_iSensorType)
{
	POSITION l_Position;
	CWlSensorsListElement *l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetAt(l_Position);
		if (l_pListElement->m_iSensorType == a_iSensorType)
		{
			RemoveAt(l_Position);
			delete l_pListElement;
			return;
		}
		GetNext(l_Position);
	}
}

CWlSensorsListElement* CWlSensorsList::GetItemByPos(int a_iPos)
{
	POSITION l_Position;
	CWlSensorsListElement*l_pListElement = NULL;
	int l_iPos;

	for (l_Position = GetHeadPosition(), l_iPos = 0;(l_iPos < a_iPos) && (l_Position != NULL);++l_iPos)
	{
		l_pListElement = GetNext(l_Position);
	}
	return l_pListElement;
}

// CWirelessList

CWirelessList::CWirelessList()
{
}

CWirelessList::~CWirelessList()
{
	DeleteAllList();
}

void CWirelessList::DeleteAllList()
{
	CWirelessListElement *l_pElementToRemove;

	while (!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove = RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}

void CWirelessList::Serialize(CArchive& archive)
{
	CWirelessListElement *l_pListElement;
	//CObject::Serialize( archive );

	// now do the stuff for our specific class
	if (archive.IsStoring())
	{
		POSITION l_Position;

		archive << GetCount();

		l_Position = GetHeadPosition();
		while (l_Position != NULL)
		{
			l_pListElement = GetNext(l_Position);

			archive << l_pListElement->m_strTagId << l_pListElement->m_strTagName << l_pListElement->m_enmTagType << l_pListElement->m_bInUnit;
			l_pListElement->m_WlSensorsList.Serialize(archive);
		}
	}
	else
	{
		int l_iListCount;
		int l_iZoneType, l_iTagType;

		DeleteAllList();

		archive >> l_iListCount;
		for (int l_iIndex = 0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement = new CWirelessListElement;
			archive >> l_pListElement->m_strTagId >> l_pListElement->m_strTagName >> l_iTagType >> l_pListElement->m_bInUnit;
			l_pListElement->m_enmTagType = (enmTagType)l_iTagType;
			l_pListElement->m_WlSensorsList.Serialize(archive);

			AddTail(l_pListElement);
		}
	}
}

CWirelessListElement* CWirelessList::AddItem(CString a_strTagId, enmTagType a_enmTagType, CString a_strTagName, bool a_bInUnit)
{
	CWirelessListElement *l_pListElement;
	bool l_bNew = false;
	if ((l_pListElement = GetItemByTagId(a_strTagId)) == NULL)
	{
		l_bNew = true;
		l_pListElement = new CWirelessListElement;
	}
	
	//Copy all the data to the list element
	l_pListElement->m_strTagId = a_strTagId;
	l_pListElement->m_strTagName = a_strTagName;
	l_pListElement->m_enmTagType = a_enmTagType;
	l_pListElement->m_bInUnit = a_bInUnit;

	if (l_bNew)
	{
		AddTail(l_pListElement);
	}
	return l_pListElement;
}

CWirelessListElement* CWirelessList::GetItemByTagId(CString a_strTagId)
{
	POSITION l_Position;
	CWirelessListElement *l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_strTagId == a_strTagId)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CWirelessListElement* CWirelessList::CloneTagById(CString a_strTagId, CString a_strNewTagId)
{
	CWirelessListElement* l_WirelessListElement = GetItemByTagId(a_strTagId);

	CWirelessListElement *l_pListElement = new CWirelessListElement;
	CWlSensorsListElement *l_WlSensorsListElement;
	
	//Copy all the data to the list element
	l_pListElement->m_strTagId = a_strNewTagId;
	l_pListElement->m_strTagName = IncrementStrSuffix(l_WirelessListElement->m_strTagName);
	l_pListElement->m_enmTagType = l_WirelessListElement->m_enmTagType;
	l_pListElement->m_bInUnit = false;
	AddTail(l_pListElement);

	for (int l_i = 1;l_i <= l_WirelessListElement->m_WlSensorsList.GetCount();++l_i)
	{
		l_WlSensorsListElement = l_WirelessListElement->m_WlSensorsList.GetItemByPos(l_i);
		switch (l_WlSensorsListElement->m_iSensorType)
		{
		case SET_TAGSTICK_GEN:
			l_pListElement->m_WlSensorsList.AddItem(l_WlSensorsListElement->m_iSensorType, l_WlSensorsListElement->m_strSensorParam);
			break;
		case SET_TAGSTICK_REED:
		case SET_TAGSTICK_LIGHT:
		case SET_TAGSTICK_TEMPERATURE:
		case SET_TAGSTICK_HUMIDITY:
			l_pListElement->m_WlSensorsList.AddItem(l_WlSensorsListElement->m_iSensorType, l_WlSensorsListElement->m_strSensorParam);
			break;
		case SET_TAGSTICK_UNKNOWN:
		default:
			break;
		}
	}
	
	return l_pListElement;

}

void CWirelessList::DeleteItem(CString a_strTagId)
{
	POSITION l_Position;
	CWirelessListElement *l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetAt(l_Position);
		if (l_pListElement->m_strTagId== a_strTagId)
		{
			RemoveAt(l_Position);
			delete l_pListElement;
			return;
		}
		GetNext(l_Position);
	}
}

CWirelessListElement* CWirelessList::GetItemByPos(int a_iPos)
{
	POSITION l_Position;
	CWirelessListElement*l_pListElement = NULL;
	int l_iPos;

	for (l_Position = GetHeadPosition(), l_iPos = 0;(l_iPos < a_iPos) && (l_Position != NULL);++l_iPos)
	{
		l_pListElement = GetNext(l_Position);
	}
	return l_pListElement;
}

// CWirelessList member functions

CString SensorTypeToText(CString a_strSensorType)
{
	if (a_strSensorType == SENSOR_TYPE_RSSI)
	{
		return GetResString(IDS_RSSI);
	}
	else if(a_strSensorType == SENSOR_TYPE_TEMPERATURE)
	{
		return GetResString(IDS_SET_TAGSTICK_TEMPERATURE);
	}
	else if (a_strSensorType == SENSOR_TYPE_HUMIDITY)
	{
		return GetResString(IDS_SET_TAGSTICK_HUMIDITY);
	}
	else if (a_strSensorType == SENSOR_TYPE_REED)
	{
		return GetResString(IDS_SET_TAGSTICK_REED);
	}
	else if (a_strSensorType == SENSOR_TYPE_LIGHT)
	{
		return GetResString(IDS_SET_TAGSTICK_LIGHT);
	}
	else  //If unknown type, return the msg itself
	{
		return a_strSensorType;
	}
}

CString SensorTypeToText(enmTagSensorType a_enmSensorType)
{
	if (a_enmSensorType == SET_TAGSTICK_GEN)
	{
		return GetResString(IDS_SET_TAGSTICK_GEN);
	}
	else if (a_enmSensorType == SET_TAGSTICK_TEMPERATURE)
	{
		return GetResString(IDS_SET_TAGSTICK_TEMPERATURE);
	}
	else if (a_enmSensorType == SET_TAGSTICK_HUMIDITY)
	{
		return GetResString(IDS_SET_TAGSTICK_HUMIDITY);
	}
	else if (a_enmSensorType == SET_TAGSTICK_REED)
	{
		return GetResString(IDS_SET_TAGSTICK_REED);
	}
	else if (a_enmSensorType == SET_TAGSTICK_LIGHT)
	{
		return GetResString(IDS_SET_TAGSTICK_LIGHT);
	}
	else  //If unknown type, return the msg itself
	{
		return CString("");
	}
}

CString IncrementStrSuffix(CString a_strData)
{
	int l_i;
	for (l_i = a_strData.GetLength()-1; l_i >=0 ; --l_i)
	{
		if (!IsNumeric(a_strData.Mid(l_i)))
		{
			break;
		}
	}

	int l_iSuffixNum = atoi((LPCSTR)(a_strData.Mid(l_i + 1)));
	CString l_strRes;
	l_strRes.Format("%s%d", a_strData.Left(l_i+1), l_iSuffixNum+1);

	return l_strRes;
}