// OneWireList.h: interface for the COneWireList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ONEWIRELIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
#define AFX_ONEWIRELIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class COneWireListElement : public CObject
{
public:
	COneWireListElement();
	~COneWireListElement();
	void operator=(const COneWireListElement& a_OneWireListElement);   // = operator

	int m_iSensorNumber;
	CString m_strSensorName;
	CString m_strDisconnectedMgs;
	CString m_strResumeConnectMgs;
	bool m_bInUnit;
};

class COneWireList : public CTypedPtrList< CObList, COneWireListElement* > 
{
public:
	DECLARE_SERIAL( COneWireList )

	COneWireList();
	virtual ~COneWireList();
	void Serialize( CArchive& archive );
	
	void DeleteAllList();
	void AddItemSorted(int a_iSensorNum,CString a_strSensorName,CString a_strDisconnectedMgs,CString a_strResumeConnectMgs,bool a_bInUnit,bool a_bNoSearch);
	void DeleteItem(int a_iSensorNum);
	COneWireListElement* GetItemBySensorNum(int a_iSensorNum);
	COneWireListElement* GetItemByPos(int a_iPos);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_ONEWIRELIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
