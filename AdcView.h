#if !defined(AFX_ADCVIEW_H__F0ABB774_CD9E_48B1_AF18_CD90E05DF620__INCLUDED_)
#define AFX_ADCVIEW_H__F0ABB774_CD9E_48B1_AF18_CD90E05DF620__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AdcView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAdcView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CAdcView : public CFormView
{
protected:
	CAdcView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CAdcView)
public:
	enum enmAdcSettingsLastSentCommand
	{
		enmAdcSettingsLastSentCommandNone,
		enmAdcSettingsLastSentCommandInputVoltageGet,
		enmAdcSettingsLastSentCommandInputVoltageSet,
		m_enmAdcSettingsLastSentCommandOneWireListGet,
		m_enmAdcSettingsLastSentCommandOneWireListSet,
		m_enmAdcSettingsLastSentCommandOneWireMark,
		m_enmAdcSettingsLastSentCommandOneWireQuery,
		m_enmAdcSettingsLastSentCommandAdcListGet,
		m_enmAdcSettingsLastSentCommandAdcListSet,
	};
// Form Data
public:
	//{{AFX_DATA(CAdcView)
	enum { IDD = IDD_ADC_VIEW_FORM };
	CEdit	m_txtResumeConnectMgs;
	CEdit	m_txtDisconnectedMgs;
	CButton	m_cmdQueryOneWireSensors;
	CListCtrl	m_lstOneWireQuery;
	CEdit	m_txtOneWireSensorName;
	CButton	m_cmdSetOneWireSensor;
	CButton	m_cmdRemoveOneWireSensor;
	CButton	m_cmdProramOneWire;
	CButton	m_cmdGetOneWireSensor;
	CButton	m_cmdAddOneWireSensor;
	CComboBox	m_cmbOneWireToProgram;
	CComboBox	m_cmbOneWire;
	CListCtrl	m_lstOneWire;
	CButton	m_chkSendVBattToCloud;
	CButton	m_chkSendVinToCloud;
	CButton	m_chkSendVinAlarmToCloud;
	BOOL	m_bSendVinToCloud;
	BOOL	m_bSendVBatToCloud;
	BOOL	m_bSendVinAlarmToCloud;
	int		m_iOneWire;
	int		m_iOneWireToProgram;
	CString	m_strOneWireSensorName;
	CString	m_strDisconnectedMgs;
	CString	m_strResumeConnectMgs;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	CMainFrame *m_pMainFram;
	CLevelAlertView *m_pLevelEventView;
	CString m_strSetString;
	CImageList m_imlAdcView;
	enmAdcSettingsLastSentCommand m_enmAdcSettingsLastSentCommand;
// Operations
public:
protected:
	void Localize();
	void EnableControls(BOOL a_bEnabled=TRUE);
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmAdcSettingsLastSentCommand &a_enmAdcSettingsLastSentCommand);
	int AddItemToOneWireList(int a_iSensorNum,CString a_strSensorName,CString a_strDisconnectedMgs,CString a_strResumeConnectMgs, bool a_bInUnit,bool a_bNoSearch);
	int AddItemToOneWireQueryList(int a_iSensorNum,CString a_strSensorName,CString a_strSensorTemperature);
	void SetOneWireItemImage(int a_iSensorNum,int a_iInUnit);
	int GetOneWireItemByNumber(int a_iSensorNum);
	void DeleteOneWireSensorList();
	int GetAdcItemByNumber(int a_iSensorNum);
	int GetAdcLutItemByMeasured(CString a_strLutMeasured);
	int AddItemToAdcList(int a_iSensorNum);
	int AddItemToAdcLutList(CString a_strLutMeasured, CString a_strLutEngineer);
	void LutListToTextBox();
	void LutTextBoxToList();
	CString GetOneWireSensorsString();
	CString GetAdcSensorsString();

// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdcView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CAdcView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	// Generated message map functions
	//{{AFX_MSG(CAdcView)
	afx_msg void OnChkSendVinToCloud();
	afx_msg void OnChkSendVBatToCloud();
	afx_msg void OnCmdGetInputVoltage();
	afx_msg void OnCmdSetInputVoltage();
	afx_msg void OnCmdAdd1WireSensor();
	afx_msg void OnClickLstOneWire(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCmdRemove1WireSensor();
	afx_msg void OnCmdGet1WireSensor();
	afx_msg void OnCmdSet1WireSensor();
	afx_msg void OnCmdProgram1Wire();
	afx_msg void OnCmdQuery1WireSensors();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_lstAdc;
	CComboBox m_cmbAdc;
	int m_iAdc;
	afx_msg void OnBnClickedCmdAddAdcSensor();
	CEdit m_txtAdcSensorName;
	CString m_strAdcSensorName;
	CEdit m_txtLut;
	CString m_strLut;
	CListCtrl m_lstAdcLut;
	CEdit m_txtAdcLutEngineer;
	CString m_strAdcLutEngineer;
	CEdit m_txtAdcLutMeasured;
	CString m_strAdcLutMeasured;
	afx_msg void OnBnClickedCmdAddAdcLut();
	afx_msg void OnClickLstAdc(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCmdRemoveAdcSensor();
	afx_msg void OnBnClickedCmdGetAdcSensor();
	afx_msg void OnBnClickedCmdSetAdcSensor();
	CButton m_cmdAddAdcSensor;
	CButton m_cmdAddAdcLut;
	CButton m_cmdGetAdcSensors;
	CButton m_cmdRemoveAdcLut;
	CButton m_cmdRemoveAdcSensor;
	CButton m_cmdSetAdcSensors;
	afx_msg void OnBnClickedCmdRemoveAdcLut();
	afx_msg void OnBnClickedChkSendVinAlarmToCloud();
	afx_msg void OnBnClickedCmdCopy1WireSensorClipboard();
	afx_msg void OnBnClickedCmdCopyAdcSensorClipboard();
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CAdcView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADCVIEW_H__F0ABB774_CD9E_48B1_AF18_CD90E05DF620__INCLUDED_)
#include "afxcmn.h"
#include "afxwin.h"
