// LevelEvents.cpp : implementation file
//

#include "stdafx.h"
#include "gsmalert.h"
#include "LevelEvents.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLevelEvents

IMPLEMENT_DYNCREATE(CLevelEvents, CFormView)

CLevelEvents::CLevelEvents()
	: CFormView(CLevelEvents::IDD)
{
	//{{AFX_DATA_INIT(CLevelEvents)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CLevelEvents::~CLevelEvents()
{
}

void CLevelEvents::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLevelEvents)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLevelEvents, CFormView)
	//{{AFX_MSG_MAP(CLevelEvents)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLevelEvents diagnostics

#ifdef _DEBUG
void CLevelEvents::AssertValid() const
{
	CFormView::AssertValid();
}

void CLevelEvents::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLevelEvents message handlers
