// SendQueue.cpp: implementation of the CSendQueue class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GsmAlert.h"
#include "SendQueue.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CSendQueueElement::CSendQueueElement()
{
	m_hWnd=0;
	m_bSentToUnit=false;
	m_bUnitMsg=true;
}

CSendQueueElement::~CSendQueueElement()
{
}

CSendQueue::~CSendQueue()
{
	EmptyQueue();
}

void CSendQueue::qEnqueue(CSendQueueElement*l_newQueueElement)
{ 
	AddTail(l_newQueueElement); // End of the queue
}

// Get first element in line
CSendQueueElement* CSendQueue::qDequeue()
{
	return IsEmpty() ? NULL : RemoveHead();
}

// Get first element in line but don't remove it
CSendQueueElement* CSendQueue::qLookHead()
{
	return IsEmpty() ? NULL : GetHead();
}

void CSendQueue::EmptyQueue()
{
	CSendQueueElement*l_pElementToRemove;
	
	while((l_pElementToRemove=qDequeue())!=NULL)	//loop on all the elements from queue
		delete l_pElementToRemove;					//delete the element
}

void CSendQueue::qVipEnqueue( CSendQueueElement* l_newQueueElement )
{
	AddHead(l_newQueueElement);	//begin of queue
}

int CSendQueue::qGetCount()
{
	return GetCount();
}