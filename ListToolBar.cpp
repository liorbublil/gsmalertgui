/*********************************************************
* List-Style Toolbar
* Version: 1.0
* Date: June 3, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#include "stdafx.h"
#include "ListToolBar.h"
#include "Language.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


CListToolBar::CListToolBar()
{
}

CListToolBar::~CListToolBar()
{
}

BEGIN_MESSAGE_MAP(CListToolBar, CToolBarCtrl)
	//{{AFX_MSG_MAP(CListToolBar)
	ON_MESSAGE(0x0363, OnIdleUpdateCmdUI)	// WM_IDLEUPDATECMDUI
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CListToolBar::Create(CWnd* pParentWnd, UINT nID)
{
	return CToolBarCtrl::Create(WS_CHILD | WS_VISIBLE | TBSTYLE_FLAT | TBSTYLE_LIST | CCS_NODIVIDER | CCS_NORESIZE,
		CRect(0, 0, 0, 0), pParentWnd, nID);
}

BOOL CListToolBar::LoadToolBar(UINT nID, int nStyle)
{
	HINSTANCE hInst = AfxFindResourceHandle(MAKEINTRESOURCE(nID), RT_TOOLBAR);
	HRSRC hRsrc = ::FindResource(hInst, MAKEINTRESOURCE(nID), RT_TOOLBAR);
	if (hRsrc == NULL)
		return FALSE;

	HGLOBAL hGlobal = LoadResource(hInst, hRsrc);
	if (hGlobal == NULL)
		return FALSE;

	struct TBDATA
	{
		WORD wVersion;
		WORD wWidth;
		WORD wHeight;
		WORD wItemCount;
		WORD aItems[1];
	};

	// get the toolbar data
	TBDATA* pData = (TBDATA*)LockResource(hGlobal);
	if (pData == NULL)
		return FALSE;
	ASSERT(pData->wVersion == 1);

	SetBitmapSize(CSize(pData->wWidth, pData->wHeight));

	TBBUTTON tbb; 
	ZeroMemory(&tbb, sizeof(tbb));

	BOOL bResult = TRUE;
	int nImage = 0;

	// add button for each menu
	for (int i=0; i<pData->wItemCount; i++)
	{
		if (pData->aItems[i])
		{
			tbb.fsState = TBSTATE_ENABLED;
			tbb.fsStyle = TBSTYLE_BUTTON | TBSTYLE_AUTOSIZE;
			CString strText;
			strText = GetResString(pData->aItems[i]);
			int nPos = strText.Find('\n');
			if (nPos >= 0)
				strText = strText.Mid(nPos+1);
			tbb.iString = AddStrings(strText);
			tbb.idCommand = pData->aItems[i];
			tbb.iBitmap = nImage++;
		}
		else
		{
			tbb.fsState = 0;
			tbb.fsStyle = TBSTYLE_SEP;
			tbb.idCommand = 0;
			tbb.iBitmap = 6;
		}

		if (!AddButtons(1, &tbb))
		{
			bResult = FALSE;
			break;
		}
	}

	if (bResult)
	{
		// load images from bitmap resource
		if (m_ImgList.Create(pData->wWidth, pData->wHeight, nStyle,
			pData->wItemCount))
		{
			bResult = m_ImgList.AddBitmap(nID);
		}
		else
			bResult = FALSE;
	}

	UnlockResource(hGlobal);
	FreeResource(hGlobal);

	if (bResult)	// let the toolbar use our image lists
	{
		SendMessage(TB_SETIMAGELIST, 0, (LPARAM)m_ImgList.GetImageList(AIL_NORMAL));
		SendMessage(TB_SETHOTIMAGELIST, 0, (LPARAM)m_ImgList.GetImageList(AIL_HOT));
		SendMessage(TB_SETDISABLEDIMAGELIST, 0, (LPARAM)m_ImgList.GetImageList(AIL_DISABLED));
	}

	UpdateSettings();

	return bResult;
}

void CListToolBar::UpdateSettings()
{
	// get last item position to measure bar size
	CRect rcItem;
	GetItemRect(GetButtonCount()-1, &rcItem);

	CRect rcRect(0, 0, rcItem.right, rcItem.bottom);

	if (GetParent() == GetParentFrame())
	{
		// not in a rebar yet, just resize the toolbar
		MoveWindow(&rcRect, FALSE);
	}
	else
	{
		// update band information
		REBARBANDINFO rbbi;
		ZeroMemory(&rbbi, sizeof(rbbi));
		rbbi.cbSize = sizeof(rbbi);
		rbbi.fMask = RBBIM_CHILD;

		CReBarCtrl* pReBar = (CReBarCtrl*)GetParent();

		for (UINT i=0; i<pReBar->GetBandCount(); i++)
		{
			pReBar->GetBandInfo(i, &rbbi);

			if (rbbi.hwndChild == m_hWnd)
			{
				rbbi.fMask = RBBIM_CHILDSIZE;
		
				rbbi.cyMinChild = rbbi.cyMaxChild = rbbi.cyChild = rcRect.Height();
				rbbi.cxMinChild = rcRect.Width();

				pReBar->SetBandInfo(i, &rbbi);

				// force updating bands layout
				CRect rcClient;
				pReBar->GetClientRect(&rcClient);
				pReBar->MoveWindow(0, 0, 1, 1, FALSE);
				pReBar->MoveWindow(&rcClient);

				// recalc frame window layout
				GetParentFrame()->RecalcLayout();
		
				break;
			}
		}

	}
}

LRESULT CListToolBar::OnIdleUpdateCmdUI(WPARAM wParam, LPARAM)
{
	if (GetStyle() & WS_VISIBLE)
	{
		// handle command updates for the toolbar
		CFrameWnd* pTarget = (CFrameWnd*)GetOwner();
		if (pTarget == NULL || !pTarget->IsFrameWnd())
			pTarget = GetParentFrame();
		if (pTarget != NULL)
			OnUpdateCmdUI(pTarget, (BOOL)wParam);
	}
	return 0;
}

class CListToolCmdUI : public CCmdUI
{
public:
	virtual void Enable(BOOL bOn);
	virtual void SetCheck(int nCheck);
	virtual void SetText(LPCTSTR lpszText);
};

void CListToolCmdUI::Enable(BOOL bOn)
{
	m_bEnableChanged = TRUE;
	CListToolBar* pToolBar = (CListToolBar*)m_pOther;

	// enable or disable the button
	TBBUTTONINFO tbbi;
	ZeroMemory(&tbbi, sizeof(tbbi));
	tbbi.cbSize = sizeof(tbbi);
	tbbi.dwMask = TBIF_STATE;
	pToolBar->GetButtonInfo(m_nID, &tbbi);
	if (bOn)
		tbbi.fsState |= TBSTATE_ENABLED;
	else
		tbbi.fsState &= ~(TBSTATE_ENABLED | TBSTATE_PRESSED);
	pToolBar->SetButtonInfo(m_nID, &tbbi);
}

void CListToolCmdUI::SetCheck(int nCheck)
{
	CListToolBar* pToolBar = (CListToolBar*)m_pOther;

	// check or uncheck the button
	TBBUTTONINFO tbbi;
	ZeroMemory(&tbbi, sizeof(tbbi));
	tbbi.cbSize = sizeof(tbbi);
	tbbi.dwMask = TBIF_STATE;
	pToolBar->GetButtonInfo(m_nID, &tbbi);
	if (nCheck == 1)
		tbbi.fsState |= TBSTATE_CHECKED;
	else
		tbbi.fsState &= ~TBSTATE_CHECKED;
	if (nCheck == 2)
		tbbi.fsState |= TBSTATE_INDETERMINATE;
	else
		tbbi.fsState &= ~TBSTATE_INDETERMINATE;
	pToolBar->SetButtonInfo(m_nID, &tbbi);
}

void CListToolCmdUI::SetText(LPCTSTR)
{
	// ignore text changes
}

void CListToolBar::OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler)
{
	// create our own CmdUI object for updates
	CListToolCmdUI state;
	state.m_pOther = this;

	// call the update function for each button
	state.m_nIndexMax = GetButtonCount();
	for (state.m_nIndex = 0; state.m_nIndex < state.m_nIndexMax; state.m_nIndex++)
	{
		TBBUTTON button;
		GetButton(state.m_nIndex, &button);
		state.m_nID = button.idCommand;

		if (!(button.fsStyle & TBSTYLE_SEP))
			state.DoUpdate(pTarget, bDisableIfNoHndler);
	}
}

