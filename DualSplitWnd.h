/*********************************************************
* Splitter Window Extension
* Version: 1.3
* Date: March 6, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* You may freely use and modify this code, but don't remove
* this copyright note.
*
* There is no warranty of any kind, express or implied, for this class.
* The author does not take the responsibility for any damage
* resulting from the use of it.
*
* Let me know if you find this code useful, and
* send me any modifications and bug reports.
*
* Please include the copyright for this code
* in your application's documentation and/or about box.
*
* Copyright (C) 2002-03 by Michal Mecinski
*********************************************************/

#pragma once

class CDualSplitWnd : public CSplitterWnd
{
	DECLARE_DYNAMIC(CDualSplitWnd);
public:
	CDualSplitWnd();
	virtual ~CDualSplitWnd();

public:
	// Initial ratio for two-pane splitter
	void SetInitialRatio(double dRatio) { m_dRatio = dRatio; }

	// Get current ratio, so it can be stored in configuration file
	double GetRatio() const { return m_dRatio; }

public:
	virtual void OnInvertTracker(const CRect& rect);
	virtual void SetSplitCursor(int ht);

protected:
	virtual void StartTracking(int ht);
	virtual void StopTracking(BOOL bAccept);

	enum HitTestValue
	{
		noHit = 0,
		vSplitterBox = 1,
		hSplitterBox = 2,
		bothSplitterBox = 3,
		vSplitterBar1 = 101,
		vSplitterBar15 = 115,
		hSplitterBar1 = 201,
		hSplitterBar15 = 215,
		splitterIntersection1 = 301,
		splitterIntersection225 = 525
	};

	int m_nPrev;
	BOOL m_bChange;
	double m_dRatio;
	BOOL m_bDragFull;
	int m_nTrackPos;

protected:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	DECLARE_MESSAGE_MAP()
};

