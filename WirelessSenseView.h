#pragma once

#include "ColumnTreeWnd.h"
#include "afxwin.h"


// CWirelessSenseView form view

class CWirelessSenseView : public CFormView
{
	DECLARE_DYNCREATE(CWirelessSenseView)

public:
	enum enmWlSensorsLastSentCommand
	{
		enmWlSensorsLastSentCommandNone,
		enmWlSensorsLastSentCommandQuerySensors,
		enmWlSensorsLastSentCommandClearSensors,
		enmWlSensorsLastSentCommandSetSensors,
		enmWlSensorsLastSentCommandGetSensors,
	};
protected:
	CWirelessSenseView();           // protected constructor used by dynamic creation
	virtual ~CWirelessSenseView();

// Attributes
public:
protected:
	bool m_bFirstRun;
	CString m_strSetString;

	CMainFrame *m_pMainFram;

	CColumnTreeWnd m_tcTags;
	CColumnTreeWnd m_tcTagsQuery;
	CImageList m_imlWirelessSenseView;

	CGsmAlertDoc* GetDocument();
	enmWlSensorsLastSentCommand m_enmWlSensorsLastSentCommand;

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WIRELESSSENSE_VIEW_FORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnInitialUpdate();
	CComboBox m_cmbTagType;
	int m_iTagType;

	enmTagSensorType TagSensorStringToEnm(CString a_strTagSensor);
	enmTagType TagTypeStringToEnm(CString a_strTagType);

protected:
	void ApplySetTypeFromTagType(bool a_bForace);
	void ApplyFiedsFromSetType(bool a_bForace);
	void SendSetCommandToUnit(enmWlSensorsLastSentCommand &a_enmWlSensorsLastSentCommand);
	void EnableControls(BOOL a_bEnabled = TRUE);
	void WirelessListToScreen();
	void AddItemToWirelessList(CString a_strTagId);
	void HandleLastSentCommand();
	CString GetWirelessSensorsString();

	afx_msg LRESULT OnCommandArrived(WPARAM wParam, LPARAM lParam);

	afx_msg void OnTagsTreeSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTagsQueryTreeSelchanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeCmbTagType();
	CComboBox m_cmbTagSetType;
	int m_iTagSetType;
	afx_msg void OnCbnSelchangeCmbTagSetType();
	CString m_strLblSet1;
	CString m_strLblSet2;
	CString m_strLblSet6;
	CString m_strLblSet5;
	CString m_strLblSet4;
	CString m_strLblSet3;
	CStatic m_LblSet6;
	CStatic m_LblSet5;
	CStatic m_LblSet4;
	CStatic m_LblSet3;
	CStatic m_LblSet2;
	CStatic m_LblSet1;
	CEdit m_txtSet6;
	CEdit m_txtSet5;
	CEdit m_txtSet4;
	CEdit m_txtSet3;
	CEdit m_txtSet2;
	CEdit m_txtSet1;
	CComboBox m_cmbSet1;
	int m_iSet1;
//	CEdit m_trvTagsQuery;
	CButton m_cmdQueryWirelessSensors;
	afx_msg void OnBnClickedCmdQueryWirelessSensors();
	CEdit m_txtTagId;
public:
	afx_msg void OnBnClickedCmdAddWirelessSensor();
	CString m_strTagId;
	CString m_strSet6;
	CString m_strSet5;
	CString m_strSet4;
	CString m_strSet3;
	CString m_strSet2;
	CString m_strSet1;
	afx_msg void OnBnClickedCmdSetWirelessSensor();
	afx_msg void OnBnClickedCmdGetWirlelessSensor();
	afx_msg void OnBnClickedCmdRemoveWirelessSensor();
	CButton m_chkRfActive;
	BOOL m_bRfActive;
	afx_msg void OnBnClickedChkRfActive();
	CButton m_cmdAddWirelessSensor;
	CButton m_cmdSetWirelessSensor;
	CButton m_cmdRemoveWirelessSensor;
	CButton m_cmdGetWirelessSensor;
	CButton m_cmdCloneWirelessSensor;
	afx_msg void OnBnClickedCmdCloneWirelessSensor();
	afx_msg void OnBnClickedCmdCopyWirelessSensorClipboard();
	CButton m_chkSet3;
	BOOL m_bSet3;
	afx_msg void OnBnClickedCmdClearWirelessSensors();
	CButton m_cmdClearWirelessSensors;
};


#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CWirelessSenseView::GetDocument()
{
	return (CGsmAlertDoc*)m_pDocument;
}
#endif