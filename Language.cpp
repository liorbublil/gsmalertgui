#include "stdafx.h"
#include "Afxtempl.h"
#include "resource.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Language.h"


#define LANGUAGE_FILE_VERSION_HIGH	1
#define LANGUAGE_FILE_VERSION_LOW	0

static HINSTANCE _hLangDLL = NULL;
CList <CLanguage*, CLanguage*> _listLanguages;


static CString LoadString( HINSTANCE hInstance, UINT uStringID)
{
	// try fixed buffer first (to avoid wasting space in the heap)
	TCHAR szTemp[512];
	int len = LoadString(hInstance, uStringID, szTemp, sizeof(szTemp));
	if (len==0)
		szTemp[0] = _T('\0');
	return szTemp;
}

static void FreeLangDLL()
{
	if (_hLangDLL != NULL && _hLangDLL != GetModuleHandle(NULL)){
		VERIFY( FreeLibrary(_hLangDLL) );
 		_hLangDLL = NULL;
	}
}

static bool CheckLangDLLVersion(const CString& rstrLangDLL)
{
	bool bResult = false;
	DWORD dwUnused;
	DWORD dwVerInfSize = GetFileVersionInfoSize(const_cast<LPTSTR>((LPCTSTR)rstrLangDLL), &dwUnused);
	if (dwVerInfSize != 0)
	{
		LPBYTE pucVerInf = (LPBYTE)calloc(dwVerInfSize, 1);
		if (pucVerInf)
		{
			if (GetFileVersionInfo(const_cast<LPTSTR>((LPCTSTR)rstrLangDLL), 0, dwVerInfSize, pucVerInf))
			{
				VS_FIXEDFILEINFO* pFileInf = NULL;
				UINT uLen = 0;
				if (VerQueryValue(pucVerInf, _T("\\"), (LPVOID*)&pFileInf, &uLen) && pFileInf && uLen)
				{
					UINT uHigh = (pFileInf->dwProductVersionMS>>16) & 0xFFFF;
					UINT uLow  = (pFileInf->dwProductVersionMS    ) & 0xFFFF;

					if (uHigh > LANGUAGE_FILE_VERSION_HIGH)
					{
						bResult = true;
					}
					else
					{
						if ((uHigh == LANGUAGE_FILE_VERSION_HIGH) &&
							(uLow  >= LANGUAGE_FILE_VERSION_LOW ))
						{
							bResult = true;
						}
					}
					
				}
			}
			free(pucVerInf);
		}
	}

	return bResult;
}

bool LoadLangLibDefault()
{
	FreeLangDLL();
	_hLangDLL = NULL;
	return true;
}

bool LoadLangLib(CString& strLangDLLPath)
{
	FreeLangDLL();
	if (!strLangDLLPath.IsEmpty())
		_hLangDLL = LoadLibrary(strLangDLLPath);
	return true;
}

void LanguagesInit(const CString& rstrLangDir, bool bReInit)
{
	static BOOL _bInitialized = FALSE;
	if (_bInitialized && !bReInit)
		return;
	_bInitialized = TRUE;
	CLanguage* pLanguage = NULL;


	// Clear list is we are in ReInit
	while(_listLanguages.GetCount())
	{
		pLanguage = _listLanguages.RemoveHead();
		delete pLanguage;
	}
		
	// Load the Default English Language
	VERIFY( LoadLangLibDefault() );
	pLanguage = new CLanguage;
	pLanguage->sDisplayName = GetResString( IDS_LANGUAGE_DISPLAY_NAME );
	pLanguage->sLangDLLFullPath.Empty();
	_listLanguages.AddHead( pLanguage );
	
	// Run through all files in Language folder
	CFileFind ff;
	BOOL bEnd = ff.FindFile(rstrLangDir + _T("Lang*.dll"), 0);
	while (bEnd)
	{
		bEnd = ff.FindNextFile();
		if (ff.IsDirectory())
			continue;

		pLanguage = new CLanguage;

		pLanguage->sLangDLLFullPath = ff.GetFilePath();

#if 0
		if (CheckLangDLLVersion( pLanguage->sLangDLLFullPath ) == FALSE)
		{
			delete pLanguage;
			continue;
		}
#endif

		VERIFY( LoadLangLib( pLanguage->sLangDLLFullPath ));
		VERIFY( _hLangDLL );
		pLanguage->sDisplayName = GetResString( IDS_LANGUAGE_DISPLAY_NAME );
		VERIFY( LoadLangLibDefault() );

		if (pLanguage->sDisplayName.IsEmpty())
		{
			delete pLanguage;
			continue;
		}


		// Place in sorted position in supported languages list
		CLanguage* pLanguageAtPos;
		POSITION curPos, pos = _listLanguages.GetHeadPosition();
		BOOL found = FALSE;
		while (pos)
		{
			curPos = pos;
			pLanguageAtPos = _listLanguages.GetNext( pos );
			if (pLanguage->sDisplayName < pLanguageAtPos->sDisplayName)
			{
				found = TRUE;
				break;
			}
		}
		
		if (found)
			_listLanguages.InsertBefore( curPos, pLanguage);
		else
			_listLanguages.InsertAfter(  curPos, pLanguage);
	}
	ff.Close();
}

void LanguagesRelease()
{
	CLanguage* pLanguage;
	while (_listLanguages.GetCount())
	{
		pLanguage = _listLanguages.GetHead();
		delete pLanguage;
		_listLanguages.RemoveHead();
	}
}

CString GetResString(UINT uStringID)
{
	CString resString;
	if (_hLangDLL)
		resString = LoadString(_hLangDLL, uStringID);
	if (resString.IsEmpty())
		resString = LoadString(GetModuleHandle(NULL), uStringID);
	return resString;
}

HINSTANCE GetCurrentLangHandle()
{
	if (_hLangDLL)
	{
		return _hLangDLL;
	}
	else
	{
		return GetModuleHandle(NULL);
	}
}
