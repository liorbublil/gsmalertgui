// GsmAlertView.cpp : implementation of the CGsmAlertView class
//

#include "stdafx.h"
#include "GsmAlert.h"

#include "GsmAlertDoc.h"
#include "GsmAlertView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertView

IMPLEMENT_DYNCREATE(CGsmAlertView, CFormView)

BEGIN_MESSAGE_MAP(CGsmAlertView, CFormView)
	//{{AFX_MSG_MAP(CGsmAlertView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertView construction/destruction

CGsmAlertView::CGsmAlertView()
	: CFormView(CGsmAlertView::IDD)
{
	//{{AFX_DATA_INIT(CGsmAlertView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here
}

CGsmAlertView::~CGsmAlertView()
{
}

void CGsmAlertView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGsmAlertView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BOOL CGsmAlertView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CGsmAlertView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ResizeParentToFit();

}

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertView diagnostics

#ifdef _DEBUG
void CGsmAlertView::AssertValid() const
{
	CFormView::AssertValid();
}

void CGsmAlertView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CGsmAlertView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertView message handlers
