//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GsmAlert.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_GSM_ALERT_FORM              101
#define IDD_PHONE_LIST_FORM             103
#define IDD_MISC_SETTINGS_FORM          104
#define IDD_INPUTBOX_DIALOG             105
#define IDD_SEARCH_MODEM_DIALOG         109
#define IDD_LOG_FORM                    110
#define IDD_CHANGE_LANG_DIAL            111
#define IDD_ZONE_LIST_FORM              113
#define IDD_ADC_VIEW_FORM               115
#define IDR_ADCVIEW_TMPL                116
#define IDD_LEVEL_ALERT_FORM            117
#define IDR_LEVELALERTVIEW_TMPL         118
#define IDD_MODBUS_VIEW_FORM            119
#define IDD_WIRELESSSENSE_VIEW_FORM     120
#define IDR_MAINFRAME                   128
#define IDR_GSM_ALERT_TYPE              129
#define IDR_MPI_BAR                     130
#define IDS_LOOK_UP_TABLE               130
#define IDR_SETTINGS                    131
#define IDR_LOG                         132
#define IDS_ADC_SENSOR_NOT_SELECTED     133
#define IDR_SETTINGS_TABS               134
#define IDS_ADC_NO_LUT                  134
#define IDS_ADC_LUT_MEASURED            135
#define IDS_ADC_LUT_ENGINEER            136
#define IDS_ADC                         137
#define IDI_NOT_LOADED                  138
#define IDS_SLAVE_ID                    138
#define IDI_LOADED                      139
#define IDS_REG_TYPE                    139
#define IDS_MODBUS_VAR_TYPE             140
#define IDS_MODBUS_REG_LEN              141
#define IDS_CLOUD_KEY                   142
#define IDS_MODBUS_REG_ADDR             143
#define IDI_SEARCH                      144
#define IDS_MODBUS_REG_NAME             144
#define IDB_TADOR                       145
#define IDS_MODBUS_REG_TYPE_COIL_STAT   145
#define IDS_MODBUS_REG_TYPE_INPUT_STAT  146
#define IDB_CELL_ACCESS                 147
#define IDS_MODBUS_REG_TYPE_HOLDING_REG 147
#define IDI_TADOR                       148
#define IDS_MODBUS_REG_TYPE_INPUT_REGISTER 148
#define IDI_CELL_ACCESS                 149
#define IDS_MODBUS_VAR_TYPE_UNIT        149
#define IDS_MODBUS_VAR_TYPE_INT         150
#define IDS_MODBUS_VAR_TYPE_ULNG        151
#define IDB_GATE_TEL                    152
#define IDS_MODBUS_VAR_TYPE_LNG         152
#define IDS_MODBUS_VAR_TYPE_FLOAT       153
#define IDS_MODBUS_VAR_TYPE_DBL         154
#define IDI_GATE_TEL                    155
#define IDS_MODBUS_VAR_TYPE_BIT         155
#define IDS_MODBUS_SLAVE_ID_NOT_OK      156
#define IDS_MODBUS_REG_NAME_NOT_OK      157
#define IDS_MODBUS_REG_ADDR_NOT_OK      158
#define IDS_REG_TYPE_NOT_SELECTED       159
#define IDS_MODBUS_VAR_TYPE_NOT_SELECTED 160
#define IDS_CLOUD_KEY_NOT_OK            161
#define IDI_AVG                         162
#define IDS_MODBUS_NOT_SELECTED         162
#define IDB_AVG                         163
#define IDS_MODBUS_REG_DECODE_LSWF      163
#define IDI_ROTEM                       164
#define IDS_MODBUS_REG_DECODE_MSWF      164
#define IDB_ROTEM                       165
#define IDS_MODBUS_REG_DECODE           165
#define IDI_PARTS_UNITED                166
#define IDS_INPUT_VOLTAGE               166
#define IDB_PARTS_UNITED                167
#define IDS_BATT_VOLTAGE                167
#define IDB_AVIEM                       168
#define IDS_ZONE_COUNTER_GAP_COL_INDEX  168
#define IDS_ZONE_TYPE_COL_INDEX         169
#define IDS_INPUT_TYPE_REGULAR          170
#define IDB_BISK                        171
#define IDS_INPUT_TYPE_PULSE_CHANGE     171
#define IDS_INPUT_TYPE_PULSE_DOWN       172
#define IDI_BISK                        173
#define IDS_INPUT_TYPE_PULSE_UP         173
#define IDS_ZONE_CNTR_GAP_ILEGAL_VALUE  174
#define IDS_ZONE_STATUS_QUERY           175
#define IDS_MODBUS_STATUS               176
#define IDS_WIRELESS_ID                 177
#define IDS_MODBUS                      178
#define IDS_WIRELESS_NAME               179
#define IDS_WIRELESS_TAG_TYPE           180
#define IDS_WIRELESS_SENSOR_TYPE        181
#define IDS_WIRELESS_SENSOR_PARAM       182
#define IDS_WIRELESS_SENSOR_RESULT      183
#define IDS_TAG_TYPE_TAGSTICK           184
#define IDS_SET_TAGSTICK_GEN            185
#define IDI_ICON1                       185
#define IDI_UNKNOWN                     185
#define IDS_SET_TAGSTICK_REED           186
#define IDI_EMPTY                       186
#define IDS_SET_TAGSTICK_LIGHT          187
#define IDS_SET_TAGSTICK_TEMPERATURE    188
#define IDS_SET_TAGSTICK_HUMIDITY       189
#define IDS_KEEP_ALIVE_INTERVAL         190
#define IDS_REED_REPORT                 191
#define IDS_REED_REPORT_HOLD            192
#define IDS_REED_REPORT_HOLD_AND_RELEASE 193
#define IDS_LIGHT_REPORT                194
#define IDS_LIGHT_REPORT_TO_DARK        195
#define IDS_LIGHT_REPORT_TO_LIGHT       196
#define IDS_LIGHT_REPORT_BOTH           197
#define IDS_SAMPLE_INTERVAL             198
#define IDS_LIGHT_DETECT_TRESH          199
#define IDS_DARK_DETECT_TRESH           200
#define IDS_HIGH_LEVEL                  201
#define IDS_LOW_LEVEL                   202
#define IDS_GAP                         203
#define IDS_ZONE                        204
#define IDS_ALARM_KEY                   205
#define IDS_WIRELESS_SENSOR_TS          206
#define IDS_RSSI                        207
#define IDS_TEMPERATURE                 208
#define IDS_HUMIDITY                    209
#define IDS_REED                        210
#define IDS_LIGHT                       211
#define IDS_GENERAL_SET_NOT_SET         212
#define IDS_EMPTY_WIRELESS_LIST_DOWNLOAD_MSG 213
#define IDS_TAG_NOT_SELECTED            214
#define IDS_GET_DATA_ON_CON             215
#define IDS_BIND_THING                  216
#define IDS_ILLEGAL_TAG_STICK_KEEP_ALIVE_INTERVAL 217
#define IDS_ILLEGAL_TAG_STICK_LIGHT_INTERVAL 218
#define IDS_ILLEGAL_TAG_STICK_HUM_TEMP_INTERVAL 219
#define IDS_WRONG_KEY                   220
#define IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD 221
#define IDS_ILLEGAL_TAG_STICK_HUM_TEMP_GAP 222
#define IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD_ORDER 223
#define IDS_LIST_NOT_SELECTED           224
#define IDS_ILLEGAL_TAG_STICK_LIGHT_THRESHOLD 225
#define IDS_LEVEL_EVENT_KEY             226
#define IDC_COM_PORTS                   1000
#define IDC_LST_IMEI_SET                1001
#define IDC_LST_ADC_LUT                 1001
#define IDC_CMD_SET                     1002
#define IDC_TXT_INDEX                   1003
#define IDC_CMD_SET_OUTPUT              1003
#define IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT 1004
#define IDC_TXT_PHONE_NUM               1005
#define IDC_LBL_INDEX                   1006
#define IDC_TXT_PHONE_NAME              1006
#define IDC_CMD_DELETE_PHONE_NUM        1008
#define IDC_LBL_IMEI                    1009
#define IDC_CMD_STOP                    1010
#define IDC_TXT_IMEI                    1011
#define IDC_CMD_EXCEL_EXPORT            1012
#define IDC_FRAM_IMEI_SN                1013
#define IDC_CMD_EXCEL_IMPORT            1014
#define IDC_CMD_REFRESH                 1015
#define IDC_CMD_QUICK_COMPARE           1016
#define IDC_LST_STATUS                  1017
#define IDC_CMD_QUICK_COMPARE_OUTPUTS   1017
#define IDC_CMD_TURN_OFF                1018
#define IDC_CMD_RESTART                 1019
#define IDC_LST_PHONE_NUM               1020
#define IDC_LST_LOG                     1021
#define IDC_GET_PHONE_NUM               1022
#define IDC_FRAM_PHONE_NUM              1023
#define IDC_TXT_LOC_IN_MEM              1024
#define IDC_LBL_LOC_IN_MEM              1025
#define IDC_LBL_PHONE_NUM               1026
#define IDC_TXT_OUTPUT_LOC_IN_MEM       1026
#define IDC_PRGS_PHONE_DOWNLOAD         1027
#define IDC_PRGS_ZONES_DOWNLOAD         1027
#define IDC_LBL_USED_PHONE              1028
#define IDC_LBL_OUTPUT_LOC_IN_MEM       1028
#define IDC_LBL_VERSION                 1029
#define IDC_LBL_PHONE_NAME              1029
#define IDC_PRGS_OUTPUTS_DOWNLOAD       1029
#define IDC_FRAM_ZONES                  1030
#define IDC_FRAM_CHANNEL_STATUS         1031
#define IDC_FRAM_OUTPUTS                1031
#define IDC_LOG                         1032
#define IDC_TXT_LOC_IN_MEM2             1032
#define IDC_CMB_LANGUAGES               1033
#define IDC_CMD_DOWNLOAD_TO_UNIT        1034
#define IDC_LST_ZONES                   1035
#define IDC_LBL_ZONE_MSG_NO_TO_NC       1036
#define IDC_TXT_ZONE_MSG_NO_TO_NC       1037
#define IDC_LST_OUTPUTS                 1038
#define IDC_INPUT_EDIT                  1039
#define IDC_TXT_OUTPUT_STATE_PARAM      1039
#define IDC_MESSAGE                     1040
#define IDC_CMD_DOWNLOAD_OUTPUTS_TO_UNIT 1040
#define IDC_DTP_UNIT_TIME               1041
#define IDC_LBL_OUTPUT_STATE_PARAM      1041
#define IDC_FRAM_UNIT_TIME              1042
#define IDC_TXT_ZONE_MSG_NC_TO_NO       1042
#define IDC_CMD_SET_UNIT_TIME           1043
#define IDC_TXT_ZONE_DELAY              1043
#define IDC_TXT_ZONE_DELAY_NO_TO_NC     1043
#define IDC_CMD_GET_PC_TIME             1044
#define IDC_LBL_ZONE_DELAY_NO_TO_NC     1044
#define IDC_FRAM_UNIT_PASS              1045
#define IDC_CMB_OUTPUT_STATE            1045
#define IDC_CMD_SET_UNIT_PASS           1046
#define IDC_LBL_OUTPUT_STATE            1046
#define IDC_LBL_PASSWORD                1047
#define IDC_TXT_ZONE_DELAY_NC_TO_NO     1047
#define IDC_TXT_PASSWORD                1048
#define IDC_LBL_ZONE_DELAY_NC_TO_NO     1048
#define IDC_TXT_NEW_PASS                1049
#define IDC_LBL_ZONE_MSG_NC_TO_NO       1049
#define IDC_LBL_NEW_PASS                1050
#define IDC_CMB_INPUT_TYPE              1050
#define IDC_FRAM_UNIT_NAME              1051
#define IDC_LBL_OUTPUT_STATE2           1051
#define IDC_CMD_SET_UNIT_NAME           1052
#define IDC_TXT_ZONE_CNTR_GAP           1052
#define IDC_TXT_UNIT_NAME               1053
#define IDC_LBL_ZONE_MSG_NC_TO_NO2      1053
#define IDC_CMD_GET_UNIT_NAME           1054
#define IDC_FRAM_GATE_DELAY             1055
#define IDC_CMD_SET_GATE_DELAY          1056
#define IDC_TXT_GATE_DELAY              1057
#define IDC_CMD_GET_GATE_DELAY          1058
#define IDC_FRAM_SELF_CU_SMS            1059
#define IDC_CMD_SELF_CU_SMS_SET         1060
#define IDC_TXT_SELF_PHONE_NUM          1061
#define IDC_CMD_SELF_CU_SMS_GET         1062
#define IDC_CMD_GET_LOG                 1063
#define IDC_LBL_SELF_PHONE_NUM          1063
#define IDC_PRGS_LOG_DOWNLOAD           1064
#define IDC_FRAM_SMS_TO_LOGGER          1064
#define IDC_TXT_LAST_LOG_ENTRIES        1065
#define IDC_TXT_UNIT_PHONE_NUMBER       1065
#define IDC_CMD_SMS_TO_LOGGER_SET       1065
#define IDC_LBL_LAST_LOG_ENTRIES        1066
#define IDC_TXT_DEVICE_ID               1066
#define IDC_CMD_LAST_LOG_ENTRIES        1067
#define IDC_CHK_USE_MODEM               1067
#define IDC_CMD_SMS_TO_LOGGER_GET       1067
#define IDC_LBL_UNIT_PHONE_NUMBER       1068
#define IDC_CMD_EXPORT_LOG              1068
#define IDC_LBL_DEVICE_ID               1068
#define IDC_LBL_COM_PORTS               1069
#define IDC_CMD_DEL_LOG                 1069
#define IDC_FRAM_VERSION                1069
#define IDC_LBL_CONFIRM_PASS            1070
#define IDC_TXT_CONFIRM_PASS            1071
#define IDC_CHK_SAVE_PASS               1072
#define IDC_TXT_VERSION                 1072
#define IDC_CHK_SELF_CU_SMS             1073
#define IDC_TXT_TEMP_DIFF               1074
#define IDC_CMD_SET_ZONE_ACTIVE_MODE    1075
#define IDC_CMD_SEARCH_PHONE_NUM        1076
#define IDC_CMD_GET_ZONE_ACTIVE_MODE    1076
#define IDC_CMD_SEARCH_PHONE_NAME       1077
#define IDC_CMD_GET_VERSION             1077
#define IDC_FRAM_RECEPTION              1078
#define IDC_TXT_RECEPTION               1079
#define IDC_CMD_GET_RECEPTION           1080
#define IDC_LBL_ZONE_ACT_REMARK         1081
#define IDC_LBL_TEMP_DIFF               1082
#define IDC_TXT_SMS_INTERVAL            1083
#define IDC_FRAM_REMINDER               1084
#define IDC_CMD_REMINDER_SET            1085
#define IDC_TXT_REMINDER_DELAY          1086
#define IDC_CMD_REMINDER_GET            1087
#define IDC_CHK_REMINDER                1088
#define IDC_CMD_ADD                     1089
#define IDC_LBL_REMINDER_DELAY          1089
#define IDC_CMD_REPLACE                 1090
#define IDC_FRAM_TEST_ALAEM             1090
#define IDC_CMB_TYPE                    1091
#define IDC_CMD_SET_TEST_ALARM          1091
#define IDC_LBL_TYPE                    1092
#define IDC_CMD_GET_TEST_ALARM          1092
#define IDC_CHK_ZONE_ACTIVE_NO_TO_NC    1093
#define IDC_CMB_TEST_ALARM_WEEK_DAY     1093
#define IDC_CHK_TEST_ALARM              1094
#define IDC_CHK_ZONE_ACTIVE_NC_TO_NO    1094
#define IDC_DTP_TEST_ALARM_TIME         1095
#define IDC_TXT_TEST_ALARM_MSG          1096
#define IDC_TXT_ADC1_FROM_MA            1097
#define IDC_LBL_TEST_ALARM_MSG          1097
#define IDC_TXT_ADC1_FROM_MESURED       1098
#define IDC_LBL_SMS_INTERVAL            1098
#define IDC_TXT_ADC1_TO_MA              1099
#define IDC_TXT_SMS_PASS                1099
#define IDC_TXT_ADC1_TO_MESURED         1100
#define IDC_FRAM_ZONE_ACTIVE_MODE       1100
#define IDC_TXT_ADC1_HIGH_TRESHOLD      1101
#define IDC_LBL_SMS_PASS                1101
#define IDC_LBL_ADC1_HIGH_TRESHOLD      1102
#define IDC_FRAM_INTERNET_PARAMS        1102
#define IDC_LBL_ADC1_LOW_TRESHOLD       1103
#define IDC_CMD_INTERNET_SET            1103
#define IDC_TXT_ADC1_LOW_TRESHOLD       1104
#define IDC_TXT_APN                     1104
#define IDC_TXT_ADC1_MESSAGE            1105
#define IDC_CMD_INTERNET_GET            1105
#define IDC_TXT_ADC1_SYMBOL             1106
#define IDC_LBL_APN                     1106
#define IDC_LBL_ADC1_FROM_MA            1107
#define IDC_CHK_GPRS_EN                 1107
#define IDC_LBL_ADC1_TO_MA              1108
#define IDC_TXT_GPRS_USER               1108
#define IDC_LBL_ADC1_FROM_MESURED       1109
#define IDC_LBL_GPRS_USER               1109
#define IDC_LBL_ADC1_TO_MESURED         1110
#define IDC_TXT_GPRS_PASS               1110
#define IDC_LBL_ADC1_TIMEOUT            1111
#define IDC_LBL_GPRS_PASS               1111
#define IDC_LBL_ADC1_MESSAGE            1112
#define IDC_TXT_CLOUD_TOKEN             1112
#define IDC_LBL_ADC1_SYMBOL             1113
#define IDC_LBL_CLOUD_TOKEN             1113
#define IDC_EX_1                        1114
#define IDC_TXT_CLOUD_DNS               1114
#define IDC_EX_2                        1115
#define IDC_LBL_CLOUD_DNS               1115
#define IDC_CHK_ADC1_ACTIVE             1116
#define IDC_TXT_CLOUD_INTERVAL          1116
#define IDC_CHK_ADC1_NORMA_RANGE_SMS    1117
#define IDC_LBL_CLOUD_INTERVAL          1117
#define IDC_FRAM_ADC1_NORMAL_RANGE      1118
#define IDC_LBL_ADC1_FROM_MV            1118
#define IDC_CHK_SSL_EN                  1118
#define IDC_TXT_ADC1_TIMEOUT            1119
#define IDC_FRAM_SELF_CU_SMS2           1119
#define IDC_CMD_GET_ADC1                1120
#define IDC_CMD_RSSI_TO_CLOUD_SET       1120
#define IDC_CMD_SET_ADC1                1121
#define IDC_CMD_RSSI_TO_CLOUD_GET       1121
#define IDC_LBL_ADC1_TO_MV              1122
#define IDC_CHK_RSSI_TO_CLOUD           1122
#define IDC_RDO_ADC1_NORMAL_RANGE_LOW   1123
#define IDC_RDO_ADC1_NORMAL_RANGE_HIGH  1124
#define IDC_LBL_SEND_1_WIRE_SENSOR_NME  1127
#define IDC_FRAM_ADC1                   1128
#define IDC_TXT_1_WIRE_SENSOR_NAME      1128
#define IDC_FRAM_ADC2                   1129
#define IDC_LBL_ZONE_DELAY_FORMAT       1129
#define IDC_LBL_SEND_1_WIRE_SENSOR_NME2 1129
#define IDC_LBL_SENSOR_RECUME_CONNECT_MSG 1129
#define IDC_LBL_OUTPUT_STATE_PARAM_FORMAT 1130
#define IDC_RDO_ZONE_ACTIVE_MOD_EN_PIN  1130
#define IDC_FRAM_INPUT_VOLTAGE          1130
#define IDC_RDO_ZONE_ACTIVE_MOD_RING    1131
#define IDC_FRAM_ONE_WIRE               1131
#define IDC_RDO_ADC1_V                  1132
#define IDC_RDO_DAILY_TEST_ALARM        1132
#define IDC_FRAM_ONE_WIRE_PROGRAM       1132
#define IDC_RDO_ADC1_A                  1133
#define IDC_RDO_WEELY_TEST_ALARM        1133
#define IDC_FRAM_ONE_WIRE_ACTIVE        1133
#define IDC_RDO_ADC2_V                  1134
#define IDC_RDO_ZONE_ACTIVE_MOD_EN_PIN2 1134
#define IDC_FRAM_ONE_WIRE_PROGRAM2      1134
#define IDC_RDO_ADC2_A                  1135
#define IDC_TXT_1_WIRE_SENSOR_NAME2     1135
#define IDC_TXT_SENSOR_RECUME_CONNECT_MSG 1135
#define IDC_RDO_ZONE_ACTIVE_MOD_SMS     1136
#define IDC_TXT_SENSOR_DISCONNECT_MSG   1136
#define IDC_FRAM_ADC                    1137
#define IDC_CHK_SEND_VIN_TO_CLOUD       1138
#define IDC_CHK_SEND_VBATT_TO_CLOUD     1139
#define IDC_LST_ONE_WIRE                1140
#define IDC_CMD_PROGRAM_1_WIRE          1141
#define IDC_CMB_1_WIRE_TO_PRGRM         1142
#define IDC_CMB_1_WIRE                  1143
#define IDC_FRAM_LEVEL_EVENTS           1144
#define IDC_LST_ONE_WIRE_QUERY          1144
#define IDC_LST_LEVEL_EVENTS            1145
#define IDC_CMB_1_WIRE2                 1145
#define IDC_LST_ADC                     1145
#define IDC_CMD_ADD_LEVEL_EVENT         1146
#define IDC_CMB_ADC                     1146
#define IDC_LBL_ADC2_FROM_MA            1147
#define IDC_CMD_REMOVE_LEVEL_EVENT      1147
#define IDC_FRAM_ADC_ACTIVE             1147
#define IDC_CHK_SEND_VIN_ALARM_TO_CLOUD 1147
#define IDC_TXT_ADC2_FROM_MA            1148
#define IDC_CMD_GET_LEVEL_EVENTS        1148
#define IDC_LBL_SEND_1_WIRE_SENSOR_NME3 1148
#define IDC_LBL_ADC2_FROM_MESURED       1149
#define IDC_LST_SENSORS_FOR_LEVEL_EVENT 1149
#define IDC_TXT_ADC_SENSOR_NAME         1149
#define IDC_TXT_ADC2_FROM_MESURED       1150
#define IDC_CMD_SET_LEVEL_EVENTS        1150
#define IDC_LBL_LUT                     1150
#define IDC_LBL_ADC2_TO_MA              1151
#define IDC_TXT_LEVEL_ALERT_LOW_HYS     1151
#define IDC_TXT_ADC_LUT                 1151
#define IDC_TXT_ADC2_TO_MA              1152
#define IDC_LBL_LEVEL_ALERT_LOW_HYS     1152
#define IDC_LST_ADC2                    1152
#define IDC_LBL_ADC_LUT_MEASURED        1152
#define IDC_LBL_ADC2_TO_MESURED         1153
#define IDC_LBL_LEVEL_ALERT_LOW_HYS2    1153
#define IDC_TXT_ADC_LUT_MEASURED        1153
#define IDC_TXT_ADC2_TO_MESURED         1154
#define IDC_TXT_LEVEL_ALERT_HIGH_HYS    1154
#define IDC_LBL_ADC_LUT_ENGINEER        1154
#define IDC_TXT_ADC2_HIGH_TRESHOLD      1155
#define IDC_LBL_LEVEL_ALERT_LOW_MSG     1155
#define IDC_TXT_ADC_LUT_ENGINEER        1155
#define IDC_LBL_ADC2_HIGH_TRESHOLD      1156
#define IDC_TXT_LEVEL_ALERT_LOW_MSG     1156
#define IDC_FRAM_LUT                    1156
#define IDC_LBL_ADC2_LOW_TRESHOLD       1157
#define IDC_LBL_LEVEL_ALERT_LOW_MSG2    1157
#define IDC_TXT_ADC2_LOW_TRESHOLD       1158
#define IDC_TXT_LEVEL_ALERT_HIGH_MSG    1158
#define IDC_LBL_ADC2_MESSAGE            1159
#define IDC_CMB_NORMAL_RANGE            1159
#define IDC_TXT_ADC2_MESSAGE            1160
#define IDC_DTP_REPEAT_ALERT            1160
#define IDC_LBL_ADC2_SYMBOL             1161
#define IDC_CMD_REPLACE_LEVEL_EVENT     1161
#define IDC_TXT_ADC2_SYMBOL             1162
#define IDC_DTP_LOW_DELAY               1162
#define IDC_LBL_ADC2_FROM_MV            1163
#define IDC_DTP_HIGH_DELAY              1163
#define IDC_LST_MODBUS_REGS             1163
#define IDC_EX_3                        1164
#define IDC_FRAM_MODBUS_REGS            1164
#define IDC_TXT_LEVEL_ALERT_ALARM_KEY   1164
#define IDC_EX_4                        1165
#define IDC_CMB_MODBUS_REG_TYPE         1165
#define IDC_LBL_LEVEL_ALERT_ALARM_KEY   1165
#define IDC_CHK_ADC2_ACTIVE             1166
#define IDC_CMB_MODBUS_VAR_TYPE         1166
#define IDC_TXT_LEVEL_ALERT_KEY         1166
#define IDC_CHK_ADC2_NORMA_RANGE_SMS    1167
#define IDC_TXT_MODBUS_ADDRESS          1167
#define IDC_LBL_LEVEL_ALERT_KEY         1167
#define IDC_CMD_SET_ADC2                1168
#define IDC_CMD_ADD_MODBUS_REG          1168
#define IDC_CMD_GET_ADC2                1169
#define IDC_CMD_REMOVE_MODBUS_REG       1169
#define IDC_LBL_ADC2_TIMEOUT            1170
#define IDC_CMD_GET_MODBUS_REGS         1170
#define IDC_TXT_ADC2_TIMEOUT            1171
#define IDC_CMD_SET_MODBUS_REGS         1171
#define IDC_LBL_ADC2_TO_MV              1172
#define IDC_TXT_MODBUS_REG_NAME         1172
#define IDC_CMD_SET_INPUT_VOLTAGE       1173
#define IDC_TXT_MODBUS_SLAVE_ID         1173
#define IDC_FRAM_QUERY_INPUTS           1173
#define IDC_CMD_GET_INPUT_VOLTAGE       1174
#define IDC_CMB_MODBUS_REG_DECODE       1174
#define IDC_LST_INPUTS_QUERY            1174
#define IDC_CMD_SET_1_WIRE_SENSOR       1175
#define IDC_TXT_MODBUS_KEY              1175
#define IDC_CMD_QUERY_INPUTS            1175
#define IDC_CMD_GET_1_WIRE_SENSOR       1176
#define IDC_CMD_REPLACE_MODBUS_REG      1176
#define IDC_CMD_ADD_1_WIRE_SENSOR       1177
#define IDC_FRAM_MODBUS_REGS2           1177
#define IDC_CMD_REMOVE_1_WIRE_SENSOR    1178
#define IDC_CMB_MODBUS_BAUD_RATE        1178
#define IDC_CMD_QUERY_1_WIRE_SENSORS    1179
#define IDC_CMB_MODBUS_CHAR_SET         1179
#define IDC_CMD_SET_ADC_SENSOR          1180
#define IDC_TXT_MODBUS_MAX_BLOCK_SIZE   1180
#define IDC_CMD_GET_ADC_SENSOR          1181
#define IDC_FRAM_QUERY_INPUTS2          1181
#define IDC_CMD_ADD_ADC_SENSOR          1182
#define IDC_LST_MODBUS_QUERY            1182
#define IDC_CMD_REMOVE_ADC_SENSOR       1183
#define IDC_CMD_QUERY_MODBUS            1183
#define IDC_CMD_ADD_ADC_LUT             1184
#define IDC_TRV_TAGS                    1184
#define IDC_CMD_REMOVE_ADC_LUT          1185
#define IDC_FRAM_WIRELESS_SENSORS       1185
#define IDC_TRV_TAGS_QUERY              1186
#define IDC_FRAM_WIRELESS_QUARY         1187
#define IDC_TXT_TAG_ID                  1188
#define IDC_CMB_TAG_TYPE                1189
#define IDC_CMB_TAG_SET_TYPE            1190
#define IDC_EX_5                        1191
#define IDC_TXT_TAG_SET_1               1191
#define IDC_EX_6                        1192
#define IDC_TXT_TAG_SET_2               1192
#define IDC_TXT_TAG_SET_3               1193
#define IDC_TXT_TAG_SET_4               1194
#define IDC_TXT_TAG_SET_5               1195
#define IDC_TXT_TAG_SET_6               1196
#define IDC_CMD_ADD_WIRELESS_SENSOR     1197
#define IDC_CMD_REMOVE_WIRELESS_SENSOR  1198
#define IDC_CMD_GET_WIRLELESS_SENSOR    1199
#define IDC_CMD_SET_WIRELESS_SENSOR     1200
#define IDC_CMD_CLONE_WIRELESS_SENSOR   1201
#define IDC_LBL_SET1                    1202
#define IDC_LBL_SET2                    1203
#define IDC_LBL_SET3                    1204
#define IDC_LBL_SET4                    1205
#define IDC_LBL_SET5                    1206
#define IDC_LBL_SET6                    1207
#define IDC_CMB_SET1                    1208
#define IDC_CMD_QUERY_WIRELESS_SENSORS  1209
#define IDC_CHK_RF_ACTIVE               1210
#define IDC_CMD_SET_WIRELESS_SENSOR2    1211
#define IDC_CMD_COPY_WIRELESS_SENSOR_CLIPBOARD 1211
#define IDC_CHK_SET3                    1212
#define IDC_CMD_CLEAR_WIRELESS_SENSORS  1213
#define IDC_CMD_COPY_1_WIRE_SENSOR_CLIPBOARD 1213
#define IDC_CMD_COPY_ADC_SENSOR_CLIPBOARD 1214
#define IDC_CMD_COPY_ZONES_CLIPBOARD    1214
#define IDC_CMD_COPY_LEVEL_EVENTS_CLIPBOARD 1215
#define ID_CHANGE_LANGUAGE              32768
#define ID_AUTO_OPEN_LAST_FILE          32769
#define ID_TAB_ZONE_LIST                32770
#define ID_MPI_SETTINGS                 32771
#define ID_MPI_LOG                      32772
#define ID_TAB_PHONE_LIST               32773
#define ID_TAB_MISC_SETTINGS            32774
#define ID_LOAD_SEARCH_MODEM_DLG        32775
#define IDS_LANGUAGE_DISPLAY_NAME       32776
#define IDS_UNIT_STATUS_MSG_CONNECTING_TO_UNIT 32777
#define IDS_UNIT_STATUS_MSG_CONNECTED_TO_UNIT 32778
#define IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT 32779
#define IDS_UNIT_STATUS_MSG_UNIT_INIT_FAIL 32780
#define IDS_UNIT_NOT_RESPONDED          32781
#define ID_CONNECT_TO_UNIT              32782
#define ID_DISCONNECT_FROM_UNIT         32783
#define IDS_OFFLINE                     32784
#define IDS_ENABLED                     32785
#define IDS_ERROR_OCCURRED              32786
#define IDS_CANNOT_OPEN_FILE            32787
#define IDS_PHONE_NUMBER                32788
#define IDS_OK                          32789
#define IDS_LOC_IN_MEM                  32790
#define IDS_LOC_IN_MEM_NOT_ENTERED      32791
#define IDS_PHONE_NUM_NOT_OK            32792
#define IDS_LOC_IN_MEM_TO_BIG           32793
#define IDS_CONNECTION_ERROR            32794
#define IDS_PHONE_NUM_EXIST             32795
#define IDS_USED_PHONE_NUM              32796
#define IDS_VERSION                     32797
#define IDS_EXCEL_EXPORT                32798
#define IDS_EXCEL_FILE                  32799
#define IDS_ERR_WHILE_EXPORT_EXCEL_FILE 32800
#define IDS_EXCEL_IMPORT                32801
#define IDS_FILE_NOT_EXIST              32802
#define IDS_ODBC_DRIVER_ERROR           32803
#define IDS_ERR_WHILE_IMPORT_EXCEL_FILE 32804
#define IDS_DATA_EXCEL_IMPORT_ERR       32805
#define IDS_LOG_DETAILES_COL_INDEX      32806
#define IDS_TIME_COL_INDEX              32807
#define IDS_GET_PHONE_NUM_WARNING       32808
#define IDS_RESTART_NEEDED              32809
#define IDS_IDC_LBL_PHONE_NUM           32810
#define IDS_IDC_LOG                     32811
#define IDS_IDC_GET_PHONE_NUM           32812
#define IDS_IDC_CMD_SET                 32813
#define IDS_IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT 32814
#define IDS_IDC_CMD_ADD                 32815
#define IDS_IDC_CMD_DELETE_PHONE_NUM    32816
#define IDS_IDC_CMD_STOP                32817
#define IDS_IDC_CMD_EXCEL_EXPORT        32818
#define IDS_IDC_CMD_EXCEL_IMPORT        32819
#define IDS_IDC_LBL_LOC_IN_MEM          32820
#define IDS_IDC_CMD_QUICK_COMPARE       32821
#define IDS_IDC_FRAM_PHONE_NUM          32822
#define IDC_GET_ZONES                   32824
#define IDS_COM_SELECTION_ERROR         32825
#define IDC_GET_OUTPUTS                 32825
#define IDS_IDD_SEARCH_MODEM_DIALOG     32826
#define IDS_IDD_CHANGE_LANG_DIAL        32827
#define IDS_CANCEL                      32828
#define IDS_CANNOT_OPEN_COM             32829
#define IDS_ZONE_MSG_NO_TO_NC           32830
#define IDS_IDC_FRAM_ZONES              32831
#define IDS_HASH_ERROR_OCCURRED         32832
#define IDS_UNIT_STATUS_MSG_DISCONNECTING_FROM_UNIT 32833
#define IDS_DISCONNECT_FROM_UNIT_MSG    32834
#define IDS_IDC_FRAM_UNIT_TIME          32835
#define IDS_IDC_CMD_SET_UNIT_TIME       32836
#define IDS_IDC_CMD_GET_PC_TIME         32837
#define IDS_IDC_FRAM_UNIT_PASS          32838
#define IDS_IDC_LBL_PASSWORD            32839
#define IDS_IDC_LBL_NEW_PASS            32840
#define IDS_IDC_CMD_SET_UNIT_PASS       32841
#define IDS_INCORRECT_PASS              32842
#define IDS_NEW_PASS_EMPTY              32843
#define IDS_IDC_CMD_GET_UNIT_NAME       32844
#define IDS_IDC_CMD_SET_UNIT_NAME       32845
#define IDS_IDC_FRAM_UNIT_NAME          32846
#define IDS_IDC_CMD_GET_GATE_DELAY      32847
#define IDS_IDC_CMD_SET_GATE_DELAY      32848
#define IDS_IDC_FRAM_GATE_DELAY         32849
#define IDS_GATE_DELAY_LIMIT            32850
#define IDS_SECONDS                     32851
#define IDS_IDC_GET_ZONES               32857
#define IDS_IDC_CMD_DOWNLOAD_TO_UNIT    32861
#define IDS_IDC_CMD_GET_LOG             32862
#define IDS_IDC_CMD_LAST_LOG_ENTRIES    32863
#define IDS_IDC_LBL_LAST_LOG_ENTRIES    32864
#define IDS_NO_LAST_ENTRIES_INSERTED    32865
#define ID_TAB_ADC_SETTINGS             32866
#define ID_TAB_WIRELESS_SENSORS         32867
#define ID_TAB_MODBUS                   32868
#define IDS_IDC_CHK_USE_MODEM           59394
#define IDS_IDC_LBL_UNIT_PHONE_NUMBER   59395
#define IDS_MODEM_NOT_REGISTER          59396
#define IDS_MODEM_INIT_FAILD            59397
#define IDS_MODEM_CREG_FAILD            59398
#define IDS_MODEM_DIAL_FAILD            59399
#define IDS_IDC_LBL_CONFIRM_PASS        59400
#define IDS_IDC_CHK_SAVE_PASS           59401
#define IDS_IDC_LBL_COM_PORTS           59402
#define IDS_CURRENT_PASS_EMPTY          59403
#define IDS_CONFIRM_PASS_ERROR          59404
#define IDS_LOC_IN_MEM_IS_ZERO          59405
#define IDS_PHONE_NUM_IN_NORMAL_LIST    59407
#define IDS_IDC_FRAM_SELF_CU_SMS        59408
#define IDS_IDC_CHK_SELF_CU_SMS         59409
#define IDS_IDC_CMD_SELF_CU_SMS_GET     59410
#define IDS_IDC_CMD_SELF_CU_SMS_SET     59411
#define IDS_IDC_LBL_SELF_PHONE_NUM      59412
#define IDS_NAME                        59413
#define IDS_SELECT_COM_PORT             59414
#define IDS_IDC_FRAM_BAND_SELECT        59415
#define IDS_IDC_CMD_GET_BAND_SELECT     59416
#define IDS_IDC_CMD_SET_BAND_SELECT     59417
#define IDS_GSM900_DCS1800              59418
#define IDS_GSM900_PCS1900              59419
#define IDS_GSM850_PCS1800              59420
#define IDS_GSM850_PCS1900              59421
#define IDS_UNIT_IS_IN_SESSION          59422
#define IDS_PHONE_NUM_NOT_EXIST         59423
#define IDS_PHONE_NAME_NOT_EXIST        59424
#define IDS_SEARCH                      59425
#define IDS_WEEK_DAYS_SELECTION_ERROR   59434
#define IDS_IDC_CMD_EXPORT_LOG          59435
#define IDS_LOG_HAVE_BEEN_DELETED       59436
#define IDS_DEL_LOG_QUEST               59437
#define IDS_TYPE_MASTER                 59438
#define IDS_TYPE_NOT_SELECTED           59439
#define IDS_TYPE                        59440
#define IDS_MORE_DATA_IN_UNIT           59441
#define IDS_ZONE_DELAY_NO_TO_NC         59442
#define IDS_ZONE_ACTIVE_NO_TO_NC        59443
#define IDS_ZONE_MSG_EMPTY              59444
#define IDS_ZONE_NAME_EXIST             59445
#define IDS_ZONE_NAME_NOT_EXIST         59446
#define IDS_IDC_LBL_OUTPUT_STATE_PARAM  59447
#define IDS_IDC_LBL_OUTPUT_STATE        59448
#define IDS_OUTPUT_STATE_HIGHT_LOW      59449
#define IDS_OUTPUT_STATE_BY_REQ         59450
#define IDS_OUTPUT_STATE_ZONE_DETECT    59451
#define IDS_OUTPUT_STATE_PARAM_EMPTY    59452
#define IDS_OUTPUT_STATE_EMPTY          59453
#define IDS_OUTPUT_STATE_PARAM_ILEGAL   59454
#define IDS_ZONE_DELAY_ILEGAL_VALUE     59455
#define IDS_TYPE_SECONDARY              59456
#define IDS_IDC_LBL_ADC1_FROM_MA        59457
#define IDS_IDC_LBL_ADC1_TO_MA          59458
#define IDS_IDC_LBL_ADC1_FROM_MESURED   59459
#define IDS_IDC_LBL_ADC1_TO_MESURED     59460
#define IDS_IDC_LBL_ADC1_LOW_TRESHOLD   59461
#define IDS_IDC_LBL_ADC1_TIMEOUT        59462
#define IDS_IDC_LBL_ADC1_MESSAGE        59463
#define IDS_IDC_LBL_ADC1_SYMBOL         59464
#define IDS_IDC_LBL_ADC1_HIGH_TRESHOLD  59465
#define IDS_IDC_RDO_NORMAL_RANGE_LOW    59466
#define IDS_IDC_RDO_NORMAL_RANGE_HIGH   59467
#define IDS_IDC_CHK_ADC1_ACTIVE         59468
#define IDS_IDC_CHK_ADC1_NORMA_RANGE_SMS 59469
#define IDS_IDC_EX_1                    59470
#define IDS_IDC_EX_2                    59471
#define IDS_IDC_CMD_GET_ADC1            59472
#define IDS_IDC_CMD_SET_ADC1            59473
#define IDS_MESURE_NOT_OK               59474
#define IDS_TRESHOLD_RANGE_NOT_GOOD     59475
#define IDS_TIMEOUT_SYNTAX_ERROR        59476
#define IDS_OUTPUT_STATE_ALARM          59478
#define IDS_ZONE_MSG_NC_TO_NO           59479
#define IDS_OUTPUT_STATE_BUZZER         59480
#define IDS_ZONE_ACTIVE_NC_TO_NO        59481
#define IDS_DEBOUNCE_LIMIT              59485
#define IDS_ERR_DELETE_FILE             59486
#define IDS_REMIDER_DELAY_NOT_OK        59487
#define IDS_WEEK_DAY_SUNDAY             59488
#define IDS_WEEK_DAY_MONDAY             59489
#define IDS_WEEK_DAY_TUESDAY            59490
#define IDS_WEEK_DAY_WEDNESDAY          59491
#define IDS_WEEK_DAY_THURSDAY           59492
#define IDS_WEEK_DAY_FRIDAY             59493
#define IDS_WEEK_DAY_SATURDAY           59494
#define IDS_EMPTY_PHONE_LIST_DOWNLOAD_MSG 59495
#define IDS_EMPTY_PHONE_LIST_DOWNLOAD_TITLE 59496
#define IDS_DISABLE_ALARM_TEST_DOWNLOAD_MSG 59497
#define IDS_DISABLE_ALARM_TEST_DOWNLOAD_TITLE 59498
#define IDS_ZONE_DELAY_NC_TO_NO         59499
#define IDS_ZONE_PIN_CABLE_POS_COL_INDEX 59500
#define ID_TAB_LEVEL_ALERTS             59501
#define IDS_SENSOR_NUMBER               59502
#define IDS_SENSOR_NAME                 59503
#define IDS_ONE_WIRE_SENSOR_NOT_SELECTED 59504
#define IDS_INDEX                       59505
#define IDS_SENSOR_TYPE                 59506
#define IDS_LOW_HYS                     59507
#define IDS_HIGH_HYS                    59508
#define IDS_LOW_MSG                     59509
#define IDS_HIGH_MSG                    59510
#define IDS_NORMAL_RANGE                59511
#define IDS_REPEAT_ALERT                59512
#define IDS_ONE_WIRE                    59513
#define IDS_NORMAL_RANGE_LOW            59514
#define IDS_NORMAL_RANGE_HIGH           59515
#define IDS_NORMAL_RANGE_NOT_SELECTED   59516
#define IDS_LOW_HYS_NOT_OK              59517
#define IDS_HIGH_HYS_NOT_OK             59518
#define IDS_SENSOR_NOT_SELECTED         59519
#define IDS_LEVEL_EVENT_NOT_SELECTED    59520
#define IDS_HYS_MSG_ERROR               59521
#define IDS_NOT_ALL_SENSORS_PRESENT     59522
#define IDS_LOW_DELAY                   59523
#define IDS_HIGH_DELAY                  59524
#define IDS_SENSOR_TEMPERATURE          59525
#define IDS_TYPE_MACHINE                59526
#define IDS_DISCONNECT_MSG              59527
#define IDS_RESUME_CONNECT_MSG          59528

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        187
#define _APS_NEXT_COMMAND_VALUE         32869
#define _APS_NEXT_CONTROL_VALUE         1218
#define _APS_NEXT_SYMED_VALUE           121
#endif
#endif
