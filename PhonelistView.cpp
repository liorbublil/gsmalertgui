// PhoneListView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MainFrm.h"
#include "GsmAlertDoc.h"
#include "PhoneListView.h"
#include "Language.h"
#include "global.h"
#include "InputDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LOC_IN_MEM_COL_INDEX 0
#define PHONE_NUM_COL_INDEX 1
#define PHONE_NAME_COL_INDEX 2
#define TYPE_COL_INDEX 3

#define MAX_HASH_FILED_TIMES 4

#define PHONE_RECORD_LEN 42

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CPhoneListView

IMPLEMENT_DYNCREATE(CPhoneListView, CFormView)

CPhoneListView::CPhoneListView()
	: CFormView(CPhoneListView::IDD)
{
	//{{AFX_DATA_INIT(CPhoneListView)
	m_strPhoneNum = _T("");
	m_strUsedPhones = _T("");
	m_strPhoneName = _T("");
	m_iType = -1;
	//}}AFX_DATA_INIT
	m_bFirstRun=true;
	m_bStopDownload=false;
	m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
}

CPhoneListView::~CPhoneListView()
{
}

void CPhoneListView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPhoneListView)
	DDX_Control(pDX, IDC_CMB_TYPE, m_cmbType);
	DDX_Control(pDX, IDC_LBL_TYPE, m_lblType);
	DDX_Control(pDX, IDC_CMD_REPLACE, m_cmdReplace);
	DDX_Control(pDX, IDC_CMD_SEARCH_PHONE_NUM, m_cmdSearchPhoneNum);
	DDX_Control(pDX, IDC_CMD_SEARCH_PHONE_NAME, m_cmdSearchPhoneName);
	DDX_Control(pDX, IDC_TXT_PHONE_NAME, m_txtPhoneName);
	DDX_Control(pDX, IDC_LBL_PHONE_NAME, m_lblPhoneName);
	DDX_Control(pDX, IDC_CMD_EXCEL_IMPORT, m_cmdExcelImport);
	DDX_Control(pDX, IDC_CMD_EXCEL_EXPORT, m_cmdExcelExport);
	DDX_Control(pDX, IDC_CMD_STOP, m_cmdStop);
	DDX_Control(pDX, IDC_LBL_USED_PHONE, m_lblUsedPhones);
	DDX_Control(pDX, IDC_CMD_DELETE_PHONE_NUM, m_cmdDeletePhoneNum);
	DDX_Control(pDX, IDC_PRGS_PHONE_DOWNLOAD, m_prgsPhoneDownload);
	DDX_Control(pDX, IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT, m_cmdDownloadAllPhonesToUnit);
	DDX_Control(pDX, IDC_CMD_ADD, m_cmdAdd);
	DDX_Control(pDX, IDC_CMD_QUICK_COMPARE, m_cmdQuickCompare);
	DDX_Control(pDX, IDC_TXT_PHONE_NUM, m_txtPhoneNum);
	DDX_Control(pDX, IDC_LBL_PHONE_NUM, m_lblPhoneNum);
	DDX_Control(pDX, IDC_FRAM_PHONE_NUM, m_framPhoneNum);
	DDX_Control(pDX, IDC_GET_PHONE_NUM, m_cmdGetPhoneNum);
	DDX_Control(pDX, IDC_LST_PHONE_NUM, m_lstPhoneNum);
	DDX_Text(pDX, IDC_TXT_PHONE_NUM, m_strPhoneNum);
	DDX_Text(pDX, IDC_LBL_USED_PHONE, m_strUsedPhones);
	DDX_Text(pDX, IDC_TXT_PHONE_NAME, m_strPhoneName);
	DDV_MaxChars(pDX, m_strPhoneName, MAX_PHONE_NAME_LEN_DEF);
	DDX_CBIndex(pDX, IDC_CMB_TYPE, m_iType);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPhoneListView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CPhoneListView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_GET_PHONE_NUM, OnGetPhoneNum)
	ON_NOTIFY(NM_CLICK, IDC_LST_PHONE_NUM, OnClickLstPhoneNum)
	ON_BN_CLICKED(IDC_CMD_ADD, OnCmdAdd)
	ON_BN_CLICKED(IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT, OnCmdDownloadAllPhonesToUnit)
	ON_BN_CLICKED(IDC_CMD_DELETE_PHONE_NUM, OnCmdDeletePhoneNum)
	ON_BN_CLICKED(IDC_CMD_STOP, OnCmdStop)
	ON_BN_CLICKED(IDC_CMD_EXCEL_EXPORT, OnCmdExcelExport)
	ON_BN_CLICKED(IDC_CMD_EXCEL_IMPORT, OnCmdExcelImport)
	ON_BN_CLICKED(IDC_CMD_QUICK_COMPARE, OnCmdQuickCompare)
	ON_BN_CLICKED(IDC_CMD_SEARCH_PHONE_NUM, OnCmdSearchPhoneNum)
	ON_BN_CLICKED(IDC_CMD_SEARCH_PHONE_NAME, OnCmdSearchPhoneName)
	ON_BN_CLICKED(IDC_CMD_REPLACE, OnCmdReplace)
	ON_EN_CHANGE(IDC_TXT_PHONE_NAME, OnChangeTxtPhoneName)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPhoneListView diagnostics

#ifdef _DEBUG
void CPhoneListView::AssertValid() const
{
	CFormView::AssertValid();
}

void CPhoneListView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CPhoneListView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CPhoneListView message handlers

void CPhoneListView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	CGsmAlertDoc *l_pDoc=GetDocument();

	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnInitialUpdate"));

	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,CString("CPhoneListView::OnInitialUpdate first timr"));
		m_bFirstRun=false;
		
		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);
		
		m_lstPhoneNum.InsertColumn(LOC_IN_MEM_COL_INDEX,GetResString(IDS_LOC_IN_MEM),LVCFMT_LEFT,120);
		m_lstPhoneNum.InsertColumn(PHONE_NUM_COL_INDEX,GetResString(IDS_PHONE_NUMBER),LVCFMT_LEFT,120);
		m_lstPhoneNum.InsertColumn(PHONE_NAME_COL_INDEX,GetResString(IDS_NAME),LVCFMT_LEFT,240);
		m_lstPhoneNum.InsertColumn(TYPE_COL_INDEX,GetResString(IDS_TYPE),LVCFMT_LEFT,240);

		m_imlPhoneNum.Create(16,16,ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_NOT_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_SEARCH),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		m_imlPhoneNum.Add(l_hIconNotLoaded_16x16);
		m_imlPhoneNum.Add(l_hIconLoaded_16x16);
		m_imlPhoneNum.Add(l_hIconSearch_16x16 );
		m_lstPhoneNum.SetImageList(&m_imlPhoneNum,LVSIL_SMALL | LVSIL_NORMAL);

		//Set the list to be full row selection and with grid lines
		DWORD l_dwStyle=m_lstPhoneNum.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstPhoneNum.SetExtendedStyle(l_dwStyle);

		int l_iIndex=m_cmbType.AddString(l_pDoc->GetTypeNameFromNum(CMB_TYPE_MASTER_DEF));
		m_cmbType.SetItemData(l_iIndex,CMB_TYPE_MASTER_DEF);
		l_iIndex=m_cmbType.AddString(l_pDoc->GetTypeNameFromNum(CMB_TYPE_SECOND_DEF));
		m_cmbType.SetItemData(l_iIndex,CMB_TYPE_SECOND_DEF);
		l_iIndex=m_cmbType.AddString(l_pDoc->GetTypeNameFromNum(CMB_TYPE_MACHINE_DEF));
		m_cmbType.SetItemData(l_iIndex,CMB_TYPE_MACHINE_DEF);
		Localize();
	}
	m_lstPhoneNum.DeleteAllItems();
	
	POSITION l_Position;
	CPhoneNumListElement *l_pListElement;
	l_Position=l_pDoc->m_PhoneNumList.GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=l_pDoc->m_PhoneNumList.GetNext(l_Position);
		AddItemToPhoneList(l_pListElement->m_iLocInMem,l_pListElement->m_strPhoneNum,l_pListElement->m_strPhoneName,l_pListElement->m_iType,l_pListElement->m_bInUnit,true);
	}
	m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
	m_strUsedPhones.Format("%s: %d\\%d",GetResString(IDS_USED_PHONE_NUM), m_lstPhoneNum.GetItemCount(),l_pDoc->m_iMaxPhoneNumAmount);
	UpdateData(FALSE);
}

void CPhoneListView::OnSize(UINT nType, int cx, int cy) 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnSize"));
	
	RECT l_rctPhoneNumLst;
	RECT l_rctPhoneNumFrame;
	RECT l_rctPhoneNumLbl;
	RECT l_rctPhoneNumTxt;
	RECT l_rctPhoneNameLbl;
	RECT l_rctPhoneNameTxt;
	RECT l_rctGetPhoneNumCmd;
	RECT l_rctAddCmd;
	RECT l_rctReplaceCmd;
	RECT l_rctDownloadAllPhonesToUnitCmd;
	RECT l_rctPhoneDownloadPrgs;
	RECT l_rctDeletePhoneNumCmd;
	RECT l_rctUsedPhoneNumLbl;
	RECT l_rctStopCmd;
	RECT l_rctExcelExportCmd;
	RECT l_rctExcelImportCmd;
	RECT l_rctQuickCompareCmd;
	RECT l_rctSearchPhoneNumCmd;
	RECT l_rctSearchPhoneNameCmd;
	RECT l_rctTypeLbl;
	RECT l_rctTypeCmb;

	int l_iHeight;
	int l_iLeft;
	if(IsWindow(m_lstPhoneNum.GetSafeHwnd()))
	{
		m_lstPhoneNum.GetWindowRect(&l_rctPhoneNumLst);
		m_framPhoneNum.GetWindowRect(&l_rctPhoneNumFrame);
		m_lblPhoneNum.GetWindowRect(&l_rctPhoneNumLbl);
		m_txtPhoneNum.GetWindowRect(&l_rctPhoneNumTxt);
		m_lblPhoneName.GetWindowRect(&l_rctPhoneNameLbl);
		m_txtPhoneName.GetWindowRect(&l_rctPhoneNameTxt);
		m_cmdGetPhoneNum.GetWindowRect(&l_rctGetPhoneNumCmd);
		m_cmdAdd.GetWindowRect(&l_rctAddCmd);
		m_cmdReplace.GetWindowRect(&l_rctReplaceCmd);
		m_cmdDownloadAllPhonesToUnit.GetWindowRect(&l_rctDownloadAllPhonesToUnitCmd);
		m_prgsPhoneDownload.GetWindowRect(&l_rctPhoneDownloadPrgs);
		m_cmdDeletePhoneNum.GetWindowRect(&l_rctDeletePhoneNumCmd);
		m_lblUsedPhones.GetWindowRect(&l_rctUsedPhoneNumLbl);
		m_cmdStop.GetWindowRect(&l_rctStopCmd);
		m_cmdExcelExport.GetWindowRect(&l_rctExcelExportCmd);
		m_cmdExcelImport.GetWindowRect(&l_rctExcelImportCmd);
		m_cmdQuickCompare.GetWindowRect(&l_rctQuickCompareCmd);
		m_cmdSearchPhoneNum.GetWindowRect(&l_rctSearchPhoneNumCmd);
		m_cmdSearchPhoneName.GetWindowRect(&l_rctSearchPhoneNameCmd);
		m_cmbType.GetWindowRect(&l_rctTypeCmb);
		m_lblType.GetWindowRect(&l_rctTypeLbl);
		
		l_iHeight=cy-l_rctPhoneNumFrame.bottom+l_rctPhoneNumFrame.top-l_rctUsedPhoneNumLbl.bottom + l_rctUsedPhoneNumLbl.top;
		m_lstPhoneNum.SetWindowPos(NULL,0,0,cx,l_iHeight,SWP_NOREPOSITION);

		m_lblUsedPhones.SetWindowPos(NULL,15,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iHeight+=(l_rctUsedPhoneNumLbl.bottom - l_rctUsedPhoneNumLbl.top );

		m_framPhoneNum.SetWindowPos(NULL,5,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iHeight+=15;

		m_lblPhoneNum.SetWindowPos(NULL,15,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft=l_rctPhoneNumLbl.right-l_rctPhoneNumLbl.left+20;
		m_txtPhoneNum.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctPhoneNumTxt.right-l_rctPhoneNumTxt.left+20;
		m_cmdSearchPhoneNum.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iHeight+= (l_rctPhoneNumTxt.bottom - l_rctPhoneNumTxt.top );

		m_lblPhoneName.SetWindowPos(NULL,15,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft=l_rctPhoneNameLbl.right-l_rctPhoneNameLbl.left+20;
		m_txtPhoneName.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctPhoneNameTxt.right-l_rctPhoneNameTxt.left+20;
		m_cmdSearchPhoneName.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iHeight+= (l_rctPhoneNameTxt.bottom - l_rctPhoneNameTxt.top );

		m_lblType.SetWindowPos(NULL,15,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft=l_rctTypeLbl.right-l_rctTypeLbl.left+20;
		m_cmbType.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctTypeCmb.right-l_rctTypeCmb.left+20;
		l_iHeight+= (l_rctTypeCmb.bottom - l_rctTypeCmb.top );
		
		m_prgsPhoneDownload.SetWindowPos(NULL,15,l_iHeight,l_rctPhoneNameTxt.right - l_rctPhoneNumLbl.left,l_rctPhoneDownloadPrgs.bottom - l_rctPhoneDownloadPrgs.top ,SWP_NOREPOSITION);
		l_iHeight+= (l_rctPhoneDownloadPrgs.bottom - l_rctPhoneDownloadPrgs.top );

		
		m_cmdAdd.SetWindowPos(NULL,15,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft=l_rctAddCmd.right-l_rctAddCmd.left + 20;
		
		m_cmdReplace.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctReplaceCmd.right-l_rctReplaceCmd.left + 5;
		
		m_cmdDeletePhoneNum.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctDeletePhoneNumCmd.right-l_rctDeletePhoneNumCmd.left + 5;

		m_cmdGetPhoneNum.SetWindowPos(NULL,l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctGetPhoneNumCmd.right-l_rctGetPhoneNumCmd.left + 5;
		
		m_cmdDownloadAllPhonesToUnit.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctDownloadAllPhonesToUnitCmd.right-l_rctDownloadAllPhonesToUnitCmd.left + 5;

		m_cmdQuickCompare.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctQuickCompareCmd.right-l_rctQuickCompareCmd.left + 5;

		m_cmdStop.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctStopCmd.right-l_rctStopCmd.left + 5;

		m_cmdExcelExport.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctExcelExportCmd.right-l_rctExcelExportCmd.left + 5;

		m_cmdExcelImport.SetWindowPos(NULL, l_iLeft,l_iHeight,0,0,SWP_NOREPOSITION | SWP_NOSIZE);
		l_iLeft+=l_rctExcelImportCmd.right-l_rctExcelImportCmd.left + 5;
	}
	CFormView::OnSize(nType, cx, cy);
}

LRESULT CPhoneListView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	CString *l_pArrivedRespond;

	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
		m_cmdStop.EnableWindow(FALSE);
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if(m_enmPhoneListLastSentCommand!=enmPhoneListLastSentCommandNone)
		{
			m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR + MB_OK);
			m_prgsPhoneDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	l_pArrivedRespond=(CString*)lParam;
	int l_iRespondStartAt;
	int l_iLastRespondStartAt=0;
	CGsmAlertDoc *l_pDoc=GetDocument();
	bool l_bItemAdd=false;

	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(PHONE_NUM_REQ_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,PHONE_NUM_REQ_MSG_RESPOND);
			l_iDataStartAt+=(CString(PHONE_NUM_REQ_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strAmountOfData,l_strRecordsData;
			CString l_strPhoneNum,l_strPhoneName,l_strAlertMode;
			int l_iLocInMem,l_iAmountOfData,l_iAlertMode;

			l_strAmountOfData=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt=FindInString(l_strAmountOfData,",");
			l_strRecordsData=l_strAmountOfData.Mid(l_iDataStartAt+1);
			l_strAmountOfData=l_strAmountOfData.Left(l_iDataStartAt);
			
			if(IsNumeric(l_strAmountOfData)==false)
			{
				++m_iHashFailedTimes;
				m_bHashOK=false;
				l_iLastRespondStartAt=l_iRespondStartAt+1;
				continue;
			}
			l_iAmountOfData=atoi(LPCTSTR(l_strAmountOfData));
			if((l_strRecordsData.GetLength()/PHONE_RECORD_LEN)!=l_iAmountOfData)
			{
				++m_iHashFailedTimes;
				m_bHashOK=false;
				l_iLastRespondStartAt=l_iRespondStartAt+1;
				continue;
			}
			
			m_bHashOK=true;
			m_iHashFailedTimes=0;
			if(m_enmPhoneListLastSentCommand==enmPhoneListLastSentCommandPhoneNumAsk)
			{
				for(l_iDataStartAt=0,l_iLocInMem=1;l_iLocInMem<=l_iAmountOfData;l_iDataStartAt+=PHONE_RECORD_LEN,++l_iLocInMem)
				{
					l_strPhoneNum=l_strRecordsData.Mid(l_iDataStartAt,MAX_PHONE_NUM_LEN_DEF);
					l_strPhoneName=l_strRecordsData.Mid(l_iDataStartAt+MAX_PHONE_NUM_LEN_DEF,MAX_PHONE_NAME_LEN_DEF);
					l_strAlertMode=l_strRecordsData.Mid(l_iDataStartAt+MAX_PHONE_NUM_LEN_DEF+MAX_PHONE_NAME_LEN_DEF,2);
					l_iAlertMode=atoi(LPCTSTR(l_strAlertMode));
					l_pDoc->m_PhoneNumList.AddItemSorted(l_iLocInMem,l_strPhoneNum,l_strPhoneName,l_iAlertMode,true,false);
					AddItemToPhoneList(l_iLocInMem,l_strPhoneNum,l_strPhoneName,l_iAlertMode,true,false);
					m_prgsPhoneDownload.SetPos(l_iLocInMem);
				}
				l_pDoc->SetModifiedFlag();
				l_bItemAdd=true;
			}
			else if(m_enmPhoneListLastSentCommand==enmPhoneListLastSentCommandQucikCompare)
			{
				int l_iPhoneListCount=l_pDoc->m_PhoneNumList.GetCount();
				CPhoneNumListElement *l_PhoneNumListElement;
				for(l_iDataStartAt=0,l_iLocInMem=1;(l_iLocInMem<=l_iAmountOfData)&&(l_iLocInMem<=l_iPhoneListCount);l_iDataStartAt+=PHONE_RECORD_LEN,++l_iLocInMem)
				{
					l_strPhoneNum=l_strRecordsData.Mid(l_iDataStartAt,MAX_PHONE_NUM_LEN_DEF);
					l_strPhoneName=l_strRecordsData.Mid(l_iDataStartAt+MAX_PHONE_NUM_LEN_DEF,MAX_PHONE_NAME_LEN_DEF);
					l_strAlertMode=l_strRecordsData.Mid(l_iDataStartAt+MAX_PHONE_NUM_LEN_DEF+MAX_PHONE_NAME_LEN_DEF,2);
					l_iAlertMode=atoi(LPCTSTR(l_strAlertMode));
					l_PhoneNumListElement = l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_iLocInMem);
					if((l_PhoneNumListElement->m_iType==l_iAlertMode)&&(l_PhoneNumListElement->m_strPhoneName==l_strPhoneName)&&(l_PhoneNumListElement->m_strPhoneNum==l_strPhoneNum))
					{
						l_PhoneNumListElement->m_bInUnit=true;
						SetItemImage(l_iLocInMem,true);
					}
					else
					{
						l_PhoneNumListElement->m_bInUnit=false;
						SetItemImage(l_iLocInMem,false);
					}
					m_prgsPhoneDownload.SetPos(l_iLocInMem);
				}
				if(l_iAmountOfData<l_iPhoneListCount)
				{
					for(l_iLocInMem=l_iAmountOfData+1;l_iLocInMem<=l_iPhoneListCount;++l_iLocInMem)
					{
						l_PhoneNumListElement = l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_iLocInMem);
						l_PhoneNumListElement->m_bInUnit=false;
						SetItemImage(l_iLocInMem,false);
					}
				}
				else if(l_iAmountOfData>l_iPhoneListCount)
				{
					m_bQuickCompareOverDataInTheUnit = true;
				}
			}
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
	} while(l_iRespondStartAt!=-1);

	if(l_bItemAdd)
	{
		m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
		m_strUsedPhones.Format("%s: %d\\%d",GetResString(IDS_USED_PHONE_NUM), m_lstPhoneNum.GetItemCount(),l_pDoc->m_iMaxPhoneNumAmount);
		UpdateData(FALSE);
	}

	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
		m_cmdStop.EnableWindow(FALSE);
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,CString("exit CPhoneListView::OnCommandArrived"));
	return 0;
}

int CPhoneListView::GetPhoneNumItemByLocInMem(int a_iLocInMem)
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::GetPhoneNumItemByLocInMem"));

	CString l_strExistLocInMem;
	for(int l_iForIndex=0;l_iForIndex<m_lstPhoneNum.GetItemCount();++l_iForIndex)
	{
		l_strExistLocInMem=m_lstPhoneNum.GetItemText(l_iForIndex,LOC_IN_MEM_COL_INDEX);
		if(atoi(l_strExistLocInMem)==a_iLocInMem)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CPhoneListView::GetPhoneNumItemByPhoneNum(CString a_strPhoneNum)
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::GetPhoneNumItemByPhoneNum"));
	CString l_strPhoneNum;
	for(int l_iForIndex=0;l_iForIndex<m_lstPhoneNum.GetItemCount();++l_iForIndex)
	{
		l_strPhoneNum=m_lstPhoneNum.GetItemText(l_iForIndex,PHONE_NUM_COL_INDEX);
		if(a_strPhoneNum==l_strPhoneNum)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CPhoneListView::AddItemToPhoneList(int a_iLocInMem,CString a_strPhoneNum,CString a_strPhoneName,int a_iType,bool a_bInUnit,bool a_bNoSearch)
{
	CGsmAlertDoc *l_pDoc=GetDocument();

	INFO_LOG(g_DbLogger,CString("CPhoneListView::AddItemToPhoneList"));

	int l_iImage;
	if(a_bInUnit)
	{
		l_iImage=1;
	}
	else
	{
		l_iImage=0;
	}

	CString l_strLocInMem;
	l_strLocInMem.Format("%d",a_iLocInMem);
	
	
	int l_iIndex;
	if(a_bNoSearch)
	{
		l_iIndex=m_lstPhoneNum.InsertItem(LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
		m_lstPhoneNum.SetItemData(l_iIndex,a_iLocInMem);
	}
	else
	{
		if((l_iIndex=GetPhoneNumItemByLocInMem(a_iLocInMem))==-1)
		{
			l_iIndex=m_lstPhoneNum.InsertItem(LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
			m_lstPhoneNum.EnsureVisible(l_iIndex,false);
			m_lstPhoneNum.SetItemData(l_iIndex,a_iLocInMem);
		}
		else
		{
			m_lstPhoneNum.DeleteItem(l_iIndex);
			l_iIndex=m_lstPhoneNum.InsertItem(LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
			m_lstPhoneNum.EnsureVisible(l_iIndex,false);
			m_lstPhoneNum.SetItemData(l_iIndex,a_iLocInMem);
		}
	}
	m_lstPhoneNum.SetItemText(l_iIndex,PHONE_NUM_COL_INDEX,a_strPhoneNum);
	m_lstPhoneNum.SetItemText(l_iIndex,PHONE_NAME_COL_INDEX,a_strPhoneName);
	m_lstPhoneNum.SetItemText(l_iIndex,TYPE_COL_INDEX,l_pDoc->GetTypeNameFromNum(a_iType));
	
	return l_iIndex;
}

void CPhoneListView::SetItemImage(int a_iLocInMem,int a_iInUnit)
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::SetItemImage"));
	int l_iImage;
	l_iImage=a_iInUnit;

	int l_iIndex;
	if((l_iIndex=GetPhoneNumItemByLocInMem(a_iLocInMem))==-1)
	{
		return;
	}
	else
	{
		CString l_strPhoneNum=m_lstPhoneNum.GetItemText(l_iIndex,PHONE_NUM_COL_INDEX);
		CString l_strPhoneName=m_lstPhoneNum.GetItemText(l_iIndex,PHONE_NAME_COL_INDEX);
		CString l_strType=m_lstPhoneNum.GetItemText(l_iIndex,TYPE_COL_INDEX);
		CString l_strLocInMem;
		l_strLocInMem.Format("%d",a_iLocInMem);

		m_lstPhoneNum.DeleteItem(l_iIndex);
		l_iIndex=m_lstPhoneNum.InsertItem(l_iIndex,l_strLocInMem,l_iImage);
		m_lstPhoneNum.SetItemData(l_iIndex,a_iLocInMem);
		m_lstPhoneNum.SetItemText(l_iIndex,PHONE_NUM_COL_INDEX,l_strPhoneNum);
		m_lstPhoneNum.SetItemText(l_iIndex,PHONE_NAME_COL_INDEX,l_strPhoneName);
		m_lstPhoneNum.SetItemText(l_iIndex,TYPE_COL_INDEX,l_strType);
		m_lstPhoneNum.EnsureVisible(l_iIndex,false);
	}	
}

void CPhoneListView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	switch(m_enmPhoneListLastSentCommand)
	{
	case enmPhoneListLastSentCommandPhoneNumAsk:
	case enmPhoneListLastSentCommandQucikCompare:
		if(m_bStopDownload)
		{
			m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
			m_prgsPhoneDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			return;
		}
		if(m_bHashOK)
		{
			m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
			m_prgsPhoneDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			if((m_bQuickCompareOverDataInTheUnit)&&(m_enmPhoneListLastSentCommand==enmPhoneListLastSentCommandQucikCompare))
			{
				MessageBox(GetResString(IDS_MORE_DATA_IN_UNIT));
			}
		}
		else
		{
			if(m_iHashFailedTimes<MAX_HASH_FILED_TIMES)
			{
				//If the hash failed, resend the last request
				SendSetCommandToUnit(m_enmPhoneListLastSentCommand);
			}
			else
			{
				//If the hash failed several times, it seems to be problem with the software
				MessageBox(GetResString(IDS_HASH_ERROR_OCCURRED));
				m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
				m_prgsPhoneDownload.ShowWindow(SW_HIDE);
				m_cmdStop.EnableWindow(FALSE);
				EnableControls();
			}
		}
		break;	
	case enmPhoneListLastSentCommandPhoneNumSetAll:
		{
			CGsmAlertDoc *l_pDoc=GetDocument();
			CPhoneNumListElement *l_PhoneNumListElement;
			for(int l_i=1;l_i<=m_iLastAmoungOfData;++l_i)
			{
				l_PhoneNumListElement=l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_i);
				l_PhoneNumListElement->m_bInUnit=true;
				SetItemImage(l_PhoneNumListElement->m_iLocInMem,true);
			}
			m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);

			m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
			m_prgsPhoneDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			l_pDoc->SetModifiedFlag();
			return;
		}
		break;
	}
}

void CPhoneListView::SendSetCommandToUnit(enmPhoneListLastSentCommand &a_enmPhoneListLastSentCommand)
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc=GetDocument();

	l_pNewCommand=new CString();
	
	switch(a_enmPhoneListLastSentCommand)
	{
	case enmPhoneListLastSentCommandPhoneNumAsk:
	case enmPhoneListLastSentCommandQucikCompare:
		l_pNewCommand->Format("%s?",PHONE_NUM_REQ_MSG_COMMAND);
		break;
	case enmPhoneListLastSentCommandPhoneNumSetAll:
		{
			m_prgsPhoneDownload.SetPos(m_iCurrentPrgrsPos);
			l_pNewCommand->Format("%s=%d,%s,",SET_PHONE_LIST_MSG_COMMAND,m_iLastAmoungOfData,m_strLastRecordsData);
		}
		break;
	}
	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandNone;
	}
}

void CPhoneListView::DeletePhoneList()
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_PhoneNumList.DeleteAllList();
	m_lstPhoneNum.DeleteAllItems();
	l_pDoc->SetModifiedFlag();
}

void CPhoneListView::OnGetPhoneNum() 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnGetPhoneNum"));
	if(!(m_pMainFram->m_bConnectedToUnit))
	{
		MessageBox(GetResString(IDS_CONNECTION_ERROR),NULL,MB_ICONERROR);
		return;
	}
	int l_iAnswer=MessageBox(GetResString(IDS_GET_PHONE_NUM_WARNING),NULL,MB_YESNO | MB_ICONWARNING);
	if(l_iAnswer!=IDYES)
	{
		return;
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxPhoneNumAmount;
	m_prgsPhoneDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsPhoneDownload.SetPos(0);

	m_bStopDownload=false;
	DeletePhoneList();
	
	m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
	m_strUsedPhones.Format("%s: %d\\%d",GetResString(IDS_USED_PHONE_NUM), 0,l_pDoc->m_iMaxPhoneNumAmount);

	m_iHashFailedTimes=0;

	UpdateData(FALSE);
	m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandPhoneNumAsk;
	SendSetCommandToUnit(m_enmPhoneListLastSentCommand);
	
	m_prgsPhoneDownload.ShowWindow(SW_SHOW);
	m_cmdStop.EnableWindow();
	EnableControls(FALSE);
}

void CPhoneListView::OnClickLstPhoneNum(NMHDR* pNMHDR, LRESULT* pResult) 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnClickLstPhoneNum"));
	POSITION l_pos = m_lstPhoneNum.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		int l_iItem = m_lstPhoneNum.GetNextSelectedItem(l_pos);	  
		
		CString l_strIndex= m_lstPhoneNum.GetItemText(l_iItem,LOC_IN_MEM_COL_INDEX);;
		int l_iIndex=atoi(LPCTSTR(l_strIndex));
		CPhoneNumListElement *l_pPhoneNumListElement=l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_iIndex);
		SetComboBoxFromItemData(m_cmbType,l_pPhoneNumListElement->m_iType);
		UpdateData();
		m_strPhoneNum= l_pPhoneNumListElement->m_strPhoneNum;
		m_strPhoneName= l_pPhoneNumListElement->m_strPhoneName;
		m_strPhoneNum.TrimRight();
		m_strPhoneNum.TrimLeft();
		m_strPhoneName.TrimRight();
		m_strPhoneName.TrimLeft();
		UpdateData(FALSE);
	}
	if(pResult)
	{
		*pResult = 0;
	}
}

void CPhoneListView::OnCmdAdd()
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdAdd"));
	CGsmAlertDoc *l_pDoc=GetDocument();

	UpdateData();
	int l_iLocInMem=l_pDoc->m_PhoneNumList.GetCount()+1;

	if(l_iLocInMem>l_pDoc->m_iMaxPhoneNumAmount)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_TO_BIG),NULL,MB_ICONERROR);
		return;
	}
	if(!IsPhoneNumOK(m_strPhoneNum))
	{
		MessageBox(GetResString(IDS_PHONE_NUM_NOT_OK),NULL,MB_ICONERROR);
		return;
	}

	if(m_iType==-1)
	{
		MessageBox(GetResString(IDS_TYPE_NOT_SELECTED),NULL,MB_ICONERROR);
		return;
	}
	int l_iType=m_cmbType.GetItemData(m_iType);

	CString l_strPhoneNum,l_strMaxPhoneLen;
	CString l_strPhoneName,l_strMaxPhoneNameLen;

	l_strMaxPhoneLen.Format("%s%d%s","%",MAX_PHONE_NUM_LEN_DEF,"s");
	l_strPhoneNum.Format(l_strMaxPhoneLen,m_strPhoneNum);
	l_strPhoneNum=l_strPhoneNum.Right(MAX_PHONE_NUM_LEN_DEF);

	l_strMaxPhoneNameLen.Format("%s%d%s","%",MAX_PHONE_NAME_LEN_DEF,"s");
	l_strPhoneName.Format(l_strMaxPhoneNameLen,m_strPhoneName);
	l_strPhoneName=l_strPhoneName.Right(MAX_PHONE_NAME_LEN_DEF);

	int l_iIndex;
	CPhoneNumListElement *l_pPhoneNumListElement= l_pDoc->m_PhoneNumList.GetItemByPhoneNum(l_strPhoneNum);
	if(l_pPhoneNumListElement)
	{
		l_iIndex=GetPhoneNumItemByPhoneNum(l_strPhoneNum);
		UpdateData(FALSE);
		BlinkItem(l_pPhoneNumListElement->m_iLocInMem,l_pPhoneNumListElement->m_bInUnit);
		MessageBox(GetResString(IDS_PHONE_NUM_EXIST),NULL,MB_OK | MB_ICONERROR);
		return;
	}

	l_pDoc->m_PhoneNumList.AddItemSorted(l_iLocInMem,l_strPhoneNum,l_strPhoneName,l_iType,false,true);
	l_pDoc->SetModifiedFlag();
	AddItemToPhoneList(l_iLocInMem,l_strPhoneNum,l_strPhoneName,l_iType,false,true);
	m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
	l_iIndex=GetPhoneNumItemByLocInMem(l_iLocInMem);
	m_lstPhoneNum.EnsureVisible(l_iIndex,false);
	m_strUsedPhones.Format("%s: %d\\%d",GetResString(IDS_USED_PHONE_NUM), m_lstPhoneNum.GetItemCount(),l_pDoc->m_iMaxPhoneNumAmount);
	m_strPhoneName="";
	m_strPhoneNum="";
	m_iType=-1;
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}

void CPhoneListView::OnCmdDownloadAllPhonesToUnit() 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdDownloadAllPhonesToUnit"));

	CGsmAlertDoc *l_pDoc=GetDocument();
	CPhoneNumListElement *l_PhoneNumListElement;
	CString l_strTmp;
	m_bStopDownload=false;
	m_iCurrentPrgrsPos=0;

	if((m_iTotalPrgrsPos=m_lstPhoneNum.GetItemCount())==0)	//If the list is empty, ask for password
	{
		CInputDialog l_InputDlg(CInputDialog::PASSWORD_LETTERS,GetResString(IDS_EMPTY_PHONE_LIST_DOWNLOAD_TITLE),GetResString(IDS_EMPTY_PHONE_LIST_DOWNLOAD_MSG),"",this);
		CString l_strInput;
		char *l_strTmp =l_strInput.GetBuffer(100);
		if(l_InputDlg.InputBox(l_strTmp)!=IDOK)
		{
			l_strInput.ReleaseBuffer();
			return;
		}
		l_strInput.ReleaseBuffer();
		if(l_strInput!=l_pDoc->m_strUnitPass)
		{
			MessageBox(GetResString(IDS_INCORRECT_PASS));
			return;
		}
	}

	m_prgsPhoneDownload.SetRange(0,m_iTotalPrgrsPos-1);
	m_prgsPhoneDownload.SetPos(0);

	m_iLastAmoungOfData=l_pDoc->m_PhoneNumList.GetCount();
	m_strLastRecordsData="";
	for(int l_i=1;l_i<=l_pDoc->m_PhoneNumList.GetCount();++l_i)
	{
		l_PhoneNumListElement=l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_i);
		m_strLastRecordsData+=l_PhoneNumListElement->m_strPhoneNum;
		m_strLastRecordsData+=l_PhoneNumListElement->m_strPhoneName;
		l_strTmp.Format("%02d",l_PhoneNumListElement->m_iType);
		m_strLastRecordsData+=l_strTmp;
	}
	
	m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandPhoneNumSetAll;
	SendSetCommandToUnit(m_enmPhoneListLastSentCommand);

	m_cmdStop.EnableWindow();
	m_prgsPhoneDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CPhoneListView::OnCmdDeletePhoneNum() 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdDeletePhoneNum"));
	CGsmAlertDoc *l_pDoc=GetDocument();

	POSITION l_pos = m_lstPhoneNum.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CString l_strIndex;
		int l_iIndex;
		int l_iItem = m_lstPhoneNum.GetNextSelectedItem(l_pos);
		l_strIndex= m_lstPhoneNum.GetItemText(l_iItem,LOC_IN_MEM_COL_INDEX);
		l_iIndex = atoi(LPCTSTR(l_strIndex));
		l_pDoc->m_PhoneNumList.DeleteItem(l_iIndex);
		m_lstPhoneNum.DeleteItem(l_iItem);
		for(;l_iItem<m_lstPhoneNum.GetItemCount();++l_iItem)
		{
			l_strIndex= m_lstPhoneNum.GetItemText(l_iItem,LOC_IN_MEM_COL_INDEX);
			l_iIndex = atoi(LPCTSTR(l_strIndex));
			--l_iIndex ;
			l_strIndex.Format("%d",l_iIndex);
			m_lstPhoneNum.SetItemText(l_iItem,LOC_IN_MEM_COL_INDEX,l_strIndex);
			m_lstPhoneNum.SetItemData(l_iItem,l_iIndex);
		}
	}
	m_strUsedPhones.Format("%s: %d\\%d",GetResString(IDS_USED_PHONE_NUM), m_lstPhoneNum.GetItemCount(),l_pDoc->m_iMaxPhoneNumAmount);
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}

void CPhoneListView::OnCmdStop() 
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdStop"));
	m_bStopDownload=true;
}

void CPhoneListView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::EnableControls"));
	m_txtPhoneNum.EnableWindow(a_bEnabled);
	m_txtPhoneName.EnableWindow(a_bEnabled);
	m_cmbType.EnableWindow(a_bEnabled);
	m_cmdAdd.EnableWindow(a_bEnabled);
	m_cmdReplace.EnableWindow(a_bEnabled);
	m_cmdDeletePhoneNum.EnableWindow(a_bEnabled);
	m_cmdGetPhoneNum.EnableWindow(a_bEnabled);
	m_cmdDownloadAllPhonesToUnit.EnableWindow(a_bEnabled);
	m_cmdQuickCompare.EnableWindow(a_bEnabled);
	m_cmdExcelExport.EnableWindow(a_bEnabled);
	m_cmdExcelImport.EnableWindow(a_bEnabled);
}

void CPhoneListView::Localize()
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::Localize"));

	SetDlgItemText( IDC_GET_PHONE_NUM,GetResString(IDS_IDC_GET_PHONE_NUM));
	SetDlgItemText( IDC_CMD_ADD,GetResString(IDS_IDC_CMD_ADD));
	SetDlgItemText( IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT,GetResString(IDS_IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT));
	SetDlgItemText( IDC_CMD_DELETE_PHONE_NUM,GetResString(IDS_IDC_CMD_DELETE_PHONE_NUM));
	SetDlgItemText( IDC_CMD_STOP,GetResString(IDS_IDC_CMD_STOP));
	SetDlgItemText( IDC_CMD_EXCEL_EXPORT,GetResString(IDS_IDC_CMD_EXCEL_EXPORT));
	SetDlgItemText( IDC_CMD_EXCEL_IMPORT,GetResString(IDS_IDC_CMD_EXCEL_IMPORT));
	SetDlgItemText( IDC_CMD_QUICK_COMPARE,GetResString(IDS_IDC_CMD_QUICK_COMPARE));
	SetDlgItemText( IDC_LBL_PHONE_NUM,GetResString(IDS_IDC_LBL_PHONE_NUM));
	SetDlgItemText( IDC_FRAM_PHONE_NUM,GetResString(IDS_IDC_FRAM_PHONE_NUM));
	SetDlgItemText( IDC_LBL_PHONE_NAME,GetResString(IDS_NAME));
	SetDlgItemText( IDC_CMD_SEARCH_PHONE_NUM,GetResString(IDS_SEARCH));
	SetDlgItemText( IDC_CMD_SEARCH_PHONE_NAME,GetResString(IDS_SEARCH));
	SetDlgItemText( IDC_LBL_PHONE_NAME,GetResString(IDS_NAME));
	SetDlgItemText( IDC_LBL_TYPE,GetResString(IDS_TYPE));
}

void CPhoneListView::OnCmdExcelExport() 
{/*
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdExcelExport"));
	CDatabase database;
	CString l_strDriver = "MICROSOFT EXCEL DRIVER (*.xls)"; // exactly the same name as in the ODBC-Manager
	CString l_strExcelFile;					// Filename and path for the file to be created
	CString l_strSql;

	CGsmAlertDoc *l_pDoc=GetDocument();
	CPhoneNumListElement *l_pListElement;

	TRY
	{
		// Get the file name to export the excel file
		CFileDialog dlgFile(FALSE);
		CString l_strLoadFilter;
		CString l_strFilter;
		CString l_strTitle=GetResString(IDS_EXCEL_EXPORT);

		VERIFY(l_strLoadFilter.LoadString(IDS_EXCEL_FILE));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.xls");
		l_strFilter+= (TCHAR)'\0';   // last string

		VERIFY(l_strLoadFilter.LoadString(AFX_IDS_ALLFILTER));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.*");
		l_strFilter+= (TCHAR)'\0';   // last string
		dlgFile.m_ofn.nMaxCustFilter++;

		dlgFile.m_ofn.lpstrFilter = l_strFilter;
		dlgFile.m_ofn.lpstrTitle = l_strTitle;
		dlgFile.m_ofn.lpstrFile = l_strExcelFile.GetBuffer(_MAX_PATH);

		int nResult = dlgFile.DoModal();
		l_strExcelFile.ReleaseBuffer();
		
		if(nResult!=IDOK)
		{
			return;
		}

		CFileFind l_FileFinder;

		if(l_FileFinder.FindFile(l_strExcelFile))
		{
			DeleteFile(l_strExcelFile);
		}

		// Build the creation string for access without DSN

		l_strSql.Format("DRIVER={%s};DSN='';FIRSTROWHASNAMES=1;READONLY=FALSE;CREATE_DB=\"%s\";DBQ=%s", l_strDriver,l_strExcelFile,l_strExcelFile);

		// Create the database (i.e. Excel sheet)
		if( database.OpenEx(l_strSql,CDatabase::noOdbcDialog))
		{
			m_prgsPhoneDownload.SetRange(0,l_pDoc->m_iMaxPhoneNumAmount);
			m_prgsPhoneDownload.SetPos(0);
			m_prgsPhoneDownload.ShowWindow(SW_SHOW);
			// Create table structure
			l_strSql = "CREATE TABLE \"Phone Numbers\" (\"Index\" NUMBER,\"Phone number\" TEXT,\"Name\" TEXT,\"Type Number\" NUMBER,\"Type Name\" TEXT,\"In Unit\" NUMBER)";
			database.ExecuteSQL(l_strSql);

			CString l_strTmpPhone,l_strTmpPhoneName;
			// Insert data
			for(int l_i=1;l_i<=l_pDoc->m_PhoneNumList.GetCount();++l_i)
			{
				m_prgsPhoneDownload.SetPos(l_i);
				l_pListElement=l_pDoc->m_PhoneNumList.GetItemByLocInMem(l_i);
				if(l_pListElement)
				{
					l_strTmpPhone=l_pListElement->m_strPhoneNum;
					l_strTmpPhone.TrimLeft();
					l_strTmpPhone.TrimRight();
					l_strTmpPhoneName=l_pListElement->m_strPhoneName;
					l_strTmpPhoneName.TrimLeft();
					l_strTmpPhoneName.TrimRight();
					l_strSql.Format("%s%d,'%s','%s','%d','%s',%d)","INSERT INTO \"Phone Numbers\" (\"Index\" ,\"Phone number\",\"Name\",\"Type Number\",\"Type Name\",\"In Unit\") VALUES (",l_pListElement->m_iLocInMem,l_strTmpPhone,l_strTmpPhoneName,l_pListElement->m_iType,l_pDoc->GetTypeNameFromNum(l_pListElement->m_iType),l_pListElement->m_bInUnit);
				}
				else
				{
					l_strSql.Format("%s%d,'%s','%s','%d','%s',%d)","INSERT INTO \"Phone Numbers\" (\"Index\" ,\"Phone number\",\"Type Number\",\"Type Name\",\"In Unit\") VALUES (",l_i,"",0,"","",true);
				}
				database.ExecuteSQL(l_strSql);
			}
		}
		
		// Close database
		database.Close();
		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
	}
	CATCH_ALL(e)
	{
		MessageBox(GetResString(IDS_ERR_WHILE_EXPORT_EXCEL_FILE),NULL,MB_OK | MB_ICONERROR);
		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
	}
	END_CATCH_ALL;
	*/
}

void CPhoneListView::OnCmdQuickCompare()
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdQuickCompare"));
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxPhoneNumAmount;
	m_prgsPhoneDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsPhoneDownload.SetPos(0);

	UpdateData(FALSE);
	m_bHashOK=true;
	m_iHashFailedTimes=0;
	m_bQuickCompareOverDataInTheUnit = false;
	m_enmPhoneListLastSentCommand=enmPhoneListLastSentCommandQucikCompare;
	SendSetCommandToUnit(m_enmPhoneListLastSentCommand);
	
	m_prgsPhoneDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CPhoneListView::OnCmdExcelImport() 
{/*
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdExcelImport"));
	CDatabase database;
	CString l_strDriver = "MICROSOFT EXCEL DRIVER (*.xls)"; // exactly the same name as in the ODBC-Manager
	CString l_strExcelFile;					// Filename and path for the file to be created
	CString l_strSql;

	CGsmAlertDoc *l_pDoc=GetDocument();

	TRY
	{
		// Get the file name to export the excel file
		CFileDialog dlgFile(TRUE);
		CString l_strLoadFilter;
		CString l_strFilter;
		CString l_strTitle=GetResString(IDS_EXCEL_IMPORT);

		VERIFY(l_strLoadFilter.LoadString(IDS_EXCEL_FILE));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.xls");
		l_strFilter+= (TCHAR)'\0';   // last string

		VERIFY(l_strLoadFilter.LoadString(AFX_IDS_ALLFILTER));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.*");
		l_strFilter+= (TCHAR)'\0';   // last string
		dlgFile.m_ofn.nMaxCustFilter++;

		dlgFile.m_ofn.lpstrFilter = l_strFilter;
		dlgFile.m_ofn.lpstrTitle = l_strTitle;
		dlgFile.m_ofn.lpstrFile = l_strExcelFile.GetBuffer(_MAX_PATH);

		int nResult = dlgFile.DoModal();
		l_strExcelFile.ReleaseBuffer();
		
		if(nResult!=IDOK)
		{
			return;
		}

		CFileFind l_FileFinder;

		if(!l_FileFinder.FindFile(l_strExcelFile))
		{
			MessageBox(GetResString(IDS_FILE_NOT_EXIST),NULL,MB_OK | MB_ICONERROR);
			return;
		}

		// Retrieve the name of the Excel driver. This is 
		// necessary because Microsoft tends to use language
		// specific names like "Microsoft Excel Driver (*.xls)" versus
		// "Microsoft Excel Treiber (*.xls)"
		l_strDriver = GetExcelDriver();
		if( l_strDriver.IsEmpty() )
		{
			//We didn�t find that driver!
			MessageBox(GetResString(IDS_ODBC_DRIVER_ERROR),NULL,MB_OK | MB_ICONERROR);
			return;
		}

		l_strSql.Format("ODBC;DRIVER={%s};DSN='';DBQ=%s",l_strDriver,l_strExcelFile);

		// Create the database (i.e. Excel sheet)
		// Open the database using the former created pseudo DSN
		database.Open(NULL,false,false,l_strSql);
		
		l_strSql="SELECT * FROM \"Phone Numbers\"";
		CRecordset l_rs(&database);
		
		//Open the recordset only for counting the records
		l_rs.Open(CRecordset::forwardOnly,l_strSql,CRecordset::readOnly);
		int j=l_rs.GetRecordCount();

		int l_i=0;
		while( !l_rs.IsEOF())
		{
			++l_i;
			l_rs.MoveNext();
		}

		m_prgsPhoneDownload.SetRange(0,l_i);
		m_prgsPhoneDownload.SetPos(0);
		m_prgsPhoneDownload.ShowWindow(SW_SHOW);

		l_rs.Close();

		//Open the recordset again in order to read them
		l_rs.Open(CRecordset::forwardOnly,l_strSql,CRecordset::readOnly);

		CString l_strLocInMem;
		CString l_strPhoneNum,l_strFormatedPhoneNum;
		CString l_strPhoneName,l_strFormatedPhoneName;
		CString l_strInUnit,l_strType;


		int l_iLocInMem;
		int l_iInUnit;
		int l_iType;
		
		CString l_strMaxPhoneLen;
		l_strMaxPhoneLen.Format("%s%d%s","%",MAX_PHONE_NUM_LEN_DEF,"s");
		CString l_strMaxNameLen;
		l_strMaxNameLen.Format("%s%d%s","%",MAX_PHONE_NAME_LEN_DEF,"s");
		

		l_i=0;
		while( !l_rs.IsEOF())
		{
			++l_i;

			l_rs.GetFieldValue("Index",l_strLocInMem);
			l_rs.GetFieldValue("Phone number",l_strPhoneNum);
			l_rs.GetFieldValue("Name",l_strPhoneName);
			l_rs.GetFieldValue("In Unit",l_strInUnit);
			l_rs.GetFieldValue("Type Number",l_strType);
			
			//If we import a phone number that is not inserted as text but as numeric, 
			//we will get it like ########.00   , so we need to delete the dot and the numbers that come after it
			if(IsNumeric(l_strPhoneNum)&&(FindInString(l_strPhoneNum,".")!=-1))
			{
				int l_iDotPlace=FindInString(l_strPhoneNum,".");
				l_strPhoneNum=l_strPhoneNum.Left(l_iDotPlace);
			}

			//Data validity check
			if((!IsNumeric(l_strLocInMem)) ||((!IsPhoneNumOK(l_strPhoneNum))&&(l_strPhoneNum!="")) || (!IsNumeric(l_strInUnit)) || (!IsNumeric(l_strType)))
			{
				CString l_strErr;
				l_strErr.Format("%s %d",GetResString(IDS_DATA_EXCEL_IMPORT_ERR),l_i);
				MessageBox(l_strErr,NULL,MB_OK | MB_ICONERROR);
				// Close database
				l_rs.Close();
				database.Close();
				break;
			}
			
			l_strFormatedPhoneNum.Format(l_strMaxPhoneLen,l_strPhoneNum);
			l_strFormatedPhoneNum=l_strFormatedPhoneNum.Right(MAX_PHONE_NUM_LEN_DEF);
			l_strFormatedPhoneName.Format(l_strMaxNameLen,l_strPhoneName);
			l_strFormatedPhoneName=l_strFormatedPhoneName.Right(MAX_PHONE_NAME_LEN_DEF);
			//Turn the strings to the correct form of types
			l_iLocInMem=atoi(LPCTSTR(l_strLocInMem));
			l_iInUnit=atoi(LPCTSTR(l_strInUnit));
			l_iType=atoi(LPCTSTR(l_strType));

			//Context validity check
			if((l_iLocInMem<1)||(l_iLocInMem>l_pDoc->m_iMaxPhoneNumAmount))
			{
				CString l_strErr;
				l_strErr.Format("%s %d",GetResString(IDS_DATA_EXCEL_IMPORT_ERR),l_i);
				MessageBox(l_strErr,NULL,MB_OK | MB_ICONERROR);
				// Close database
				l_rs.Close();
				database.Close();
				break;
			}

			if((l_iInUnit!=0)&&(l_iInUnit!=1))
			{
				CString l_strErr;
				l_strErr.Format("%s %d",GetResString(IDS_DATA_EXCEL_IMPORT_ERR),l_i);
				MessageBox(l_strErr,NULL,MB_OK | MB_ICONERROR);
				// Close database
				l_rs.Close();
				database.Close();
				break;
			}

			l_pDoc->m_PhoneNumList.AddItemSorted(l_iLocInMem,l_strFormatedPhoneNum,l_strFormatedPhoneName,l_iType,bool(l_iInUnit),false);
			AddItemToPhoneList(l_iLocInMem,l_strFormatedPhoneNum,l_strFormatedPhoneName,l_iType,bool(l_iInUnit),false);
			l_pDoc->SetModifiedFlag();

			m_prgsPhoneDownload.SetPos(l_i);
			l_rs.MoveNext();
		}
		
		//Sort the list
		m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
		
		// Close database
		l_rs.Close();
		database.Close();
		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
	}
	CATCH_ALL(e)
	{
		TCHAR   l_szCause[255];
		e->GetErrorMessage(&(l_szCause[1]),254);
		
		l_szCause[0]='\n';
		MessageBox(GetResString(IDS_ERR_WHILE_IMPORT_EXCEL_FILE) + l_szCause,NULL,MB_OK | MB_ICONERROR);

		m_prgsPhoneDownload.ShowWindow(SW_HIDE);
	}
	END_CATCH_ALL;
	*/
}

void CPhoneListView::OnCmdSearchPhoneNum() 
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	UpdateData();
	if(!IsPhoneNumOK(m_strPhoneNum))
	{
		MessageBox(GetResString(IDS_PHONE_NUM_NOT_OK),NULL,MB_ICONERROR);
		return;
	}

	CString l_strPhoneNum,l_strMaxPhoneLen;

	l_strMaxPhoneLen.Format("%s%d%s","%",MAX_PHONE_NUM_LEN_DEF,"s");
	l_strPhoneNum.Format(l_strMaxPhoneLen,m_strPhoneNum);
	l_strPhoneNum=l_strPhoneNum.Right(MAX_PHONE_NUM_LEN_DEF);
	
	CPhoneNumListElement *l_pPhoneNumListElement= l_pDoc->m_PhoneNumList.GetItemByPhoneNum(l_strPhoneNum);
	if(!l_pPhoneNumListElement)
	{
		MessageBox(GetResString(IDS_PHONE_NUM_NOT_EXIST),NULL,MB_OK | MB_ICONERROR);
		return;
	}
	BlinkItem(l_pPhoneNumListElement->m_iLocInMem,l_pPhoneNumListElement->m_bInUnit);
}

void CPhoneListView::OnCmdSearchPhoneName() 
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	UpdateData();
	
	m_strPhoneName.TrimLeft();
	m_strPhoneName.TrimRight();
	if (m_strPhoneName=="")
	{
		MessageBox(GetResString(IDS_PHONE_NAME_NOT_EXIST),NULL,MB_OK | MB_ICONERROR);
		return;
	}
	CPhoneNumListElement *l_pPhoneNumListElement= l_pDoc->m_PhoneNumList.GetItemByPhoneName(m_strPhoneName);
	if(!l_pPhoneNumListElement)
	{
		MessageBox(GetResString(IDS_PHONE_NAME_NOT_EXIST),NULL,MB_OK | MB_ICONERROR);
		return;
	}
	BlinkItem(l_pPhoneNumListElement->m_iLocInMem,l_pPhoneNumListElement->m_bInUnit);
}

void CPhoneListView::BlinkItem(int a_iLocInMem,bool a_bInUnit)
{
	for(int l_iCount=0;l_iCount<4;++l_iCount)
	{
		SetItemImage(a_iLocInMem,2);
		RedrawWindow();
		Sleep(200);
		SetItemImage(a_iLocInMem,a_bInUnit);
		RedrawWindow();
		Sleep(200);
	}
}

void CPhoneListView::OnCmdReplace()
{
	INFO_LOG(g_DbLogger,CString("CPhoneListView::OnCmdDeletePhoneNum"));
	CGsmAlertDoc *l_pDoc=GetDocument();

	POSITION l_pos = m_lstPhoneNum.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		UpdateData();

		if(!IsPhoneNumOK(m_strPhoneNum))
		{
			MessageBox(GetResString(IDS_PHONE_NUM_NOT_OK),NULL,MB_ICONERROR);
			return;
		}

		if(m_iType==-1)
		{
			MessageBox(GetResString(IDS_TYPE_NOT_SELECTED),NULL,MB_ICONERROR);
			return;
		}
		CString l_strPhoneNum,l_strMaxPhoneLen;
		CString l_strPhoneName,l_strMaxPhoneNameLen;

		l_strMaxPhoneLen.Format("%s%d%s","%",MAX_PHONE_NUM_LEN_DEF,"s");
		l_strPhoneNum.Format(l_strMaxPhoneLen,m_strPhoneNum);
		l_strPhoneNum=l_strPhoneNum.Right(MAX_PHONE_NUM_LEN_DEF);

		l_strMaxPhoneNameLen.Format("%s%d%s","%",MAX_PHONE_NAME_LEN_DEF,"s");
		l_strPhoneName.Format(l_strMaxPhoneNameLen,m_strPhoneName);
		l_strPhoneName=l_strPhoneName.Right(MAX_PHONE_NAME_LEN_DEF);

		int l_iType=m_cmbType.GetItemData(m_iType);

		CString l_strIndex;
		int l_iIndex;
		int l_iItem = m_lstPhoneNum.GetNextSelectedItem(l_pos);
		l_strIndex = m_lstPhoneNum.GetItemText(l_iItem,LOC_IN_MEM_COL_INDEX);
		l_iIndex = atoi(LPCTSTR(l_strIndex));
		
		l_pDoc->m_PhoneNumList.AddItemSorted(l_iIndex,l_strPhoneNum,l_strPhoneName,l_iType,false,false);
		l_pDoc->SetModifiedFlag();
		AddItemToPhoneList(l_iIndex,l_strPhoneNum,l_strPhoneName,l_iType,false,false);
		m_lstPhoneNum.SortItems(SimpleSortCompareFunc,0);
		m_lstPhoneNum.EnsureVisible(l_iIndex,false);
		m_strPhoneName="";
		m_strPhoneNum="";
		m_iType=-1;
		UpdateData(FALSE);
		l_pDoc->SetModifiedFlag();
	}
}

void CPhoneListView::OnChangeTxtPhoneName() 
{
	UpdateData();
	int l_iIndex;
	while((l_iIndex=m_strPhoneName.Find(","))!=-1)
	{
		m_strPhoneName=m_strPhoneName.Left(l_iIndex) + m_strPhoneName.Mid(l_iIndex+1);
	}
	UpdateData(FALSE);
}
