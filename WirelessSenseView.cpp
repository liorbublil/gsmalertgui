// WirelessSenseView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MainFrm.h"
#include "WirelessSenseView.h"
#include "Language.h"

#include "json.hpp"

#include <http.h>

using json = nlohmann::json;

#define WIRELESS_ID_COL_INDEX 0
#define WIRELESS_NAME_COL_INDEX 1
#define WIRELESS_TAG_TYPE_COL_INDEX 2
#define WIRELESS_SENSOR_TYPE_COL_INDEX 3
#define WIRELESS_SENSOR_PARAM_COL_INDEX 4


#define WIRELESS_QUERY_ID_COL_INDEX 0
#define WIRELESS_QUERY_NAME_COL_INDEX 1
#define WIRELESS_QUERY_SENSOR_TYPE_COL_INDEX 2
#define WIRELESS_QUERY_SENSOR_RES_COL_INDEX 3
#define WIRELESS_QUERY_SENSOR_TS_COL_INDEX 4

#define DEFAULT_REED_SAMPLE_INTERVARL 1
#define DEFAULT_TEMP_HYST 10
#define DEFAULT_HUMID_HYST 50

#define MIN_TAG_STICK_KEEP_ALIVE_INTERVAL 30
#define MAX_TAG_STICK_KEEP_ALIVE_INTERVAL 32400
#define MIN_TAG_STICK_LIGHT_INTERVAL 2
#define MAX_TAG_STICK_LIGHT_INTERVAL 60
#define MIN_TAG_STICK_HUM_TEMP_INTERVAL 5
#define MAX_TAG_STICK_HUM_TEMP_INTERVAL 3600
#define MAX_LIGHT_DETECT_THRESHOLD 1010
#define MIN_LIGHT_DETECT_THRESHOLD 10

#define MIN_TAG_STICK_TEMP_TRSHLD_LOW -40.0
#define MIN_TAG_STICK_TEMP_TRSHLD_HIGH 120.0
#define MIN_TAG_STICK_HUM_TRSHLD_LOW 0.0
#define MIN_TAG_STICK_HUM_TRSHLD_HIGH 100.0
#define MIN_TAG_STICK_HUM_TEMP_GAP_LOW 0.5
#define MIN_TAG_STICK_HUM_TEMP_GAP_HIGH 100

// CWirelessSenseView
IMPLEMENT_DYNCREATE(CWirelessSenseView, CFormView)

CWirelessSenseView::CWirelessSenseView()
	: CFormView(IDD_WIRELESSSENSE_VIEW_FORM)
	, m_iTagType(-1)
	, m_iTagSetType(0),
	m_bFirstRun(true)
	, m_strLblSet1(_T(""))
	, m_strLblSet2(_T(""))
	, m_strLblSet6(_T(""))
	, m_strLblSet5(_T(""))
	, m_strLblSet4(_T(""))
	, m_strLblSet3(_T(""))
	, m_iSet1(0)
	, m_strTagId(_T(""))
	, m_strSet6(_T(""))
	, m_strSet5(_T(""))
	, m_strSet4(_T(""))
	, m_strSet3(_T(""))
	, m_strSet2(_T(""))
	, m_strSet1(_T(""))
	, m_bRfActive(FALSE)
	, m_bSet3(FALSE)
{
	m_enmWlSensorsLastSentCommand= enmWlSensorsLastSentCommandNone;
}

CWirelessSenseView::~CWirelessSenseView()
{
}

void CWirelessSenseView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CMB_TAG_TYPE, m_cmbTagType);
	DDX_CBIndex(pDX, IDC_CMB_TAG_TYPE, m_iTagType);
	DDX_Control(pDX, IDC_CMB_TAG_SET_TYPE, m_cmbTagSetType);
	DDX_CBIndex(pDX, IDC_CMB_TAG_SET_TYPE, m_iTagSetType);
	DDX_Text(pDX, IDC_LBL_SET1, m_strLblSet1);
	DDX_Text(pDX, IDC_LBL_SET2, m_strLblSet2);
	DDX_Text(pDX, IDC_LBL_SET6, m_strLblSet6);
	DDX_Text(pDX, IDC_LBL_SET5, m_strLblSet5);
	DDX_Text(pDX, IDC_LBL_SET4, m_strLblSet4);
	DDX_Text(pDX, IDC_LBL_SET3, m_strLblSet3);
	DDX_Control(pDX, IDC_LBL_SET6, m_LblSet6);
	DDX_Control(pDX, IDC_LBL_SET5, m_LblSet5);
	DDX_Control(pDX, IDC_LBL_SET4, m_LblSet4);
	DDX_Control(pDX, IDC_LBL_SET3, m_LblSet3);
	DDX_Control(pDX, IDC_LBL_SET2, m_LblSet2);
	DDX_Control(pDX, IDC_LBL_SET1, m_LblSet1);
	DDX_Control(pDX, IDC_TXT_TAG_SET_6, m_txtSet6);
	DDX_Control(pDX, IDC_TXT_TAG_SET_5, m_txtSet5);
	DDX_Control(pDX, IDC_TXT_TAG_SET_4, m_txtSet4);
	DDX_Control(pDX, IDC_TXT_TAG_SET_3, m_txtSet3);
	DDX_Control(pDX, IDC_TXT_TAG_SET_2, m_txtSet2);
	DDX_Control(pDX, IDC_TXT_TAG_SET_1, m_txtSet1);
	DDX_Control(pDX, IDC_CMB_SET1, m_cmbSet1);
	DDX_CBIndex(pDX, IDC_CMB_SET1, m_iSet1);
	//  DDX_Control(pDX, IDC_TRV_TAGS_QUERY, m_trvTagsQuery);
	DDX_Control(pDX, IDC_CMD_QUERY_WIRELESS_SENSORS, m_cmdQueryWirelessSensors);
	DDX_Control(pDX, IDC_TXT_TAG_ID, m_txtTagId);
	DDX_Text(pDX, IDC_TXT_TAG_ID, m_strTagId);
	DDX_Text(pDX, IDC_TXT_TAG_SET_6, m_strSet6);
	DDX_Text(pDX, IDC_TXT_TAG_SET_5, m_strSet5);
	DDX_Text(pDX, IDC_TXT_TAG_SET_4, m_strSet4);
	DDX_Text(pDX, IDC_TXT_TAG_SET_3, m_strSet3);
	DDX_Text(pDX, IDC_TXT_TAG_SET_2, m_strSet2);
	DDX_Text(pDX, IDC_TXT_TAG_SET_1, m_strSet1);
	DDX_Control(pDX, IDC_CHK_RF_ACTIVE, m_chkRfActive);
	DDX_Check(pDX, IDC_CHK_RF_ACTIVE, m_bRfActive);
	DDX_Control(pDX, IDC_CMD_ADD_WIRELESS_SENSOR, m_cmdAddWirelessSensor);
	DDX_Control(pDX, IDC_CMD_SET_WIRELESS_SENSOR, m_cmdSetWirelessSensor);
	DDX_Control(pDX, IDC_CMD_REMOVE_WIRELESS_SENSOR, m_cmdRemoveWirelessSensor);
	DDX_Control(pDX, IDC_CMD_GET_WIRLELESS_SENSOR, m_cmdGetWirelessSensor);
	DDX_Control(pDX, IDC_CMD_CLONE_WIRELESS_SENSOR, m_cmdCloneWirelessSensor);
	DDX_Control(pDX, IDC_CHK_SET3, m_chkSet3);
	DDX_Check(pDX, IDC_CHK_SET3, m_bSet3);
	DDX_Control(pDX, IDC_CMD_CLEAR_WIRELESS_SENSORS, m_cmdClearWirelessSensors);
}

BEGIN_MESSAGE_MAP(CWirelessSenseView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED, OnCommandArrived)
	ON_CBN_SELCHANGE(IDC_CMB_TAG_TYPE, &CWirelessSenseView::OnCbnSelchangeCmbTagType)
	ON_CBN_SELCHANGE(IDC_CMB_TAG_SET_TYPE, &CWirelessSenseView::OnCbnSelchangeCmbTagSetType)
	ON_BN_CLICKED(IDC_CMD_QUERY_WIRELESS_SENSORS, &CWirelessSenseView::OnBnClickedCmdQueryWirelessSensors)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TRV_TAGS_QUERY, OnTagsQueryTreeSelchanged)
	ON_NOTIFY(TVN_SELCHANGED, IDC_TRV_TAGS, OnTagsTreeSelchanged)
	ON_BN_CLICKED(IDC_CMD_ADD_WIRELESS_SENSOR, &CWirelessSenseView::OnBnClickedCmdAddWirelessSensor)
	ON_BN_CLICKED(IDC_CMD_SET_WIRELESS_SENSOR, &CWirelessSenseView::OnBnClickedCmdSetWirelessSensor)
	ON_BN_CLICKED(IDC_CMD_GET_WIRLELESS_SENSOR, &CWirelessSenseView::OnBnClickedCmdGetWirlelessSensor)
	ON_BN_CLICKED(IDC_CMD_REMOVE_WIRELESS_SENSOR, &CWirelessSenseView::OnBnClickedCmdRemoveWirelessSensor)
	ON_BN_CLICKED(IDC_CHK_RF_ACTIVE, &CWirelessSenseView::OnBnClickedChkRfActive)
	ON_WM_ACTIVATE()
	ON_WM_SETFOCUS()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_CMD_CLONE_WIRELESS_SENSOR, &CWirelessSenseView::OnBnClickedCmdCloneWirelessSensor)
	ON_BN_CLICKED(IDC_CMD_COPY_WIRELESS_SENSOR_CLIPBOARD, &CWirelessSenseView::OnBnClickedCmdCopyWirelessSensorClipboard)
	ON_BN_CLICKED(IDC_CMD_CLEAR_WIRELESS_SENSORS, &CWirelessSenseView::OnBnClickedCmdClearWirelessSensors)
END_MESSAGE_MAP()


// CWirelessSenseView diagnostics

#ifdef _DEBUG
void CWirelessSenseView::AssertValid() const
{
	CFormView::AssertValid();
}

CGsmAlertDoc* CWirelessSenseView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}

#ifndef _WIN32_WCE
void CWirelessSenseView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CWirelessSenseView message handlers

LRESULT CWirelessSenseView::OnCommandArrived(WPARAM wParam, LPARAM lParam)
{
	CString *l_pArrivedRespond;
	CGsmAlertDoc *l_pDoc = GetDocument();
	if (wParam == COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmWlSensorsLastSentCommand= enmWlSensorsLastSentCommandNone;
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED), NULL, MB_ICONERROR | MB_OK);
		return 0;
	}
	else if (wParam == COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		if (l_pDoc->m_bGetDataOnCon)
		{
			//If we need to get teh data from the unit once we got connected, get it
			OnBnClickedCmdGetWirlelessSensor();
		}
		return 0;
	}
	else if (wParam == COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if (m_enmWlSensorsLastSentCommand!= enmWlSensorsLastSentCommandNone)
		{
			m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandNone;
			CString l_strError = GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError, NULL, MB_ICONERROR | MB_OK);
			EnableControls();
		}
		return 0;
	}
	else if (wParam != COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	l_pArrivedRespond = (CString*)lParam;
	int l_iRespondStartAt;
	int l_iLastRespondStartAt = 0;
	bool l_bItemAdd = false;

	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(WL_SENSORS_QUERY_SENSORS_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			int l_iDataStartAt = FindInString(*l_pArrivedRespond, WL_SENSORS_QUERY_SENSORS_MSG_RESPOND);
			l_iDataStartAt += (CString(WL_SENSORS_QUERY_SENSORS_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			CString l_strRecordsData;

			CString l_strDataToPars, l_strTagId, l_strTagName, l_strInUnit, l_strSensorType, l_strSensorRes, l_strTimeStamp, l_strTmp;
			int l_iIndex, l_iIndex2, l_iInUnit, l_iTimeStamp, l_iImg;
			float l_fSensorRes;

			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);
			
			CTreeCtrl& treeQuery = m_tcTagsQuery.GetTreeCtrl();
			HTREEITEM hCat;

			treeQuery.DeleteAllItems();
			// last argument is the delimiter
			for (l_iIndex = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strRecordsData, l_strDataToPars, l_iIndex, _T(';')); ++l_iIndex)
			{
				//The first two arguments for each Tag response are the TagId and InUnit flag (if the unit just saw the tag or the tag is registered in the unit as well)
				AfxExtractSubString(l_strTagId, l_strRecordsData, 0, _T(','));
				AfxExtractSubString(l_strInUnit, l_strRecordsData, 1, _T(','));
				AfxExtractSubString(l_strTagName, l_strRecordsData, 2, _T(','));

				l_iInUnit = atoi((LPCSTR)l_strInUnit);
				
				if (l_iInUnit)
				{
					l_iImg = 1;
				}
				else
				{
					l_iImg = 3;
				}
				l_strTmp.Format("%s\t%s\t\t", l_strTagId, l_strTagName);
				hCat = treeQuery.InsertItem(l_strTmp, l_iImg, l_iImg, TVI_ROOT);
			

				for (l_iIndex2 = 3; (l_strRecordsData != CString("")) && AfxExtractSubString(l_strSensorType, l_strRecordsData, l_iIndex2, _T(',')); l_iIndex2 += 3)
				{
					AfxExtractSubString(l_strSensorRes, l_strRecordsData, l_iIndex2 + 1, _T(','));
					AfxExtractSubString(l_strTimeStamp, l_strRecordsData, l_iIndex2 + 2, _T(','));

					l_strTmp.Format("\t\t%s\t%s\t%s", SensorTypeToText(l_strSensorType), l_strSensorRes, l_strTimeStamp);
					/*
					l_fSensorRes = atof((LPCSTR)l_strSensorRes);
					l_iTimeStamp = atoi((LPCSTR)l_strTimeStamp);
					*/
					treeQuery.InsertItem(l_strTmp, 5, 5, hCat);
					treeQuery.Expand(hCat, TVE_EXPAND);
				}
			}
			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;
		}
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(WL_SENSORS_GET_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			int l_iDataStartAt = FindInString(*l_pArrivedRespond, WL_SENSORS_GET_MSG_RESPOND);
			l_iDataStartAt += (CString(WL_SENSORS_GET_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			CString l_strRecordsData;

			CString l_strDataToPars, l_strTagId, l_strTagName, l_strBindTag, l_strTagType, l_strSensorParams,l_strRfEn;
			int l_iIndex, l_iIndex2, l_iCount, l_iInUnit, l_iTimeStamp;
			float l_fSensorRes;

			CWirelessListElement *l_WirelessListElement;

			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			l_pDoc->m_WirelessList.DeleteAllList();

			AfxExtractSubString(l_strRfEn, l_strDataToPars, 0, _T(','));
			l_pDoc->m_bRfActive = atoi((LPCSTR)l_strRfEn);
			if (l_pDoc->m_bRfActive)
			{
				l_iIndex = l_strDataToPars.Find(";");		//The data of the sensors themself apears after the first semicolom
				if (l_iIndex != -1)
				{
					l_strDataToPars = l_strDataToPars.Mid(l_iIndex + 1);
				}
				else
				{
					l_strDataToPars = "";
				}

				// last argument is the delimiter
				for (l_iIndex = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strRecordsData, l_strDataToPars, l_iIndex, _T(';')); ++l_iIndex)
				{
					//The first two arguments for each Tag response are the TagId and InUnit flag (if the unit just saw the tag or the tag is registered in the unit as well)
					AfxExtractSubString(l_strTagId, l_strRecordsData, 0, _T(','));
					AfxExtractSubString(l_strTagName, l_strRecordsData, 1, _T(','));
					AfxExtractSubString(l_strTagType, l_strRecordsData, 2, _T(','));
					AfxExtractSubString(l_strBindTag, l_strRecordsData, 3, _T(','));

					l_WirelessListElement = l_pDoc->m_WirelessList.AddItem(l_strTagId, (enmTagType)atoi((LPCSTR)l_strTagType), l_strTagName, true);

					//Get the data after the 7th comma
					for (l_iIndex2=-1, l_iCount = 0;l_iCount < 7;++l_iCount)
					{
						l_iIndex2 = l_strRecordsData.Find(",", l_iIndex2 + 1);
					}
					l_strSensorParams = l_strRecordsData.Mid(l_iIndex2 + 1);
					l_WirelessListElement->m_WlSensorsList.ProtocolToSensors(l_strSensorParams);
				}
			}
			m_bRfActive = l_pDoc->m_bRfActive;
			WirelessListToScreen();
			UpdateData(FALSE);
			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;
		}
	} while (l_iRespondStartAt != -1);

	if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, ACK_RESPOND)) != -1)
	{
		HandleLastSentCommand();
	}
	else if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, ERROR_RESPOND)) != -1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandNone;
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger, CString("exit CWirelessSenseView::OnCommandArrived"));
	return 0;
}

void CWirelessSenseView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	switch (m_enmWlSensorsLastSentCommand)
	{
	case enmWlSensorsLastSentCommandSetSensors:
	{
		CWirelessListElement *l_ListElement;
		for (int l_i = 1;l_i <= l_pDoc->m_WirelessList.GetCount();++l_i)
		{
			l_ListElement = l_pDoc->m_WirelessList.GetItemByPos(l_i);
			l_ListElement->m_bInUnit = true;
		}
		WirelessListToScreen();
		l_pDoc->SetModifiedFlag();
	}
	case enmWlSensorsLastSentCommandQuerySensors:
	case enmWlSensorsLastSentCommandGetSensors:
		m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandNone;
		EnableControls();
		break;
	case enmWlSensorsLastSentCommandClearSensors:
		m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandNone;
		EnableControls();
		CTreeCtrl& treeQuery = m_tcTagsQuery.GetTreeCtrl();
		treeQuery.DeleteAllItems();
		break;
	}
}


void CWirelessSenseView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	INFO_LOG(g_DbLogger, CString("CLevelAlertView::OnInitialUpdate"));
	if (m_bFirstRun)
	{
		CString str;

		INFO_LOG(g_DbLogger, CString("CLevelAlertView::OnInitialUpdate first run"));

		m_bFirstRun = false;

		//Create the tags tree view list
		// find the placeholder, get its position and destroy it
		CRect rcTreeWnd;
		CWnd* pPlaceholderWnd = GetDlgItem(IDC_TRV_TAGS);
		pPlaceholderWnd->GetWindowRect(&rcTreeWnd);
		ScreenToClient(&rcTreeWnd);
		pPlaceholderWnd->DestroyWindow();

		// create the multi-column tree window
		m_tcTags.CreateEx(WS_EX_CLIENTEDGE, NULL, NULL, WS_CHILD | WS_VISIBLE,
			rcTreeWnd, this, IDC_TRV_TAGS);

		CTreeCtrl& tree = m_tcTags.GetTreeCtrl();
		CHeaderCtrl& header = m_tcTags.GetHeaderCtrl();

		DWORD dwStyle = GetWindowLong(tree, GWL_STYLE);
		dwStyle |= TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT | TVS_FULLROWSELECT | TVS_DISABLEDRAGDROP;
		SetWindowLong(tree, GWL_STYLE, dwStyle);

		
		m_imlWirelessSenseView.Create(16, 16, ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_NOT_LOADED), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LOADED), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_SEARCH), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconUnknown = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_UNKNOWN), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconEmpty= (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_EMPTY), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		m_imlWirelessSenseView.Add(l_hIconNotLoaded_16x16);
		m_imlWirelessSenseView.Add(l_hIconLoaded_16x16);
		m_imlWirelessSenseView.Add(l_hIconSearch_16x16);
		m_imlWirelessSenseView.Add(l_hIconUnknown);
		m_imlWirelessSenseView.Add(l_hIconEmpty);
		
		tree.SetImageList(&m_imlWirelessSenseView, TVSIL_NORMAL);

		HDITEM hditem;
		hditem.mask = HDI_TEXT | HDI_WIDTH | HDI_FORMAT;
		hditem.fmt = HDF_LEFT | HDF_STRING;
		hditem.cxy = 150;
		str = GetResString(IDS_WIRELESS_ID);
		hditem.pszText = str.GetBuffer(0);
		header.InsertItem(WIRELESS_ID_COL_INDEX, &hditem);
		hditem.cxy = 100;
		str = GetResString(IDS_WIRELESS_NAME);
		hditem.pszText = str.GetBuffer(0);
		header.InsertItem(WIRELESS_NAME_COL_INDEX, &hditem);
		hditem.cxy = 70;
		str = GetResString(IDS_WIRELESS_TAG_TYPE);
		hditem.pszText = str.GetBuffer(0);
		header.InsertItem(WIRELESS_TAG_TYPE_COL_INDEX, &hditem);
		hditem.cxy = 100;
		str = GetResString(IDS_WIRELESS_SENSOR_TYPE);
		hditem.pszText = str.GetBuffer(0);
		header.InsertItem(WIRELESS_SENSOR_TYPE_COL_INDEX, &hditem);
		hditem.cxy = 620;
		str = GetResString(IDS_WIRELESS_SENSOR_PARAM);
		hditem.pszText = str.GetBuffer(0);
		header.InsertItem(WIRELESS_SENSOR_PARAM_COL_INDEX, &hditem);
		m_tcTags.UpdateColumns();

		//Create the tags query tree view list
		// find the placeholder, get its position and destroy it
		pPlaceholderWnd = GetDlgItem(IDC_TRV_TAGS_QUERY);
		pPlaceholderWnd->GetWindowRect(&rcTreeWnd);
		ScreenToClient(&rcTreeWnd);
		pPlaceholderWnd->DestroyWindow();

		// create the multi-column tree window
		m_tcTagsQuery.CreateEx(WS_EX_CLIENTEDGE, NULL, NULL, WS_CHILD | WS_VISIBLE,
			rcTreeWnd, this, IDC_TRV_TAGS_QUERY);

		CTreeCtrl& treeQuery = m_tcTagsQuery.GetTreeCtrl();
		CHeaderCtrl& headerQuery = m_tcTagsQuery.GetHeaderCtrl();

		SetWindowLong(treeQuery, GWL_STYLE, dwStyle);

		treeQuery.SetImageList(&m_imlWirelessSenseView, TVSIL_NORMAL);


		hditem.mask = HDI_TEXT | HDI_WIDTH | HDI_FORMAT;
		hditem.fmt = HDF_LEFT | HDF_STRING;
		hditem.cxy = 150;
		str = GetResString(IDS_WIRELESS_ID);
		hditem.pszText = str.GetBuffer(0);
		headerQuery.InsertItem(WIRELESS_QUERY_ID_COL_INDEX, &hditem);
		hditem.cxy = 100;
		str = GetResString(IDS_WIRELESS_NAME);
		hditem.pszText = str.GetBuffer(0);
		headerQuery.InsertItem(WIRELESS_QUERY_NAME_COL_INDEX, &hditem);
		hditem.cxy = 70;
		str = GetResString(IDS_WIRELESS_SENSOR_TYPE);
		hditem.pszText = str.GetBuffer(0);
		headerQuery.InsertItem(WIRELESS_QUERY_SENSOR_TYPE_COL_INDEX, &hditem);
		hditem.cxy = 50;
		str = GetResString(IDS_WIRELESS_SENSOR_RESULT);
		hditem.pszText = str.GetBuffer(0);
		headerQuery.InsertItem(WIRELESS_QUERY_SENSOR_RES_COL_INDEX, &hditem);
		hditem.cxy = 100;
		str = GetResString(IDS_WIRELESS_SENSOR_TS);
		hditem.pszText = str.GetBuffer(0);
		headerQuery.InsertItem(WIRELESS_QUERY_SENSOR_TS_COL_INDEX, &hditem);
		m_tcTagsQuery.UpdateColumns();

		
		//Add the Tag types
		int l_iNewIndex = m_cmbTagType.AddString(GetResString(IDS_TAG_TYPE_TAGSTICK));
		m_cmbTagType.SetItemData(l_iNewIndex, TAG_TYPE_TAGSTICK);
		
		CWnd *l_pWnd = GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame, l_pWnd);
		m_pMainFram = (CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame, m_pMainFram);

		//Localize();
	}

	//Show the data on the screen
	CGsmAlertDoc *l_pDoc = GetDocument();
	m_bRfActive = l_pDoc->m_bRfActive;
	WirelessListToScreen();

	UpdateData(FALSE);
	EnableControls();
}


void CWirelessSenseView::OnCbnSelchangeCmbTagType()
{
	ApplySetTypeFromTagType(false);
}

void CWirelessSenseView::ApplySetTypeFromTagType(bool a_bForace)
{
	if (!a_bForace)
	{
		int l_iOldRegType = m_iTagType;
		UpdateData();

		if (l_iOldRegType == m_iTagType)
		{
			return;
		}
	}

	int l_iRegType = m_cmbTagType.GetItemData(m_iTagType);
	m_cmbTagSetType.ResetContent();

	int l_iNewIndex;

	switch (l_iRegType)
	{
	case TAG_TYPE_TAGSTICK:
		l_iNewIndex = m_cmbTagSetType.AddString(GetResString(IDS_SET_TAGSTICK_GEN));
		m_cmbTagSetType.SetItemData(l_iNewIndex, SET_TAGSTICK_GEN);
		l_iNewIndex = m_cmbTagSetType.AddString(GetResString(IDS_SET_TAGSTICK_REED));
		m_cmbTagSetType.SetItemData(l_iNewIndex, SET_TAGSTICK_REED);
		l_iNewIndex = m_cmbTagSetType.AddString(GetResString(IDS_SET_TAGSTICK_LIGHT));
		m_cmbTagSetType.SetItemData(l_iNewIndex, SET_TAGSTICK_LIGHT);
		l_iNewIndex = m_cmbTagSetType.AddString(GetResString(IDS_SET_TAGSTICK_TEMPERATURE));
		m_cmbTagSetType.SetItemData(l_iNewIndex, SET_TAGSTICK_TEMPERATURE);
		l_iNewIndex = m_cmbTagSetType.AddString(GetResString(IDS_SET_TAGSTICK_HUMIDITY));
		m_cmbTagSetType.SetItemData(l_iNewIndex, SET_TAGSTICK_HUMIDITY);
		break;

	}
	UpdateData(FALSE);
}

void CWirelessSenseView::ApplyFiedsFromSetType(bool a_bForace)
{
	if (!a_bForace)
	{
		int l_iOldRegType = m_iTagSetType;
		UpdateData();

		if (l_iOldRegType == m_iTagSetType)
		{
			return;
		}
	}

	int l_iRegType = m_cmbTagSetType.GetItemData(m_iTagSetType);

	int l_iNewIndex;

	//For each setttings type, set the fields names and make only the wanted visible
	switch (l_iRegType)
	{
	case SET_TAGSTICK_GEN:
		m_strLblSet1 = GetResString(IDS_KEEP_ALIVE_INTERVAL);
		m_strLblSet2 = GetResString(IDS_WIRELESS_NAME);
		m_chkSet3.SetWindowTextA(GetResString(IDS_BIND_THING));

		m_LblSet1.ShowWindow(TRUE);
		m_LblSet2.ShowWindow(TRUE);
		m_LblSet3.ShowWindow(FALSE);
		m_LblSet4.ShowWindow(FALSE);
		m_LblSet5.ShowWindow(FALSE);
		m_LblSet6.ShowWindow(FALSE);

		m_txtSet1.ShowWindow(TRUE);
		m_txtSet2.ShowWindow(TRUE);
		m_txtSet3.ShowWindow(FALSE);
		m_txtSet4.ShowWindow(FALSE);
		m_txtSet5.ShowWindow(FALSE);
		m_txtSet6.ShowWindow(FALSE);
		m_cmbSet1.ShowWindow(FALSE);
		m_chkSet3.ShowWindow(TRUE);

		m_strSet1 = "";
		m_strSet2 = "";
		m_bSet3 = FALSE;
		break;
	case SET_TAGSTICK_REED:
		m_strLblSet1 = GetResString(IDS_REED_REPORT);
		m_cmbSet1.ResetContent();
		l_iNewIndex = m_cmbSet1.AddString(GetResString(IDS_REED_REPORT_HOLD));
		m_cmbSet1.SetItemData(l_iNewIndex, REED_REPORT_HOLD);
		l_iNewIndex = m_cmbSet1.AddString(GetResString(IDS_REED_REPORT_HOLD_AND_RELEASE));
		m_cmbSet1.SetItemData(l_iNewIndex, REED_REPORT_HOLD_AND_RELEASE);
		m_strLblSet2 = GetResString(IDS_CLOUD_KEY);

		m_LblSet1.ShowWindow(TRUE);
		m_LblSet2.ShowWindow(TRUE);
		m_LblSet3.ShowWindow(FALSE);
		m_LblSet4.ShowWindow(FALSE);
		m_LblSet5.ShowWindow(FALSE);
		m_LblSet6.ShowWindow(FALSE);

		m_txtSet1.ShowWindow(FALSE);
		m_txtSet2.ShowWindow(TRUE);
		m_txtSet3.ShowWindow(FALSE);
		m_txtSet4.ShowWindow(FALSE);
		m_txtSet5.ShowWindow(FALSE);
		m_txtSet6.ShowWindow(FALSE);
		m_cmbSet1.ShowWindow(TRUE);
		m_chkSet3.ShowWindow(FALSE);

		m_iSet1 = -1;
		m_strSet2 = "";
		break;
	case SET_TAGSTICK_LIGHT:
		m_strLblSet1 = GetResString(IDS_LIGHT_REPORT);
		m_cmbSet1.ResetContent();
		l_iNewIndex = m_cmbSet1.AddString(GetResString(IDS_LIGHT_REPORT_TO_DARK));
		m_cmbSet1.SetItemData(l_iNewIndex, LIGHT_REPORT_TO_DARK);
		l_iNewIndex = m_cmbSet1.AddString(GetResString(IDS_LIGHT_REPORT_TO_LIGHT));
		m_cmbSet1.SetItemData(l_iNewIndex, LIGHT_REPORT_TO_LIGHT);
		l_iNewIndex = m_cmbSet1.AddString(GetResString(IDS_LIGHT_REPORT_BOTH));
		m_cmbSet1.SetItemData(l_iNewIndex, LIGHT_REPORT_BOTH);
		m_strLblSet2 = GetResString(IDS_SAMPLE_INTERVAL);
		m_strLblSet3 = GetResString(IDS_DARK_DETECT_TRESH);
		m_strLblSet4 = GetResString(IDS_LIGHT_DETECT_TRESH);
		m_strLblSet5 = GetResString(IDS_CLOUD_KEY);
		

		m_LblSet1.ShowWindow(TRUE);
		m_LblSet2.ShowWindow(TRUE);
		m_LblSet3.ShowWindow(TRUE);
		m_LblSet4.ShowWindow(TRUE);
		m_LblSet5.ShowWindow(TRUE);
		m_LblSet6.ShowWindow(FALSE);

		m_txtSet1.ShowWindow(FALSE);
		m_txtSet2.ShowWindow(TRUE);
		m_txtSet3.ShowWindow(TRUE);
		m_txtSet4.ShowWindow(TRUE);
		m_txtSet5.ShowWindow(TRUE);
		m_txtSet6.ShowWindow(FALSE);
		m_cmbSet1.ShowWindow(TRUE);
		m_chkSet3.ShowWindow(FALSE);

		m_iSet1 = -1;
		m_strSet2 = "";
		m_strSet3 = "";
		m_strSet4 = "";
		m_strSet5 = "";
		break;
	case SET_TAGSTICK_TEMPERATURE:
	case SET_TAGSTICK_HUMIDITY:
		m_strLblSet1 = GetResString(IDS_SAMPLE_INTERVAL);
		m_strLblSet2 = GetResString(IDS_LOW_LEVEL);
		m_strLblSet3 = GetResString(IDS_HIGH_LEVEL);
		m_strLblSet4 = GetResString(IDS_GAP);
		m_strLblSet5 = GetResString(IDS_CLOUD_KEY);

		m_LblSet1.ShowWindow(TRUE);
		m_LblSet2.ShowWindow(TRUE);
		m_LblSet3.ShowWindow(TRUE);
		m_LblSet4.ShowWindow(TRUE);
		m_LblSet5.ShowWindow(TRUE);
		m_LblSet6.ShowWindow(FALSE);

		m_txtSet1.ShowWindow(TRUE);
		m_txtSet2.ShowWindow(TRUE);
		m_txtSet3.ShowWindow(TRUE);
		m_txtSet4.ShowWindow(TRUE);
		m_txtSet5.ShowWindow(TRUE);
		m_txtSet6.ShowWindow(FALSE);
		m_cmbSet1.ShowWindow(FALSE);
		m_chkSet3.ShowWindow(FALSE);

		m_strSet1 = "";
		m_strSet2 = "";
		m_strSet3 = "";
		m_strSet4 = "";
		m_strSet5 = "";
		break;
	default:
		m_LblSet1.ShowWindow(FALSE);
		m_LblSet2.ShowWindow(FALSE);
		m_LblSet3.ShowWindow(FALSE);
		m_LblSet4.ShowWindow(FALSE);
		m_LblSet5.ShowWindow(FALSE);
		m_LblSet6.ShowWindow(FALSE);

		m_txtSet1.ShowWindow(FALSE);
		m_txtSet2.ShowWindow(FALSE);
		m_txtSet3.ShowWindow(FALSE);
		m_txtSet4.ShowWindow(FALSE);
		m_txtSet5.ShowWindow(FALSE);
		m_txtSet6.ShowWindow(FALSE);
		m_cmbSet1.ShowWindow(FALSE);
		m_chkSet3.ShowWindow(FALSE);
		break;
	}
	UpdateData(FALSE);
}

void CWirelessSenseView::OnCbnSelchangeCmbTagSetType()
{
	ApplyFiedsFromSetType(false);
}


void CWirelessSenseView::OnBnClickedCmdQueryWirelessSensors()
{
	m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandQuerySensors;
	SendSetCommandToUnit(m_enmWlSensorsLastSentCommand);
	
	EnableControls(FALSE);
}

void CWirelessSenseView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::EnableControls"));

	m_cmdQueryWirelessSensors.EnableWindow(a_bEnabled);
	m_cmdClearWirelessSensors.EnableWindow(a_bEnabled);
	m_cmdGetWirelessSensor.EnableWindow(a_bEnabled);
	m_cmdSetWirelessSensor.EnableWindow(a_bEnabled);
	CTreeCtrl& treeQuery = m_tcTagsQuery.GetTreeCtrl();
	CTreeCtrl& tree = m_tcTags.GetTreeCtrl();
	treeQuery.EnableWindow(a_bEnabled);

	if (m_bRfActive)
	{
		m_cmdAddWirelessSensor.EnableWindow(a_bEnabled);
		m_cmdRemoveWirelessSensor.EnableWindow(a_bEnabled);
		m_cmdCloneWirelessSensor.EnableWindow(a_bEnabled);
		tree.EnableWindow(a_bEnabled);
		m_txtTagId.EnableWindow(a_bEnabled);
		m_cmbTagType.EnableWindow(a_bEnabled);
		m_cmbTagSetType.EnableWindow(a_bEnabled);
	}
	else
	{
		m_cmdAddWirelessSensor.EnableWindow(FALSE);
		m_cmdRemoveWirelessSensor.EnableWindow(FALSE);
		m_cmdCloneWirelessSensor.EnableWindow(FALSE);
		tree.EnableWindow(FALSE);
		m_txtTagId.EnableWindow(FALSE);
		m_cmbTagType.EnableWindow(FALSE);
		m_cmbTagSetType.EnableWindow(FALSE);
	}
}

void CWirelessSenseView::SendSetCommandToUnit(enmWlSensorsLastSentCommand &a_enmWlSensorsLastSentCommand)
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc = GetDocument();

	l_pNewCommand = new CString();

	switch (a_enmWlSensorsLastSentCommand)
	{
	case enmWlSensorsLastSentCommandQuerySensors:
		l_pNewCommand->Format("%s", WL_SENSORS_QUERY_SENSORS_MSG_COMMAND);
		break;
	case enmWlSensorsLastSentCommandClearSensors:
		l_pNewCommand->Format("%s", WL_SENSORS_CLEAR_SENSORS_MSG_COMMAND);
		break;
	case enmWlSensorsLastSentCommandSetSensors:
		*l_pNewCommand = m_strSetString;
		break;
	case enmWlSensorsLastSentCommandGetSensors:
		l_pNewCommand->Format("%s", WL_SENSORS_GET_MSG_COMMAND);
		break;
	}
	if (*l_pNewCommand != "")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND, (WPARAM)GetSafeHwnd(), (LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandNone;
	}
}

void CWirelessSenseView::OnTagsTreeSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMTREEVIEW* pNMTreeView = (NMTREEVIEW*)pNMHDR;
	// find the TagId of the selected item
	CString l_strParent, l_strSensor, l_strTagId, l_strTagType, l_strTagName;

	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	HTREEITEM hItemParent;
	if (hItem)
	{
		CGsmAlertDoc *l_pDoc = GetDocument();
		CString l_strSensorType;
		enmTagSensorType l_enmTagSensorType;
		CString l_strParam1, l_strParam2, l_strParam3, l_strParam4, l_strParam5, l_strParam6;

		//Get the TAG ID from the tree parent
		CTreeCtrl& tree = m_tcTags.GetTreeCtrl();
		if (tree.GetParentItem(hItem) != NULL)
		{
			hItemParent = tree.GetParentItem(hItem);
			l_strSensor = tree.GetItemText(hItem);
			AfxExtractSubString(l_strSensorType, l_strSensor, 3, _T('\t'));
			l_enmTagSensorType = TagSensorStringToEnm(l_strSensorType);
		}
		else   //If there is no treee paternt, it means that we're on the partent itself
		{
			hItemParent = hItem;
			//If the user selects the TAG name itself, set it to the general settings
			l_enmTagSensorType = SET_TAGSTICK_GEN;
		}
		l_strParent = tree.GetItemText(hItemParent);


		AfxExtractSubString(l_strTagId, l_strParent, 0, _T('\t'));
		CWirelessListElement *l_WirelessListElement = l_pDoc->m_WirelessList.GetItemByTagId(l_strTagId);
		if (l_WirelessListElement)
		{
			SetComboBoxFromItemData(m_cmbTagType, l_WirelessListElement->m_enmTagType);
			UpdateData();
			ApplySetTypeFromTagType(true);
			
			SetComboBoxFromItemData(m_cmbTagSetType, l_enmTagSensorType);
			UpdateData();
			ApplyFiedsFromSetType(true);

			CWlSensorsListElement *l_WlSensorsListElement = l_WirelessListElement->m_WlSensorsList.GetItemBySensorType(l_enmTagSensorType);

			switch (l_enmTagSensorType)
			{
			case SET_TAGSTICK_GEN:
				l_strParam1 = l_WlSensorsListElement->GetSensorParams(1);
				l_strParam2 = l_WlSensorsListElement->GetSensorParams(2);
				m_strSet1 = GetSecAsTimedStr(atoi((LPCSTR)l_strParam1));	//Keep alive interval
				m_strSet2 = l_WirelessListElement->m_strTagName;			//The Tag name
				m_bSet3 = atoi((LPCSTR)l_strParam2);				//Binded tag
				break;
			case SET_TAGSTICK_REED:
				{
					l_strParam1 = l_WlSensorsListElement->GetSensorParams(1);		//Reed Report on 
					l_strParam2 = l_WlSensorsListElement->GetSensorParams(2);		//Cloud key

					int l_iReedReport = atoi((LPCSTR)l_strParam2);
					SetComboBoxFromItemData(m_cmbSet1, l_iReedReport);
					UpdateData();

					m_strSet2 = l_strParam1;
				}
				break;
			case SET_TAGSTICK_LIGHT:
				{
					l_strParam1 = l_WlSensorsListElement->GetSensorParams(1);
					l_strParam2 = l_WlSensorsListElement->GetSensorParams(2);
					l_strParam3 = l_WlSensorsListElement->GetSensorParams(3);
					l_strParam4 = l_WlSensorsListElement->GetSensorParams(4);
					l_strParam5 = l_WlSensorsListElement->GetSensorParams(5);

					int l_iLightReport = atoi((LPCSTR)l_strParam3);
					SetComboBoxFromItemData(m_cmbSet1, l_iLightReport);
					UpdateData();

					m_strSet2 = GetSecAsTimedStr(atoi((LPCSTR)l_strParam2));		//Sample interval
					m_strSet3 = l_strParam5;		//Dark detection threshold
					m_strSet4 = l_strParam4;		//Light detection threshold
					m_strSet5 = l_strParam1;		//Cloud key
				}
				break;
			case SET_TAGSTICK_TEMPERATURE:
			case SET_TAGSTICK_HUMIDITY:
				{
					l_strParam1 = l_WlSensorsListElement->GetSensorParams(1);
					l_strParam2 = l_WlSensorsListElement->GetSensorParams(2);
					l_strParam3 = l_WlSensorsListElement->GetSensorParams(3);
					l_strParam4 = l_WlSensorsListElement->GetSensorParams(4);
					l_strParam5 = l_WlSensorsListElement->GetSensorParams(6);

					float l_fHigh = atof((LPCSTR)l_strParam3) / 10.0;
					float l_fLow = atof((LPCSTR)l_strParam4) / 10.0;
					float l_fGap = atof((LPCSTR)l_strParam5) / 10.0;

					m_strSet1 = GetSecAsTimedStr(atoi((LPCSTR)l_strParam2));		//Sampling interval
					m_strSet2.Format("%.01f", l_fLow);
					m_strSet3.Format("%.01f", l_fHigh);
					m_strSet4.Format("%.01f", l_fGap);
					m_strSet5 = l_strParam1;		//Cloud key
				}
				break;
			case SET_TAGSTICK_UNKNOWN:
			default:
				break;
			}
			//hhhhhhhhhhh
			m_strTagId = l_strTagId;
			UpdateData(FALSE);
		}
	}

	*pResult = 0;
}

void CWirelessSenseView::OnTagsQueryTreeSelchanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMTREEVIEW* pNMTreeView = (NMTREEVIEW*)pNMHDR;
	// find the TagId of the selected item
	CString l_strTagId;
	
	HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	if (hItem)
	{
		CTreeCtrl& tree = m_tcTagsQuery.GetTreeCtrl();
		if (tree.GetParentItem(hItem) != NULL)
		{
			hItem = tree.GetParentItem(hItem);
		}
		l_strTagId = tree.GetItemText(hItem);
		int nPos = l_strTagId.Find('\t');
		if (nPos >= 0)
			l_strTagId = l_strTagId.Left(nPos);
	}

	// display it on the static control
	SetDlgItemText(IDC_TXT_TAG_ID, l_strTagId);

	*pResult = 0;
}


void CWirelessSenseView::OnBnClickedCmdAddWirelessSensor()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	CWirelessListElement *l_WirelessListElement;

	UpdateData();

	int l_iTagType= m_cmbTagType.GetItemData(m_iTagType);
	int l_iSensorType = m_cmbTagSetType.GetItemData(m_iTagSetType);

	CString l_strSensorParams;

	//Handle TagStick parameters
	if (l_iTagType == TAG_TYPE_TAGSTICK)
	{
		//For each setttings type, set the fields names and make only the wanted visible
		if(l_iSensorType == SET_TAGSTICK_GEN)
		{
			//Set2 text  is the keep alive interval
			//Set2 text  is the unit's name
			//Set3 check box indicates if the unit is binded or not
			int l_iKeepAliveInterval = GetTimedStrAsSec(m_strSet1);
			if ((l_iKeepAliveInterval < MIN_TAG_STICK_KEEP_ALIVE_INTERVAL) || (l_iKeepAliveInterval > MAX_TAG_STICK_KEEP_ALIVE_INTERVAL))
			{
				MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_KEEP_ALIVE_INTERVAL));
				return;
			}
			l_strSensorParams.Format("%d,%d", l_iKeepAliveInterval, m_bSet3);
			
			l_WirelessListElement = l_pDoc->m_WirelessList.AddItem(m_strTagId, (enmTagType)l_iTagType, m_strSet2, false);
			l_WirelessListElement->m_WlSensorsList.AddItem((enmTagSensorType)l_iSensorType, l_strSensorParams);
		}
		else
		{
			l_WirelessListElement = l_pDoc->m_WirelessList.GetItemByTagId(m_strTagId);
			if (l_WirelessListElement == NULL)
			{
				MessageBox(GetResString(IDS_GENERAL_SET_NOT_SET));
				return;
			}

			l_WirelessListElement->m_bInUnit = false;
			switch (l_iSensorType)
			{
			case SET_TAGSTICK_REED:
			{
				if (m_iSet1 == -1)
				{
					MessageBox(GetResString(IDS_LIST_NOT_SELECTED));
					return;
				}
				int l_iReed = m_cmbSet1.GetItemData(m_iSet1);
				if (IsNumeric(m_strSet2))	//Numeric key is not allowed
				{
					MessageBox(GetResString(IDS_WRONG_KEY));
					return;
				}
				l_strSensorParams.Format("%s,%d,%d", m_strSet2, DEFAULT_REED_SAMPLE_INTERVARL, l_iReed);
				l_WirelessListElement->m_WlSensorsList.AddItem((enmTagSensorType)l_iSensorType, l_strSensorParams);
			}
			break;
			case SET_TAGSTICK_LIGHT:
			{
				if (m_iSet1 == -1)
				{
					MessageBox(GetResString(IDS_LIST_NOT_SELECTED));
					return;
				}
				int l_iLightReport = m_cmbSet1.GetItemData(m_iSet1);
				
				int l_iSampleInterval = GetTimedStrAsSec(m_strSet2);
				if ((l_iSampleInterval < MIN_TAG_STICK_LIGHT_INTERVAL) || (l_iSampleInterval > MAX_TAG_STICK_LIGHT_INTERVAL))
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_LIGHT_INTERVAL));
					return;
				}

				int l_iTmp = atoi((LPCSTR)m_strSet3);
				if ((l_iTmp < MIN_LIGHT_DETECT_THRESHOLD) || (l_iTmp > MAX_LIGHT_DETECT_THRESHOLD))
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_LIGHT_THRESHOLD));
					return;
				}
				l_iTmp = atoi((LPCSTR)m_strSet4);
				if ((l_iTmp < MIN_LIGHT_DETECT_THRESHOLD) || (l_iTmp > MAX_LIGHT_DETECT_THRESHOLD))
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_LIGHT_THRESHOLD));
					return;
				}
				
				if (IsNumeric(m_strSet5))	//Numeric key is not allowed
				{
					MessageBox(GetResString(IDS_WRONG_KEY));
					return;
				}
				l_strSensorParams.Format("%s,%d,%d,%s,%s", m_strSet5, l_iSampleInterval, l_iLightReport, m_strSet4, m_strSet3);
				l_WirelessListElement->m_WlSensorsList.AddItem((enmTagSensorType)l_iSensorType, l_strSensorParams);
			}
			break;
			case SET_TAGSTICK_TEMPERATURE:
			case SET_TAGSTICK_HUMIDITY:
			{
				int l_iHyst, l_iHigh, l_iLow, l_iGap,l_iSampleInterval;
				float l_fHigh, l_fLow, l_fGap;
				if (l_iSensorType == SET_TAGSTICK_TEMPERATURE)
				{
					l_iHyst = DEFAULT_TEMP_HYST;
				}
				else
				{
					l_iHyst = DEFAULT_HUMID_HYST;
				}

				l_iSampleInterval = GetTimedStrAsSec(m_strSet1);
				if ((l_iSampleInterval < MIN_TAG_STICK_HUM_TEMP_INTERVAL) || (l_iSampleInterval > MAX_TAG_STICK_HUM_TEMP_INTERVAL))
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_INTERVAL));
					return;
				}
				if (IsNumeric(m_strSet5))	//Numeric key is not allowed
				{
					MessageBox(GetResString(IDS_WRONG_KEY));
					return;
				}
				l_fLow = atof(m_strSet2);
				l_fHigh = atof(m_strSet3);
				l_fGap = atof(m_strSet4);

				if (l_iSensorType == SET_TAGSTICK_TEMPERATURE)
				{
					if ((l_fLow < MIN_TAG_STICK_TEMP_TRSHLD_LOW) || (l_fLow > MIN_TAG_STICK_TEMP_TRSHLD_HIGH))
					{
						MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD));
						return;
					}
					if ((l_fHigh < MIN_TAG_STICK_TEMP_TRSHLD_LOW) || (l_fHigh > MIN_TAG_STICK_TEMP_TRSHLD_HIGH))
					{
						MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD));
						return;
					}
				}
				else
				{
					if ((l_fLow < MIN_TAG_STICK_HUM_TRSHLD_LOW) || (l_fLow > MIN_TAG_STICK_HUM_TRSHLD_HIGH))
					{
						MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD));
						return;
					}
					if ((l_fHigh < MIN_TAG_STICK_HUM_TRSHLD_LOW) || (l_fHigh > MIN_TAG_STICK_HUM_TRSHLD_HIGH))
					{
						MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD));
						return;
					}
				}
				if (l_fHigh < l_fLow)
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_TRSHLD_ORDER));
					return;
				}
				if ((l_fGap < MIN_TAG_STICK_HUM_TEMP_GAP_LOW) || (l_fGap > MIN_TAG_STICK_HUM_TEMP_GAP_HIGH))
				{
					MessageBox(GetResString(IDS_ILLEGAL_TAG_STICK_HUM_TEMP_GAP));
					return;
				}

				//multiple the levels  in 10 for the protocol
				l_iLow = (int)(l_fLow * 10);
				l_iHigh = (int)(l_fHigh * 10);
				l_iGap = (int)(l_fGap * 10);
				
				l_strSensorParams.Format("%s,%d,%d,%d,%d,%d", m_strSet5, l_iSampleInterval, l_iHigh, l_iLow, l_iHyst, l_iGap);
				l_WirelessListElement->m_WlSensorsList.AddItem((enmTagSensorType)l_iSensorType, l_strSensorParams);
			}
			break;
			}
		}		
	}

WirelessListToScreen();
}


void CWirelessSenseView::WirelessListToScreen()
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::WirelessListToScreen()"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	CTreeCtrl& treeTags = m_tcTags.GetTreeCtrl();

	treeTags.DeleteAllItems();

	POSITION l_Position = l_pDoc->m_WirelessList.GetHeadPosition();
	CWirelessListElement *l_pListElement;

	//Go over all the level events list and add them to the level event list control
	while (l_Position != NULL)
	{
		l_pListElement = l_pDoc->m_WirelessList.GetNext(l_Position);
		AddItemToWirelessList(l_pListElement->m_strTagId);
	}
}

void CWirelessSenseView::AddItemToWirelessList(CString a_strTagId)
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::AddItemToWirelessList"));
	CGsmAlertDoc *l_pDoc = GetDocument();
	int l_iImg;

	CTreeCtrl& treeTags = m_tcTags.GetTreeCtrl();
	HTREEITEM hCat;
	CString l_strTmp, l_strTagType;
	CWirelessListElement *l_pListElement = l_pDoc->m_WirelessList.GetItemByTagId(a_strTagId);

	if (l_pListElement->m_bInUnit)
	{
		l_iImg = 1;
	}
	else
	{
		l_iImg = 0;
	}

	switch (l_pListElement->m_enmTagType)
	{
	case TAG_TYPE_TAGSTICK:
		l_strTagType = GetResString(IDS_TAG_TYPE_TAGSTICK);

	}

	l_strTmp.Format("%s\t%s\t%s", l_pListElement->m_strTagId, l_pListElement->m_strTagName, l_strTagType);
	hCat = treeTags.InsertItem(l_strTmp, l_iImg, l_iImg, TVI_ROOT);

	POSITION l_Position = l_pListElement->m_WlSensorsList.GetHeadPosition();
	CWlSensorsListElement *l_pSensorListElement;

	//Go over all the level events list and add them to the level event list control
	while (l_Position != NULL)
	{
		l_pSensorListElement = l_pListElement->m_WlSensorsList.GetNext(l_Position);
		l_strTmp.Format("\t\t\t%s\t%s", SensorTypeToText(l_pSensorListElement->m_iSensorType), l_pSensorListElement->m_strSensorParamHuminRead);

		treeTags.InsertItem(l_strTmp, 5, 5, hCat);
	}
	treeTags.Expand(hCat, TVE_EXPAND);
}

CString CWirelessSenseView::GetWirelessSensorsString()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	CWirelessListElement *l_pListElement;
	CWlSensorsListElement *l_pSensorListElement;
	CString l_strSetRes, l_strTmp, l_strTmpSensors, l_strSensorId;

	l_strSetRes.Format("%s%d,,,,;", WL_SENSORS_SET_MSG_COMMAND, l_pDoc->m_bRfActive);

	if ((l_pDoc->m_WirelessList.GetCount() == 0) || (l_pDoc->m_bRfActive == FALSE))
	{
		if (MessageBox(GetResString(IDS_EMPTY_WIRELESS_LIST_DOWNLOAD_MSG), NULL, MB_YESNO) == IDNO)
		{
			return "";
		}
	}
	for (int l_i = 0;l_i < l_pDoc->m_WirelessList.GetCount();++l_i)
	{
		l_pListElement = l_pDoc->m_WirelessList.GetItemByPos(l_i + 1);

		l_strTmp.Format("%s,%s,%d,,,,", l_pListElement->m_strTagId, l_pListElement->m_strTagName, l_pListElement->m_enmTagType);

		for (int l_j = 0;l_j < l_pListElement->m_WlSensorsList.GetCount();++l_j)
		{
			l_pSensorListElement = l_pListElement->m_WlSensorsList.GetItemByPos(l_j + 1);
			l_strTmp += CString(",") + l_pSensorListElement->SensorParamsToProtocol();
		}
		if (l_strTmpSensors != "")	//For the second, third,.. items add semi column
		{
			l_strTmpSensors += ";";
		}
		l_strTmpSensors += l_strTmp;
	}
	if (l_strTmpSensors.GetLength())
	{
		l_strSetRes += l_strTmpSensors;
	}

	return l_strSetRes;
}

void CWirelessSenseView::OnBnClickedCmdSetWirelessSensor()
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnBnClickedCmdSetWirelessSensor"));

	m_strSetString = GetWirelessSensorsString();
	if (m_strSetString.GetLength() == 0)
	{
		return;
	}
	m_enmWlSensorsLastSentCommand= enmWlSensorsLastSentCommandSetSensors;
	SendSetCommandToUnit(m_enmWlSensorsLastSentCommand);

	EnableControls(FALSE);
}


void CWirelessSenseView::OnBnClickedCmdGetWirlelessSensor()
{
	m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandGetSensors;
	SendSetCommandToUnit(m_enmWlSensorsLastSentCommand);

	EnableControls(FALSE);
}


void CWirelessSenseView::OnBnClickedCmdRemoveWirelessSensor()
{
	CTreeCtrl& tree = m_tcTags.GetTreeCtrl();
	HTREEITEM hItemParent;
	HTREEITEM hItem = tree.GetSelectedItem();
	bool l_bDelTag=false;
	if (hItem)
	{
		CGsmAlertDoc *l_pDoc = GetDocument();

		if (tree.GetParentItem(hItem) == NULL)
		{
			hItemParent = hItem;
			hItem = NULL;
			l_bDelTag = true;
		}
		else
		{
			hItemParent = tree.GetParentItem(hItem);
		}
		CString l_strTagId = tree.GetItemText(hItemParent);
		int nPos = l_strTagId.Find('\t');
		if (nPos >= 0)
			l_strTagId = l_strTagId.Left(nPos);

		if (hItem)
		{
			CWirelessListElement *l_WirelessListElement = l_pDoc->m_WirelessList.GetItemByTagId(l_strTagId);

			CString l_strTagParams = tree.GetItemText(hItem);
			CString l_strTagSensor;
			enmTagSensorType l_enmTagSensorType;

			AfxExtractSubString(l_strTagSensor, l_strTagParams, 3, _T('\t'));
			
			l_enmTagSensorType = TagSensorStringToEnm(l_strTagSensor);

			l_WirelessListElement->m_WlSensorsList.DeleteItem(l_enmTagSensorType);
			if ((l_WirelessListElement->m_WlSensorsList.GetCount() == 0) || (l_enmTagSensorType == SET_TAGSTICK_GEN))
			{
				l_bDelTag = true;
			}
		}

		if (l_bDelTag)
		{
			l_pDoc->m_WirelessList.DeleteItem(l_strTagId);
		}
		
		WirelessListToScreen();
	}
	else
	{
		MessageBox(GetResString(IDS_TAG_NOT_SELECTED));
	}
}


void CWirelessSenseView::OnBnClickedChkRfActive()
{
	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_bRfActive = m_bRfActive;
	EnableControls();
}

enmTagSensorType CWirelessSenseView::TagSensorStringToEnm(CString a_strTagSensor)
{
	if (a_strTagSensor == GetResString(IDS_SET_TAGSTICK_GEN))
	{
		return SET_TAGSTICK_GEN;
	}
	else if (a_strTagSensor == GetResString(IDS_SET_TAGSTICK_REED))
	{
		return SET_TAGSTICK_REED;
	}
	else if (a_strTagSensor == GetResString(IDS_SET_TAGSTICK_LIGHT))
	{
		return SET_TAGSTICK_LIGHT;
	}
	else if (a_strTagSensor == GetResString(IDS_SET_TAGSTICK_TEMPERATURE))
	{
		return SET_TAGSTICK_TEMPERATURE;
	}
	else if (a_strTagSensor == GetResString(IDS_SET_TAGSTICK_HUMIDITY))
	{
		return SET_TAGSTICK_HUMIDITY;
	}
	else
	{
		return SET_TAGSTICK_UNKNOWN;
	}
}

enmTagType CWirelessSenseView::TagTypeStringToEnm(CString a_strTagType)
{
	if (a_strTagType == GetResString(IDS_TAG_TYPE_TAGSTICK))
	{
		return TAG_TYPE_TAGSTICK;
	}
	else
	{
		return TAG_TYPE_UNKNOWN;
	}
}


void CWirelessSenseView::OnBnClickedCmdCloneWirelessSensor()
{
	CTreeCtrl& tree = m_tcTags.GetTreeCtrl();
	HTREEITEM hItemParent;
	HTREEITEM hItem = tree.GetSelectedItem();
	UpdateData();
	if (hItem)
	{
		CGsmAlertDoc *l_pDoc = GetDocument();

		if (tree.GetParentItem(hItem) == NULL)
		{
			hItemParent = hItem;
			hItem = NULL;
		}
		else
		{
			hItemParent = tree.GetParentItem(hItem);
		}
		CString l_strTagId = tree.GetItemText(hItemParent);
		int nPos = l_strTagId.Find('\t');
		if (nPos >= 0)
			l_strTagId = l_strTagId.Left(nPos);

		l_pDoc->m_WirelessList.CloneTagById(l_strTagId,m_strTagId);


		WirelessListToScreen();
	}
	else
	{
		MessageBox(GetResString(IDS_TAG_NOT_SELECTED));
	}
}


void CWirelessSenseView::OnBnClickedCmdCopyWirelessSensorClipboard()
{
	CopyStringToClipbard(GetWirelessSensorsString(),GetSafeHwnd());
}


void CWirelessSenseView::OnBnClickedCmdClearWirelessSensors()
{
	INFO_LOG(g_DbLogger, CString("CWirelessSenseView::OnBnClickedCmdClearWirelessSensors"));

	m_enmWlSensorsLastSentCommand = enmWlSensorsLastSentCommandClearSensors;
	SendSetCommandToUnit(m_enmWlSensorsLastSentCommand);

	EnableControls(FALSE);
}
