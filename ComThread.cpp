#include "ComThread.h"
#include "Logger.h"

extern CLogger g_DbLogger;

#define INPUT_BUFF_SIZE 2000

UINT ComThread(void *a_ComPortTrdVars)
{
	unsigned long l_ulWritenData;
	int l_iResult;
	char l_strBuffToRead[INPUT_BUFF_SIZE+1];
	unsigned long l_ulBytesRead;
	MSG l_oMsg;
	CComTrdVars *l_ComPortTrdVars=(CComTrdVars*)a_ComPortTrdVars;
	CString *l_pSendCommandVars;
	
	//Clear the com if any data remain in it.
	do {
		l_iResult = ReadFile(*l_ComPortTrdVars->l_hComPort , l_strBuffToRead, INPUT_BUFF_SIZE, &l_ulBytesRead, NULL) ; 
	} while(l_ulBytesRead>0);

	while(TRUE)
	{
		l_iResult = ReadFile(*l_ComPortTrdVars->l_hComPort , l_strBuffToRead, INPUT_BUFF_SIZE, &l_ulBytesRead, NULL) ; 
		if ((l_iResult)&&(l_ulBytesRead>0)) 
		{
			l_strBuffToRead[l_ulBytesRead]=NULL;

			INFO_LOG(g_DbLogger, CString("Recv from unit:") + l_strBuffToRead);

			TRACE("Recv from unit:");
			if(strlen(l_strBuffToRead)>512)
			{
				CString l_strWrite;
				l_strWrite.Format(" %d bytes",strlen(l_strBuffToRead));
				TRACE(l_strWrite);
			}
			else
			{
				//TRACE(l_strBuffToRead);
			}
			TRACE("\n");

			char *l_strTmpBuff=new char[l_ulBytesRead];
			memcpy(l_strTmpBuff,l_strBuffToRead,l_ulBytesRead);
			CComTrdDataInVars *l_pComTrdDataInVars=new CComTrdDataInVars;
			l_pComTrdDataInVars->m_iDataLen=l_ulBytesRead;
			l_pComTrdDataInVars->m_pData=l_strTmpBuff;
			PostMessage(l_ComPortTrdVars->m_hWnd,UM_COMMAND_ARRIVED,COMMAND_MESSAGE_MODEM_RESPOND,(LPARAM)l_pComTrdDataInVars);		//Post cellular call is in progress message
			TRACE("Post message to main window");
		}
		if(PeekMessage (&l_oMsg,NULL,0,0,PM_REMOVE))
		{
			switch(l_oMsg.message)
			{
			case UM_SEND_COMMAND:
				l_pSendCommandVars=(CString*)(l_oMsg.lParam);

				INFO_LOG(g_DbLogger, CString("Write to unit:") + *l_pSendCommandVars);

				TRACE("Write to unit:");
				if(strlen(*l_pSendCommandVars)>512)
				{
					CString l_strWrite;
					l_strWrite.Format(" %d bytes",strlen(*l_pSendCommandVars));
					TRACE(l_strWrite);
				}
				else
				{
					TRACE(*l_pSendCommandVars);
				}
				TRACE("\n");
				WriteFile(*l_ComPortTrdVars->l_hComPort,(LPCTSTR)(*l_pSendCommandVars),l_pSendCommandVars->GetLength(),&l_ulWritenData,NULL);
				delete l_pSendCommandVars;
				break;
			case WM_QUIT:
				return 1;
			}
			continue;
		}
	}
	return 1;
}