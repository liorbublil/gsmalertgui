#if !defined(AFX_CHANGELANGUAGE_H__49350354_67E0_46B6_9F9D_E316B88485A6__INCLUDED_)
#define AFX_CHANGELANGUAGE_H__49350354_67E0_46B6_9F9D_E316B88485A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChangeLanguage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChangeLanguage dialog

class CChangeLanguage : public CDialog
{
// Construction
public:
	CChangeLanguage(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CChangeLanguage)
	enum { IDD = IDD_CHANGE_LANG_DIAL };
	CComboBox	m_cmbLanguage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChangeLanguage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void Localize();
	// Generated message map functions
	//{{AFX_MSG(CChangeLanguage)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHANGELANGUAGE_H__49350354_67E0_46B6_9F9D_E316B88485A6__INCLUDED_)
