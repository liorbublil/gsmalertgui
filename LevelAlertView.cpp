// LevelAlertView.cpp : implementation file
//

#include "stdafx.h"
#include "gsmalert.h"
#include "MainFrm.h"
#include "GsmAlertDoc.h"
#include "LevelAlertView.h"
#include "Language.h"
#include "global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LEVEL_EVENT_INDEX_COL_INDEX 0
#define LEVEL_EVENT_SENSOR_TYPE_COL_INDEX 1
#define LEVEL_EVENT_SENSOR_NUM_COL_INDEX 2
#define LEVEL_EVENT_SENSOR_NAME_COL_INDEX 3
#define LEVEL_EVENT_LOW_HYS_COL_INDEX 4
#define LEVEL_EVENT_HIGH_HYS_COL_INDEX 5
#define LEVEL_EVENT_LOW_MSG_COL_INDEX 6
#define LEVEL_EVENT_HIGH_MSG_COL_INDEX 7
#define LEVEL_EVENT_NORMAL_RANGE_COL_INDEX 8
#define LEVEL_EVENT_REPEAT_ALERT_COL_INDEX 9
#define LEVEL_EVENT_LOW_DELAY_COL_INDEX 10
#define LEVEL_EVENT_HIGH_DELAY_COL_INDEX 11
#define LEVEL_EVENT_CLOUD_ALARM_KEY_COL_INDEX 12
#define LEVEL_EVENT_KEY_COL_INDEX 13

#define SENSOR_TYPE_COL_INDEX 0
#define SENSOR_NUM_COL_INDEX 1
#define ONE_WIRE_SENSOR_NAME_COL_INDEX 2

#define NORMAL_RANGE_LOW 0
#define NORMAL_RANGE_HIGH 1

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CLevelAlertView

IMPLEMENT_DYNCREATE(CLevelAlertView, CFormView)

CLevelAlertView::CLevelAlertView()
	: CFormView(CLevelAlertView::IDD)
	, m_strAlarmKey(_T(""))
	, m_strLevelEventKey(_T(""))
{
	//{{AFX_DATA_INIT(CLevelAlertView)
	m_iNormlRange = -1;
	m_strHighHys = _T("");
	m_strHighMsg = _T("");
	m_strLowHys = _T("");
	m_strLowMsg = _T("");
	m_odtLowDelay = COleDateTime::GetCurrentTime();
	m_odtHighDelay = COleDateTime::GetCurrentTime();
	//}}AFX_DATA_INIT
	m_odtRepeatAlert.SetTime(0,0,0);
	m_odtLowDelay.SetTime(0,0,0);
	m_odtHighDelay.SetTime(0,0,0);
	m_bFirstRun=true;
	m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
}

CLevelAlertView::~CLevelAlertView()
{
}

void CLevelAlertView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLevelAlertView)
	DDX_Control(pDX, IDC_DTP_HIGH_DELAY, m_dtpHighDelay);
	DDX_Control(pDX, IDC_DTP_LOW_DELAY, m_dtpLowDelay);
	DDX_Control(pDX, IDC_CMD_REPLACE_LEVEL_EVENT, m_cmdReplaceLevelEvent);
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_LOW_MSG, m_txtLowMsg);
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_LOW_HYS, m_txtLowHys);
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_HIGH_MSG, m_txtHighMsg);
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_HIGH_HYS, m_txtHighHys);
	DDX_Control(pDX, IDC_DTP_REPEAT_ALERT, m_dtpRepeatAlert);
	DDX_Control(pDX, IDC_CMB_NORMAL_RANGE, m_cmbNormalRange);
	DDX_Control(pDX, IDC_CMD_SET_LEVEL_EVENTS, m_cmdSetLevelEvents);
	DDX_Control(pDX, IDC_CMD_REMOVE_LEVEL_EVENT, m_cmdRemoveLevelEvent);
	DDX_Control(pDX, IDC_CMD_GET_LEVEL_EVENTS, m_cmdGetLevelEvents);
	DDX_Control(pDX, IDC_CMD_ADD_LEVEL_EVENT, m_cmdAddLevelEvent);
	DDX_Control(pDX, IDC_LST_SENSORS_FOR_LEVEL_EVENT, m_lstSensorsForLevelEvent);
	DDX_Control(pDX, IDC_LST_LEVEL_EVENTS, m_lstLevelEvent);
	DDX_CBIndex(pDX, IDC_CMB_NORMAL_RANGE, m_iNormlRange);
	DDX_DateTimeCtrl(pDX, IDC_DTP_REPEAT_ALERT, m_odtRepeatAlert);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_HIGH_HYS, m_strHighHys);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_HIGH_MSG, m_strHighMsg);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_LOW_HYS, m_strLowHys);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_LOW_MSG, m_strLowMsg);
	DDX_DateTimeCtrl(pDX, IDC_DTP_LOW_DELAY, m_odtLowDelay);
	DDX_DateTimeCtrl(pDX, IDC_DTP_HIGH_DELAY, m_odtHighDelay);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_ALARM_KEY, m_txtAlarmKey);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_ALARM_KEY, m_strAlarmKey);
	DDX_Control(pDX, IDC_TXT_LEVEL_ALERT_KEY, m_txtLevelEventKey);
	DDX_Text(pDX, IDC_TXT_LEVEL_ALERT_KEY, m_strLevelEventKey);
}


BEGIN_MESSAGE_MAP(CLevelAlertView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	ON_MESSAGE(UM_SENSORS_LIST_CHANGED,OnUpdateSensorsList)
	//{{AFX_MSG_MAP(CLevelAlertView)
	ON_BN_CLICKED(IDC_CMD_ADD_LEVEL_EVENT, OnCmdAddLevelEvent)
	ON_BN_CLICKED(IDC_CMD_REMOVE_LEVEL_EVENT, OnCmdRemoveLevelEvent)
	ON_BN_CLICKED(IDC_CMD_REPLACE_LEVEL_EVENT, OnCmdReplaceLevelEvent)
	ON_NOTIFY(NM_CLICK, IDC_LST_LEVEL_EVENTS, OnClickLstLevelEvents)
	ON_BN_CLICKED(IDC_CMD_GET_LEVEL_EVENTS, OnCmdGetLevelEvents)
	ON_BN_CLICKED(IDC_CMD_SET_LEVEL_EVENTS, OnCmdSetLevelEvents)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_TXT_LEVEL_ALERT_LOW_HYS, &CLevelAlertView::OnEnChangeTxtLevelAlertLowHys)
	ON_BN_CLICKED(IDC_CMD_COPY_LEVEL_EVENTS_CLIPBOARD, &CLevelAlertView::OnBnClickedCmdCopyLevelEventsClipboard)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLevelAlertView diagnostics

#ifdef _DEBUG
void CLevelAlertView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLevelAlertView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CLevelAlertView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLevelAlertView message handlers

LRESULT CLevelAlertView::OnUpdateSensorsList (WPARAM wParam,LPARAM lParam)
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	LRESULT l_bAllLevelEventsOK=1;

	m_lstSensorsForLevelEvent.DeleteAllItems();
	CString	l_strTmp;
	//Add the 1-wire sensors to the sensor list
	POSITION l_Position;
	COneWireListElement *l_pListElement;
	CAdcListElement *l_pAdcListElement;
	CModbusListElement *l_pModbusListElement;
	CZoneListElement *l_pZoneListElement;

	//Add the 1-wire sensors to the sensors list
	l_Position=l_pDoc->m_OneWireList.GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=l_pDoc->m_OneWireList.GetNext(l_Position);
		l_strTmp.Format("%d",l_pListElement->m_iSensorNumber);
		AddItemToSensorsForLevelEventList(GetResString(IDS_ONE_WIRE),ONE_WIRE_SENSOR_TYPE,l_strTmp,l_pListElement->m_strSensorName);
	}

	//Add the ADC sensors to the sensors list
	l_Position = l_pDoc->m_AdcList.GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pAdcListElement = l_pDoc->m_AdcList.GetNext(l_Position);
		l_strTmp.Format("%d", l_pAdcListElement->m_iSensorNumber);
		AddItemToSensorsForLevelEventList(GetResString(IDS_ADC), ADC_SENSOR_TYPE, l_strTmp, l_pAdcListElement->m_strSensorName);
	}

	//Add the Modbus registers to the sensors list
	l_Position = l_pDoc->m_ModbusList.GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pModbusListElement = l_pDoc->m_ModbusList.GetNext(l_Position);
		AddItemToSensorsForLevelEventList(GetResString(IDS_MODBUS), MODBUS_SENSOR_TYPE, l_pModbusListElement->m_strModbusKey, l_pModbusListElement->m_strRegName);
	}

	//Add the digital inputs that are not defined as counters to the sensors list
	l_Position = l_pDoc->m_ZoneList.GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pZoneListElement = l_pDoc->m_ZoneList.GetNext(l_Position);
		if (l_pZoneListElement->m_enmZoneType == enmZoneTypeRegular)
		{
			l_strTmp.Format("%d", l_pZoneListElement->m_iLocInMem);
			AddItemToSensorsForLevelEventList(GetResString(IDS_ZONE), DIGITAL_INPUT_TYPE, l_strTmp, l_strTmp);
		}
	}

	//Add the Vin and VBat to the level events sensor
	AddItemToSensorsForLevelEventList(GetResString(IDS_INPUT_VOLTAGE), VIN_SENSOR_TYPE, VIN_SENSOR_ID_STR, GetResString(IDS_INPUT_VOLTAGE));
	AddItemToSensorsForLevelEventList(GetResString(IDS_BATT_VOLTAGE), VBATT_SENSOR_TYPE, VBATT_SENSOR_ID_STR, GetResString(IDS_BATT_VOLTAGE));

	//Verify that all the level events have sensor attached
	CLevelEventListElement*l_pLevelEventListElement;
	POSITION l_PosLevelEvent=l_pDoc->m_LevelEventList.GetHeadPosition();
	bool l_bSensorFound;
	while(l_PosLevelEvent!=NULL)
	{
		l_bSensorFound=false;
		l_pLevelEventListElement=l_pDoc->m_LevelEventList.GetNext(l_PosLevelEvent);
		switch(l_pLevelEventListElement->m_iSensorType)	//Find in the proper list according to the sensor type
		{
		case ONE_WIRE_SENSOR_TYPE:
			l_Position=l_pDoc->m_OneWireList.GetHeadPosition();
			while(l_Position!=NULL)
			{
				l_pListElement=l_pDoc->m_OneWireList.GetNext(l_Position);
				if (l_pListElement->m_iSensorNumber==atoi(l_pLevelEventListElement->m_strSensorNum))
				{
					l_bSensorFound=true;
					break;
				}
			}
			break;
		case ADC_SENSOR_TYPE:
			l_Position = l_pDoc->m_AdcList.GetHeadPosition();
			while (l_Position != NULL)
			{
				l_pAdcListElement = l_pDoc->m_AdcList.GetNext(l_Position);
				if (l_pAdcListElement->m_iSensorNumber == atoi(l_pLevelEventListElement->m_strSensorNum))
				{
					l_bSensorFound = true;
					break;
				}
			}
			break;
		case MODBUS_SENSOR_TYPE:
			l_Position = l_pDoc->m_ModbusList.GetHeadPosition();
			while (l_Position != NULL)
			{
				l_pModbusListElement = l_pDoc->m_ModbusList.GetNext(l_Position);
				if (l_pModbusListElement->m_strModbusKey == l_pLevelEventListElement->m_strSensorNum)
				{
					l_bSensorFound = true;
					break;
				}
			}
			break;
		case VIN_SENSOR_TYPE:
		case VBATT_SENSOR_TYPE:
			l_bSensorFound = true;
			break;
		case DIGITAL_INPUT_TYPE:
			l_Position = l_pDoc->m_ZoneList.GetHeadPosition();
			while (l_Position != NULL)
			{
				l_pZoneListElement = l_pDoc->m_ZoneList.GetNext(l_Position);
				l_strTmp.Format("%d", l_pZoneListElement->m_iLocInMem);
				if (l_strTmp == l_pLevelEventListElement->m_strSensorNum)
				{
					if (l_pZoneListElement->m_enmZoneType == enmZoneTypeRegular)
					{
						l_bSensorFound = true;
					}
					break;
				}
			}
			break;
		}
		if (!l_bSensorFound)	//If the correspond sensor hasn't been found, delete this level event
		{
			l_bAllLevelEventsOK = 0;
			l_pDoc->m_LevelEventList.DeleteItem(l_pLevelEventListElement->m_iIndex);
			l_PosLevelEvent = l_pDoc->m_LevelEventList.GetHeadPosition();	//Start from the beginning of the list because we lost the position
		}
	}

	ApplyLevelEventListToScreen();
	return l_bAllLevelEventsOK;
}

void CLevelAlertView::ApplyLevelEventListToScreen()
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::ApplyLevelEventListToScreen"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	POSITION l_Position=l_pDoc->m_LevelEventList.GetHeadPosition();
	CLevelEventListElement *l_pListElement;
	CString l_strSensorNum;
	m_lstLevelEvent.DeleteAllItems();
	//Go over all the level events list and add them to the level event list control
	while(l_Position!=NULL)
	{
		l_pListElement=l_pDoc->m_LevelEventList.GetNext(l_Position);
		AddItemToLevelEventList(l_pListElement->m_iIndex);
	}
	m_lstLevelEvent.SortItems(SimpleSortCompareFunc,0);
}

LRESULT CLevelAlertView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	CString *l_pArrivedRespond;
	
	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if(m_enmLevelEventLastSentCommand!=enmLevelEventLastSentCommandNone)
		{
			m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR | MB_OK);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}
	
	l_pArrivedRespond=(CString*)lParam;
	int l_iRespondStartAt=-1;
	int l_iLastRespondStartAt=0;
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);
	
	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(LEVEL_EVENT_GET_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			CString l_strDataToPars,l_strLevelEvent,l_strField,l_strSensorId,l_strLowHys,l_strHighHys,l_strLowMsg,l_strHighMsg,l_strNormalRange;
			CString l_strRepeatAlert, l_strSensorNum,l_strLowDelay,l_strHighDelay, l_strAlarmKey, l_strLevelAlertKey;
			double l_dLowHys,l_dHighHys;
			int l_inormalRange,l_iRepeatAlert, l_iSensorType,l_iLowDelay,l_iHighDelay;
			COleDateTime l_odtRepeatAlert,l_odtLowDelay,l_odtHighDelay;

			CGsmAlertDoc *l_pDoc=GetDocument();
			

			int l_iDataStartAt=FindInString(*l_pArrivedRespond,LEVEL_EVENT_GET_MSG_RESPOND);
			l_iDataStartAt+=(CString(LEVEL_EVENT_GET_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			l_strDataToPars=l_pArrivedRespond->Mid(l_iDataStartAt);


			l_pDoc->m_LevelEventList.DeleteAllList();
			// last argument is the delimiter
			for(int l_iIndex=0; (l_strDataToPars!=CString("")) && AfxExtractSubString(l_strLevelEvent,l_strDataToPars,l_iIndex,_T(';')) ; ++l_iIndex)
			{
				AfxExtractSubString(l_strSensorId,l_strLevelEvent,0,_T(','));
				AfxExtractSubString(l_strLowHys,l_strLevelEvent,1,_T(','));
				AfxExtractSubString(l_strHighHys,l_strLevelEvent,2,_T(','));
				AfxExtractSubString(l_strLowMsg,l_strLevelEvent,3,_T(','));
				AfxExtractSubString(l_strHighMsg,l_strLevelEvent,4,_T(','));
				AfxExtractSubString(l_strNormalRange,l_strLevelEvent,5,_T(','));
				AfxExtractSubString(l_strRepeatAlert,l_strLevelEvent,6,_T(','));
				AfxExtractSubString(l_strLowDelay,l_strLevelEvent,7,_T(','));
				AfxExtractSubString(l_strHighDelay,l_strLevelEvent,8,_T(','));
				AfxExtractSubString(l_strAlarmKey, l_strLevelEvent, 9, _T(','));
				AfxExtractSubString(l_strLevelAlertKey, l_strLevelEvent, 10, _T(','));
				

				l_dLowHys = atof(l_strLowHys);
				l_dHighHys = atof(l_strHighHys);
				l_inormalRange = atoi(l_strNormalRange);
				l_iRepeatAlert = atoi(l_strRepeatAlert);
				l_odtRepeatAlert.SetTime(l_iRepeatAlert/3600,(l_iRepeatAlert % 3600) / 60, l_iRepeatAlert % 60);
				l_iLowDelay = atoi(l_strLowDelay);
				l_odtLowDelay.SetTime(l_iLowDelay/3600,(l_iLowDelay % 3600) / 60, l_iLowDelay % 60);
				l_iHighDelay = atoi(l_strHighDelay);
				l_odtHighDelay.SetTime(l_iHighDelay/3600,(l_iHighDelay % 3600) / 60, l_iHighDelay % 60);

				if (l_strSensorId.Find(ONE_WIRE_SENSOR_ID_STR)!=-1)
				{
					l_strSensorNum = l_strSensorId.Mid(l_strSensorId.Find(ONE_WIRE_SENSOR_ID_STR) + strlen(ONE_WIRE_SENSOR_ID_STR) + 1);
					l_iSensorType=ONE_WIRE_SENSOR_TYPE;
				}
				else if(l_strSensorId.Find(ADC_SENSOR_ID_STR) != -1)
				{
					l_strSensorNum = l_strSensorId.Mid(l_strSensorId.Find(ADC_SENSOR_ID_STR) + strlen(ADC_SENSOR_ID_STR) + 1);
					l_iSensorType = ADC_SENSOR_TYPE;
				}
				else if (l_strSensorId.Find(MODBUS_SENSOR_ID_STR) != -1)
				{
					l_strSensorNum = l_strSensorId.Mid(l_strSensorId.Find(MODBUS_SENSOR_ID_STR) + strlen(MODBUS_SENSOR_ID_STR) + 1);
					l_iSensorType = MODBUS_SENSOR_TYPE;
				}
				else if (l_strSensorId.Find(VIN_SENSOR_ID_STR) != -1)
				{
					l_strSensorNum = VIN_SENSOR_ID_STR;
					l_iSensorType = VIN_SENSOR_TYPE;
				}
				else if (l_strSensorId.Find(VBATT_SENSOR_ID_STR) != -1)
				{
					l_strSensorNum = VBATT_SENSOR_ID_STR;
					l_iSensorType = VBATT_SENSOR_TYPE;
				}
				else if (l_strSensorId.Find(DIGITAL_INPUT_SENSOR_ID_STR) != -1)
				{
					l_strSensorNum = l_strSensorId.Mid(l_strSensorId.Find(DIGITAL_INPUT_SENSOR_ID_STR) + strlen(DIGITAL_INPUT_SENSOR_ID_STR) + 1);
					l_iSensorType = DIGITAL_INPUT_TYPE;
				}
				l_pDoc->m_LevelEventList.AddItemSorted(l_pDoc->m_LevelEventList.GetCount()+1,l_iSensorType,l_strSensorNum,l_strSensorId,l_dLowHys,l_dHighHys,l_strLowMsg,l_strHighMsg,l_inormalRange,l_odtRepeatAlert,l_odtLowDelay,l_odtHighDelay, l_strAlarmKey, l_strLevelAlertKey,true);
			}

			if (!OnUpdateSensorsList(0,0))
			{
				MessageBox(GetResString(IDS_NOT_ALL_SENSORS_PRESENT));
			}
			l_pDoc->SetModifiedFlag();
			
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
	} while(l_iRespondStartAt!=-1);
	UpdateData(FALSE);
	
	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,CString("exit CLevelAlertView::OnCommandArrived"));

	return 0;
}

void CLevelAlertView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	switch(m_enmLevelEventLastSentCommand)
	{
	case enmLevelEventLastSentCommandLevelEventsGet:
		m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
		EnableControls();
		return;
	case enmLevelEventLastSentCommandLevelEventsSet:
		{
			CGsmAlertDoc *l_pDoc=GetDocument();
			CLevelEventListElement*l_ListElement;
			for(int l_i=1;l_i<=l_pDoc->m_LevelEventList.GetCount();++l_i)
			{
				l_ListElement=l_pDoc->m_LevelEventList.GetItemByIndex(l_i);
				l_ListElement->m_bInUnit=true;
				SetLevelEventItemImage(l_ListElement->m_iIndex,true);
			}
			m_lstLevelEvent.SortItems(SimpleSortCompareFunc,0);
			
			m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
			EnableControls();
			l_pDoc->SetModifiedFlag();
			return;
		}
	}
}

void CLevelAlertView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::EnableControls"));
	
	UpdateData();
	
	m_lstLevelEvent.EnableWindow(a_bEnabled);
	m_lstSensorsForLevelEvent.EnableWindow(a_bEnabled);
	m_txtLowHys.EnableWindow(a_bEnabled);
	m_txtHighHys.EnableWindow(a_bEnabled);
	m_txtLowMsg.EnableWindow(a_bEnabled);
	m_txtHighMsg.EnableWindow(a_bEnabled);
	m_cmbNormalRange.EnableWindow(a_bEnabled);
	m_dtpRepeatAlert.EnableWindow(a_bEnabled);
	m_dtpLowDelay.EnableWindow(a_bEnabled);
	m_dtpHighDelay.EnableWindow(a_bEnabled);

	m_cmdAddLevelEvent.EnableWindow(a_bEnabled);
	m_cmdRemoveLevelEvent.EnableWindow(a_bEnabled);
	m_cmdReplaceLevelEvent.EnableWindow(a_bEnabled);
	m_cmdGetLevelEvents.EnableWindow(a_bEnabled);
	m_cmdSetLevelEvents.EnableWindow(a_bEnabled);
}

void CLevelAlertView::SendSetCommandToUnit(enmLevelEventLastSentCommand &a_enmLevelEventLastSentCommand)
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	l_pNewCommand=new CString();
	
	switch(a_enmLevelEventLastSentCommand)
	{
	case enmLevelEventLastSentCommandLevelEventsGet:
		l_pNewCommand->Format("%s",LEVEL_EVENT_GET_MSG_COMMAND);
		break;
	case enmLevelEventLastSentCommandLevelEventsSet:
		l_pNewCommand->Format("%s",m_strSetString);
		break;
	}
	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandNone;
	}
}

void CLevelAlertView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnInitialUpdate"));
	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnInitialUpdate first run"));
		
		m_bFirstRun=false;
		
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_INDEX_COL_INDEX,GetResString(IDS_INDEX),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_SENSOR_TYPE_COL_INDEX,GetResString(IDS_SENSOR_TYPE),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_SENSOR_NUM_COL_INDEX,GetResString(IDS_SENSOR_NUMBER),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_SENSOR_NAME_COL_INDEX,GetResString(IDS_SENSOR_NAME),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_LOW_HYS_COL_INDEX,GetResString(IDS_LOW_HYS),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_HIGH_HYS_COL_INDEX,GetResString(IDS_HIGH_HYS),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_LOW_MSG_COL_INDEX,GetResString(IDS_LOW_MSG),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_HIGH_MSG_COL_INDEX,GetResString(IDS_HIGH_MSG),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_NORMAL_RANGE_COL_INDEX,GetResString(IDS_NORMAL_RANGE),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_REPEAT_ALERT_COL_INDEX,GetResString(IDS_REPEAT_ALERT),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_LOW_DELAY_COL_INDEX,GetResString(IDS_LOW_DELAY),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_HIGH_DELAY_COL_INDEX,GetResString(IDS_HIGH_DELAY),LVCFMT_LEFT,120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_CLOUD_ALARM_KEY_COL_INDEX, GetResString(IDS_ALARM_KEY), LVCFMT_LEFT, 120);
		m_lstLevelEvent.InsertColumn(LEVEL_EVENT_KEY_COL_INDEX, GetResString(IDS_LEVEL_EVENT_KEY), LVCFMT_LEFT, 120);
				
		m_imlLevelEventView.Create(16,16,ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_NOT_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_SEARCH),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		m_imlLevelEventView.Add(l_hIconNotLoaded_16x16);
		m_imlLevelEventView.Add(l_hIconLoaded_16x16);
		m_imlLevelEventView.Add(l_hIconSearch_16x16 );
		m_lstLevelEvent.SetImageList(&m_imlLevelEventView,LVSIL_SMALL | LVSIL_NORMAL);
		
		//Set the list to be full row selection and with grid lines
		DWORD l_dwStyle=m_lstLevelEvent.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstLevelEvent.SetExtendedStyle(l_dwStyle);

		l_dwStyle=m_lstSensorsForLevelEvent.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstSensorsForLevelEvent.SetExtendedStyle(l_dwStyle);
		
		m_lstSensorsForLevelEvent.InsertColumn(SENSOR_TYPE_COL_INDEX,GetResString(IDS_SENSOR_TYPE),LVCFMT_LEFT,120);
		m_lstSensorsForLevelEvent.InsertColumn(SENSOR_NUM_COL_INDEX,GetResString(IDS_SENSOR_NUMBER),LVCFMT_LEFT,120);
		m_lstSensorsForLevelEvent.InsertColumn(ONE_WIRE_SENSOR_NAME_COL_INDEX,GetResString(IDS_SENSOR_NAME),LVCFMT_LEFT,120);

		int l_iNewIndex = m_cmbNormalRange.AddString(GetResString(IDS_NORMAL_RANGE_LOW));
		m_cmbNormalRange.SetItemData(l_iNewIndex,NORMAL_RANGE_LOW);
		l_iNewIndex = m_cmbNormalRange.AddString(GetResString(IDS_NORMAL_RANGE_HIGH));
		m_cmbNormalRange.SetItemData(l_iNewIndex,NORMAL_RANGE_HIGH);

		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);
		
		Localize();
	}
	CGsmAlertDoc *l_pDoc=GetDocument();

	OnUpdateSensorsList(0,0);
	
	UpdateData(FALSE);
	EnableControls();
}

void CLevelAlertView::Localize()
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::Localize"));
	
	//SetDlgItemText( IDC_CHK_ADC1_NORMA_RANGE_SMS,GetResString(IDS_IDC_CHK_ADC1_NORMA_RANGE_SMS));
}

int CLevelAlertView::AddItemToSensorsForLevelEventList(CString a_strSensorType,int a_iSensorType,CString a_strSensorNum,CString a_strSensorName)
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::AddItemToSensorsForLevelEventList"));

	
	int l_iIndex;
	
	l_iIndex=m_lstSensorsForLevelEvent.InsertItem(SENSOR_TYPE_COL_INDEX,a_strSensorType);
	m_lstSensorsForLevelEvent.SetItemText(l_iIndex,SENSOR_NUM_COL_INDEX,a_strSensorNum);
	m_lstSensorsForLevelEvent.SetItemText(l_iIndex,ONE_WIRE_SENSOR_NAME_COL_INDEX,a_strSensorName);
	m_lstSensorsForLevelEvent.SetItemData(l_iIndex,a_iSensorType);		//Store in the item data the sensor type as integer

	return l_iIndex;
}

void CLevelAlertView::OnCmdAddLevelEvent() 
{	
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCmdAddLevelEvent"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	UpdateData();
	int l_iIndex=l_pDoc->m_LevelEventList.GetCount()+1;
	
	if(l_iIndex>MAX_LEVEL_EVENTS_ALLOWED)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_TO_BIG),NULL,MB_ICONERROR);
		return;
	}

	ApplyLevelEvent(l_iIndex);
}

int CLevelAlertView::AddItemToLevelEventList(int a_iIndex)
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::AddItemToLevelEventList"));
	CLevelEventListElement *l_pListElement = l_pDoc->m_LevelEventList.GetItemByIndex(a_iIndex);
	
	int l_iImage;
	if(l_pListElement->m_bInUnit)
	{
		l_iImage=1;
	}
	else
	{
		l_iImage=0;
	}
	
	CString l_strIndex;
	l_strIndex.Format("%d",a_iIndex);
	
	
	int l_iIndex;
	if((l_iIndex=GetLevelEventItemByIndex(a_iIndex))==-1)
	{
		l_iIndex=m_lstLevelEvent.InsertItem(LEVEL_EVENT_INDEX_COL_INDEX,l_strIndex,l_iImage);
		m_lstLevelEvent.EnsureVisible(l_iIndex,false);
		m_lstLevelEvent.SetItemData(l_iIndex,a_iIndex);
	}
	else
	{
		m_lstLevelEvent.DeleteItem(l_iIndex);
		l_iIndex=m_lstLevelEvent.InsertItem(LEVEL_EVENT_INDEX_COL_INDEX,l_strIndex,l_iImage);
		m_lstLevelEvent.EnsureVisible(l_iIndex,false);
		m_lstLevelEvent.SetItemData(l_iIndex,a_iIndex);
	}

	CString l_strTmp,l_strSensorName;
	
	//Get the sensor type string and its sensor name according to the sensor type and its number
	switch(l_pListElement->m_iSensorType)
	{
	case ONE_WIRE_SENSOR_TYPE:
		l_strTmp.LoadString(IDS_ONE_WIRE);
		l_strSensorName = l_pDoc->m_OneWireList.GetItemBySensorNum(atoi(l_pListElement->m_strSensorNum))->m_strSensorName;
		break;
	case ADC_SENSOR_TYPE:
		l_strTmp.LoadString(IDS_ADC);
		l_strSensorName = l_pDoc->m_AdcList.GetItemBySensorNum(atoi(l_pListElement->m_strSensorNum))->m_strSensorName;
		break;
	case MODBUS_SENSOR_TYPE:
		l_strTmp.LoadString(IDS_MODBUS);
		l_strSensorName = l_pDoc->m_ModbusList.GetItemByKey(l_pListElement->m_strSensorNum)->m_strRegName;
		break;
	case VIN_SENSOR_TYPE:
		l_strTmp.LoadString(IDS_INPUT_VOLTAGE);
		l_strSensorName = VIN_SENSOR_ID_STR;
		break;
	case VBATT_SENSOR_TYPE:
		l_strTmp.LoadString(IDS_BATT_VOLTAGE);
		l_strSensorName = VBATT_SENSOR_ID_STR;
		break;
	case DIGITAL_INPUT_TYPE:
		l_strTmp.LoadString(IDS_ZONE);
		l_strSensorName = l_pListElement->m_strSensorNum;
		break;
	}
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_TYPE_COL_INDEX,l_strTmp);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NUM_COL_INDEX, l_pListElement->m_strSensorNum);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NAME_COL_INDEX, l_strSensorName);
	l_strTmp.Format("%.02f", l_pListElement->m_dLowHys);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_HYS_COL_INDEX,l_strTmp);
	l_strTmp.Format("%.02f", l_pListElement->m_dHighHys);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_HYS_COL_INDEX,l_strTmp);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_MSG_COL_INDEX, l_pListElement->m_strLowMsg);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_MSG_COL_INDEX, l_pListElement->m_strHighMsg);
	switch(l_pListElement->m_iNormalRange)
	{
	case NORMAL_RANGE_LOW:
		l_strTmp.LoadString(IDS_NORMAL_RANGE_LOW);
		break;
	case NORMAL_RANGE_HIGH:
		l_strTmp.LoadString(IDS_NORMAL_RANGE_HIGH);
		break;
	}
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_NORMAL_RANGE_COL_INDEX,l_strTmp);
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_REPEAT_ALERT_COL_INDEX, l_pListElement->m_odtRepeatAlert.Format("%H:%M:%S"));
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_DELAY_COL_INDEX, l_pListElement->m_odtLowDelay.Format("%H:%M:%S"));
	m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_DELAY_COL_INDEX, l_pListElement->m_odtHighDelay.Format("%H:%M:%S"));
	m_lstLevelEvent.SetItemText(l_iIndex, LEVEL_EVENT_CLOUD_ALARM_KEY_COL_INDEX, l_pListElement->m_strCloudAlarmKey);
	m_lstLevelEvent.SetItemText(l_iIndex, LEVEL_EVENT_KEY_COL_INDEX, l_pListElement->m_strLevelEventKey);

	return l_iIndex;
}

int CLevelAlertView::GetLevelEventItemByIndex(int a_iIndex)
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::GetLevelEventItemByIndex"));
	
	CString l_strExistIndex;
	for(int l_iForIndex=0;l_iForIndex<m_lstLevelEvent.GetItemCount();++l_iForIndex)
	{
		l_strExistIndex=m_lstLevelEvent.GetItemText(l_iForIndex,LEVEL_EVENT_INDEX_COL_INDEX);
		if(atoi(l_strExistIndex)==a_iIndex)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

void CLevelAlertView::OnCmdRemoveLevelEvent() 
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCmdRemoveLevelEvent"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	POSITION l_pos = m_lstLevelEvent.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	
	CString l_strIndex;
	int l_iIndex;
	int l_iItem = m_lstLevelEvent.GetNextSelectedItem(l_pos);
	l_strIndex= m_lstLevelEvent.GetItemText(l_iItem,LEVEL_EVENT_INDEX_COL_INDEX);
	l_iIndex = atoi(LPCTSTR(l_strIndex));
	l_pDoc->m_LevelEventList.DeleteItem(l_iIndex);
	ApplyLevelEventListToScreen();
	
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}

void CLevelAlertView::ApplyLevelEvent(int a_iIndex)
{
	CGsmAlertDoc *l_pDoc = GetDocument();

	if (!IsNumeric(m_strLowHys, true))
	{
		MessageBox(GetResString(IDS_LOW_HYS_NOT_OK), NULL, MB_ICONERROR);
		return;
	}
	if (!IsNumeric(m_strHighHys, true))
	{
		MessageBox(GetResString(IDS_HIGH_HYS_NOT_OK), NULL, MB_ICONERROR);
		return;
	}

	if (atof(m_strLowHys) > atof(m_strHighHys))
	{
		MessageBox(GetResString(IDS_LOW_HYS_NOT_OK), NULL, MB_ICONERROR);
		return;
	}

	if (m_iNormlRange == -1)
	{
		MessageBox(GetResString(IDS_NORMAL_RANGE_NOT_SELECTED), NULL, MB_ICONERROR);
		return;
	}

	if ((m_strHighMsg.Find(',') != -1) || (m_strHighMsg.Find(';') != -1) || (m_strLowMsg.Find(',') != -1) || (m_strLowMsg.Find(';') != -1))
	{
		MessageBox(GetResString(IDS_HYS_MSG_ERROR), NULL, MB_ICONERROR);
		return;
	}

	POSITION l_pos = m_lstSensorsForLevelEvent.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		MessageBox(GetResString(IDS_SENSOR_NOT_SELECTED), NULL, MB_ICONERROR);
		return;
	}

	int l_iItem = m_lstSensorsForLevelEvent.GetNextSelectedItem(l_pos);
	int l_iSensorType = m_lstSensorsForLevelEvent.GetItemData(l_iItem);
	CString l_strSensorNum = m_lstSensorsForLevelEvent.GetItemText(l_iItem, SENSOR_NUM_COL_INDEX);
	CString l_strSensorId;

	switch (l_iSensorType)
	{
	case ONE_WIRE_SENSOR_TYPE:
		l_strSensorId.Format("%s-%s", ONE_WIRE_SENSOR_ID_STR, l_strSensorNum);
		break;
	case ADC_SENSOR_TYPE:
		l_strSensorId.Format("%s-%s", ADC_SENSOR_ID_STR, l_strSensorNum);
		break;
	case MODBUS_SENSOR_TYPE:
		l_strSensorId.Format("%s-%s", MODBUS_SENSOR_ID_STR, l_strSensorNum);
		break;
	case VIN_SENSOR_TYPE:
		l_strSensorId = VIN_SENSOR_ID_STR;
		break;
	case VBATT_SENSOR_TYPE:
		l_strSensorId = VBATT_SENSOR_ID_STR;
		break;
	case DIGITAL_INPUT_TYPE:
		l_strSensorId.Format("%s-%s", DIGITAL_INPUT_SENSOR_ID_STR, l_strSensorNum);
		break;
	}

	int l_iNormlRange = m_cmbNormalRange.GetItemData(m_iNormlRange);

	double l_dLowHys, l_dHighHys;
	l_dLowHys = atof(m_strLowHys);
	l_dHighHys = atof(m_strHighHys);

	l_pDoc->m_LevelEventList.AddItemSorted(a_iIndex, l_iSensorType, l_strSensorNum, l_strSensorId, l_dLowHys, l_dHighHys, m_strLowMsg, m_strHighMsg, l_iNormlRange, m_odtRepeatAlert, m_odtLowDelay, m_odtHighDelay, m_strAlarmKey, m_strLevelEventKey, false);
	l_pDoc->SetModifiedFlag();
	AddItemToLevelEventList(a_iIndex);
	m_lstLevelEvent.SortItems(SimpleSortCompareFunc, 0);
	l_iItem = GetLevelEventItemByIndex(a_iIndex);
	m_lstLevelEvent.EnsureVisible(l_iItem, false);

	m_strLowHys = "";
	m_strHighHys = "";
	m_strLowMsg = "";
	m_strHighMsg = "";
	m_iNormlRange = -1;
	m_odtRepeatAlert.SetTime(0, 0, 0);
	m_odtLowDelay.SetTime(0, 0, 0);
	m_odtHighDelay.SetTime(0, 0, 0);
	m_strAlarmKey = "";
	m_strLevelEventKey = "";

	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}
void CLevelAlertView::OnCmdReplaceLevelEvent() 
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCmdReplaceLevelEvent"));
	
	UpdateData();
	
	POSITION l_posLevelEvent = m_lstLevelEvent.GetFirstSelectedItemPosition();
	if (l_posLevelEvent == NULL)
	{
		MessageBox(GetResString(IDS_LEVEL_EVENT_NOT_SELECTED), NULL, MB_ICONERROR);
		return;
	}

	//Get the level event index from the selected list item
	int l_iLevelEventItem = m_lstLevelEvent.GetNextSelectedItem(l_posLevelEvent);
	int l_iIndex = atoi(m_lstLevelEvent.GetItemText(l_iLevelEventItem, LEVEL_EVENT_INDEX_COL_INDEX));

	ApplyLevelEvent(l_iIndex);
}

void CLevelAlertView::SetLevelEventItemImage(int a_iIndex,int a_iInUnit)
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::SetLevelEventItemImage"));
	int l_iImage;
	l_iImage=a_iInUnit;
	
	int l_iIndex;
	if((l_iIndex=GetLevelEventItemByIndex(a_iIndex))==-1)
	{
		return;
	}
	else
	{


		CString l_strIndex=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_INDEX_COL_INDEX);
		CString l_strSensorType=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_SENSOR_TYPE_COL_INDEX);
		CString l_strSensorNum=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NUM_COL_INDEX);
		CString l_strSensorName=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NAME_COL_INDEX);
		CString l_strLowHys=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_LOW_HYS_COL_INDEX);
		CString l_strHighHys=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_HIGH_HYS_COL_INDEX);
		CString l_strLowMsg=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_LOW_MSG_COL_INDEX);
		CString l_strHighMsg=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_HIGH_MSG_COL_INDEX);
		CString l_strNormalRange=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_NORMAL_RANGE_COL_INDEX);
		CString l_strRepeatAlert=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_REPEAT_ALERT_COL_INDEX);
		CString l_strLowDelay=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_HIGH_DELAY_COL_INDEX);
		CString l_strHighDelay=m_lstLevelEvent.GetItemText(l_iIndex,LEVEL_EVENT_LOW_DELAY_COL_INDEX);
		CString l_strAlarmKey= m_lstLevelEvent.GetItemText(l_iIndex, LEVEL_EVENT_CLOUD_ALARM_KEY_COL_INDEX);
		CString l_strLevelEventKey= m_lstLevelEvent.GetItemText(l_iIndex, LEVEL_EVENT_KEY_COL_INDEX);


		m_lstLevelEvent.DeleteItem(l_iIndex);
		l_iIndex=m_lstLevelEvent.InsertItem(l_iIndex,l_strIndex,l_iImage);
		m_lstLevelEvent.SetItemData(l_iIndex,a_iIndex);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_TYPE_COL_INDEX,l_strSensorType);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NUM_COL_INDEX,l_strSensorNum);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_SENSOR_NAME_COL_INDEX,l_strSensorName);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_HYS_COL_INDEX,l_strLowHys);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_HYS_COL_INDEX,l_strHighHys);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_MSG_COL_INDEX,l_strLowMsg);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_MSG_COL_INDEX,l_strHighMsg);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_NORMAL_RANGE_COL_INDEX,l_strNormalRange);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_REPEAT_ALERT_COL_INDEX,l_strRepeatAlert);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_HIGH_DELAY_COL_INDEX,l_strLowDelay);
		m_lstLevelEvent.SetItemText(l_iIndex,LEVEL_EVENT_LOW_DELAY_COL_INDEX,l_strHighDelay);
		m_lstLevelEvent.SetItemText(l_iIndex, LEVEL_EVENT_CLOUD_ALARM_KEY_COL_INDEX, l_strAlarmKey);
		m_lstLevelEvent.SetItemText(l_iIndex, LEVEL_EVENT_KEY_COL_INDEX, l_strLevelEventKey);

		m_lstLevelEvent.EnsureVisible(l_iIndex,false);
	}	
}

void CLevelAlertView::OnClickLstLevelEvents(NMHDR* pNMHDR, LRESULT* pResult) 
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnClickLstLevelEvents"));
	POSITION l_pos = m_lstLevelEvent.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		int l_iItem = m_lstLevelEvent.GetNextSelectedItem(l_pos);	  
		
		int l_iIndex= atoi(m_lstLevelEvent.GetItemText(l_iItem,LEVEL_EVENT_INDEX_COL_INDEX));
		CLevelEventListElement *l_pLevelEventListElement=l_pDoc->m_LevelEventList.GetItemByIndex(l_iIndex);
		SetComboBoxFromItemData(m_cmbNormalRange,l_pLevelEventListElement->m_iNormalRange);

		//Find the correspond sensor and mark it
		for(int l_iForIndex=0;l_iForIndex<m_lstSensorsForLevelEvent.GetItemCount();++l_iForIndex)
		{
			//The sensor numeric type is in the list item data
			if((l_pLevelEventListElement->m_iSensorType == m_lstSensorsForLevelEvent.GetItemData(l_iForIndex))
				&& (  l_pLevelEventListElement->m_strSensorNum == m_lstSensorsForLevelEvent.GetItemText(l_iForIndex,SENSOR_NUM_COL_INDEX)) )
			{
				m_lstSensorsForLevelEvent.SetItemState(l_iForIndex,LVIS_SELECTED,LVIS_SELECTED);
				break;
			}
		}
		
		UpdateData();

		m_strLowHys.Format("%.02f",l_pLevelEventListElement->m_dLowHys);
		m_strHighHys.Format("%.02f",l_pLevelEventListElement->m_dHighHys);
		m_strLowMsg=l_pLevelEventListElement->m_strLowMsg;
		m_strHighMsg=l_pLevelEventListElement->m_strHighMsg;
		m_odtRepeatAlert=l_pLevelEventListElement->m_odtRepeatAlert;
		m_odtLowDelay=l_pLevelEventListElement->m_odtLowDelay;
		m_odtHighDelay=l_pLevelEventListElement->m_odtHighDelay;
		m_strAlarmKey= l_pLevelEventListElement->m_strCloudAlarmKey;
		m_strLevelEventKey= l_pLevelEventListElement->m_strLevelEventKey;

		UpdateData(FALSE);
	}
	if(pResult)
	{
		*pResult = 0;
	}
}

void CLevelAlertView::DeleteLevelEventList()
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_LevelEventList.DeleteAllList();
	m_lstLevelEvent.DeleteAllItems();
	l_pDoc->SetModifiedFlag();
}

void CLevelAlertView::OnCmdGetLevelEvents() 
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCmdGetLevelEvents"));
	if(!(m_pMainFram->m_bConnectedToUnit))
	{
		MessageBox(GetResString(IDS_CONNECTION_ERROR),NULL,MB_ICONERROR);
		return;
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	DeleteLevelEventList();
	
	m_lstLevelEvent.SortItems(SimpleSortCompareFunc,0);
	
	
	UpdateData(FALSE);
	m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandLevelEventsGet;
	SendSetCommandToUnit(m_enmLevelEventLastSentCommand);
	
	EnableControls(FALSE);
}

CString CLevelAlertView::GetLevelAlertsString()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	CLevelEventListElement *l_pListElement;
	CString l_strTmp, l_strSetString;

	int l_i;

	for (int l_i = 0;l_i<l_pDoc->m_LevelEventList.GetCount();++l_i)
	{
		l_pListElement = l_pDoc->m_LevelEventList.GetItemByIndex(l_i + 1);
		l_strTmp.Format("%s,%.02f,%.02f,%s,%s,%d,%d,%d,%d,%s,%s", l_pListElement->m_strSensorId, l_pListElement->m_dLowHys, l_pListElement->m_dHighHys, l_pListElement->m_strLowMsg, l_pListElement->m_strHighMsg, l_pListElement->m_iNormalRange, (l_pListElement->m_odtRepeatAlert.GetHour() * 3600) + (l_pListElement->m_odtRepeatAlert.GetMinute() * 60) + (l_pListElement->m_odtRepeatAlert.GetSecond()), (l_pListElement->m_odtLowDelay.GetHour() * 3600) + (l_pListElement->m_odtLowDelay.GetMinute() * 60) + (l_pListElement->m_odtLowDelay.GetSecond()), (l_pListElement->m_odtHighDelay.GetHour() * 3600) + (l_pListElement->m_odtHighDelay.GetMinute() * 60) + (l_pListElement->m_odtHighDelay.GetSecond()), l_pListElement->m_strCloudAlarmKey, l_pListElement->m_strLevelEventKey);
		if (l_strSetString != "")	//For the second, third,.. items add semi column
		{
			l_strSetString += ";";
		}

		l_strSetString += l_strTmp;
	}

	return CString(LEVEL_EVENT_SET_MSG_COMMAND) + l_strSetString;
}

void CLevelAlertView::OnCmdSetLevelEvents() 
{
	INFO_LOG(g_DbLogger,CString("CLevelAlertView::OnCmdSetLevelEvents"));
	
	m_strSetString = GetLevelAlertsString();

	m_enmLevelEventLastSentCommand=enmLevelEventLastSentCommandLevelEventsSet;
	SendSetCommandToUnit(m_enmLevelEventLastSentCommand);
	
	EnableControls(FALSE);
}

void CLevelAlertView::OnEnChangeTxtLevelAlertLowHys()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}


void CLevelAlertView::OnBnClickedCmdCopyLevelEventsClipboard()
{
	CopyStringToClipbard(GetLevelAlertsString(), GetSafeHwnd());
}
