// GsmAlertDoc.h : interface of the CGsmAlertDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GSM_ALERT_DOC_H__87734A87_5D5E_45D6_9E29_A17ACA241AB5__INCLUDED_)
#define AFX_GSM_ALERT_DOC_H__87734A87_5D5E_45D6_9E29_A17ACA241AB5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "PhoneNumList.h"
#include "ZoneList.h"
#include "OutputList.h"
#include "OneWireList.h"
#include "LevelEventList.h"
#include "ModbusList.h"
#include "AdcList.h"
#include "WirelessList.h"

#define MAX_PHONE_NUM_IN_MEM_DEF 50
#define MAX_ZONES_IN_MEM_DEF 8
#define MAX_OUTPUT_IN_MEM_DEF 4
#define MAX_PHONE_NAME_LEN_DEF 20

#define MAX_GATE_DELAY_LIMIT_DEF 60

#define INPUT_DEBOUNCE_DEF 3
#define INPUT_DEBOUNCE_MIN 1
#define INPUT_DEBOUNCE_MAX 9

#define REMINDER_DELAY_DEF 15
#define REMINDER_DELAY_MIN 1
#define REMINDER_DELAY_MAX 1440

#define REMINDER_ENABLE_DEF 1

#define TEST_ALARM_ENABLE_DEF 1
#define TEST_ALARM_PERIOD_DAILY 0
#define TEST_ALARM_PERIOD_WEEKLY 1
#define TEST_ALARM_PERIOD_DEF TEST_ALARM_PERIOD_DAILY
#define TEST_ALARM_WEEK_DAY_DEF 0	//Sunday
#define TEST_ALARM_TIME_DEF 720	//12:00  --> 12*60 + 0 = 720
#define TEST_ALARM_MSG_DEF "Alarm Test"

#define DEVICE_ID_DEF 1
#define DEVICE_ID_MIN 0
#define DEVICE_ID_MAX 63

#define TEMP_DIFF_DEF 1.1
#define TEMP_DIFF_MIN 0.5
#define TEMP_DIFF_MAX 100

#define SMS_LOGGER_INTERVAL_DEF 1
#define SMS_LOGGER_INTERVAL_MIN 1
#define SMS_LOGGER_INTERVAL_MAX 86400

#define SMS_LOGGER_PASS_DEF "1234"

#define WEEK_DAY_SUNDAY_DEF 0
#define WEEK_DAY_MONDAY_DEF 1
#define WEEK_DAY_TUESDAY_DEF 2
#define WEEK_DAY_WEDNESDAY_DEF 3
#define WEEK_DAY_THURSDAY_DEF 4
#define WEEK_DAY_FRIDAY_DEF 5
#define WEEK_DAY_SATURDAY_DEF 6

#define DEFAULT_GATE_DELAY_DEF 3
#define DEFAULT_UNIT_PASS_DEF "1234"
#define DEFAULT_ZONE_ACTIVE_MODE 0

#define DEFAULT_ZONE_MSG_NO_TO_NC "Input %d activated"
#define DEFAULT_ZONE_MSG_NC_TO_NO "Input %d deactivated"
#define DEFAULT_ZONE_CNTR_GAP 100

#define DEFAULT_ZONE_DELAY 3
#define MAX_ZONE_DELAY 999999


#define DEFAULT_OUTPUT_STATE 1
#define DEFAULT_OUTPUT_STATE_PARAM 0

#define DEFAULT_OUTPUT_1_STATE 4
#define DEFAULT_OUTPUT_1_STATE_PARAM 10

#define CMB_TYPE_MASTER_DEF 1
#define CMB_TYPE_SECOND_DEF 2
#define CMB_TYPE_MACHINE_DEF 3

#define CMB_OUTPUT_STATE_BY_REQ	2
#define CMB_OUTPUT_STATE_ZONE_DETECT 3
#define CMB_OUTPUT_STATE_ALARM 4
#define CMB_OUTPUT_STATE_BUZZER 5

#define CMB_OUTPUT_STATE_HIGTH_LOW 1
#define CMB_OUTPUT_STATE_BY_REQ	2
#define CMB_OUTPUT_STATE_ZONE_DETECT 3
#define CMB_OUTPUT_STATE_ALARM 4
#define CMB_OUTPUT_STATE_BUZZER 5

#define GSM900_DCS1800_DEF 0
#define GSM900_PCS1900_DEF 1
#define GSM850_PCS1800_DEF 2
#define GSM850_PCS1900_DEF 3

#define MAX_1_WIRE_SENSORS 4
#define MAX_ADC_SENSORS 4

#define ONE_WIRE_SENSOR_TYPE 0
#define ADC_SENSOR_TYPE 1
#define RF_SENSOR_TYPE 2
#define MODBUS_SENSOR_TYPE 2
#define VIN_SENSOR_TYPE 3
#define VBATT_SENSOR_TYPE 4
#define DIGITAL_INPUT_TYPE 5

#define ONE_WIRE_SENSOR_ID_STR "1W"
#define ADC_SENSOR_ID_STR "ADC"
#define MODBUS_SENSOR_ID_STR "MDBS"
#define VIN_SENSOR_ID_STR "Vin"
#define VBATT_SENSOR_ID_STR "VBatt"
#define DIGITAL_INPUT_SENSOR_ID_STR "DIN"

#define MAX_LEVEL_EVENTS_ALLOWED 100

#define CLOUD_DNS_DEF "api.deviceWISE.com"
#define APN_DEF "internet"
#define GPRS_USER_DEF ""
#define GPRS_PASS_DEF ""
#define CLOUD_TOKEN_DEF ""
#define CLOUD_INTERVAL_DEF 300

#define BAUD_RATE_300 300
#define BAUD_RATE_1200 1200
#define BAUD_RATE_2400 2400
#define BAUD_RATE_4800 4800
#define BAUD_RATE_9600 9600
#define BAUD_RATE_19200 19200
#define BAUD_RATE_38400 38400
#define BAUD_RATE_57600 57600
#define BAUD_RATE_115200 115200

#define CHAR_SET_STR_8N1 "8N1"
#define CHAR_SET_STR_8N2 "8N2"
#define CHAR_SET_STR_8E1 "8E1"
#define CHAR_SET_STR_8E2 "8E2"
#define CHAR_SET_STR_8O1 "8O1"
#define CHAR_SET_STR_8O2 "8O2"
#define CHAR_SET_STR_7N1 "7N1"
#define CHAR_SET_STR_7N2 "7N2"
#define CHAR_SET_STR_7E1 "7E1"
#define CHAR_SET_STR_7E2 "7E2"
#define CHAR_SET_STR_7O1 "7O1"
#define CHAR_SET_STR_7O2 "7O2"

#define CHAR_SET_INT_8N1 0
#define CHAR_SET_INT_8N2 1
#define CHAR_SET_INT_8E1 2
#define CHAR_SET_INT_8E2 3
#define CHAR_SET_INT_8O1 4
#define CHAR_SET_INT_8O2 5
#define CHAR_SET_INT_7N1 6
#define CHAR_SET_INT_7N2 7
#define CHAR_SET_INT_7E1 8
#define CHAR_SET_INT_7E2 9
#define CHAR_SET_INT_7O1 10
#define CHAR_SET_INT_7O2 11

#define MODBUS_BAUD_RATE_DEF BAUD_RATE_9600
#define MODBUS_CHAR_SET_DEF CHAR_SET_STR_8N1
#define MODBUS_MAX_BLOCK_SIZE_DEF 99

class CGsmAlertDoc : public CDocument
{
protected: // create from serialization only
	CGsmAlertDoc();
	DECLARE_DYNCREATE(CGsmAlertDoc)

// Attributes
public:
	//CMiscSettingsView
	CString m_strUnitName;
	CString m_strUnitPass;
	CString m_strTempUnitPass;
	bool m_bSavePass;
	int m_iGateDelay;
	CString m_strSelfPhoneNum;
	bool m_bSelfCuSmsEn;
	int m_iBandSelect;
	int m_iZoneActiveMode;
	int m_iInputDebounce;
	bool m_bReminderEnabled;
	int m_iReminderDelay;
	bool m_bTestAlarmEnabled;
	int m_iTestAlarmWeekDay;
	int m_iTestAlarmTime;	
	CString m_strTestAlarmMsg;
	int m_iSmsLoggerDeviceId;
	float m_fSmsLoggerTempDiff;
	int m_iSmsLoggerInterval;
	CString m_strSmsLoggerPass;

	bool m_bGprsEn;
	CString m_strApn;
	CString m_strGprsUser;
	CString m_strGprsPass;
	CString m_strCloudToken;
	CString m_strCloudDns;
	int m_iCloudInterval;
	bool m_bUseSsl;

	bool m_bRssiToCloud;


	//CAdcView
	//1-wire sensors
	COneWireList m_OneWireList;
	CAdcList m_AdcList;

	//Input Voltage
	BOOL m_bSendVinToCloud;
	BOOL m_bSendVBatToCloud;
	BOOL m_bSendVinAlarmToCloud;


	//CPhoneListView
	CPhoneNumList m_PhoneNumList;
	int m_iMaxPhoneNumAmount;
	int m_iMaxZonesAmount;
	int m_iMaxOutputsAmount;

	//Zones and output view
	CZoneList m_ZoneList;
	COutputList m_OutputList;

	//Level events view
	CLevelEventList m_LevelEventList;

	//Modbus view
	CModbusList m_ModbusList;
	int m_iModbusBaudRate;
	CString m_strModbusCharSet;
	int m_iModbusMaxBlockSize;

	//Wireless view
	BOOL m_bRfActive;
	CWirelessList m_WirelessList;

	//The comport
	CString m_strCom;
	bool m_bUseModem;
	CString m_strUnitPhoneNumber;

	//General
	bool m_bGetDataOnCon;

// Operations
public:
	CString GetTypeNameFromNum(int a_iTypeNum);
	CString GetOutputStateNameFromNum(int a_iOutputStateNum);
	CString GetInputTypeNameFromNum(enmZoneType a_iInputType);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmAlertDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGsmAlertDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGsmAlertDoc)
		afx_msg void OnFileSave();
		afx_msg void OnFileSaveAs();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSM_ALERT_DOC_H__87734A87_5D5E_45D6_9E29_A17ACA241AB5__INCLUDED_)