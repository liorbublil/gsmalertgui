// PhoneNumList.h: interface for the CPhoneNumList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PHONENUMLIST_H__03901DA9_6126_472C_BD84_153F48EB5DE1__INCLUDED_)
#define AFX_PHONENUMLIST_H__03901DA9_6126_472C_BD84_153F48EB5DE1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

class CPhoneNumListElement : public CObject
{
public:
	CPhoneNumListElement();
	~CPhoneNumListElement();
	void operator=(const CPhoneNumListElement& a_PhoneNumListElement);   // = operator

	int m_iLocInMem;
	CString m_strPhoneNum;
	CString m_strPhoneName;
	int m_iType;
	bool m_bInUnit;
};

class CPhoneNumList : public CTypedPtrList< CObList, CPhoneNumListElement* > 
{
public:
	DECLARE_SERIAL( CPhoneNumList )

	CPhoneNumList();
	virtual ~CPhoneNumList();
	void Serialize( CArchive& archive );
	
	void DeleteAllList();
	void AddItemSorted(int a_iLocInMem,CString a_strPhoneNum,CString a_strPhoneName,int a_iType,bool a_bInUnit,bool a_bNoSearch);
	void DeleteItem(int a_iLocInMem);
	CPhoneNumListElement* GetItemByPhoneNum(CString a_strPhoneNum);
	CPhoneNumListElement* GetItemByLocInMem(int a_iLocInMem);
	CPhoneNumListElement* GetItemByPhoneName(CString a_strPhoneName);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_PHONENUMLIST_H__03901DA9_6126_472C_BD84_153F48EB5DE1__INCLUDED_)
