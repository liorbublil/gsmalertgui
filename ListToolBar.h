/*********************************************************
* List-Style Toolbar
* Version: 1.0
* Date: June 3, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* You may freely use and modify this code, but don't remove
* this copyright note.
*
* There is no warranty of any kind, express or implied, for this class.
* The author does not take the responsibility for any damage
* resulting from the use of it.
*
* Let me know if you find this code useful, and
* send me any modifications and bug reports.
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#pragma once

#include "AlphaImageList.h"

class CListToolBar : public CToolBarCtrl
{
public:
	CListToolBar();
	virtual ~CListToolBar();

public:
	// Create the toolbar
	BOOL Create(CWnd* pParentWnd, UINT nID=0);

	// Load toolbar from toolbar resource
	BOOL LoadToolBar(UINT nID, int nStyle=AILS_OLD);
	
	// Resize the toolbar
	void UpdateSettings();

protected:
	virtual void OnUpdateCmdUI(CFrameWnd* pTarget, BOOL bDisableIfNoHndler);

protected:
	CAlphaImageList m_ImgList;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnIdleUpdateCmdUI(WPARAM wParam, LPARAM);
};
