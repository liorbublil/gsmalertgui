#if !defined(AFX_ZONE_LIST_VIEW_H__1852A359_B6EB_490A_B92D_32AFC5C0E779__INCLUDED_)
#define AFX_ZONE_LIST_VIEW_H__1852A359_B6EB_490A_B92D_32AFC5C0E779__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ZoneListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CZoneListView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "GsmAlertDoc.h"
#include "afxwin.h"
#include "afxcmn.h"

class CZoneListView : public CFormView
{
protected:
	CZoneListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CZoneListView)
public:
	enum enmZoneListLastSentCommand
	{
		enmZoneListLastSentCommandNone,
		enmZoneListLastSentCommandZonesAsk,
		enmZoneListLastSentCommandZonesSetAll,
		enmZoneListLastSentCommandQucikCompare,
		enmZoneListLastSentCommandQuryInputs,
		enmZoneListLastSentCommandOutputsAsk,
		enmZoneListLastSentCommandOutputsSetAll,
		enmZoneListLastSentCommandOutputsQucikCompare,
	};

// Form Data
public:
	//{{AFX_DATA(CZoneListView)
	enum { IDD = IDD_ZONE_LIST_FORM };
	CStatic	m_lblOutputStateParamFormat;
	CStatic	m_lblZoneDelayFormat;
	CButton	m_cmdQuickCompareOutputs;
	CButton	m_cmdDownloadOutputsToUnit;
	CButton	m_cmdSetOutput;
	CButton	m_cmdGetOutputs;
	CProgressCtrl	m_prgsOutputsDownload;
	CComboBox	m_cmbOutputState;
	CStatic	m_lblOutputState;
	CEdit	m_txtOutputStateParam;
	CStatic	m_lblOutputStateParam;
	CEdit	m_txtOutputLocInMem;
	CStatic	m_lblOutputLocInMem;
	CButton	m_framOutputs;
	CListCtrl	m_lstOutputs;

	CStatic	m_lblLocInMem;
	CEdit	m_txtLocInMem;
	CStatic	m_lblZoneMsgNoToNc;
	CEdit	m_txtZoneMsgNoToNc;
	CStatic	m_lblZoneMsgNcToNo;
	CEdit	m_txtZoneMsgNcToNo;
	CStatic	m_lblZoneDelayNoToNc;
	CEdit	m_txtZoneDelayNoToNc;
	CStatic	m_lblZoneDelayNcToNo;
	CEdit	m_txtZoneDelayNcToNo;
	CButton	m_chkZoneActiveNoToNc;
	CButton	m_chkZoneActiveNcToNo;

	CButton	m_cmdExcelImport;
	CButton	m_cmdQuickCompare;
	CButton	m_framSpSettings;
	CProgressCtrl	m_prgsZonesDownload;
	CButton	m_cmdDownloadToUnit;
	CButton	m_cmdSet;
	CButton	m_framZones;
	CButton	m_cmdGetZones;
	CListCtrl	m_lstZones;

	CString	m_strLocInMem;
	CString	m_strZoneMsgNoToNc;
	CString	m_strZoneMsgNcToNo;
	CString	m_strZoneDelayNoToNc;
	CString	m_strZoneDelayNcToNo;
	BOOL	m_bZoneActiveNoToNc;
	BOOL	m_bZoneActiveNcToNo;

	int m_iOutputState;
	CString	m_strOutputStateParam;
	CString	m_strOutputLocInMem;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	
	CMainFrame *m_pMainFram;
	CLevelAlertView *m_pLevelEventView;
	enmZoneListLastSentCommand m_enmZoneListLastSentCommand;
	CImageList m_imlZones;
	POSITION m_posLastSentNum;
	int m_iLastLocInMem;
	int m_iTotalPrgrsPos;
	int m_iCurrentPrgrsPos;
	bool m_bHashOK;
	int m_iHashFailedTimes;
	CString m_strLastZonesData;
	int m_iMaxZonesAmount;

	CString m_strZoneIndexToPinNum[MAX_ZONES_IN_MEM_DEF];
	CString m_strOutputIndexToPinNum[MAX_OUTPUT_IN_MEM_DEF];
// Operations
public:
protected:
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmZoneListLastSentCommand &a_enmZoneListLastSentCommand);
	int GetZoneItemByLocInMem(int a_iLocInMem);
	int GetOutputItemByLocInMem(int a_iLocInMem);
	//Add item to the phone list and return the index
	int AddItemToZoneList(int a_iLocInMem);
	int AddItemToOutputList(int a_iLocInMem,int a_iOutputState,int a_iOutputStateParam,bool a_bInUnit,bool a_bNoSearch);
	void Localize();
	void EnableControls(BOOL a_bEnabled=TRUE);
	void SetItemImage(int a_iLocInMem,int a_iInUnit);
	void SetOutputItemImage(int a_iLocInMem,int a_iInUnit);
	void BlinkItem(int a_iLocInMem,bool a_bInUnit);
	void ApplyVarTypeFromInputType(bool a_bForace);
	CString GetZonesString();

// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CZoneListView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CZoneListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	// Generated message map functions
	//{{AFX_MSG(CZoneListView)
	afx_msg void OnCmdSet();
	afx_msg void OnClickLstZones(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetZones();
	afx_msg void OnCmdDownloadToUnit();
	afx_msg void OnCmdQuickCompare();
	afx_msg void OnClickLstOutputs(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCmdSetOutput();
	afx_msg void OnGetOutputs();
	afx_msg void OnCmdQuickCompareOutputs();
	afx_msg void OnCmdDownloadOutputsToUnit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	// Describe what kind of input is it, regular digital or counter
	CComboBox m_cmbInputType;
	int m_iInputType;
	afx_msg void OnCbnSelchangeCmbInputType();
	CEdit m_txtZoneCntrGap;
	CString m_strZoneCntrGap;
	CListCtrl m_lstZonesQuery;
	afx_msg void OnBnClickedCmdQueryInputs();
	afx_msg void OnLvnItemchangedLstInputsQuery(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCmdCopyZonesClipboard();
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CZoneListView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ZONE_LIST_VIEW_H__1852A359_B6EB_490A_B92D_32AFC5C0E779__INCLUDED_)
