// OneWireList.cpp: implementation of the COneWireList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gsmalert.h"
#include "OneWireList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COneWireListElement::COneWireListElement()
{
}

COneWireListElement::~COneWireListElement()
{
}

void COneWireListElement::operator=(const COneWireListElement& a_OneWireListElement)
{
	m_bInUnit=a_OneWireListElement.m_bInUnit;
	m_iSensorNumber=a_OneWireListElement.m_iSensorNumber;
	m_strSensorName=a_OneWireListElement.m_strSensorName;
	m_strDisconnectedMgs=a_OneWireListElement.m_strDisconnectedMgs;
	m_strResumeConnectMgs=a_OneWireListElement.m_strResumeConnectMgs;
}

COneWireList::COneWireList()
{

}

void COneWireList::DeleteAllList()
{
	COneWireListElement *l_pElementToRemove;
	
	while(!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove=RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}


COneWireList::~COneWireList()
{
	DeleteAllList();
}

void COneWireList::Serialize( CArchive& archive )
{
	COneWireListElement *l_pListElement;
    //CObject::Serialize( archive );
	
    // now do the stuff for our specific class
    if( archive.IsStoring() )
	{
		POSITION l_Position;
		
        archive << GetCount();
		
		l_Position=GetHeadPosition();
		while(l_Position!=NULL)
		{
			l_pListElement=GetNext(l_Position);
			archive << l_pListElement->m_iSensorNumber << l_pListElement->m_strSensorName <<l_pListElement->m_strDisconnectedMgs<<l_pListElement->m_strResumeConnectMgs<< l_pListElement->m_bInUnit;
		}
	}
    else
	{
		int l_iListCount;
		DeleteAllList();
		
		archive >> l_iListCount;
		for(int l_iIndex=0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement=new COneWireListElement ;
			archive >> l_pListElement->m_iSensorNumber >> l_pListElement->m_strSensorName >>l_pListElement->m_strDisconnectedMgs>>l_pListElement->m_strResumeConnectMgs>> l_pListElement->m_bInUnit;
			
			AddTail(l_pListElement);
		}
	}
}

void COneWireList::AddItemSorted(int a_iSensorNum,CString a_strSensorName,CString a_strDisconnectedMgs,CString a_strResumeConnectMgs,bool a_bInUnit,bool a_bNoSearch)
{
	COneWireListElement *l_pListElement;
	bool l_bNew=false;
	if(a_bNoSearch)
	{
		l_bNew=true;
		l_pListElement=new COneWireListElement;
	}
	else
	{
		if((l_pListElement=GetItemBySensorNum(a_iSensorNum))==NULL)
		{
			l_bNew=true;
			l_pListElement=new COneWireListElement;
		}
	}
	
	l_pListElement->m_iSensorNumber=a_iSensorNum;
	l_pListElement->m_strSensorName=a_strSensorName;
	l_pListElement->m_strDisconnectedMgs=a_strDisconnectedMgs;
	l_pListElement->m_strResumeConnectMgs=a_strResumeConnectMgs;
	l_pListElement->m_bInUnit=a_bInUnit;

	if(l_bNew)
	{
		AddTail(l_pListElement);
	}
}

void COneWireList::DeleteItem(int a_iSensorNum)
{
	POSITION l_Position;
	COneWireListElement *l_pListElement;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetAt(l_Position);
		if(l_pListElement->m_iSensorNumber==a_iSensorNum)
		{
			RemoveAt(l_Position);
			delete l_pListElement;
			return;
		}
		GetNext(l_Position);
	}
}

COneWireListElement* COneWireList::GetItemBySensorNum(int a_iSensorNum)
{
	POSITION l_Position;
	COneWireListElement*l_pListElement;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iSensorNumber==a_iSensorNum)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

COneWireListElement* COneWireList::GetItemByPos(int a_iPos)
{
	POSITION l_Position;
	COneWireListElement*l_pListElement = NULL;
	int l_iPos;

	for (l_Position = GetHeadPosition(), l_iPos = 0;(l_iPos < a_iPos) && (l_Position != NULL) ;++l_iPos)
	{
		l_pListElement = GetNext(l_Position);
	}
	return l_pListElement;
}

int COneWireList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	COneWireListElement *l_pListElement;
	int l_iCounter=0;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_bInUnit==false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(COneWireList,  CObList, 0)
