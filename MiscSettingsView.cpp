// CellSettingsView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MainFrm.h"
#include "MiscSettingsView.h"
#include "Language.h"
#include "InputDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_UNIT_NAME_LEN_DEF 30
#define MAX_GATE_DELAY_LEN_DEF 2

/////////////////////////////////////////////////////////////////////////////
// CMiscSettingsView

IMPLEMENT_DYNCREATE(CMiscSettingsView, CFormView)

CMiscSettingsView::CMiscSettingsView()
	: CFormView(CMiscSettingsView::IDD)
	, m_strApn(_T(""))
	, m_strCloudDns(_T(""))
	, m_strCloudToken(_T(""))
	, m_strGprsPass(_T(""))
	, m_strGprsUser(_T(""))
	, m_bGprsEn(FALSE)
	, m_strCloudInterval(_T(""))
	, m_bUseSsl(FALSE)
	, m_bRssiToCloud(FALSE)
{
	//{{AFX_DATA_INIT(CMiscSettingsView)
	m_odtUnitTime = COleDateTime::GetCurrentTime();
	m_strNewPass = _T("");
	m_strCurrentPass = _T("");
	m_strUnitName = _T("");
	m_strGateDelay = _T("");
	m_strConfirmPass = _T("");
	m_bSelfCuSmsEn = FALSE;
	m_strSelfPhoneNum = _T("");
	m_iZoneActiveMode = -1;
	m_strVersion = _T("");
	m_strReception = _T("");
	m_strReminderDelay = _T("");
	m_bReminderEnable = FALSE;
	m_bTestAlarmEnable = FALSE;
	m_iTestAlarmPeriod = -1;
	m_iTestAlarmWeekDay = -1;
	m_strTestAlarmMsg = _T("");
	m_strDeviceId = _T("");
	m_strTempDiff = _T("");
	m_strSmsInterval = _T("");
	m_strSmsPass = _T("");
	//}}AFX_DATA_INIT
	m_odtTestAlarmTime.SetTime(0,0,0);
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
	m_bFirstRun=true;
}

CMiscSettingsView::~CMiscSettingsView()
{
}

void CMiscSettingsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMiscSettingsView)
	DDX_Control(pDX, IDC_CMD_SMS_TO_LOGGER_SET, m_cmdSmsToLoggerSet);
	DDX_Control(pDX, IDC_CMD_SMS_TO_LOGGER_GET, m_cmdSmsToLoggerGet);
	DDX_Control(pDX, IDC_TXT_SMS_PASS, m_txtSmsPass);
	DDX_Control(pDX, IDC_TXT_SMS_INTERVAL, m_txtSmsInterval);
	DDX_Control(pDX, IDC_TXT_TEMP_DIFF, m_txtTempDiff);
	DDX_Control(pDX, IDC_TXT_DEVICE_ID, m_txtDeviceId);
	DDX_Control(pDX, IDC_TXT_TEST_ALARM_MSG, m_txtTestAlarmMsg);
	DDX_Control(pDX, IDC_DTP_TEST_ALARM_TIME, m_dtpTestAlarmTime);
	DDX_Control(pDX, IDC_CMB_TEST_ALARM_WEEK_DAY, m_cmbTestAlarmWeekDay);
	DDX_Control(pDX, IDC_RDO_DAILY_TEST_ALARM, m_rdoDailyTestAlarm);
	DDX_Control(pDX, IDC_RDO_WEELY_TEST_ALARM, m_rdoWeeklyTestAlarm);
	DDX_Control(pDX, IDC_CHK_TEST_ALARM, m_chkTestAlarm);
	DDX_Control(pDX, IDC_TXT_REMINDER_DELAY, m_txtReminderDelay);
	DDX_Control(pDX, IDC_CHK_REMINDER, m_chkReminder);
	DDX_Control(pDX, IDC_CMD_REMINDER_GET, m_cmdReminderGet);
	DDX_Control(pDX, IDC_CMD_REMINDER_SET, m_cmdReminderSet);
	DDX_Control(pDX, IDC_CMD_GET_RECEPTION, m_cmdGetReception);
	DDX_Control(pDX, IDC_CMD_GET_VERSION, m_cmdGetVersion);
	DDX_Control(pDX, IDC_TXT_RECEPTION, m_txtReception);
	DDX_Control(pDX, IDC_TXT_VERSION, m_txtVersion);
	DDX_Control(pDX, IDC_CMD_SET_ZONE_ACTIVE_MODE, m_cmdSetZoneActiveMode);
	DDX_Control(pDX, IDC_CMD_GET_ZONE_ACTIVE_MODE, m_cmdGetZoneActiveMode);
	DDX_Control(pDX, IDC_RDO_ZONE_ACTIVE_MOD_EN_PIN, m_rdoZoneActiveModeEnPin);
	DDX_Control(pDX, IDC_RDO_ZONE_ACTIVE_MOD_RING, m_rdoZoneActiveModeRing);
	DDX_Control(pDX, IDC_RDO_ZONE_ACTIVE_MOD_SMS, m_rdoZoneActiveModeSms);
	DDX_Control(pDX, IDC_TXT_SELF_PHONE_NUM, m_txtSelfPhoneNum);
	DDX_Control(pDX, IDC_CMD_SELF_CU_SMS_SET, m_cmdSelfCuSmsSet);
	DDX_Control(pDX, IDC_CMD_SELF_CU_SMS_GET, m_cmdSelfCuSmsGet);
	DDX_Control(pDX, IDC_CHK_SELF_CU_SMS, m_chkSelfCuSms);
	DDX_Control(pDX, IDC_CMD_GET_PC_TIME, m_cmdGetPcTime);
	DDX_Control(pDX, IDC_TXT_CONFIRM_PASS, m_txtConfirmPass);
	DDX_Control(pDX, IDC_DTP_UNIT_TIME, m_dtpUnitTime);
	DDX_Control(pDX, IDC_TXT_GATE_DELAY, m_txtGateDelay);
	DDX_Control(pDX, IDC_TXT_UNIT_NAME, m_txtUnitName);
	DDX_Control(pDX, IDC_TXT_NEW_PASS, m_txtNewPass);
	DDX_Control(pDX, IDC_TXT_PASSWORD, m_txtCurrentPass);
	DDX_Control(pDX, IDC_CMD_SET_UNIT_NAME, m_cmdSetUnitName);
	DDX_Control(pDX, IDC_CMD_GET_UNIT_NAME, m_cmdGetUnitName);
	DDX_Control(pDX, IDC_CMD_SET_UNIT_PASS, m_cmdSetUnitPass);
	DDX_Control(pDX, IDC_CMD_SET_UNIT_TIME, m_cmdSetUnitTime);
	DDX_Control(pDX, IDC_CMD_SET_GATE_DELAY, m_cmdSetGateDelay);
	DDX_Control(pDX, IDC_CMD_GET_GATE_DELAY, m_cmdGetGateDelay);
	DDX_DateTimeCtrl(pDX, IDC_DTP_UNIT_TIME, m_odtUnitTime);
	DDX_Text(pDX, IDC_TXT_NEW_PASS, m_strNewPass);
	DDV_MaxChars(pDX, m_strNewPass, MAX_PASS_LEN_DEF);
	DDX_Text(pDX, IDC_TXT_PASSWORD, m_strCurrentPass);
	DDV_MaxChars(pDX, m_strCurrentPass, MAX_PASS_LEN_DEF);
	DDX_Text(pDX, IDC_TXT_UNIT_NAME, m_strUnitName);
	DDV_MaxChars(pDX, m_strUnitName, MAX_UNIT_NAME_LEN_DEF);
	DDX_Text(pDX, IDC_TXT_GATE_DELAY, m_strGateDelay);
	DDX_Text(pDX, IDC_TXT_CONFIRM_PASS, m_strConfirmPass);
	DDV_MaxChars(pDX, m_strConfirmPass, MAX_PASS_LEN_DEF);
	DDX_Check(pDX, IDC_CHK_SELF_CU_SMS, m_bSelfCuSmsEn);
	DDX_Text(pDX, IDC_TXT_SELF_PHONE_NUM, m_strSelfPhoneNum);
	DDX_Radio(pDX, IDC_RDO_ZONE_ACTIVE_MOD_EN_PIN, m_iZoneActiveMode);
	DDX_Text(pDX, IDC_TXT_VERSION, m_strVersion);
	DDX_Text(pDX, IDC_TXT_RECEPTION, m_strReception);
	DDX_Text(pDX, IDC_TXT_REMINDER_DELAY, m_strReminderDelay);
	DDV_MaxChars(pDX, m_strReminderDelay, 4);
	DDX_Check(pDX, IDC_CHK_REMINDER, m_bReminderEnable);
	DDX_Check(pDX, IDC_CHK_TEST_ALARM, m_bTestAlarmEnable);
	DDX_Radio(pDX, IDC_RDO_DAILY_TEST_ALARM, m_iTestAlarmPeriod);
	DDX_CBIndex(pDX, IDC_CMB_TEST_ALARM_WEEK_DAY, m_iTestAlarmWeekDay);
	DDX_DateTimeCtrl(pDX, IDC_DTP_TEST_ALARM_TIME, m_odtTestAlarmTime);
	DDX_Text(pDX, IDC_TXT_TEST_ALARM_MSG, m_strTestAlarmMsg);
	DDV_MaxChars(pDX, m_strTestAlarmMsg, 50);
	DDX_Text(pDX, IDC_TXT_DEVICE_ID, m_strDeviceId);
	DDV_MaxChars(pDX, m_strDeviceId, 3);
	DDX_Text(pDX, IDC_TXT_TEMP_DIFF, m_strTempDiff);
	DDV_MaxChars(pDX, m_strTempDiff, 6);
	DDX_Text(pDX, IDC_TXT_SMS_INTERVAL, m_strSmsInterval);
	DDV_MaxChars(pDX, m_strSmsInterval, 10);
	DDX_Text(pDX, IDC_TXT_SMS_PASS, m_strSmsPass);
	DDV_MaxChars(pDX, m_strSmsPass, 4);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_TXT_APN, m_txtApn);
	DDX_Text(pDX, IDC_TXT_APN, m_strApn);
	DDX_Control(pDX, IDC_TXT_CLOUD_DNS, m_txtCloudDns);
	DDX_Text(pDX, IDC_TXT_CLOUD_DNS, m_strCloudDns);
	DDX_Control(pDX, IDC_TXT_CLOUD_TOKEN, m_txtCloudToken);
	DDX_Text(pDX, IDC_TXT_CLOUD_TOKEN, m_strCloudToken);
	DDX_Control(pDX, IDC_TXT_GPRS_PASS, m_txtGprsPass);
	DDX_Text(pDX, IDC_TXT_GPRS_PASS, m_strGprsPass);
	DDX_Control(pDX, IDC_TXT_GPRS_USER, m_txtGprsUser);
	DDX_Text(pDX, IDC_TXT_GPRS_USER, m_strGprsUser);
	DDX_Control(pDX, IDC_CHK_GPRS_EN, m_chkGprsEn);
	DDX_Check(pDX, IDC_CHK_GPRS_EN, m_bGprsEn);
	DDX_Control(pDX, IDC_TXT_CLOUD_INTERVAL, m_txtCloudInterval);
	DDX_Text(pDX, IDC_TXT_CLOUD_INTERVAL, m_strCloudInterval);
	DDX_Control(pDX, IDC_CHK_SSL_EN, m_chkUseSsl);
	DDX_Check(pDX, IDC_CHK_SSL_EN, m_bUseSsl);
	DDX_Check(pDX, IDC_CHK_RSSI_TO_CLOUD, m_bRssiToCloud);
	DDX_Control(pDX, IDC_CMD_RSSI_TO_CLOUD_GET, m_cmdRssiToCloudGet);
	DDX_Control(pDX, IDC_CMD_RSSI_TO_CLOUD_SET, m_cmdRssiToCloudSet);
}

BEGIN_MESSAGE_MAP(CMiscSettingsView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CMiscSettingsView)
	ON_BN_CLICKED(IDC_CMD_GET_PC_TIME, OnCmdSetUnitCurrentTime)
	ON_BN_CLICKED(IDC_CMD_SET_UNIT_TIME, OnCmdSetUnitTime)
	ON_EN_CHANGE(IDC_TXT_PASSWORD, OnChangeTxtCurrentPass)
	ON_BN_CLICKED(IDC_CMD_SET_UNIT_PASS, OnCmdSetUnitPass)
	ON_BN_CLICKED(IDC_CMD_GET_UNIT_NAME, OnCmdGetUnitName)
	ON_BN_CLICKED(IDC_CMD_SET_UNIT_NAME, OnCmdSetUnitName)
	ON_BN_CLICKED(IDC_CMD_SET_GATE_DELAY, OnCmdSetGateDelay)
	ON_BN_CLICKED(IDC_CMD_GET_GATE_DELAY, OnCmdGetGateDelay)
	ON_BN_CLICKED(IDC_CMD_SELF_CU_SMS_GET, OnCmdSelfCuSmsGet)
	ON_BN_CLICKED(IDC_CMD_SELF_CU_SMS_SET, OnCmdSelfCuSmsSet)
	ON_EN_CHANGE(IDC_TXT_GATE_DELAY, OnChangeTxtGateDelay)
	ON_BN_CLICKED(IDC_CMD_GET_ZONE_ACTIVE_MODE, OnCmdGetZoneActiveMode)
	ON_BN_CLICKED(IDC_CMD_SET_ZONE_ACTIVE_MODE, OnCmdSetZoneActiveMode)
	ON_BN_CLICKED(IDC_RDO_ZONE_ACTIVE_MOD_EN_PIN, OnRdoZoneActiveModEnPin)
	ON_BN_CLICKED(IDC_RDO_ZONE_ACTIVE_MOD_RING, OnRdoZoneActiveModRing)
	ON_BN_CLICKED(IDC_RDO_ZONE_ACTIVE_MOD_SMS, OnRdoZoneActiveModSms)
	ON_BN_CLICKED(IDC_CMD_GET_VERSION, OnCmdGetVersion)
	ON_BN_CLICKED(IDC_CMD_GET_RECEPTION, OnCmdGetReception)
	ON_EN_CHANGE(IDC_TXT_REMINDER_DELAY, OnChangeTxtReminderDelay)
	ON_EN_CHANGE(IDC_TXT_UNIT_NAME, OnChangeTxtUnitName)
	ON_BN_CLICKED(IDC_CHK_SELF_CU_SMS, OnChkSelfCuSms)
	ON_EN_CHANGE(IDC_TXT_SELF_PHONE_NUM, OnChangeTxtSelfPhoneNum)
	ON_BN_CLICKED(IDC_CHK_REMINDER, OnChkReminder)
	ON_BN_CLICKED(IDC_CMD_REMINDER_SET, OnCmdReminderSet)
	ON_BN_CLICKED(IDC_CMD_REMINDER_GET, OnCmdReminderGet)
	ON_BN_CLICKED(IDC_RDO_DAILY_TEST_ALARM, OnRdoDailyTestAlarm)
	ON_BN_CLICKED(IDC_RDO_WEELY_TEST_ALARM, OnRdoWeelyTestAlarm)
	ON_BN_CLICKED(IDC_CHK_TEST_ALARM, OnChkTestAlarm)
	ON_CBN_SELCHANGE(IDC_CMB_TEST_ALARM_WEEK_DAY, OnSelchangeCmbTestAlarmWeekDay)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_DTP_TEST_ALARM_TIME, OnDatetimechangeDtpTestAlarmTime)
	ON_BN_CLICKED(IDC_CMD_SET_TEST_ALARM, OnCmdSetTestAlarm)
	ON_BN_CLICKED(IDC_CMD_GET_TEST_ALARM, OnCmdGetTestAlarm)
	ON_EN_CHANGE(IDC_TXT_TEST_ALARM_MSG, OnChangeTxtTestAlarmMsg)
	ON_BN_CLICKED(IDC_CMD_SMS_TO_LOGGER_GET, OnCmdSmsToLoggerGet)
	ON_BN_CLICKED(IDC_CMD_SMS_TO_LOGGER_SET, OnCmdSmsToLoggerSet)
	ON_EN_CHANGE(IDC_TXT_DEVICE_ID, OnChangeTxtDeviceId)
	ON_EN_CHANGE(IDC_TXT_TEMP_DIFF, OnChangeTxtTempDiff)
	ON_EN_CHANGE(IDC_TXT_SMS_INTERVAL, OnChangeTxtSmsInterval)
	ON_EN_CHANGE(IDC_TXT_SMS_PASS, OnChangeTxtSmsPass)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CMD_INTERNET_GET, &CMiscSettingsView::OnBnClickedCmdInternetGet)
	ON_EN_CHANGE(IDC_TXT_APN, &CMiscSettingsView::OnEnChangeTxtApn)
	ON_EN_CHANGE(IDC_TXT_GPRS_USER, &CMiscSettingsView::OnEnChangeTxtGprsUser)
	ON_EN_CHANGE(IDC_TXT_GPRS_PASS, &CMiscSettingsView::OnEnChangeTxtGprsPass)
	ON_EN_CHANGE(IDC_TXT_CLOUD_TOKEN, &CMiscSettingsView::OnEnChangeTxtCloudToken)
	ON_EN_CHANGE(IDC_TXT_CLOUD_DNS, &CMiscSettingsView::OnEnChangeTxtCloudDns)
	ON_BN_CLICKED(IDC_CHK_GPRS_EN, &CMiscSettingsView::OnBnClickedChkGprsEn)
	ON_BN_CLICKED(IDC_CMD_INTERNET_SET, &CMiscSettingsView::OnBnClickedCmdInternetSet)
	ON_EN_CHANGE(IDC_TXT_CLOUD_INTERVAL, &CMiscSettingsView::OnEnChangeTxtCloudInterval)
	ON_BN_CLICKED(IDC_CHK_SSL_EN, &CMiscSettingsView::OnBnClickedChkSslEn)
	ON_BN_CLICKED(IDC_CMD_RSSI_TO_CLOUD_GET, &CMiscSettingsView::OnBnClickedCmdRssiToCloudGet)
	ON_BN_CLICKED(IDC_CMD_RSSI_TO_CLOUD_SET, &CMiscSettingsView::OnBnClickedCmdRssiToCloudSet)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMiscSettingsView diagnostics

#ifdef _DEBUG
void CMiscSettingsView::AssertValid() const
{
	CFormView::AssertValid();
}

void CMiscSettingsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CMiscSettingsView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMiscSettingsView message handlers

void CMiscSettingsView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	INFO_LOG(g_DbLogger,"CMiscSettingsView::OnInitialUpdate()");
	
	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,"CMiscSettingsView::OnInitialUpdate() first run");

		m_bFirstRun=false;

		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);
		
		m_dtpUnitTime.SetFormat("HH:mm:ss, dd/MM/yy");	//Set the time format

		//Add the week days to the list
		int l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_SUNDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_SUNDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_MONDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_MONDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_TUESDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_TUESDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_WEDNESDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_WEDNESDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_THURSDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_THURSDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_FRIDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_FRIDAY_DEF);
		l_iIndex=m_cmbTestAlarmWeekDay.AddString(GetResString(IDS_WEEK_DAY_SATURDAY));
		m_cmbTestAlarmWeekDay.SetItemData(l_iIndex,WEEK_DAY_SATURDAY_DEF);

		m_dtpTestAlarmTime.SetFormat("HH:mm");	//Set the test alarm time format
		Localize();
	}

	CGsmAlertDoc *l_pDoc=GetDocument();
	
	UpdateData();		//First get the values from the set combo boxes

	m_strUnitName=l_pDoc->m_strUnitName;
	m_strNewPass="";
	m_strGateDelay = GetSecAsTimedStr(l_pDoc->m_iGateDelay);
	m_bSelfCuSmsEn=l_pDoc->m_bSelfCuSmsEn;
	m_strSelfPhoneNum=l_pDoc->m_strSelfPhoneNum;
	m_iZoneActiveMode= l_pDoc->m_iZoneActiveMode;
	m_bReminderEnable=l_pDoc->m_bReminderEnabled;
	m_strReminderDelay.Format("%d",l_pDoc->m_iReminderDelay);

	m_bTestAlarmEnable=l_pDoc->m_bTestAlarmEnabled;
	if (l_pDoc->m_iTestAlarmWeekDay==7)
	{
		m_iTestAlarmPeriod=TEST_ALARM_PERIOD_DAILY;
		m_iTestAlarmWeekDay=0;	//The first in the list
	}
	else
	{
		m_iTestAlarmPeriod=TEST_ALARM_PERIOD_WEEKLY;
		m_iTestAlarmWeekDay=l_pDoc->m_iTestAlarmWeekDay;
	}
	m_odtTestAlarmTime.SetTime(l_pDoc->m_iTestAlarmTime/60,l_pDoc->m_iTestAlarmTime%60,0);
	m_strTestAlarmMsg=l_pDoc->m_strTestAlarmMsg;

	m_strDeviceId.Format("%d",l_pDoc->m_iSmsLoggerDeviceId);
	m_strTempDiff.Format("%.03f",l_pDoc->m_fSmsLoggerTempDiff);
	m_strSmsInterval.Format("%d",l_pDoc->m_iSmsLoggerInterval);
	m_strSmsPass = l_pDoc->m_strSmsLoggerPass;

	m_bGprsEn = l_pDoc->m_bGprsEn;
	m_strApn = l_pDoc->m_strApn;
	m_strGprsUser= l_pDoc->m_strGprsUser;
	m_strGprsPass= l_pDoc->m_strGprsPass;
	m_strCloudToken= l_pDoc->m_strCloudToken;
	m_strCloudDns= l_pDoc->m_strCloudDns;
	m_strApn = l_pDoc->m_strApn;
	m_strCloudInterval = GetSecAsTimedStr(l_pDoc->m_iCloudInterval);
	m_bUseSsl= l_pDoc->m_bUseSsl;

	SelectiveEnableControls();

	UpdateData(FALSE);	//After it set the controls from the variables
}

LRESULT CMiscSettingsView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	CString *l_pArrivedRespond;
	CGsmAlertDoc *l_pDoc=GetDocument();

	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,"CMiscSettingsView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT");
		m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,"CMiscSettingsView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND");
		//If we use modem communication, don't allow to change the band select over the air
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,"CMiscSettingsView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED");
		if(m_enmMiscSettingsLastSentCommand!=enmMiscSettingsLastSentCommandNone)
		{
			m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR + MB_OK);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,"CMiscSettingsView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND");
		return 0;
	}

	l_pArrivedRespond=(CString*)lParam;
	int l_iLastRespondStartAt=0;
	int l_iRespondStartAt;

	INFO_LOG(g_DbLogger,CString("CMiscSettingsView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_UNIT_NAME_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_UNIT_NAME_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_UNIT_NAME_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			
			m_strUnitName=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_pDoc->m_strUnitName=m_strUnitName;
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_GATE_DELAY_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_GATE_DELAY_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_GATE_DELAY_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			
			CString l_strGateDelay=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_pDoc->m_iGateDelay=atoi(LPCTSTR(l_strGateDelay));
			m_strGateDelay = GetSecAsTimedStr(l_pDoc->m_iGateDelay);
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_SELF_SMS_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_SELF_SMS_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_SELF_SMS_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strSelfCuSmsEn=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt=l_strSelfCuSmsEn.Find(",");
			CString l_strSelfPhoneNum=l_strSelfCuSmsEn.Mid(l_iDataStartAt + 1);
			l_strSelfCuSmsEn=l_strSelfCuSmsEn.Left(l_iDataStartAt);
			m_strSelfPhoneNum=l_strSelfPhoneNum;
			m_bSelfCuSmsEn=atoi(LPCTSTR(l_strSelfCuSmsEn));

			l_pDoc->m_strSelfPhoneNum=m_strSelfPhoneNum;
			l_pDoc->m_bSelfCuSmsEn=(bool)m_bSelfCuSmsEn;
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_BAND_SELECT_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_BAND_SELECT_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_BAND_SELECT_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strBandSelect=l_pArrivedRespond->Mid(l_iDataStartAt);
			int l_iBandSelect=atoi(LPCTSTR(l_strBandSelect));
			l_pDoc->m_iBandSelect=l_iBandSelect;
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_ZONE_ACTIVE_MODE_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_ZONE_ACTIVE_MODE_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_ZONE_ACTIVE_MODE_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strZoneActiveMode=l_pArrivedRespond->Mid(l_iDataStartAt);
			int l_iZoneActiveMode=atoi(LPCTSTR(l_strZoneActiveMode));
			m_iZoneActiveMode=l_iZoneActiveMode;
			l_pDoc->m_iZoneActiveMode=l_iZoneActiveMode;
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_INPUT_DEBOUNCE_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_INPUT_DEBOUNCE_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_INPUT_DEBOUNCE_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strInputDebounce=l_pArrivedRespond->Mid(l_iDataStartAt);
			int l_iInputDebounce=atoi(LPCTSTR(l_strInputDebounce));
			l_pDoc->m_iInputDebounce=l_iInputDebounce;
			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_VERSION_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_VERSION_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_VERSION_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			m_strVersion=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}

		else if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(RSSI_TO_CLOUD_GET_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			int l_iDataStartAt = FindInString(*l_pArrivedRespond, RSSI_TO_CLOUD_GET_MSG_RESPOND);
			l_iDataStartAt += (CString(RSSI_TO_CLOUD_GET_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			m_bRssiToCloud = atoi((LPCSTR)(l_pArrivedRespond->Mid(l_iDataStartAt)));

			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;
		}

		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_RECEPTION_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_RECEPTION_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_RECEPTION_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			m_strReception=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_REMINDER_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_REMINDER_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_REMINDER_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			
			CString l_strReminderEn=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt=l_strReminderEn.Find(",");
			CString l_strReminderDelay=l_strReminderEn.Mid(l_iDataStartAt + 1);
			l_strReminderEn=l_strReminderEn.Left(l_iDataStartAt);
			m_strReminderDelay=l_strReminderDelay;
			m_bReminderEnable=atoi(LPCTSTR(l_strReminderEn));

			l_pDoc->m_iReminderDelay=atoi(LPCTSTR(m_strReminderDelay));
			l_pDoc->m_bReminderEnabled=(bool)m_bReminderEnable;
			l_pDoc->SetModifiedFlag();

			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_TEST_ALARM_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_TEST_ALARM_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_TEST_ALARM_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			
			CString l_strTestAlarmEn=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt=l_strTestAlarmEn.Find(",");
			CString l_strTestAlarmWeekDay=l_strTestAlarmEn.Mid(l_iDataStartAt + 1);
			l_strTestAlarmEn=l_strTestAlarmEn.Left(l_iDataStartAt);

			l_iDataStartAt=l_strTestAlarmWeekDay.Find(",");
			CString l_strTestAlarmTime=l_strTestAlarmWeekDay.Mid(l_iDataStartAt + 1);
			l_strTestAlarmWeekDay=l_strTestAlarmWeekDay.Left(l_iDataStartAt);

			l_iDataStartAt=l_strTestAlarmTime.Find(",");
			CString l_strTestAlarmMsg=l_strTestAlarmTime.Mid(l_iDataStartAt + 1);
			l_strTestAlarmTime=l_strTestAlarmTime.Left(l_iDataStartAt);
			
			l_pDoc->m_bTestAlarmEnabled=atoi(LPCTSTR(l_strTestAlarmEn));
			l_pDoc->m_iTestAlarmWeekDay=atoi(LPCTSTR(l_strTestAlarmWeekDay));
			l_pDoc->m_iTestAlarmTime=atoi(LPCTSTR(l_strTestAlarmTime));
			l_pDoc->m_strTestAlarmMsg=l_strTestAlarmMsg;
			
			m_bTestAlarmEnable=l_pDoc->m_bTestAlarmEnabled;
			if (l_pDoc->m_iTestAlarmWeekDay==7)
			{
				m_iTestAlarmPeriod=TEST_ALARM_PERIOD_DAILY;
				m_iTestAlarmWeekDay=0;	//The first in the list
			}
			else
			{
				m_iTestAlarmPeriod=TEST_ALARM_PERIOD_WEEKLY;
				m_iTestAlarmWeekDay=l_pDoc->m_iTestAlarmWeekDay;
			}
			m_odtTestAlarmTime.SetTime(l_pDoc->m_iTestAlarmTime/60,l_pDoc->m_iTestAlarmTime%60,0);
			m_strTestAlarmMsg=l_pDoc->m_strTestAlarmMsg;
			
			l_pDoc->SetModifiedFlag();
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_SMS_TO_LOGGER_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_SMS_TO_LOGGER_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_SMS_TO_LOGGER_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();


			CString l_strDeviceId=l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt=l_strDeviceId.Find(",");
			CString l_strTempDiff=l_strDeviceId.Mid(l_iDataStartAt + 1);
			l_strDeviceId=l_strDeviceId.Left(l_iDataStartAt);
			
			l_iDataStartAt=l_strTempDiff.Find(",");
			CString l_strInterval=l_strTempDiff.Mid(l_iDataStartAt + 1);
			l_strTempDiff=l_strTempDiff.Left(l_iDataStartAt);
			
			l_iDataStartAt=l_strInterval.Find(",");
			CString l_strPassword=l_strInterval.Mid(l_iDataStartAt + 1);
			l_strInterval=l_strInterval.Left(l_iDataStartAt);
			
			l_pDoc->m_iSmsLoggerDeviceId=atoi(LPCTSTR(l_strDeviceId));
			l_pDoc->m_fSmsLoggerTempDiff=atof(LPCTSTR(l_strTempDiff));
			l_pDoc->m_iSmsLoggerInterval=atoi(LPCTSTR(l_strInterval))/60;
			l_pDoc->m_strSmsLoggerPass=l_strPassword;
			
			m_strDeviceId = l_strDeviceId;
			m_strTempDiff = l_strTempDiff;
			m_strSmsInterval.Format("%d",l_pDoc->m_iSmsLoggerInterval);
			m_strSmsPass=l_strPassword;
			
			l_pDoc->SetModifiedFlag();
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(GET_INTERNET_PARAMS_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			int l_iDataStartAt = FindInString(*l_pArrivedRespond, GET_INTERNET_PARAMS_MSG_RESPOND);
			l_iDataStartAt += (CString(GET_INTERNET_PARAMS_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();

			CString l_strGprsEn= l_pArrivedRespond->Mid(l_iDataStartAt);
			l_iDataStartAt = l_strGprsEn.Find(",");
			CString l_strApn= l_strGprsEn.Mid(l_iDataStartAt + 1);
			l_strGprsEn = l_strGprsEn.Left(l_iDataStartAt);
			l_iDataStartAt = l_strApn.Find(",");
			CString l_strGprsUser= l_strApn.Mid(l_iDataStartAt + 1);
			l_strApn = l_strApn.Left(l_iDataStartAt);
			l_iDataStartAt = l_strGprsUser.Find(",");
			CString l_strGprsPass= l_strGprsUser.Mid(l_iDataStartAt + 1);
			l_strGprsUser = l_strGprsUser.Left(l_iDataStartAt);
			l_iDataStartAt = l_strGprsPass.Find(",");
			CString l_strCloudToken= l_strGprsPass.Mid(l_iDataStartAt + 1);
			l_strGprsPass = l_strGprsPass.Left(l_iDataStartAt);
			l_iDataStartAt = l_strCloudToken.Find(",");
			CString l_strCloudDns = l_strCloudToken.Mid(l_iDataStartAt + 1);
			l_strCloudToken = l_strCloudToken.Left(l_iDataStartAt);
			l_iDataStartAt = l_strCloudDns.Find(",");
			CString l_strCloudInterval = l_strCloudDns.Mid(l_iDataStartAt + 1);
			l_strCloudDns = l_strCloudDns.Left(l_iDataStartAt);
			l_iDataStartAt = l_strCloudInterval.Find(",");
			CString l_strUseSsl = l_strCloudInterval.Mid(l_iDataStartAt + 1);
			l_strCloudInterval = l_strCloudInterval.Left(l_iDataStartAt);

			l_pDoc->m_bGprsEn = atoi(LPCTSTR(l_strGprsEn));
			l_pDoc->m_strApn = l_strApn;
			l_pDoc->m_strGprsUser= l_strGprsUser;
			l_pDoc->m_strGprsPass= l_strGprsPass;
			l_pDoc->m_strCloudToken= l_strCloudToken;
			l_pDoc->m_strCloudDns= l_strCloudDns;
			l_pDoc->m_iCloudInterval = atoi(LPCTSTR(l_strCloudInterval));
			l_pDoc->m_bUseSsl= atoi(LPCTSTR(l_strUseSsl));

			m_bGprsEn = l_pDoc->m_bGprsEn;
			m_strApn = l_pDoc->m_strApn;
			m_strGprsUser = l_pDoc->m_strGprsUser;
			m_strGprsPass = l_pDoc->m_strGprsPass;
			m_strCloudToken = l_pDoc->m_strCloudToken;
			m_strCloudDns = l_pDoc->m_strCloudDns;
			m_strCloudInterval = GetSecAsTimedStr(l_pDoc->m_iCloudInterval);
			m_bUseSsl= l_pDoc->m_bUseSsl;

			l_pDoc->SetModifiedFlag();

			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;
		}
		


		
	} while(l_iRespondStartAt!=-1);

	//After set the variables, apply them to the controls
	UpdateData(FALSE);

	
	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		switch(m_enmMiscSettingsLastSentCommand)
		{
		case enmMiscSettingsLastSentCommandChangePass:
			m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
			EnableControls();
			MessageBox(GetResString(IDS_INCORRECT_PASS),NULL,MB_ICONERROR);
			break;
		default:
			MessageBox(GetResString(IDS_ERROR_OCCURRED));
			m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
			EnableControls();
		}
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,"Exit CMiscSettingsView::OnCommandArrived");

	return 0;
}

void CMiscSettingsView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,"CMiscSettingsView::HandleLastSentCommand()");	
	switch(m_enmMiscSettingsLastSentCommand)
	{
	case enmMiscSettingsLastSentCommandGetUnitName:
	case enmMiscSettingsLastSentCommandGetSelfSms:
	case enmMiscSettingsLastSentCommandSetSelfSms:
	case enmMiscSettingsLastSentCommandGetBandSelect:
	case enmMiscSettingsLastSentCommandSetUnitName:
	case enmMiscSettingsLastSentCommandGetGateDelay:
	case enmMiscSettingsLastSentCommandSetGateDelay:
	case enmMiscSettingsLastSentCommandSetTime:
	case enmMiscSettingsLastSentCommandSetBandSelect:
	case enmMiscSettingsLastSentCommandGetZoneActiveMode:
	case enmMiscSettingsLastSentCommandSetZoneActiveMode:
	case enmMiscSettingsLastSentCommandGetVersion:
	case enmMiscSettingsLastSentCommandGetReception:
	case enmMiscSettingsLastSentCommandGetDebaunce:
	case enmMiscSettingsLastSentCommandSetDebaunce:
	case enmMiscSettingsLastSentCommandGetRemider:
	case enmMiscSettingsLastSentCommandSetRemider:
	case enmMiscSettingsLastSentCommandGetTestAlarm:
	case enmMiscSettingsLastSentCommandSetTestAlarm:
	case enmMiscSettingsLastSentCommandSetSmsLogger:
	case enmMiscSettingsLastSentCommandGetSmsLogger:
	case enmMiscSettingsLastSentCommandGetInternetParam:
	case enmMiscSettingsLastSentCommandSetInternetParam:
	case enmMiscSettingsLastSentCommandGetRssiToCloud:
	case enmMiscSettingsLastSentCommandSetRssiToCloud:
		m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
		EnableControls();
		break;
	case enmMiscSettingsLastSentCommandChangePass:
		{
			CGsmAlertDoc *l_pDoc=GetDocument();
			m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandNone;
			EnableControls();
			l_pDoc->m_strUnitPass=m_strNewPass;
			l_pDoc->m_strTempUnitPass=m_strNewPass;
			l_pDoc->SetModifiedFlag();
			m_strCurrentPass="";
			m_strNewPass="";
			m_strConfirmPass="";
			UpdateData(FALSE);
		}
		break;
	}
}

void CMiscSettingsView::SendSetCommandToUnit(enmMiscSettingsLastSentCommand &a_enmMiscSettingsLastSentCommand)
{
	INFO_LOG(g_DbLogger,"CMiscSettingsView::SendSetCommandToUnit()");

	CString *l_pNewCommand;

	l_pNewCommand=new CString();
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	switch (a_enmMiscSettingsLastSentCommand)
	{
	case enmMiscSettingsLastSentCommandSetTime:
	{
		UpdateData();
		CString l_strTimeToSet;
		l_strTimeToSet = m_odtUnitTime.Format("%y/%m/%d,%H:%M:%S");
		l_pNewCommand->Format("%s=%s", SET_UNIT_TIME_MSG_COMMAND, l_strTimeToSet);
	}
	break;
	case enmMiscSettingsLastSentCommandChangePass:
	{
		UpdateData();
		l_pNewCommand->Format("%s=%s,%s", CHANGE_PASS_MSG_COMMAND, m_strCurrentPass, m_strNewPass);
	}
	break;
	case enmMiscSettingsLastSentCommandGetUnitName:
		l_pNewCommand->Format("%s", GET_UNIT_NAME_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandGetGateDelay:
		l_pNewCommand->Format("%s", GET_GATE_DELAY_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetGateDelay:
		l_pNewCommand->Format("%s=%d", SET_GATE_DELAY_MSG_COMMAND, l_pDoc->m_iGateDelay);
		break;
	case enmMiscSettingsLastSentCommandSetUnitName:
		l_pNewCommand->Format("%s=%s", SET_UNIT_NAME_MSG_COMMAND, l_pDoc->m_strUnitName);
		break;
	case enmMiscSettingsLastSentCommandGetSelfSms:
		l_pNewCommand->Format("%s", GET_SELF_SMS_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetSelfSms:
	{
		UpdateData();
		l_pNewCommand->Format("%s=%d,%s", SET_SELF_SMS_MSG_COMMAND, m_bSelfCuSmsEn, m_strSelfPhoneNum);
	}
	break;
	case enmMiscSettingsLastSentCommandGetBandSelect:
		l_pNewCommand->Format("%s", GET_BAND_SELECT_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetBandSelect:
		l_pNewCommand->Format("%s=%d", SET_BAND_SELECT_MSG_COMMAND, l_pDoc->m_iBandSelect);
		break;
	case enmMiscSettingsLastSentCommandGetVersion:
		l_pNewCommand->Format("%s", GET_VERSION_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandGetReception:
		l_pNewCommand->Format("%s", GET_RECEPTION_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandGetZoneActiveMode:
		l_pNewCommand->Format("%s", GET_ZONE_ACTIVE_MODE_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetZoneActiveMode:
		l_pNewCommand->Format("%s=%d", SET_ZONE_ACTIVE_MODE_MSG_COMMAND, l_pDoc->m_iZoneActiveMode);
		break;
	case enmMiscSettingsLastSentCommandSetDebaunce:
		l_pNewCommand->Format("%s=%d", SET_INPUT_DEBOUNCE_MSG_COMMAND, l_pDoc->m_iInputDebounce);
		break;
	case enmMiscSettingsLastSentCommandGetDebaunce:
		l_pNewCommand->Format("%s", GET_INPUT_DEBOUNCE_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandGetRemider:
		l_pNewCommand->Format("%s", GET_REMINDER_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetRemider:
		l_pNewCommand->Format("%s=%d,%d", SET_REMINDER_MSG_COMMAND, l_pDoc->m_bReminderEnabled, l_pDoc->m_iReminderDelay);
		break;
	case enmMiscSettingsLastSentCommandGetTestAlarm:
		l_pNewCommand->Format("%s", GET_TEST_ALARM_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetTestAlarm:
		l_pNewCommand->Format("%s=%d,%d,%d,%s", SET_TEST_ALARM_MSG_COMMAND, l_pDoc->m_bTestAlarmEnabled, l_pDoc->m_iTestAlarmWeekDay, l_pDoc->m_iTestAlarmTime, l_pDoc->m_strTestAlarmMsg);
		break;

	case enmMiscSettingsLastSentCommandSetSmsLogger:
		l_pNewCommand->Format("%s=%d,%.03f,%d,%s", SET_SMS_TO_LOGGER_MSG_COMMAND, l_pDoc->m_iSmsLoggerDeviceId, l_pDoc->m_fSmsLoggerTempDiff, l_pDoc->m_iSmsLoggerInterval * 60, l_pDoc->m_strSmsLoggerPass);
		break;
	case enmMiscSettingsLastSentCommandGetSmsLogger:
		l_pNewCommand->Format("%s", GET_SMS_TO_LOGGER_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandGetInternetParam:
		l_pNewCommand->Format("%s", GET_INTERNET_PARAMS_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetInternetParam:
	{
		int l_iUseSsl = 0;
		if (l_pDoc->m_bUseSsl)
		{
			l_iUseSsl = 1;
		}
		l_pNewCommand->Format("%s=%d,%s,%s,%s,%s,%s,%d,%d", SET_INTERNET_PARAMS_MSG_COMMAND, l_pDoc->m_bGprsEn, l_pDoc->m_strApn, l_pDoc->m_strGprsUser, l_pDoc->m_strGprsPass, l_pDoc->m_strCloudToken, l_pDoc->m_strCloudDns, l_pDoc->m_iCloudInterval, l_iUseSsl);
		break;
	}
	case enmMiscSettingsLastSentCommandGetRssiToCloud:
		l_pNewCommand->Format("%s", RSSI_TO_CLOUD_GET_MSG_COMMAND);
		break;
	case enmMiscSettingsLastSentCommandSetRssiToCloud:
		{
			int l_iRssiToCloud = 0;
			if (l_pDoc->m_bRssiToCloud)
			{
				l_iRssiToCloud = 1;
			}
			l_pNewCommand->Format("%s=%d", RSSI_TO_CLOUD_SET_MSG_COMMAND, l_iRssiToCloud);
			break;
		}
	}
	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmMiscSettingsLastSentCommand =enmMiscSettingsLastSentCommandNone;
	}
}

void CMiscSettingsView::Localize()
{
	INFO_LOG(g_DbLogger,"CMiscSettingsView::Localize()");

	SetDlgItemText( IDC_FRAM_UNIT_NAME,GetResString(IDS_IDC_FRAM_UNIT_NAME));
	SetDlgItemText( IDC_CMD_GET_UNIT_NAME,GetResString(IDS_IDC_CMD_GET_UNIT_NAME));
	SetDlgItemText( IDC_CMD_SET_UNIT_NAME,GetResString(IDS_IDC_CMD_SET_UNIT_NAME));
	
	SetDlgItemText( IDC_FRAM_UNIT_TIME,GetResString(IDS_IDC_FRAM_UNIT_TIME));
	SetDlgItemText( IDC_CMD_SET_UNIT_TIME,GetResString(IDS_IDC_CMD_SET_UNIT_TIME));
	SetDlgItemText( IDC_CMD_GET_PC_TIME,GetResString(IDS_IDC_CMD_GET_PC_TIME));

	SetDlgItemText( IDC_FRAM_UNIT_PASS,GetResString(IDS_IDC_FRAM_UNIT_PASS));
	SetDlgItemText( IDC_LBL_NEW_PASS,GetResString(IDS_IDC_LBL_NEW_PASS));
	SetDlgItemText( IDC_CMD_SET_UNIT_PASS,GetResString(IDS_IDC_CMD_SET_UNIT_PASS));

	SetDlgItemText( IDC_FRAM_GATE_DELAY,GetResString(IDS_IDC_FRAM_GATE_DELAY));
	SetDlgItemText( IDC_CMD_GET_GATE_DELAY,GetResString(IDS_IDC_CMD_GET_GATE_DELAY));
	SetDlgItemText( IDC_CMD_SET_GATE_DELAY,GetResString(IDS_IDC_CMD_SET_GATE_DELAY));
	SetDlgItemText( IDC_LBL_CONFIRM_PASS,GetResString(IDS_IDC_LBL_CONFIRM_PASS));

	SetDlgItemText( IDC_FRAM_SELF_CU_SMS,GetResString(IDS_IDC_FRAM_SELF_CU_SMS));
	SetDlgItemText( IDC_CHK_SELF_CU_SMS,GetResString(IDS_IDC_CHK_SELF_CU_SMS));
	SetDlgItemText( IDC_LBL_SELF_PHONE_NUM,GetResString(IDS_IDC_LBL_SELF_PHONE_NUM));
	SetDlgItemText( IDC_CMD_SELF_CU_SMS_GET,GetResString(IDS_IDC_CMD_SELF_CU_SMS_GET));
	SetDlgItemText( IDC_CMD_SELF_CU_SMS_SET,GetResString(IDS_IDC_CMD_SELF_CU_SMS_SET));
}

void CMiscSettingsView::OnCmdSetUnitCurrentTime() 
{
	m_odtUnitTime = COleDateTime::GetCurrentTime();
	UpdateData(FALSE);
}

void CMiscSettingsView::OnCmdSetUnitTime() 
{
	UpdateData();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetTime;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::EnableControls(BOOL a_bEnabled)
{
	m_cmdSetUnitTime.EnableWindow(a_bEnabled);
	m_cmdSetUnitPass.EnableWindow(a_bEnabled);
	m_cmdGetUnitName.EnableWindow(a_bEnabled);
	m_cmdSetUnitName.EnableWindow(a_bEnabled);
	m_cmdSetGateDelay.EnableWindow(a_bEnabled);
	m_cmdGetGateDelay.EnableWindow(a_bEnabled);
	m_txtUnitName.EnableWindow(a_bEnabled);
	m_txtCurrentPass.EnableWindow(a_bEnabled);
	m_txtConfirmPass.EnableWindow(a_bEnabled);
	m_txtNewPass.EnableWindow(a_bEnabled);
	m_txtGateDelay.EnableWindow(a_bEnabled);
	m_dtpUnitTime.EnableWindow(a_bEnabled);
	m_cmdGetPcTime.EnableWindow(a_bEnabled);
	m_chkSelfCuSms.EnableWindow(a_bEnabled);
	m_txtSelfPhoneNum.EnableWindow(a_bEnabled);
	m_cmdSelfCuSmsSet.EnableWindow(a_bEnabled);
	m_cmdSelfCuSmsGet.EnableWindow(a_bEnabled);
	m_rdoZoneActiveModeEnPin.EnableWindow(a_bEnabled);
	m_rdoZoneActiveModeRing.EnableWindow(a_bEnabled);
	m_rdoZoneActiveModeSms.EnableWindow(a_bEnabled);
	m_cmdGetZoneActiveMode.EnableWindow(a_bEnabled);
	m_cmdSetZoneActiveMode.EnableWindow(a_bEnabled);
	m_cmdGetVersion.EnableWindow(a_bEnabled);
	m_cmdGetReception.EnableWindow(a_bEnabled);
	m_txtVersion.EnableWindow(a_bEnabled);
	m_txtReception.EnableWindow(a_bEnabled);
	m_cmdReminderGet.EnableWindow(a_bEnabled);
	m_cmdReminderSet.EnableWindow(a_bEnabled);
	m_txtReminderDelay.EnableWindow(a_bEnabled);
	m_chkReminder.EnableWindow(a_bEnabled);
	m_txtDeviceId.EnableWindow(a_bEnabled);
	m_txtTempDiff.EnableWindow(a_bEnabled);
	m_txtSmsInterval.EnableWindow(a_bEnabled);
	m_txtSmsPass.EnableWindow(a_bEnabled);
	m_cmdSmsToLoggerSet.EnableWindow(a_bEnabled);
	m_cmdSmsToLoggerGet.EnableWindow(a_bEnabled);

	m_cmdRssiToCloudGet.EnableWindow(a_bEnabled);
	m_cmdRssiToCloudSet.EnableWindow(a_bEnabled);

	//Enabled the selective controls
	SelectiveEnableControls(a_bEnabled);
}

void CMiscSettingsView::SelectiveEnableControls(BOOL a_bEnabled)
{
	//Reminder
	m_chkReminder.EnableWindow(a_bEnabled);
	if (m_bReminderEnable)
	{
		m_txtReminderDelay.EnableWindow(a_bEnabled);
	}
	else
	{
		m_txtReminderDelay.EnableWindow(FALSE);
	}
	
	//Alarm Test
	m_chkTestAlarm.EnableWindow(a_bEnabled);
	if (m_bTestAlarmEnable)
	{
		m_rdoDailyTestAlarm.EnableWindow(a_bEnabled);
		m_rdoWeeklyTestAlarm.EnableWindow(a_bEnabled);
		m_dtpTestAlarmTime.EnableWindow(a_bEnabled);
		if (m_iTestAlarmPeriod==TEST_ALARM_PERIOD_DAILY)
		{
			m_cmbTestAlarmWeekDay.EnableWindow(FALSE);
		}
		else	//(m_iTestAlarmPeriod==TEST_ALAEM_PERIOD_WEEKLY)
		{
			m_cmbTestAlarmWeekDay.EnableWindow(a_bEnabled);
		}
		m_txtTestAlarmMsg.EnableWindow(a_bEnabled);
	}
	else
	{
		m_rdoDailyTestAlarm.EnableWindow(FALSE);
		m_rdoWeeklyTestAlarm.EnableWindow(FALSE);
		m_dtpTestAlarmTime.EnableWindow(FALSE);
		m_cmbTestAlarmWeekDay.EnableWindow(FALSE);
		m_txtTestAlarmMsg.EnableWindow(FALSE);
	}

	m_chkGprsEn.EnableWindow(a_bEnabled);
	if (m_bGprsEn)
	{
		m_txtApn.EnableWindow(a_bEnabled);
		m_txtGprsUser.EnableWindow(a_bEnabled);
		m_txtGprsPass.EnableWindow(a_bEnabled);
		m_txtCloudToken.EnableWindow(a_bEnabled);
		m_txtCloudDns.EnableWindow(a_bEnabled);
		m_txtCloudInterval.EnableWindow(a_bEnabled);
		m_chkUseSsl.EnableWindow(a_bEnabled);
	}
	else
	{
		m_txtApn.EnableWindow(FALSE);
		m_txtGprsUser.EnableWindow(FALSE);
		m_txtGprsPass.EnableWindow(FALSE);
		m_txtCloudToken.EnableWindow(FALSE);
		m_txtCloudDns.EnableWindow(FALSE);
		m_txtCloudInterval.EnableWindow(FALSE);
		m_chkUseSsl.EnableWindow(FALSE);
	}
}

void CMiscSettingsView::OnChangeTxtCurrentPass() 
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	UpdateData();
}

void CMiscSettingsView::OnCmdSetUnitPass() 
{
	UpdateData();
	if(m_strCurrentPass=="")
	{
		MessageBox(GetResString(IDS_CURRENT_PASS_EMPTY),NULL,MB_ICONERROR);
		return;
	}
	if(m_strNewPass=="")
	{
		MessageBox(GetResString(IDS_NEW_PASS_EMPTY),NULL,MB_ICONERROR);
		return;
	}

	if(m_strNewPass!=m_strConfirmPass)
	{
		MessageBox(GetResString(IDS_CONFIRM_PASS_ERROR),NULL,MB_ICONERROR);
		return;
	}

	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandChangePass;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdGetUnitName() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetUnitName;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdSetUnitName() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_strUnitName=m_strUnitName;
	l_pDoc->SetModifiedFlag();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetUnitName;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);	
}

void CMiscSettingsView::OnCmdSetGateDelay() 
{
	UpdateData();
	int l_iDelay =GetTimedStrAsSec(m_strGateDelay);
	if(l_iDelay ==-1)
	{
		MessageBox(GetResString(IDS_GATE_DELAY_LIMIT),NULL,MB_ICONERROR);
		return;
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iGateDelay=l_iDelay;
	l_pDoc->SetModifiedFlag();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetGateDelay;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdGetGateDelay() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetGateDelay;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdSelfCuSmsGet() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetSelfSms;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdSelfCuSmsSet() 
{
	UpdateData();
	if((!IsPhoneNumOK(m_strSelfPhoneNum)) &&(m_bSelfCuSmsEn==TRUE))
	{
		MessageBox(GetResString(IDS_PHONE_NUM_NOT_OK));
		return;
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_bSelfCuSmsEn=(bool)m_bSelfCuSmsEn;
	l_pDoc->m_strSelfPhoneNum=m_strSelfPhoneNum;
	l_pDoc->SetModifiedFlag();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetSelfSms;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnChangeTxtGateDelay() 
{
	UpdateData();
	int l_iDelay =GetTimedStrAsSec(m_strGateDelay);
	if(l_iDelay ==-1)
	{
		return;
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iGateDelay=l_iDelay;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnCmdGetZoneActiveMode() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetZoneActiveMode;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);	
}

void CMiscSettingsView::OnCmdSetZoneActiveMode() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iZoneActiveMode=m_iZoneActiveMode;
	l_pDoc->SetModifiedFlag();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetZoneActiveMode;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);	
}

void CMiscSettingsView::OnRdoZoneActiveModEnPin() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iZoneActiveMode=m_iZoneActiveMode;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnRdoZoneActiveModRing() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iZoneActiveMode=m_iZoneActiveMode;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnRdoZoneActiveModSms() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iZoneActiveMode=m_iZoneActiveMode;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnCmdGetVersion() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetVersion;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdGetReception() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetReception;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnChangeTxtReminderDelay() 
{
	UpdateData();
	if (!IsNumeric((LPCTSTR)m_strReminderDelay))
	{
		return;
	}
	int l_iReminderTime =atoi((LPCTSTR)m_strReminderDelay);
	if((l_iReminderTime>REMINDER_DELAY_MAX)||(l_iReminderTime<REMINDER_DELAY_MIN))
	{
		l_iReminderTime=REMINDER_DELAY_DEF;
		m_strReminderDelay.Format("%d",l_iReminderTime);
		UpdateData(FALSE);
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iReminderDelay =l_iReminderTime ;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnChangeTxtUnitName() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_strUnitName=m_strUnitName;
	l_pDoc->SetModifiedFlag();	
}

void CMiscSettingsView::OnChkSelfCuSms() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_bSelfCuSmsEn=(bool)m_bSelfCuSmsEn;
	l_pDoc->SetModifiedFlag();		
}

void CMiscSettingsView::OnChangeTxtSelfPhoneNum() 
{
	UpdateData();
	if(!IsPhoneNumOK(m_strSelfPhoneNum))
	{
		return;
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_strSelfPhoneNum=m_strSelfPhoneNum;
	l_pDoc->SetModifiedFlag();
	
}

void CMiscSettingsView::OnChkReminder() 
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_bReminderEnabled=(bool)m_bReminderEnable;
	l_pDoc->SetModifiedFlag();
	SelectiveEnableControls();
}

void CMiscSettingsView::OnCmdReminderSet() 
{
	UpdateData();
	if(((atoi((LPCTSTR)m_strReminderDelay)>REMINDER_DELAY_MAX)||(atoi((LPCTSTR)m_strReminderDelay)<REMINDER_DELAY_MIN)) &&(m_bReminderEnable==TRUE))
	{
		MessageBox(GetResString(IDS_REMIDER_DELAY_NOT_OK));
		return;
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_bReminderEnabled=(bool)m_bReminderEnable;
	l_pDoc->m_iReminderDelay=atoi((LPCTSTR)m_strReminderDelay);
	if((l_pDoc->m_iReminderDelay>REMINDER_DELAY_MAX)||(l_pDoc->m_iReminderDelay<REMINDER_DELAY_MIN))
	{
		l_pDoc->m_iReminderDelay=REMINDER_DELAY_DEF;
	}
	
	l_pDoc->SetModifiedFlag();
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetRemider;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdReminderGet() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetRemider;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);	
}

void CMiscSettingsView::TestAlarmChange()
{
	UpdateData();
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_bTestAlarmEnabled=m_bTestAlarmEnable;
	if (m_iTestAlarmPeriod==TEST_ALARM_PERIOD_DAILY)
	{
		l_pDoc->m_iTestAlarmWeekDay=7;	//0-6 - sun to sat; 7 - daily alarm
	}
	else	//(m_iTestAlarmPeriod==TEST_ALAEM_PERIOD_WEEKLY)
	{
		if (m_iTestAlarmWeekDay==-1)
		{
			m_iTestAlarmWeekDay=0;	//Select a day
			UpdateData(FALSE);
		}
		l_pDoc->m_iTestAlarmWeekDay=m_cmbTestAlarmWeekDay.GetItemData(m_iTestAlarmWeekDay);
	}
	l_pDoc->m_iTestAlarmTime=m_odtTestAlarmTime.GetHour()*60 + m_odtTestAlarmTime.GetMinute();
	l_pDoc->m_strTestAlarmMsg=m_strTestAlarmMsg;
	l_pDoc->SetModifiedFlag();

	SelectiveEnableControls();
}

void CMiscSettingsView::OnRdoDailyTestAlarm() 
{
	TestAlarmChange();
}

void CMiscSettingsView::OnRdoWeelyTestAlarm() 
{
	TestAlarmChange();
}

void CMiscSettingsView::OnChkTestAlarm() 
{
	TestAlarmChange();
}

void CMiscSettingsView::OnSelchangeCmbTestAlarmWeekDay() 
{
	TestAlarmChange();
}

void CMiscSettingsView::OnDatetimechangeDtpTestAlarmTime(NMHDR* pNMHDR, LRESULT* pResult) 
{
	TestAlarmChange();
	*pResult = 0;
}

void CMiscSettingsView::OnCmdSetTestAlarm() 
{
	TestAlarmChange();

	if(m_bTestAlarmEnable==FALSE)	//If the alarm test feature is disabled
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		CInputDialog l_InputDlg(CInputDialog::PASSWORD_LETTERS,GetResString(IDS_DISABLE_ALARM_TEST_DOWNLOAD_TITLE),GetResString(IDS_DISABLE_ALARM_TEST_DOWNLOAD_MSG),"",this);
		CString l_strInput;
		char *l_strTmp =l_strInput.GetBuffer(100);
		if(l_InputDlg.InputBox(l_strTmp)!=IDOK)
		{
			l_strInput.ReleaseBuffer();
			return;
		}
		l_strInput.ReleaseBuffer();
		if(l_strInput!=l_pDoc->m_strUnitPass)
		{
			MessageBox(GetResString(IDS_INCORRECT_PASS));
			return;
		}
	}

	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetTestAlarm;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdGetTestAlarm() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetTestAlarm;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnChangeTxtTestAlarmMsg() 
{
	TestAlarmChange();
}

void CMiscSettingsView::OnCmdSmsToLoggerGet() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandGetSmsLogger;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnCmdSmsToLoggerSet() 
{
	m_enmMiscSettingsLastSentCommand=enmMiscSettingsLastSentCommandSetSmsLogger;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}

void CMiscSettingsView::OnChangeTxtDeviceId() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	UpdateData();
	if (!IsNumeric((LPCTSTR)m_strDeviceId))
	{
		return;
	}
	int l_iDeviceId =atoi((LPCTSTR)m_strDeviceId);
	if((l_iDeviceId>DEVICE_ID_MAX)||(l_iDeviceId<DEVICE_ID_MIN))
	{
		l_iDeviceId=DEVICE_ID_DEF;
		m_strDeviceId.Format("%d",l_iDeviceId);
		UpdateData(FALSE);
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iSmsLoggerDeviceId =l_iDeviceId ;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnChangeTxtTempDiff() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	UpdateData();
	if (!IsNumeric((LPCTSTR)m_strTempDiff))
	{
		return;
	}
	float l_fTempDiff=atoi((LPCTSTR)m_strTempDiff);
	if((l_fTempDiff>TEMP_DIFF_MAX)||(l_fTempDiff<TEMP_DIFF_MIN))
	{
		l_fTempDiff=TEMP_DIFF_DEF;
		m_strTempDiff.Format("%.01f",l_fTempDiff);
		UpdateData(FALSE);
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_fSmsLoggerTempDiff=l_fTempDiff ;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnChangeTxtSmsInterval() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	UpdateData();
	if (!IsNumeric((LPCTSTR)m_strSmsInterval))
	{
		return;
	}
	int l_iSmsLoggerInterval=atoi((LPCTSTR)m_strSmsInterval);
	if((l_iSmsLoggerInterval>SMS_LOGGER_INTERVAL_MAX)||(l_iSmsLoggerInterval<SMS_LOGGER_INTERVAL_MIN))
	{
		l_iSmsLoggerInterval=SMS_LOGGER_INTERVAL_DEF;
		m_strSmsInterval.Format("%d",l_iSmsLoggerInterval);
		UpdateData(FALSE);
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_iSmsLoggerInterval=l_iSmsLoggerInterval ;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnChangeTxtSmsPass() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	UpdateData();
	
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_strSmsLoggerPass=m_strSmsPass;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnBnClickedCmdInternetGet()
{
	m_enmMiscSettingsLastSentCommand = enmMiscSettingsLastSentCommandGetInternetParam;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);

}


void CMiscSettingsView::OnEnChangeTxtApn()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_strApn= m_strApn;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnEnChangeTxtGprsUser()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_strGprsUser = m_strGprsUser;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnEnChangeTxtGprsPass()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_strGprsPass = m_strGprsPass;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnEnChangeTxtCloudToken()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_strCloudToken = m_strCloudToken;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnEnChangeTxtCloudDns()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_strCloudDns = m_strCloudDns;
	l_pDoc->SetModifiedFlag();
}

void CMiscSettingsView::OnBnClickedChkGprsEn()
{
	// TODO: Add your control notification handler code here
	UpdateData();
	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_bGprsEn= (bool)m_bGprsEn;
	l_pDoc->SetModifiedFlag();
	SelectiveEnableControls();
}


void CMiscSettingsView::OnBnClickedCmdInternetSet()
{
	m_enmMiscSettingsLastSentCommand = enmMiscSettingsLastSentCommandSetInternetParam;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}


void CMiscSettingsView::OnEnChangeTxtCloudInterval()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
	UpdateData();
	int l_iCloudInterval = GetTimedStrAsSec(m_strCloudInterval);
	if (l_iCloudInterval == -1)
	{
		return;
	}
	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_iCloudInterval = l_iCloudInterval;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnBnClickedChkSslEn()
{
	UpdateData();
	
	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_bUseSsl = m_bUseSsl;
	l_pDoc->SetModifiedFlag();
}


void CMiscSettingsView::OnBnClickedCmdRssiToCloudGet()
{
	m_enmMiscSettingsLastSentCommand = enmMiscSettingsLastSentCommandGetRssiToCloud;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}


void CMiscSettingsView::OnBnClickedCmdRssiToCloudSet()
{

	UpdateData();
	
	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_bRssiToCloud = (bool)m_bRssiToCloud;
	l_pDoc->m_strSelfPhoneNum = m_strSelfPhoneNum;
	l_pDoc->SetModifiedFlag();
	
	m_enmMiscSettingsLastSentCommand = enmMiscSettingsLastSentCommandSetRssiToCloud;
	SendSetCommandToUnit(m_enmMiscSettingsLastSentCommand);
	EnableControls(FALSE);
}
