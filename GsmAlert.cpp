// GsmAlert.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GsmAlert.h"

#include "MPIDocTemplate.h"
#include "MPIChildFrame.h"
#include "MainFrm.h"
#include "GsmAlertDoc.h"
#include "Language.h"
#include "LogView.h"
#include "ZoneListView.h"
#include "AdcView.h"
#include "LevelAlertView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertApp

CGsmAlertApp *g_pApp;
CLogger g_DbLogger(GetResString(AFX_IDS_APP_TITLE),true);

BEGIN_MESSAGE_MAP(CGsmAlertApp, CWinApp)
	//{{AFX_MSG_MAP(CGsmAlertApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_AUTO_OPEN_LAST_FILE, OnAutoOpenLastFile)
	ON_UPDATE_COMMAND_UI(ID_AUTO_OPEN_LAST_FILE, OnUpdateAutoOpenLastFile)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	//ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	ON_COMMAND(ID_AUTO_OPEN_LAST_FILE, OnAutoOpenLastFile)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertApp construction

CGsmAlertApp::CGsmAlertApp()
{
	g_pApp=this;
	m_pDbFileLogHandler=NULL;	
}

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertApp destruction

CGsmAlertApp::~CGsmAlertApp()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertApp::~CGsmAlertApp"));

	if(m_pDbFileLogHandler)
	{
		delete m_pDbFileLogHandler;
	}
	int i=_listLanguages.GetCount();
	LanguagesRelease();
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGsmAlertApp object

CGsmAlertApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertApp initialization

BOOL CGsmAlertApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.

	SetRegistryKey(_T(REG_PATH));
	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)


	//Load the Languages
	TCHAR szPath[MAX_PATH] = {0};
	GetModuleFileName( AfxGetInstanceHandle(), szPath, sizeof(szPath) );
	LPSTR p = strrchr( szPath, '\\' );
	if( p )	*(++p) = 0;
	CString LanguageFolder;
	LanguageFolder  = szPath;
	LanguageFolder += "Language\\";

	LanguagesInit( LanguageFolder);

	m_strLanguageFilePath=GetProfileString(REG_SETTINGS_SECTION,REG_LANGUAGE);
	VERIFY( LoadLangLib( m_strLanguageFilePath ) );

	// Instantiate Logger handlers (Window or/and File)
	// Qualify the log file folder path to the same location as our exe
	TCHAR l_szPath[MAX_PATH] = {0};
	GetModuleFileName( AfxGetInstanceHandle(), l_szPath, sizeof(l_szPath) );
	LPSTR l_strTmp = strrchr( l_szPath, '\\' );
	if( l_strTmp )	*(++l_strTmp) = 0;
	CString l_strLogFile = l_szPath;

	// Log file name
	l_strLogFile+= GetResString(AFX_IDS_APP_TITLE);
	l_strLogFile+= " Debug Log";

	//m_pDbFileLogHandler = new CFileLogHandler( (LEVEL)0, l_strLogFile);
	g_DbLogger.AddHandler( m_pDbFileLogHandler );
	INFO_LOG(g_DbLogger,"CGsmAlertApp::InitInstance()");
	INFO_LOG(g_DbLogger,CString("Version") + g_pApp->GetAppVersion());
	
	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	CMPIDocTemplate* pDocTemplate=NULL;
	pDocTemplate = new CMPIDocTemplate(
	IDR_GSM_ALERT_TYPE,
	RUNTIME_CLASS(CGsmAlertDoc),
	RUNTIME_CLASS(CMPIChildFrame));
	AddDocTemplate(pDocTemplate);

	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame->LoadFrame(IDR_MAINFRAME))
		return FALSE;

	m_pMainWnd = pMainFrame;

	CDocument* pDocument = pDocTemplate->CreateNewDocument();
	
	//Get from the registry the last file that have been worked on
	CString l_strLastSetFile=GetProfileString(REG_SETTINGS_SECTION,REG_LAST_SET_FILE_ENTRY);

	m_iAutoOpenLastFile=GetProfileInt(REG_SETTINGS_SECTION,REG_AUTO_OPEN_LAST_FILE,1);

	if (m_iAutoOpenLastFile)
	{
		CFileFind l_FileFinder;
		//If a file found find it and open it
		if(l_strLastSetFile!="")
		{
			if(l_FileFinder.FindFile(l_strLastSetFile))
			{
				if (!pDocTemplate->OpenDocumentFile(l_strLastSetFile))
				{
					//If failed to open the last file, recreate new document
					WriteProfileString(REG_SETTINGS_SECTION, REG_LAST_SET_FILE_ENTRY, "");
					pDocTemplate->CreateNewDocument();
				}
			}
			else
			{
				WriteProfileString(REG_SETTINGS_SECTION,REG_LAST_SET_FILE_ENTRY,"");
			}
		}
	}
	int i=_listLanguages.GetCount();
	pDocTemplate->CreateAppFrames(pMainFrame);	//Create the application frames
	
	pMainFrame->MDIActivate(pDocTemplate->GetChildFrame(0));
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED);
	pMainFrame->UpdateWindow();

	pMainFrame->m_wndToolBar.SetButtonStyle(DISCONNECT_TOOL_BAR_INDEX,TBBS_HIDDEN);
	pMainFrame->m_wndToolBar.RedrawWindow();
	pMainFrame->m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CString	m_strVersion;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	m_strVersion = _T("");
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Text(pDX, IDC_LBL_VERSION, m_strVersion);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, &CAboutDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_strVersion=GetResString(IDS_VERSION) + " " + g_pApp->GetAppVersion();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// App command to run the dialog
void CGsmAlertApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CGsmAlertApp::OnFileNew()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertApp::OnFileNew"));
	
	CWinApp::OnFileNew();
	CMainFrame *l_pMainFrame=(CMainFrame *)m_pMainWnd;
	ASSERT_KINDOF(CMainFrame,l_pMainFrame);
	//l_pMainFrame->MDIActivate(m_pLogFrame);
}

void CGsmAlertApp::OnFileOpen()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertApp::OnFileOpen"));

	CMainFrame *l_pMainFrame=(CMainFrame *)m_pMainWnd;

	if((l_pMainFrame->m_bConnectedToUnit)||(l_pMainFrame->m_bConnectingToUnit))
	{
		AfxMessageBox(GetResString(IDS_DISCONNECT_FROM_UNIT_MSG),NULL,MB_ICONERROR);
		return;
	}

	l_pMainFrame->CloseComPort();

	CWinApp::OnFileOpen();
	ASSERT_KINDOF(CMainFrame,l_pMainFrame);
	//l_pMainFrame->MDIActivate(m_pLogFrame);
}

///////////////////////////////////////////////////////////////////////////////////////////
//
CString CGsmAlertApp::GetAppVersion()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertApp::GetAppVersion"));

    CString szResult;
    TCHAR szFullPath[256];
    DWORD dwVerHnd;
    DWORD dwVerInfoSize;
	HINSTANCE hInst = AfxGetInstanceHandle( );
	 
    // Get version information from the application
    GetModuleFileName( hInst, szFullPath, sizeof(szFullPath) );
    dwVerInfoSize = GetFileVersionInfoSize( szFullPath, &dwVerHnd );

	if( dwVerInfoSize )
    {
        // If we were able to get the information, process it:
        TCHAR   szGetName[256];

        HANDLE hMem = GlobalAlloc(GMEM_MOVEABLE, dwVerInfoSize);
        LPVOID lpvMem = GlobalLock(hMem);
        GetFileVersionInfo(szFullPath, dwVerHnd, dwVerInfoSize, lpvMem);
        lstrcpy(szGetName, _T("\\StringFileInfo\\040904b0\\") );
        int cchRoot = lstrlen( szGetName );
		lstrcpy( &szGetName[cchRoot], "ProductVersion" );

        UINT   cchVer = 0;
        LPTSTR lszVer = NULL;
		BOOL   bRet = VerQueryValue( lpvMem, szGetName, (void**)&lszVer, &cchVer );

		if( bRet && cchVer && lszVer )
			szResult = lszVer;

        GlobalUnlock( hMem );
        GlobalFree( hMem );
    }

	szResult.Replace( ",", ".");
	szResult.Replace( " ", "");
	return szResult;
}

void CGsmAlertApp::OnAutoOpenLastFile()
{
	m_iAutoOpenLastFile=!m_iAutoOpenLastFile;
	WriteProfileInt(REG_SETTINGS_SECTION,REG_AUTO_OPEN_LAST_FILE,m_iAutoOpenLastFile);
}

void CGsmAlertApp::OnUpdateAutoOpenLastFile(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(m_iAutoOpenLastFile);
}


void CAboutDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}
