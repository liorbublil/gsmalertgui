#pragma once

#include <afxtempl.h>

//Tag types
enum enmTagType
{
	TAG_TYPE_UNKNOWN = -1,
	TAG_TYPE_TAGSTICK = 0,
};


//Tag settings types
enum enmTagSensorType
{
	SET_TAGSTICK_UNKNOWN = -1,
	SET_TAGSTICK_GEN = 0,
	SET_TAGSTICK_REED = 1,
	SET_TAGSTICK_LIGHT = 2,
	SET_TAGSTICK_TEMPERATURE = 3,
	SET_TAGSTICK_HUMIDITY = 4,
};

//Settings combo types
enum enmSetSubTypes
{
	REED_REPORT_HOLD = 0,
	REED_REPORT_HOLD_AND_RELEASE = 1,
	LIGHT_REPORT_TO_LIGHT = 0,
	LIGHT_REPORT_TO_DARK = 1,
	LIGHT_REPORT_BOTH = 2,
};

#define SENSOR_TYPE_RSSI "RSSI"
#define SENSOR_TYPE_GENERAL "MISC"
#define SENSOR_TYPE_TEMPERATURE "T"
#define SENSOR_TYPE_HUMIDITY "H"
#define SENSOR_TYPE_REED "R"
#define SENSOR_TYPE_LIGHT "L"

// CWirelessList command target

class CWlSensorsListElement : public CObject
{
public:
	void SensorParamToHuminRead();
	CString SensorParamsToProtocol();
	CString GetSensorParams(int a_iParamNum);
	int ProtocolToSensorParams(CString a_strProtoParams);		//Convert protocol string to sensors paramters and return the index of the string that ends the current sensor
	enmTagSensorType m_iSensorType;
	CString m_strSensorParam;
	CString m_strSensorParamHuminRead;
};

class CWlSensorsList : public CTypedPtrList< CObList, CWlSensorsListElement* >
{
public:
	//DECLARE_SERIAL(CWlSensorsList)

	void Serialize(CArchive& archive);

	void DeleteAllList();
	CWlSensorsListElement* AddItem(enmTagSensorType a_iSensorType, CString a_strSensorParam);
	CWlSensorsListElement* GetItemBySensorType(enmTagSensorType a_iSensorType);
	void DeleteItem(enmTagSensorType a_iSensorType);
	void ProtocolToSensors(CString a_strProtoParams);
	CWlSensorsListElement* GetItemByPos(int a_iPos);
};

class CWirelessListElement : public CObject
{
public:
	CString m_strTagId;			//The RF ID of the tag
	CString m_strTagName;		//humin name of the Tag
	enmTagType m_enmTagType;	//The type of the Tag
	bool m_bInUnit;

	CWlSensorsList m_WlSensorsList;		//The settings of each sensor the Tag has
};

class CWirelessList : public CTypedPtrList< CObList, CWirelessListElement* >
{
public:
	//DECLARE_SERIAL(CWirelessList)

	CWirelessList();
	virtual ~CWirelessList();

	void Serialize(CArchive& archive);

	void DeleteAllList();
	CWirelessListElement* AddItem(CString a_strTagId, enmTagType a_enmTagType, CString a_strTagName, bool a_bInUnit);
	CWirelessListElement* GetItemByTagId(CString a_strTagId);
	CWirelessListElement* CloneTagById(CString a_strTagId, CString a_strNewTagId);
	void DeleteItem(CString a_strTagId);
	CWirelessListElement* GetItemByPos(int a_iPos);
};

CString SensorTypeToText(CString a_strSensorType);
CString SensorTypeToText(enmTagSensorType a_enmSensorType);
CString IncrementStrSuffix(CString a_strData);
