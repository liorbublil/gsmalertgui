// ZoneListView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "ZoneListView.h"
#include "MainFrm.h"
#include "LevelAlertView.h"
#include "Language.h"
#include "ZoneList.h"
#include "global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define LOC_IN_MEM_COL_INDEX 0
#define ZONE_PIN_CABLE_POS_COL_INDEX 1
#define ZONE_TYPE_COL_INDEX 2
#define ZONE_MSG_NO_TO_NC_COL_INDEX 3
#define ZONE_MSG_NC_TO_NO_COL_INDEX 4
#define ZONE_DELAY_NO_TO_NC_COL_INDEX 5
#define ZONE_DELAY_NC_TO_NO_COL_INDEX 6
#define ZONE_ACTIVE_NO_TO_NC_COL_INDEX 7
#define ZONE_ACTIVE_NC_TO_NO_COL_INDEX 8
#define ZONE_COUNTER_GAP_COL_INDEX 9

#define OUTPUT_LOC_IN_MEM_COL_INDEX 0
#define OUTPUT_PIN_CABLE_POS_COL_INDEX 1
#define OUTPUT_STATE_COL_INDEX 2
#define OUTPUT_STATE_PARAM_COL_INDEX 3

#define ZONE_TYPE_QUERY_COL_INDEX 1
#define ZONE_STATUS_QUERY_COL_INDEX 2

#define MAX_HASH_FILED_TIMES 4

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CZoneListView

IMPLEMENT_DYNCREATE(CZoneListView, CFormView)

CZoneListView::CZoneListView()
	: CFormView(CZoneListView::IDD)
	, m_iInputType(0)
	, m_strZoneCntrGap(_T(""))
{
	//{{AFX_DATA_INIT(CZoneListView)
	m_strLocInMem = _T("");
	m_strZoneMsgNoToNc = _T("");
	m_strZoneMsgNcToNo = _T("");
	m_strZoneDelayNoToNc = _T("");
	m_strZoneDelayNcToNo = _T("");
	m_bZoneActiveNoToNc = FALSE;
	m_bZoneActiveNcToNo = FALSE;

	m_strOutputStateParam = _T("");
	m_strOutputLocInMem = _T("");
	//}}AFX_DATA_INIT
	m_bFirstRun=true;
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;

	m_strZoneIndexToPinNum[0] = "1 PD";
	m_strZoneIndexToPinNum[1] = "2 PD";
	m_strZoneIndexToPinNum[2] = "3 PU";
	m_strZoneIndexToPinNum[3] = "4 PU";
	m_strZoneIndexToPinNum[4] = "15 PD";
	m_strZoneIndexToPinNum[5] = "16 PD";
	m_strZoneIndexToPinNum[6] = "17 PU";
	m_strZoneIndexToPinNum[7] = "18 PU";

	m_strOutputIndexToPinNum[0]="5";
	m_strOutputIndexToPinNum[1]="6";
	m_strOutputIndexToPinNum[2]="7";
	m_strOutputIndexToPinNum[3]="8";
}

CZoneListView::~CZoneListView()
{
}

void CZoneListView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CZoneListView)
	DDX_Control(pDX, IDC_LBL_OUTPUT_STATE_PARAM_FORMAT, m_lblOutputStateParamFormat);
	DDX_Control(pDX, IDC_LBL_ZONE_DELAY_FORMAT, m_lblZoneDelayFormat);
	DDX_Control(pDX, IDC_CMD_QUICK_COMPARE_OUTPUTS, m_cmdQuickCompareOutputs);
	DDX_Control(pDX, IDC_CMD_DOWNLOAD_OUTPUTS_TO_UNIT, m_cmdDownloadOutputsToUnit);
	DDX_Control(pDX, IDC_CMD_SET_OUTPUT, m_cmdSetOutput);
	DDX_Control(pDX, IDC_GET_OUTPUTS, m_cmdGetOutputs);
	DDX_Control(pDX, IDC_PRGS_OUTPUTS_DOWNLOAD, m_prgsOutputsDownload);
	DDX_Control(pDX, IDC_CMB_OUTPUT_STATE, m_cmbOutputState);
	DDX_Control(pDX, IDC_LBL_OUTPUT_STATE, m_lblOutputState);
	DDX_Control(pDX, IDC_TXT_OUTPUT_STATE_PARAM, m_txtOutputStateParam);
	DDX_Control(pDX, IDC_LBL_OUTPUT_STATE_PARAM, m_lblOutputStateParam);
	DDX_Control(pDX, IDC_TXT_OUTPUT_LOC_IN_MEM, m_txtOutputLocInMem);
	DDX_Control(pDX, IDC_LBL_OUTPUT_LOC_IN_MEM, m_lblOutputLocInMem);
	DDX_Control(pDX, IDC_FRAM_OUTPUTS, m_framOutputs);
	DDX_Control(pDX, IDC_LST_OUTPUTS, m_lstOutputs);

	DDX_Control(pDX, IDC_LBL_LOC_IN_MEM, m_lblLocInMem);
	DDX_Control(pDX, IDC_TXT_LOC_IN_MEM, m_txtLocInMem);
	DDX_Control(pDX, IDC_LBL_ZONE_MSG_NO_TO_NC, m_lblZoneMsgNoToNc);
	DDX_Control(pDX, IDC_TXT_ZONE_MSG_NO_TO_NC, m_txtZoneMsgNoToNc);
	DDX_Control(pDX, IDC_LBL_ZONE_MSG_NC_TO_NO, m_lblZoneMsgNcToNo);
	DDX_Control(pDX, IDC_TXT_ZONE_MSG_NC_TO_NO, m_txtZoneMsgNcToNo);
	DDX_Control(pDX, IDC_LBL_ZONE_DELAY_NO_TO_NC, m_lblZoneDelayNoToNc);
	DDX_Control(pDX, IDC_TXT_ZONE_DELAY_NO_TO_NC, m_txtZoneDelayNoToNc);
	DDX_Control(pDX, IDC_LBL_ZONE_DELAY_NC_TO_NO, m_lblZoneDelayNcToNo);
	DDX_Control(pDX, IDC_TXT_ZONE_DELAY_NC_TO_NO, m_txtZoneDelayNcToNo);
	DDX_Control(pDX, IDC_CHK_ZONE_ACTIVE_NO_TO_NC, m_chkZoneActiveNoToNc);
	DDX_Control(pDX, IDC_CHK_ZONE_ACTIVE_NC_TO_NO, m_chkZoneActiveNcToNo);

	DDX_Control(pDX, IDC_CMD_QUICK_COMPARE, m_cmdQuickCompare);
	DDX_Control(pDX, IDC_PRGS_ZONES_DOWNLOAD, m_prgsZonesDownload);
	DDX_Control(pDX, IDC_CMD_DOWNLOAD_TO_UNIT, m_cmdDownloadToUnit);
	DDX_Control(pDX, IDC_CMD_SET, m_cmdSet);
	DDX_Control(pDX, IDC_FRAM_ZONES, m_framZones);
	DDX_Control(pDX, IDC_GET_ZONES, m_cmdGetZones);
	DDX_Control(pDX, IDC_LST_ZONES, m_lstZones);

	DDX_Text(pDX, IDC_TXT_LOC_IN_MEM, m_strLocInMem);
	DDV_MaxChars(pDX, m_strLocInMem, 3);
	DDX_Text(pDX, IDC_TXT_ZONE_MSG_NO_TO_NC, m_strZoneMsgNoToNc);
	DDX_Text(pDX, IDC_TXT_ZONE_MSG_NC_TO_NO, m_strZoneMsgNcToNo);
	DDX_Text(pDX, IDC_TXT_ZONE_DELAY_NO_TO_NC, m_strZoneDelayNoToNc);
	DDX_Text(pDX, IDC_TXT_ZONE_DELAY_NC_TO_NO, m_strZoneDelayNcToNo);
	DDX_Check(pDX, IDC_CHK_ZONE_ACTIVE_NO_TO_NC, m_bZoneActiveNoToNc);
	DDX_Check(pDX, IDC_CHK_ZONE_ACTIVE_NC_TO_NO, m_bZoneActiveNcToNo);

	DDX_CBIndex(pDX, IDC_CMB_OUTPUT_STATE, m_iOutputState);
	DDX_Text(pDX, IDC_TXT_OUTPUT_STATE_PARAM, m_strOutputStateParam);
	DDX_Text(pDX, IDC_TXT_OUTPUT_LOC_IN_MEM, m_strOutputLocInMem);
	DDV_MaxChars(pDX, m_strOutputLocInMem, 1);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_CMB_INPUT_TYPE, m_cmbInputType);
	DDX_CBIndex(pDX, IDC_CMB_INPUT_TYPE, m_iInputType);
	DDX_Control(pDX, IDC_TXT_ZONE_CNTR_GAP, m_txtZoneCntrGap);
	DDX_Text(pDX, IDC_TXT_ZONE_CNTR_GAP, m_strZoneCntrGap);
	DDX_Control(pDX, IDC_LST_INPUTS_QUERY, m_lstZonesQuery);
}

BEGIN_MESSAGE_MAP(CZoneListView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CZoneListView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CMD_SET, OnCmdSet)
	ON_NOTIFY(NM_CLICK, IDC_LST_ZONES, OnClickLstZones)
	ON_BN_CLICKED(IDC_GET_ZONES, OnGetZones)
	ON_BN_CLICKED(IDC_CMD_DOWNLOAD_TO_UNIT, OnCmdDownloadToUnit)
	ON_BN_CLICKED(IDC_CMD_QUICK_COMPARE, OnCmdQuickCompare)
	ON_NOTIFY(NM_CLICK, IDC_LST_OUTPUTS, OnClickLstOutputs)
	ON_BN_CLICKED(IDC_CMD_SET_OUTPUT, OnCmdSetOutput)
	ON_BN_CLICKED(IDC_GET_OUTPUTS, OnGetOutputs)
	ON_BN_CLICKED(IDC_CMD_QUICK_COMPARE_OUTPUTS, OnCmdQuickCompareOutputs)
	ON_BN_CLICKED(IDC_CMD_DOWNLOAD_OUTPUTS_TO_UNIT, OnCmdDownloadOutputsToUnit)
	//}}AFX_MSG_MAP
	ON_CBN_SELCHANGE(IDC_CMB_INPUT_TYPE, &CZoneListView::OnCbnSelchangeCmbInputType)
	ON_BN_CLICKED(IDC_CMD_QUERY_INPUTS, &CZoneListView::OnBnClickedCmdQueryInputs)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LST_INPUTS_QUERY, &CZoneListView::OnLvnItemchangedLstInputsQuery)
	ON_BN_CLICKED(IDC_CMD_COPY_ZONES_CLIPBOARD, &CZoneListView::OnBnClickedCmdCopyZonesClipboard)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZoneListView diagnostics

#ifdef _DEBUG
void CZoneListView::AssertValid() const
{
	CFormView::AssertValid();
}

void CZoneListView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CZoneListView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CZoneListView message handlers

LRESULT CZoneListView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	CString *l_pArrivedRespond;

	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CZoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
		m_prgsZonesDownload.ShowWindow(SW_HIDE);
		m_prgsOutputsDownload.ShowWindow(SW_HIDE);
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,CString("CZoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,CString("CZoneListView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if(m_enmZoneListLastSentCommand!=enmZoneListLastSentCommandNone)
		{
			m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR | MB_OK);
			m_prgsZonesDownload.ShowWindow(SW_HIDE);
			m_prgsOutputsDownload.ShowWindow(SW_HIDE);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CZoneListView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	l_pArrivedRespond=(CString*)lParam;
	int l_iRespondStartAt;
	int l_iLastRespondStartAt=0;
	CGsmAlertDoc *l_pDoc=GetDocument();
	bool l_bItemAdd=false;
 
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(ZONES_REQ_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,ZONES_REQ_MSG_RESPOND);
			l_iDataStartAt+=(CString(ZONES_REQ_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strRecordsData;

			CString l_strDataToPars, l_strZoneMsgNoToNc,l_strZoneMsgNcToNo,l_strZoneDelayNoToNc,l_strZoneDelayNcToNo,l_strZoneActiveNoToNc,l_strZoneActiveNcToNo, l_strZoneType, l_strZoneCntrGap;
			int l_iLocInMem,l_iZoneDelayNoToNc,l_iZoneDelayNcToNo,l_iZoneActiveNoToNc,l_iZoneActiveNcToNo, l_iZoneType, l_iZoneCntrGap;

			l_strDataToPars =l_pArrivedRespond->Mid(l_iDataStartAt);
			
			if(m_enmZoneListLastSentCommand==enmZoneListLastSentCommandZonesAsk)
			{
				l_pDoc->m_ZoneList.DeleteAllList();
				m_lstZones.DeleteAllItems();
				// last argument is the delimiter
				for (l_iLocInMem = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strRecordsData, l_strDataToPars, l_iLocInMem, _T(';')); ++l_iLocInMem)
				{
					AfxExtractSubString(l_strZoneMsgNoToNc, l_strRecordsData, 0, _T(','));
					AfxExtractSubString(l_strZoneMsgNcToNo, l_strRecordsData, 1, _T(','));
					AfxExtractSubString(l_strZoneDelayNoToNc, l_strRecordsData, 2, _T(','));
					AfxExtractSubString(l_strZoneDelayNcToNo, l_strRecordsData, 3, _T(','));
					AfxExtractSubString(l_strZoneActiveNoToNc, l_strRecordsData, 4, _T(','));
					AfxExtractSubString(l_strZoneActiveNcToNo, l_strRecordsData, 5, _T(','));
					AfxExtractSubString(l_strZoneType, l_strRecordsData, 6, _T(','));
					AfxExtractSubString(l_strZoneCntrGap, l_strRecordsData, 7, _T(','));


					l_iZoneDelayNoToNc = atoi(LPCTSTR(l_strZoneDelayNoToNc));
					l_iZoneDelayNcToNo = atoi(LPCTSTR(l_strZoneDelayNcToNo));
					l_iZoneActiveNoToNc = atoi(LPCTSTR(l_strZoneActiveNoToNc));
					l_iZoneActiveNcToNo = atoi(LPCTSTR(l_strZoneActiveNcToNo));
					l_iZoneType = atoi(LPCTSTR(l_strZoneType));
					l_iZoneCntrGap = atoi(LPCTSTR(l_strZoneCntrGap));

					l_pDoc->m_ZoneList.AddItemSorted(l_iLocInMem+1, (enmZoneType)l_iZoneType, l_strZoneMsgNoToNc, l_strZoneMsgNcToNo, l_iZoneDelayNoToNc, l_iZoneDelayNcToNo, l_iZoneActiveNoToNc, l_iZoneActiveNcToNo, l_iZoneCntrGap, true, false);
					AddItemToZoneList(l_iLocInMem+1);
					m_prgsZonesDownload.SetPos(l_iLocInMem+1);
				}
				l_pDoc->SetModifiedFlag();
				l_bItemAdd=true;
			}
			else if(m_enmZoneListLastSentCommand==enmZoneListLastSentCommandQucikCompare)
			{
				int l_iEnd;
				CZoneListElement l_ZoneListElement, *l_pZoneListElement;
				for (l_iLocInMem = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strRecordsData, l_strDataToPars, l_iLocInMem, _T(';')); ++l_iLocInMem)
				{
					l_ZoneListElement.StrToElement(l_strRecordsData);

					l_pZoneListElement = l_pDoc->m_ZoneList.GetItemByLocInMem(l_ZoneListElement.m_iLocInMem);
					if(*l_pZoneListElement == l_ZoneListElement)
					{ 
						l_pZoneListElement->m_bInUnit = true;
					}
					else
					{
						l_pZoneListElement->m_bInUnit = false;
					}
				}
				l_pDoc->SetModifiedFlag();
			}
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(OUTPUTS_REQ_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,OUTPUTS_REQ_MSG_RESPOND);
			l_iDataStartAt+=(CString(OUTPUTS_REQ_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			CString l_strRecordsData;
			CString l_strOutputState,l_strOutputStateParam;
			int l_iLocInMem,l_iOutputState,l_iOutputStateParam;

			l_strRecordsData=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			if(m_enmZoneListLastSentCommand==enmZoneListLastSentCommandOutputsAsk)
			{
				for(l_iDataStartAt=0,l_iLocInMem=1;l_iLocInMem<=l_pDoc->m_iMaxOutputsAmount;++l_iLocInMem)
				{
					l_strOutputState=(l_strRecordsData.Mid(l_iDataStartAt)).Left(2);
					l_iDataStartAt +=2;
					l_strOutputStateParam=(l_strRecordsData.Mid(l_iDataStartAt)).Left(9);
					l_iDataStartAt += 9;
					l_iOutputState = atoi(LPCTSTR(l_strOutputState));
					l_iOutputStateParam= atoi(LPCTSTR(l_strOutputStateParam));
					l_pDoc->m_OutputList.AddItemSorted(l_iLocInMem,l_iOutputState,l_iOutputStateParam,true,false);
					AddItemToOutputList(l_iLocInMem,l_iOutputState,l_iOutputStateParam,true,false);
					m_prgsOutputsDownload.SetPos(l_iLocInMem);
				}
				l_pDoc->SetModifiedFlag();
				l_bItemAdd=true;
			}
			else if(m_enmZoneListLastSentCommand==enmZoneListLastSentCommandOutputsQucikCompare)
			{
				COutputListElement *l_OutputListElement;
				for(l_iDataStartAt=0,l_iLocInMem=1;l_iLocInMem<=l_pDoc->m_iMaxOutputsAmount;++l_iLocInMem)
				{
					l_strOutputState=(l_strRecordsData.Mid(l_iDataStartAt)).Left(2);
					l_iDataStartAt +=2;
					l_strOutputStateParam=(l_strRecordsData.Mid(l_iDataStartAt)).Left(9);
					l_iDataStartAt += 9;
					l_iOutputState = atoi(LPCTSTR(l_strOutputState));
					l_iOutputStateParam= atoi(LPCTSTR(l_strOutputStateParam));
					l_OutputListElement= l_pDoc->m_OutputList.GetItemByLocInMem(l_iLocInMem);
					if((l_OutputListElement->m_iOutputState==l_iOutputState)&&(l_OutputListElement->m_iOutputStateParam==l_iOutputStateParam))
					{
						l_OutputListElement->m_bInUnit=true;
						SetOutputItemImage(l_iLocInMem,true);
					}
					else
					{
						l_OutputListElement->m_bInUnit=false;
						SetOutputItemImage(l_iLocInMem,false);
					}
					m_prgsOutputsDownload.SetPos(l_iLocInMem);
				}
				l_pDoc->SetModifiedFlag();
			}
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(GET_ZONES_QUERY_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			int l_iDataStartAt = FindInString(*l_pArrivedRespond, GET_ZONES_QUERY_MSG_RESPOND);
			l_iDataStartAt += (CString(GET_ZONES_QUERY_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			CString l_strRecordsData;

			CString l_strDataToPars, l_strLocInMem, l_strZoneType, l_strZoneStatus;
			int l_iLocInMem,  l_iZoneType, l_iZoneStatus, l_iIndex;

			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);
			m_lstZonesQuery.DeleteAllItems();
			for (l_iLocInMem = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strRecordsData, l_strDataToPars, l_iLocInMem, _T(';')); ++l_iLocInMem)
			{
				AfxExtractSubString(l_strZoneType, l_strRecordsData, 0, _T(','));
				AfxExtractSubString(l_strZoneStatus, l_strRecordsData, 1, _T(','));
				
				l_iZoneType = atoi(LPCTSTR(l_strZoneType));
				l_strLocInMem.Format("%d", l_iLocInMem+1);
				l_iIndex = m_lstZonesQuery.InsertItem(LOC_IN_MEM_COL_INDEX, l_strLocInMem);
				m_lstZonesQuery.SetItemText(l_iIndex, ZONE_TYPE_QUERY_COL_INDEX, l_pDoc->GetInputTypeNameFromNum((enmZoneType)l_iZoneType));
				m_lstZonesQuery.SetItemText(l_iIndex, ZONE_STATUS_QUERY_COL_INDEX, l_strZoneStatus);
			}
			m_lstZonesQuery.SortItems(SimpleSortCompareFunc, 0);
			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;
		}
	} while(l_iRespondStartAt!=-1);

	if(l_bItemAdd)
	{
		m_lstZones.SortItems(SimpleSortCompareFunc,0);
		m_lstOutputs.SortItems(SimpleSortCompareFunc,0);
		UpdateData(FALSE);
	}

	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
		m_prgsZonesDownload.ShowWindow(SW_HIDE);
		m_prgsOutputsDownload.ShowWindow(SW_HIDE);
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,CString("exit CZoneListView::OnCommandArrived"));
	return 0;
}

void CZoneListView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	switch(m_enmZoneListLastSentCommand)
	{
	case enmZoneListLastSentCommandZonesAsk:
	case enmZoneListLastSentCommandQucikCompare:
	case enmZoneListLastSentCommandOutputsAsk:
	case enmZoneListLastSentCommandOutputsQucikCompare:
	case enmZoneListLastSentCommandQuryInputs:
		if((m_enmZoneListLastSentCommand==enmZoneListLastSentCommandZonesAsk)||(m_enmZoneListLastSentCommand==enmZoneListLastSentCommandQucikCompare))
		{
			m_prgsZonesDownload.ShowWindow(SW_HIDE);
		}
		else
		{
			m_prgsOutputsDownload.ShowWindow(SW_HIDE);
		}
		m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
		EnableControls();
		break;
	case enmZoneListLastSentCommandZonesSetAll:
		{
			CGsmAlertDoc *l_pDoc=GetDocument();
			CZoneListElement *l_ZoneListElement;
			for(int l_i=1;l_i<=l_pDoc->m_iMaxZonesAmount;++l_i)
			{
				l_ZoneListElement=l_pDoc->m_ZoneList.GetItemByLocInMem(l_i);
				l_ZoneListElement->m_bInUnit=true;
				SetItemImage(l_ZoneListElement->m_iLocInMem,true);
			}
			m_lstZones.SortItems(SimpleSortCompareFunc,0);

			m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
			m_prgsZonesDownload.ShowWindow(SW_HIDE);
			EnableControls();
			l_pDoc->SetModifiedFlag();
			return;
		}
		break;
	case enmZoneListLastSentCommandOutputsSetAll:
		{
			CGsmAlertDoc *l_pDoc=GetDocument();
			COutputListElement *l_OutputListElement;
			for(int l_i=1;l_i<=l_pDoc->m_iMaxOutputsAmount;++l_i)
			{
				l_OutputListElement=l_pDoc->m_OutputList.GetItemByLocInMem(l_i);
				l_OutputListElement->m_bInUnit=true;
				SetOutputItemImage(l_OutputListElement->m_iLocInMem,true);
			}
			m_lstOutputs.SortItems(SimpleSortCompareFunc,0);

			m_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
			m_prgsOutputsDownload.ShowWindow(SW_HIDE);
			EnableControls();
			l_pDoc->SetModifiedFlag();
			return;
		}
		break;
	}
}

void CZoneListView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnInitialUpdate"));

	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,CString("CZoneListView::OnInitialUpdate first time"));
		m_bFirstRun=false;
		
		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);
		m_pLevelEventView = ((CMPIChildFrame*)l_pWnd)->m_pLevelAlertView;
		ASSERT_KINDOF(CLevelAlertView, m_pLevelEventView);		
		
		m_lstZones.InsertColumn(LOC_IN_MEM_COL_INDEX,GetResString(IDS_LOC_IN_MEM),LVCFMT_LEFT,50);
		m_lstZones.InsertColumn(ZONE_PIN_CABLE_POS_COL_INDEX,GetResString(IDS_ZONE_PIN_CABLE_POS_COL_INDEX),LVCFMT_LEFT,100);
		m_lstZones.InsertColumn(ZONE_TYPE_COL_INDEX, GetResString(IDS_ZONE_TYPE_COL_INDEX), LVCFMT_LEFT, 100);
		m_lstZones.InsertColumn(ZONE_MSG_NO_TO_NC_COL_INDEX,GetResString(IDS_ZONE_MSG_NO_TO_NC),LVCFMT_LEFT,180);
		m_lstZones.InsertColumn(ZONE_MSG_NC_TO_NO_COL_INDEX,GetResString(IDS_ZONE_MSG_NC_TO_NO),LVCFMT_LEFT,180);
		m_lstZones.InsertColumn(ZONE_DELAY_NO_TO_NC_COL_INDEX,GetResString(IDS_ZONE_DELAY_NO_TO_NC),LVCFMT_LEFT,120);
		m_lstZones.InsertColumn(ZONE_DELAY_NC_TO_NO_COL_INDEX,GetResString(IDS_ZONE_DELAY_NC_TO_NO),LVCFMT_LEFT,120);
		m_lstZones.InsertColumn(ZONE_ACTIVE_NO_TO_NC_COL_INDEX,GetResString(IDS_ZONE_ACTIVE_NO_TO_NC),LVCFMT_LEFT,120);
		m_lstZones.InsertColumn(ZONE_ACTIVE_NC_TO_NO_COL_INDEX,GetResString(IDS_ZONE_ACTIVE_NC_TO_NO),LVCFMT_LEFT,120);
		m_lstZones.InsertColumn(ZONE_COUNTER_GAP_COL_INDEX, GetResString(IDS_ZONE_COUNTER_GAP_COL_INDEX), LVCFMT_LEFT, 120);

		m_imlZones.Create(16,16,ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_NOT_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_SEARCH),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		m_imlZones.Add(l_hIconNotLoaded_16x16);
		m_imlZones.Add(l_hIconLoaded_16x16);
		m_imlZones.Add(l_hIconSearch_16x16);
		m_lstZones.SetImageList(&m_imlZones,LVSIL_SMALL | LVSIL_NORMAL);

		//Set the list to be full row selection and with grid lines
		DWORD l_dwStyle=m_lstZones.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstZones.SetExtendedStyle(l_dwStyle);

		m_lstOutputs.InsertColumn(OUTPUT_LOC_IN_MEM_COL_INDEX ,GetResString(IDS_LOC_IN_MEM),LVCFMT_LEFT,50);
		m_lstOutputs.InsertColumn(OUTPUT_PIN_CABLE_POS_COL_INDEX ,GetResString(IDS_ZONE_PIN_CABLE_POS_COL_INDEX),LVCFMT_LEFT,100);
		m_lstOutputs.InsertColumn(OUTPUT_STATE_COL_INDEX,GetResString(IDS_IDC_LBL_OUTPUT_STATE),LVCFMT_LEFT,160);
		m_lstOutputs.InsertColumn(OUTPUT_STATE_PARAM_COL_INDEX,GetResString(IDS_IDC_LBL_OUTPUT_STATE_PARAM),LVCFMT_LEFT,150);
		m_lstOutputs.SetImageList(&m_imlZones,LVSIL_SMALL | LVSIL_NORMAL);

		//Set the list to be full row selection and with grid lines
		l_dwStyle=m_lstOutputs.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstOutputs.SetExtendedStyle(l_dwStyle);

		int l_iIndex=m_cmbOutputState.AddString(l_pDoc->GetOutputStateNameFromNum(CMB_OUTPUT_STATE_HIGTH_LOW));
		m_cmbOutputState.SetItemData(l_iIndex,CMB_OUTPUT_STATE_HIGTH_LOW);
		l_iIndex=m_cmbOutputState.AddString(l_pDoc->GetOutputStateNameFromNum(CMB_OUTPUT_STATE_BY_REQ));
		m_cmbOutputState.SetItemData(l_iIndex,CMB_OUTPUT_STATE_BY_REQ);
		l_iIndex=m_cmbOutputState.AddString(l_pDoc->GetOutputStateNameFromNum(CMB_OUTPUT_STATE_ZONE_DETECT));
		m_cmbOutputState.SetItemData(l_iIndex,CMB_OUTPUT_STATE_ZONE_DETECT);
		l_iIndex=m_cmbOutputState.AddString(l_pDoc->GetOutputStateNameFromNum(CMB_OUTPUT_STATE_ALARM));
		m_cmbOutputState.SetItemData(l_iIndex,CMB_OUTPUT_STATE_ALARM);
		l_iIndex=m_cmbOutputState.AddString(l_pDoc->GetOutputStateNameFromNum(CMB_OUTPUT_STATE_BUZZER));
		m_cmbOutputState.SetItemData(l_iIndex,CMB_OUTPUT_STATE_BUZZER);
		
		//Add the input type entries to its combo box
		l_iIndex = m_cmbInputType.AddString(l_pDoc->GetInputTypeNameFromNum(enmZoneTypeRegular));
		m_cmbInputType.SetItemData(l_iIndex, enmZoneTypeRegular);
		l_iIndex = m_cmbInputType.AddString(l_pDoc->GetInputTypeNameFromNum(enmZoneTypePulseChange));
		m_cmbInputType.SetItemData(l_iIndex, enmZoneTypePulseChange);
		l_iIndex = m_cmbInputType.AddString(l_pDoc->GetInputTypeNameFromNum(enmZoneTypePulseDown));
		m_cmbInputType.SetItemData(l_iIndex, enmZoneTypePulseDown);
		l_iIndex = m_cmbInputType.AddString(l_pDoc->GetInputTypeNameFromNum(enmZoneTypePulseUp));
		m_cmbInputType.SetItemData(l_iIndex, enmZoneTypePulseUp);

		//Set the list to be full row selection and with grid lines
		l_dwStyle = m_lstZonesQuery.GetExtendedStyle();
		l_dwStyle |= LVS_EX_FULLROWSELECT;
		l_dwStyle |= LVS_EX_GRIDLINES;
		m_lstZonesQuery.SetExtendedStyle(l_dwStyle);

		m_lstZonesQuery.InsertColumn(OUTPUT_LOC_IN_MEM_COL_INDEX, GetResString(IDS_LOC_IN_MEM), LVCFMT_LEFT, 50);
		m_lstZonesQuery.InsertColumn(ZONE_TYPE_QUERY_COL_INDEX, GetResString(IDS_ZONE_TYPE_COL_INDEX), LVCFMT_LEFT, 100);
		m_lstZonesQuery.InsertColumn(ZONE_STATUS_QUERY_COL_INDEX, GetResString(IDS_ZONE_STATUS_QUERY), LVCFMT_LEFT, 160);

		Localize();
	}

	POSITION l_Position;
	CZoneListElement *l_pListElement;
	UpdateData();

	m_lstZones.DeleteAllItems();
	l_Position=l_pDoc->m_ZoneList.GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=l_pDoc->m_ZoneList.GetNext(l_Position);
		AddItemToZoneList(l_pListElement->m_iLocInMem);
	}
	m_lstZones.SortItems(SimpleSortCompareFunc,0);

	COutputListElement *l_pOutputListElement;
	m_lstOutputs.DeleteAllItems();
	l_Position=l_pDoc->m_OutputList.GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pOutputListElement=l_pDoc->m_OutputList.GetNext(l_Position);
		AddItemToOutputList(l_pOutputListElement->m_iLocInMem,l_pOutputListElement->m_iOutputState,l_pOutputListElement->m_iOutputStateParam,l_pOutputListElement->m_bInUnit,true);
	}
	m_lstOutputs.SortItems(SimpleSortCompareFunc,0);
	UpdateData(FALSE);
}

int CZoneListView::GetZoneItemByLocInMem(int a_iLocInMem)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::GetZoneItemByLocInMem"));

	CString l_strExistLocInMem;
	for(int l_iForIndex=0;l_iForIndex<m_lstZones.GetItemCount();++l_iForIndex)
	{
		l_strExistLocInMem=m_lstZones.GetItemText(l_iForIndex,LOC_IN_MEM_COL_INDEX);
		if(atoi(l_strExistLocInMem)==a_iLocInMem)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CZoneListView::GetOutputItemByLocInMem(int a_iLocInMem)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::GetOutputItemByLocInMem"));

	CString l_strExistLocInMem;
	for(int l_iForIndex=0;l_iForIndex<m_lstOutputs.GetItemCount();++l_iForIndex)
	{
		l_strExistLocInMem=m_lstOutputs.GetItemText(l_iForIndex,OUTPUT_LOC_IN_MEM_COL_INDEX);
		if(atoi(l_strExistLocInMem)==a_iLocInMem)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

//The function add an item to the zone list
int CZoneListView::AddItemToZoneList(int a_iLocInMem)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::AddItemToZoneList"));
	CGsmAlertDoc *l_pDoc=GetDocument();

	CZoneListElement *l_ZoneListElement = l_pDoc->m_ZoneList.GetItemByLocInMem(a_iLocInMem);

	int l_iImage;
	if(l_ZoneListElement->m_bInUnit)
	{
		l_iImage=1;
	}
	else
	{
		l_iImage=0;
	}

	CString l_strLocInMem, l_strZoneDelay,l_strCntrGap;
	l_strLocInMem.Format("%d",a_iLocInMem);

	int l_iIndex;
	
	if((l_iIndex=GetZoneItemByLocInMem(a_iLocInMem))==-1)
	{
		l_iIndex=m_lstZones.InsertItem(LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
		m_lstZones.EnsureVisible(l_iIndex,false);
		m_lstZones.SetItemData(l_iIndex,a_iLocInMem);
	}
	else
	{
		m_lstZones.DeleteItem(l_iIndex);
		l_iIndex=m_lstZones.InsertItem(LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
		m_lstZones.EnsureVisible(l_iIndex,false);
		m_lstZones.SetItemData(l_iIndex,a_iLocInMem);
	}

	m_lstZones.SetItemText(l_iIndex, ZONE_PIN_CABLE_POS_COL_INDEX, m_strZoneIndexToPinNum[a_iLocInMem - 1]);
	m_lstZones.SetItemText(l_iIndex, ZONE_TYPE_COL_INDEX, l_pDoc->GetInputTypeNameFromNum(l_ZoneListElement->m_enmZoneType));
	
	switch (l_ZoneListElement->m_enmZoneType)
	{
	case enmZoneTypeRegular:
		m_lstZones.SetItemText(l_iIndex, ZONE_MSG_NO_TO_NC_COL_INDEX, l_ZoneListElement->m_strZoneMsgNoToNc);
		m_lstZones.SetItemText(l_iIndex, ZONE_MSG_NC_TO_NO_COL_INDEX, l_ZoneListElement->m_strZoneMsgNcToNo);

		l_strZoneDelay = GetSecAsTimedStr(l_ZoneListElement->m_iZoneDelayNoToNc);
		m_lstZones.SetItemText(l_iIndex, ZONE_DELAY_NO_TO_NC_COL_INDEX, l_strZoneDelay);
		l_strZoneDelay = GetSecAsTimedStr(l_ZoneListElement->m_iZoneDelayNcToNo);
		m_lstZones.SetItemText(l_iIndex, ZONE_DELAY_NC_TO_NO_COL_INDEX, l_strZoneDelay);

		if (l_ZoneListElement->m_bZoneActiveNoToNc)
		{
			m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NO_TO_NC_COL_INDEX, "V");
		}
		else
		{
			m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NO_TO_NC_COL_INDEX, "");
		}

		if (l_ZoneListElement->m_bZoneActiveNcToNo)
		{
			m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NC_TO_NO_COL_INDEX, "V");
		}
		else
		{
			m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NC_TO_NO_COL_INDEX, "");
		}
		m_lstZones.SetItemText(l_iIndex, IDC_TXT_ZONE_CNTR_GAP, "");
		break;
	case enmZoneTypePulseChange:
	case enmZoneTypePulseDown:
	case enmZoneTypePulseUp:
		
		m_lstZones.SetItemText(l_iIndex, ZONE_MSG_NO_TO_NC_COL_INDEX, "");
		m_lstZones.SetItemText(l_iIndex, ZONE_MSG_NC_TO_NO_COL_INDEX, "");
		m_lstZones.SetItemText(l_iIndex, ZONE_DELAY_NO_TO_NC_COL_INDEX, "");
		m_lstZones.SetItemText(l_iIndex, ZONE_DELAY_NC_TO_NO_COL_INDEX, "");
		m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NO_TO_NC_COL_INDEX, "");
		m_lstZones.SetItemText(l_iIndex, ZONE_ACTIVE_NC_TO_NO_COL_INDEX, "");

		l_strCntrGap.Format("%d", l_ZoneListElement->m_iZoneCounterGap);
		m_lstZones.SetItemText(l_iIndex, ZONE_COUNTER_GAP_COL_INDEX, l_strCntrGap);
	}
	
	return l_iIndex;
}

int CZoneListView::AddItemToOutputList(int a_iLocInMem,int a_iOutputState,int a_iOutputStateParam,bool a_bInUnit,bool a_bNoSearch)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::AddItemToOutputList"));
	CGsmAlertDoc *l_pDoc=GetDocument();

	int l_iImage;
	if(a_bInUnit)
	{
		l_iImage=1;
	}
	else
	{
		l_iImage=0;
	}

	CString l_strLocInMem;
	l_strLocInMem.Format("%d",a_iLocInMem);

	int l_iIndex;
	if(a_bNoSearch)
	{
		l_iIndex=m_lstOutputs.InsertItem(OUTPUT_LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
		m_lstOutputs.EnsureVisible(l_iIndex,false);
		m_lstOutputs.SetItemData(l_iIndex,a_iLocInMem);
	}
	else
	{
		if((l_iIndex=GetOutputItemByLocInMem(a_iLocInMem))==-1)
		{
			l_iIndex=m_lstOutputs.InsertItem(OUTPUT_LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
			m_lstOutputs.EnsureVisible(l_iIndex,false);
			m_lstOutputs.SetItemData(l_iIndex,a_iLocInMem);
		}
		else
		{
			m_lstOutputs.DeleteItem(l_iIndex);
			l_iIndex=m_lstOutputs.InsertItem(OUTPUT_LOC_IN_MEM_COL_INDEX,l_strLocInMem,l_iImage);
			m_lstOutputs.EnsureVisible(l_iIndex,false);
			m_lstOutputs.SetItemData(l_iIndex,a_iLocInMem);
		}
	}
	
	m_lstOutputs.SetItemText(l_iIndex,OUTPUT_PIN_CABLE_POS_COL_INDEX,m_strOutputIndexToPinNum[a_iLocInMem-1]);
	m_lstOutputs.SetItemText(l_iIndex,OUTPUT_STATE_COL_INDEX,l_pDoc->GetOutputStateNameFromNum(a_iOutputState));
	CString l_strOutputStateParam;
	switch(a_iOutputState)
	{
	case CMB_OUTPUT_STATE_HIGTH_LOW:
		l_strOutputStateParam.Format("%d",a_iOutputStateParam);
		break;
	case CMB_OUTPUT_STATE_BY_REQ:
	case CMB_OUTPUT_STATE_ZONE_DETECT:
	case CMB_OUTPUT_STATE_ALARM:
		l_strOutputStateParam=GetSecAsTimedStr(a_iOutputStateParam);
		break;
	case CMB_OUTPUT_STATE_BUZZER:
		l_strOutputStateParam="";
		break;
	}
	m_lstOutputs.SetItemText(l_iIndex,OUTPUT_STATE_PARAM_COL_INDEX,l_strOutputStateParam);
	
	return l_iIndex;
}

void CZoneListView::Localize()
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::Localize"));

	SetDlgItemText( IDC_FRAM_ZONES,GetResString(IDS_IDC_FRAM_ZONES));

	SetDlgItemText( IDC_LBL_LOC_IN_MEM,GetResString(IDS_IDC_LBL_LOC_IN_MEM));
	SetDlgItemText( IDC_LBL_ZONE_MSG_NO_TO_NC,GetResString(IDS_ZONE_MSG_NO_TO_NC));
	SetDlgItemText( IDC_LBL_ZONE_MSG_NC_TO_NO,GetResString(IDS_ZONE_MSG_NC_TO_NO));
	SetDlgItemText( IDC_LBL_ZONE_DELAY_NO_TO_NC,GetResString(IDS_ZONE_DELAY_NO_TO_NC));
	SetDlgItemText( IDC_LBL_ZONE_DELAY_NC_TO_NO,GetResString(IDS_ZONE_DELAY_NC_TO_NO));
	SetDlgItemText( IDC_CHK_ZONE_ACTIVE_NO_TO_NC,GetResString(IDS_ZONE_ACTIVE_NO_TO_NC));
	SetDlgItemText( IDC_CHK_ZONE_ACTIVE_NC_TO_NO,GetResString(IDS_ZONE_ACTIVE_NC_TO_NO));

	SetDlgItemText( IDC_GET_ZONES,GetResString(IDS_IDC_GET_ZONES));
	SetDlgItemText( IDC_CMD_SET,GetResString(IDS_IDC_CMD_SET));
	SetDlgItemText( IDC_CMD_DOWNLOAD_TO_UNIT,GetResString(IDS_IDC_CMD_DOWNLOAD_TO_UNIT));
	SetDlgItemText( IDC_CMD_QUICK_COMPARE,GetResString(IDS_IDC_CMD_QUICK_COMPARE));

	SetDlgItemText( IDC_LBL_OUTPUT_LOC_IN_MEM,GetResString(IDS_LOC_IN_MEM));
	SetDlgItemText( IDC_LBL_OUTPUT_STATE_PARAM, GetResString(IDS_IDC_LBL_OUTPUT_STATE_PARAM));
	SetDlgItemText( IDC_LBL_OUTPUT_STATE,GetResString(IDS_IDC_LBL_OUTPUT_STATE));
}

void CZoneListView::OnCmdSet() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdSet"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	int l_iZoneDelayNoToNc,l_iZoneDelayNcToNo;
	int l_iZoneCntrGap;

	UpdateData();
	if(m_strLocInMem=="")
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_NOT_ENTERED),NULL,MB_ICONERROR);
		return;
	}
	int l_iLocInMem=atoi(m_strLocInMem);

	if(l_iLocInMem<=0)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_IS_ZERO),NULL,MB_ICONERROR);
		return;
	}
	if(l_iLocInMem>l_pDoc->m_iMaxZonesAmount)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_TO_BIG),NULL,MB_ICONERROR);
		return;
	}
	int l_iInputType = m_cmbInputType.GetItemData(m_iInputType);
	switch (l_iInputType)
	{
	case enmZoneTypeRegular:
		m_strZoneMsgNoToNc.TrimRight();
		m_strZoneMsgNoToNc.TrimLeft();
		if ((m_strZoneMsgNoToNc == "") && (m_bZoneActiveNoToNc))
		{
			MessageBox(GetResString(IDS_ZONE_MSG_EMPTY), NULL, MB_ICONERROR);
			return;
		}
		m_strZoneMsgNcToNo.TrimRight();
		m_strZoneMsgNcToNo.TrimLeft();
		if ((m_strZoneMsgNcToNo == "") && (m_bZoneActiveNcToNo))
		{
			MessageBox(GetResString(IDS_ZONE_MSG_EMPTY), NULL, MB_ICONERROR);
			return;
		}
		l_iZoneDelayNoToNc = GetTimedStrAsSec(m_strZoneDelayNoToNc);
		if ((l_iZoneDelayNoToNc == -1) || (l_iZoneDelayNoToNc<1) || (l_iZoneDelayNoToNc>MAX_ZONE_DELAY))
		{
			MessageBox(GetResString(IDS_ZONE_DELAY_ILEGAL_VALUE), NULL, MB_ICONERROR);
			return;
		}
		l_iZoneDelayNcToNo = GetTimedStrAsSec(m_strZoneDelayNcToNo);
		if ((l_iZoneDelayNcToNo == -1) || (l_iZoneDelayNcToNo<1) || (l_iZoneDelayNcToNo>MAX_ZONE_DELAY))
		{
			MessageBox(GetResString(IDS_ZONE_DELAY_ILEGAL_VALUE), NULL, MB_ICONERROR);
			return;
		}
		break;
	case enmZoneTypePulseChange:
	case enmZoneTypePulseDown:
	case enmZoneTypePulseUp:
		l_iZoneDelayNoToNc = 1;
		l_iZoneDelayNcToNo = 1;
		l_iZoneCntrGap = atoi(m_strZoneCntrGap);
		if (l_iZoneCntrGap < 1)
		{
			MessageBox(GetResString(IDS_ZONE_CNTR_GAP_ILEGAL_VALUE), NULL, MB_ICONERROR);
			return;
		}
	}
	int l_iIndex;
	
	l_pDoc->m_ZoneList.AddItemSorted(l_iLocInMem, (enmZoneType)l_iInputType, m_strZoneMsgNoToNc,m_strZoneMsgNcToNo,l_iZoneDelayNoToNc,l_iZoneDelayNcToNo,m_bZoneActiveNoToNc,m_bZoneActiveNcToNo, l_iZoneCntrGap,false,false);
	l_pDoc->SetModifiedFlag();
	AddItemToZoneList(l_iLocInMem);
	m_lstZones.SortItems(SimpleSortCompareFunc,0);
	l_iIndex=GetZoneItemByLocInMem(l_iLocInMem);
	m_lstZones.EnsureVisible(l_iIndex,false);
	UpdateData(FALSE);

	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);
}

void CZoneListView::OnClickLstZones(NMHDR* pNMHDR, LRESULT* pResult)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnClickLstZones"));
	POSITION l_pos = m_lstZones.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		int l_iItem = m_lstZones.GetNextSelectedItem(l_pos);
		
		CString l_strLocInMem= m_lstZones.GetItemText(l_iItem,LOC_IN_MEM_COL_INDEX);
		int l_iLocInMem=atoi(LPCTSTR(l_strLocInMem));
		CZoneListElement *l_pZoneListElement=l_pDoc->m_ZoneList.GetItemByLocInMem(l_iLocInMem);

		SetComboBoxFromItemData(m_cmbInputType, l_pZoneListElement->m_enmZoneType);
		UpdateData();
		ApplyVarTypeFromInputType(true);
		m_strLocInMem = l_strLocInMem;
		switch (l_pZoneListElement->m_enmZoneType)
		{
		case enmZoneTypeRegular:
			m_strZoneMsgNoToNc = l_pZoneListElement->m_strZoneMsgNoToNc;
			m_strZoneMsgNcToNo = l_pZoneListElement->m_strZoneMsgNcToNo;
			m_strZoneDelayNoToNc = GetSecAsTimedStr(l_pZoneListElement->m_iZoneDelayNoToNc);
			m_strZoneDelayNcToNo = GetSecAsTimedStr(l_pZoneListElement->m_iZoneDelayNcToNo);
			m_bZoneActiveNoToNc = l_pZoneListElement->m_bZoneActiveNoToNc;
			m_bZoneActiveNcToNo = l_pZoneListElement->m_bZoneActiveNcToNo;
			m_strZoneCntrGap = "";
			break;
		case enmZoneTypePulseChange:
		case enmZoneTypePulseDown:
		case enmZoneTypePulseUp:
			m_strZoneMsgNoToNc = "";
			m_strZoneMsgNcToNo = "";
			m_strZoneDelayNoToNc = "";
			m_strZoneDelayNcToNo = "";
			m_bZoneActiveNoToNc = FALSE;
			m_bZoneActiveNcToNo = FALSE;
			m_strZoneCntrGap.Format("%d", l_pZoneListElement->m_iZoneCounterGap);
			break;
		}
		
		UpdateData(FALSE);
	}
	if(pResult)
	{
		*pResult = 0;
	}
}

void CZoneListView::OnGetZones() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnGetPhoneNum"));
	if(!(m_pMainFram->m_bConnectedToUnit))
	{
		MessageBox(GetResString(IDS_CONNECTION_ERROR),NULL,MB_ICONERROR);
		return;
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxZonesAmount;
	m_prgsZonesDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsZonesDownload.SetPos(0);

	m_iHashFailedTimes=0;

	UpdateData(FALSE);
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandZonesAsk;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);
	
	m_prgsZonesDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::SendSetCommandToUnit(enmZoneListLastSentCommand &a_enmZoneListLastSentCommand)
{

	INFO_LOG(g_DbLogger,CString("CZoneListView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc=GetDocument();

	l_pNewCommand=new CString();
	
	switch(a_enmZoneListLastSentCommand)
	{
	case enmZoneListLastSentCommandZonesAsk:
	case enmZoneListLastSentCommandQucikCompare:
		l_pNewCommand->Format("%s?",ZONES_REQ_MSG_COMMAND);
		break;
	case enmZoneListLastSentCommandZonesSetAll:
		{
			m_prgsZonesDownload.SetPos(m_iCurrentPrgrsPos);
			l_pNewCommand->Format("%s",m_strLastZonesData);
		}
		break;
	case enmZoneListLastSentCommandOutputsAsk:
	case enmZoneListLastSentCommandOutputsQucikCompare:
		l_pNewCommand->Format("%s?",OUTPUTS_REQ_MSG_COMMAND);
		break;
	case enmZoneListLastSentCommandQuryInputs:
		l_pNewCommand->Format("%s", GET_ZONES_QUERY_MSG_COMMAND);
		break;
	case enmZoneListLastSentCommandOutputsSetAll:
		{
			m_prgsOutputsDownload.SetPos(m_iCurrentPrgrsPos);
			l_pNewCommand->Format("%s=%s,",SET_OUTPUTS_MSG_COMMAND,m_strLastZonesData);
		}
		break;
	}
	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmZoneListLastSentCommand=enmZoneListLastSentCommandNone;
	}
}


void CZoneListView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::EnableControls"));

	m_txtLocInMem.EnableWindow(a_bEnabled);
	m_txtZoneMsgNoToNc.EnableWindow(a_bEnabled);
	m_txtZoneMsgNcToNo.EnableWindow(a_bEnabled);
	m_txtZoneDelayNoToNc.EnableWindow(a_bEnabled);
	m_txtZoneDelayNcToNo.EnableWindow(a_bEnabled);
	m_chkZoneActiveNoToNc.EnableWindow(a_bEnabled);
	m_chkZoneActiveNcToNo.EnableWindow(a_bEnabled);

	m_cmdGetZones.EnableWindow(a_bEnabled);
	m_cmdDownloadToUnit.EnableWindow(a_bEnabled);
	m_cmdSet.EnableWindow(a_bEnabled);
	m_cmdQuickCompare.EnableWindow(a_bEnabled);

	m_txtOutputLocInMem.EnableWindow(a_bEnabled);
	m_cmbOutputState.EnableWindow(a_bEnabled);
	m_txtOutputStateParam.EnableWindow(a_bEnabled);
	m_cmdGetOutputs.EnableWindow(a_bEnabled);
	m_cmdDownloadOutputsToUnit.EnableWindow(a_bEnabled);
	m_cmdSetOutput.EnableWindow(a_bEnabled);
	m_cmdQuickCompareOutputs.EnableWindow(a_bEnabled);
}

void CZoneListView::SetItemImage(int a_iLocInMem,int a_iInUnit)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::SetItemImage"));
	int l_iImage;
	l_iImage=a_iInUnit;
	
	int l_iIndex;
	if((l_iIndex=GetZoneItemByLocInMem(a_iLocInMem))==-1)
	{
		return;
	}
	else
	{
		CString l_strLocInMem;
		CString l_strPinPos= m_lstZones.GetItemText(l_iIndex, ZONE_PIN_CABLE_POS_COL_INDEX);
		CString l_strZoneMsgNoToNc=m_lstZones.GetItemText(l_iIndex,ZONE_MSG_NO_TO_NC_COL_INDEX);
		CString l_strZoneMsgNcToNo=m_lstZones.GetItemText(l_iIndex,ZONE_MSG_NC_TO_NO_COL_INDEX);
		CString l_strZoneDelayNoToNc=m_lstZones.GetItemText(l_iIndex,ZONE_DELAY_NO_TO_NC_COL_INDEX);
		CString l_strZoneDelayNcToNo=m_lstZones.GetItemText(l_iIndex,ZONE_DELAY_NC_TO_NO_COL_INDEX);
		CString l_strZoneActiveNoToNc=m_lstZones.GetItemText(l_iIndex,ZONE_ACTIVE_NO_TO_NC_COL_INDEX);
		CString l_strZoneActiveNcToNo=m_lstZones.GetItemText(l_iIndex,ZONE_ACTIVE_NC_TO_NO_COL_INDEX);

		l_strLocInMem.Format("%d",a_iLocInMem);

		m_lstZones.DeleteItem(l_iIndex);
		l_iIndex=m_lstZones.InsertItem(l_iIndex,l_strLocInMem,l_iImage);
		m_lstZones.SetItemData(l_iIndex,a_iLocInMem);
		m_lstZones.SetItemText(l_iIndex, ZONE_PIN_CABLE_POS_COL_INDEX, l_strPinPos);
		m_lstZones.SetItemText(l_iIndex,ZONE_MSG_NO_TO_NC_COL_INDEX,l_strZoneMsgNoToNc);
		m_lstZones.SetItemText(l_iIndex,ZONE_MSG_NC_TO_NO_COL_INDEX,l_strZoneMsgNcToNo);
		m_lstZones.SetItemText(l_iIndex,ZONE_DELAY_NO_TO_NC_COL_INDEX,l_strZoneDelayNoToNc);
		m_lstZones.SetItemText(l_iIndex,ZONE_DELAY_NC_TO_NO_COL_INDEX,l_strZoneDelayNcToNo);
		m_lstZones.SetItemText(l_iIndex,ZONE_ACTIVE_NO_TO_NC_COL_INDEX,l_strZoneActiveNoToNc);
		m_lstZones.SetItemText(l_iIndex,ZONE_ACTIVE_NC_TO_NO_COL_INDEX,l_strZoneActiveNcToNo);
		
		m_lstZones.EnsureVisible(l_iIndex,false);
	}	
}

void CZoneListView::SetOutputItemImage(int a_iLocInMem,int a_iInUnit)
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::SetOutputItemImage"));
	int l_iImage;
	l_iImage=a_iInUnit;
	
	int l_iIndex;
	if((l_iIndex=GetOutputItemByLocInMem(a_iLocInMem))==-1)
	{
		return;
	}
	else
	{
		CString l_strLocInMem;
		CString l_strPinPos = m_lstOutputs.GetItemText(l_iIndex, OUTPUT_PIN_CABLE_POS_COL_INDEX);
		CString l_strOutputState=m_lstOutputs.GetItemText(l_iIndex,OUTPUT_STATE_COL_INDEX);
		CString l_strOutputStateParam=m_lstOutputs.GetItemText(l_iIndex,OUTPUT_STATE_PARAM_COL_INDEX);

		l_strLocInMem.Format("%d",a_iLocInMem);

		m_lstOutputs.DeleteItem(l_iIndex);
		l_iIndex=m_lstOutputs.InsertItem(l_iIndex,l_strLocInMem,l_iImage);
		m_lstOutputs.SetItemData(l_iIndex,a_iLocInMem);

		m_lstOutputs.SetItemText(l_iIndex, OUTPUT_PIN_CABLE_POS_COL_INDEX, l_strPinPos);
		m_lstOutputs.SetItemText(l_iIndex,OUTPUT_STATE_COL_INDEX,l_strOutputState);
		m_lstOutputs.SetItemText(l_iIndex,OUTPUT_STATE_PARAM_COL_INDEX,l_strOutputStateParam);
		m_lstOutputs.EnsureVisible(l_iIndex,false);
	}
}

CString CZoneListView::GetZonesString()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	CZoneListElement *l_ZoneListElement;
	CString l_strLastZonesData;

	l_strLastZonesData.Format("%s=", SET_ZONES_MSG_COMMAND);

	for (int l_i = 1;l_i <= l_pDoc->m_iMaxZonesAmount;++l_i)
	{
		l_ZoneListElement = l_pDoc->m_ZoneList.GetItemByLocInMem(l_i);
		if (l_i != 1)
		{
			l_strLastZonesData += CString(";");
		}
		l_strLastZonesData += l_ZoneListElement->ElementToStr();
	}

	return l_strLastZonesData;
}

void CZoneListView::OnCmdDownloadToUnit() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdDownloadToUnit"));

	m_iCurrentPrgrsPos = 0;

	m_iTotalPrgrsPos = m_lstZones.GetItemCount();
	m_prgsZonesDownload.SetRange(0, m_iTotalPrgrsPos - 1);
	m_prgsZonesDownload.SetPos(0);
	
	m_strLastZonesData = GetZonesString();

	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandZonesSetAll;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);

	m_prgsZonesDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::OnCmdQuickCompare() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdQuickCompare"));
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxZonesAmount;
	m_prgsZonesDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsZonesDownload.SetPos(0);

	UpdateData(FALSE);
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandQucikCompare;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);
	
	m_prgsZonesDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::BlinkItem(int a_iLocInMem,bool a_bInUnit) 
{
	for(int l_iCount=0;l_iCount<4;++l_iCount)
	{
		SetItemImage(a_iLocInMem,2);
		RedrawWindow();
		Sleep(300);
		SetItemImage(a_iLocInMem,a_bInUnit);
		RedrawWindow();
		Sleep(300);
	}
}

void CZoneListView::OnClickLstOutputs(NMHDR* pNMHDR, LRESULT* pResult) 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnClickLstOutputs"));
	POSITION l_pos = m_lstOutputs.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		int l_iItem = m_lstOutputs.GetNextSelectedItem(l_pos);	  
		
		CString l_strLocInMem= m_lstOutputs.GetItemText(l_iItem,OUTPUT_LOC_IN_MEM_COL_INDEX);;
		int l_iIndex=atoi(LPCTSTR(l_strLocInMem));
		COutputListElement *l_pOutputListElement=l_pDoc->m_OutputList.GetItemByLocInMem(l_iIndex);
		SetComboBoxFromItemData(m_cmbOutputState,l_pOutputListElement->m_iOutputState);
		UpdateData();
		m_strOutputLocInMem=l_strLocInMem;
		m_strOutputStateParam= m_lstOutputs.GetItemText(l_iItem,OUTPUT_STATE_PARAM_COL_INDEX);;
		UpdateData(FALSE);
	}
	if(pResult)
	{
		*pResult = 0;
	}
}

void CZoneListView::OnCmdSetOutput() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdSetOutput"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	int l_iOutputStateParam,l_iOutputState;

	UpdateData();
	if(m_strOutputLocInMem=="")
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_NOT_ENTERED),NULL,MB_ICONERROR);
		return;
	}
	int l_iLocInMem=atoi(m_strOutputLocInMem);

	if(l_iLocInMem<=0)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_IS_ZERO),NULL,MB_ICONERROR);
		return;
	}
	if(l_iLocInMem>l_pDoc->m_iMaxZonesAmount)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_TO_BIG),NULL,MB_ICONERROR);
		return;
	}
	if(m_iOutputState==-1)
	{
		MessageBox(GetResString(IDS_OUTPUT_STATE_EMPTY),NULL,MB_ICONERROR);
		return;
	}
	l_iOutputState=m_cmbOutputState.GetItemData(m_iOutputState);
	if((m_strOutputStateParam=="")&&(l_iOutputState!=CMB_OUTPUT_STATE_BUZZER))
	{
		MessageBox(GetResString(IDS_OUTPUT_STATE_PARAM_EMPTY),NULL,MB_ICONERROR);
		return;
	}
	
	switch(l_iOutputState)
	{
	case CMB_OUTPUT_STATE_HIGTH_LOW:
		if(IsNumeric(m_strOutputStateParam)==false)
		{
			MessageBox(GetResString(IDS_OUTPUT_STATE_PARAM_ILEGAL),NULL,MB_ICONERROR);
			return;
		}
		l_iOutputStateParam=atoi(LPCTSTR(m_strOutputStateParam));
		if((l_iOutputStateParam!=0)&&(l_iOutputStateParam!=1))
		{
			MessageBox(GetResString(IDS_OUTPUT_STATE_PARAM_ILEGAL),NULL,MB_ICONERROR);
			return;
		}
		break;
	case CMB_OUTPUT_STATE_BY_REQ:
	case CMB_OUTPUT_STATE_ZONE_DETECT:
	case CMB_OUTPUT_STATE_ALARM:
		l_iOutputStateParam=GetTimedStrAsSec(m_strOutputStateParam);
		if(l_iOutputStateParam==-1)
		{
			MessageBox(GetResString(IDS_OUTPUT_STATE_PARAM_ILEGAL),NULL,MB_ICONERROR);
			return;
		}
		break;
	case CMB_OUTPUT_STATE_BUZZER:
		l_iOutputStateParam=0;
	}
	l_pDoc->m_OutputList.AddItemSorted(l_iLocInMem,l_iOutputState,l_iOutputStateParam,false,false);
	l_pDoc->SetModifiedFlag();
	AddItemToOutputList(l_iLocInMem,l_iOutputState,l_iOutputStateParam,false,false);
	m_lstOutputs.SortItems(SimpleSortCompareFunc,0);
	int l_iIndex=GetOutputItemByLocInMem(l_iLocInMem);
	m_lstOutputs.EnsureVisible(l_iIndex,false);
	UpdateData(FALSE);
}

void CZoneListView::OnGetOutputs() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnGetOutputs"));
	if(!(m_pMainFram->m_bConnectedToUnit))
	{
		MessageBox(GetResString(IDS_CONNECTION_ERROR),NULL,MB_ICONERROR);
		return;
	}
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxOutputsAmount;
	m_prgsOutputsDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsOutputsDownload.SetPos(0);

	m_iHashFailedTimes=0;

	UpdateData(FALSE);
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandOutputsAsk;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);
	
	m_prgsOutputsDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::OnCmdQuickCompareOutputs() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdQuickCompareOutputs"));
	
	CGsmAlertDoc *l_pDoc=GetDocument();

	m_iTotalPrgrsPos=l_pDoc->m_iMaxOutputsAmount;
	m_prgsOutputsDownload.SetRange(0,m_iTotalPrgrsPos);
	m_prgsOutputsDownload.SetPos(0);

	UpdateData(FALSE);
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandOutputsQucikCompare;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);
	
	m_prgsOutputsDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::OnCmdDownloadOutputsToUnit() 
{
	INFO_LOG(g_DbLogger,CString("CZoneListView::OnCmdDownloadOutputsToUnit"));

	CGsmAlertDoc *l_pDoc=GetDocument();
	COutputListElement *l_OutputListElement;
	m_iCurrentPrgrsPos=0;

	m_iTotalPrgrsPos=m_lstOutputs.GetItemCount();
	m_prgsOutputsDownload.SetRange(0,m_iTotalPrgrsPos-1);
	m_prgsOutputsDownload.SetPos(0);

	m_strLastZonesData="";
	
	for(int l_i=1;l_i<=l_pDoc->m_iMaxOutputsAmount;++l_i)
	{
		l_OutputListElement=l_pDoc->m_OutputList.GetItemByLocInMem(l_i);
		m_strLastZonesData+=l_OutputListElement->ElementToStr();
	}
	
	m_enmZoneListLastSentCommand=enmZoneListLastSentCommandOutputsSetAll;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);

	m_prgsOutputsDownload.ShowWindow(SW_SHOW);
	EnableControls(FALSE);
}

void CZoneListView::ApplyVarTypeFromInputType(bool a_bForace)
{
	if (!a_bForace)
	{
		int l_iOldType = m_iInputType;
		UpdateData();

		if (l_iOldType == m_iInputType)
		{
			return;
		}
	}

	int l_iRegType = m_cmbInputType.GetItemData(m_iInputType);

	switch (l_iRegType)
	{
	case enmZoneTypeRegular:
		m_txtZoneMsgNoToNc.EnableWindow();
		m_txtZoneMsgNcToNo.EnableWindow();
		m_txtZoneDelayNoToNc.EnableWindow();
		m_txtZoneDelayNcToNo.EnableWindow();
		m_chkZoneActiveNoToNc.EnableWindow();
		m_chkZoneActiveNcToNo.EnableWindow();
		m_txtZoneCntrGap.EnableWindow(FALSE);
		break;
	case enmZoneTypePulseChange:
	case enmZoneTypePulseDown:
	case enmZoneTypePulseUp:
		m_txtZoneMsgNoToNc.EnableWindow(FALSE);
		m_txtZoneMsgNcToNo.EnableWindow(FALSE);
		m_txtZoneDelayNoToNc.EnableWindow(FALSE);
		m_txtZoneDelayNcToNo.EnableWindow(FALSE);
		m_chkZoneActiveNoToNc.EnableWindow(FALSE);
		m_chkZoneActiveNcToNo.EnableWindow(FALSE);
		m_txtZoneCntrGap.EnableWindow();
		break;

	}
	UpdateData(FALSE);
}

void CZoneListView::OnCbnSelchangeCmbInputType()
{
	ApplyVarTypeFromInputType(false);
}


void CZoneListView::OnBnClickedCmdQueryInputs()
{
	m_enmZoneListLastSentCommand = enmZoneListLastSentCommandQuryInputs;
	SendSetCommandToUnit(m_enmZoneListLastSentCommand);
}


void CZoneListView::OnLvnItemchangedLstInputsQuery(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}


void CZoneListView::OnBnClickedCmdCopyZonesClipboard()
{
	CopyStringToClipbard(GetZonesString(), GetSafeHwnd());
}
