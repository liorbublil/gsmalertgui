/*********************************************************
* Multi-Page Interface
* Version: 1.2
* Date: September 2, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* You may freely use and modify this code, but don't remove
* this copyright note.
*
* There is no warranty of any kind, express or implied, for this class.
* The author does not take the responsibility for any damage
* resulting from the use of it.
*
* Let me know if you find this code useful, and
* send me any modifications and bug reports.
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#pragma once

#include "AlphaImageList.h"

class CMPITabCtrl : public CTabCtrl
{
public:
	CMPITabCtrl();
	virtual ~CMPITabCtrl();

public:
	// Load tabs from toolbar resource
	int LoadTabs(int nID);

protected:
	CAlphaImageList m_ImgList;
	CFont m_Font;

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
};

