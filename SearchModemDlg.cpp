// SearchModemDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "SearchModemDlg.h"
#include "Language.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSearchModemDlg dialog

CSearchModemDlg::CSearchModemDlg(CGsmAlertDoc *a_GsmAlertDoc,CWnd* pParent /*=NULL*/)
	: CDialog(CSearchModemDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSearchModemDlg)
	m_bUseModem = FALSE;
	m_strUnitPhoneNumber = _T("");
	m_bSavePass = FALSE;
	m_strPass = _T("");
	//}}AFX_DATA_INIT
	m_GsmAlertDoc=a_GsmAlertDoc;
}

void CSearchModemDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSearchModemDlg)
	DDX_Control(pDX, IDC_LBL_UNIT_PHONE_NUMBER, m_lblUnitPhoneNumber);
	DDX_Control(pDX, IDC_TXT_UNIT_PHONE_NUMBER, m_txtUnitPhoneNumber);
	DDX_Control(pDX, IDC_COM_PORTS, m_ctrlComPorts);
	DDX_Check(pDX, IDC_CHK_USE_MODEM, m_bUseModem);
	DDX_Text(pDX, IDC_TXT_UNIT_PHONE_NUMBER, m_strUnitPhoneNumber);
	DDX_Check(pDX, IDC_CHK_SAVE_PASS, m_bSavePass);
	DDX_Text(pDX, IDC_TXT_PASSWORD, m_strPass);
	DDV_MaxChars(pDX, m_strPass, MAX_PASS_LEN_DEF);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSearchModemDlg, CDialog)
	//{{AFX_MSG_MAP(CSearchModemDlg)
	ON_BN_CLICKED(IDC_CHK_USE_MODEM, OnChkUseModem)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSearchModemDlg message handlers

BOOL CSearchModemDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	int l_i,l_iNewIndex;
	char l_strComPort[10];
	CString l_strCurrentCom;
	int l_iComStartAt;

	l_strCurrentCom=m_GsmAlertDoc->m_strCom;
	for(l_i=1;l_i<=MAX_COM_PORTS;++l_i)
	{
		sprintf(l_strComPort,"%s%d","COM",l_i);
		l_iNewIndex=m_ctrlComPorts.AddString(l_strComPort);
		m_ctrlComPorts.SetItemData(l_iNewIndex, l_i);

		l_iComStartAt = l_strCurrentCom.Find("C");

		if (l_iComStartAt != -1)
		{
			if (l_strCurrentCom.Mid(l_iComStartAt) == l_strComPort)
			{
				m_ctrlComPorts.SetCurSel(l_iNewIndex);
			}
		}
	}
	m_bUseModem=m_GsmAlertDoc->m_bUseModem;
	m_strUnitPhoneNumber=m_GsmAlertDoc->m_strUnitPhoneNumber;
	m_bSavePass=m_GsmAlertDoc->m_bSavePass;
	m_strPass=m_GsmAlertDoc->m_strUnitPass;
	UpdateData(FALSE);

	m_txtUnitPhoneNumber.EnableWindow(m_bUseModem);
	m_lblUnitPhoneNumber.EnableWindow(m_bUseModem);

	Localize();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSearchModemDlg::OnOK() 
{
	int l_iSelectedComIndex;
	CString l_strSelectedCom;
	
	UpdateData();
	if((l_iSelectedComIndex=m_ctrlComPorts.GetCurSel())==CB_ERR)
	{
		MessageBox(GetResString(IDS_COM_SELECTION_ERROR));
		return;
	}
	if(m_bUseModem)
	{
		if(!IsPhoneNumOK(m_strUnitPhoneNumber))
		{
			MessageBox(GetResString(IDS_PHONE_NUM_NOT_OK),NULL,MB_ICONERROR);
			return;
		}
	}

	m_ctrlComPorts.GetLBText(l_iSelectedComIndex, l_strSelectedCom);

	if (m_ctrlComPorts.GetItemData(l_iSelectedComIndex)<10)
	{
		l_strSelectedCom.Format("COM%d", m_ctrlComPorts.GetItemData(l_iSelectedComIndex));
	}
	else
	{
		l_strSelectedCom.Format("\\\\.\\COM%d", m_ctrlComPorts.GetItemData(l_iSelectedComIndex));
	}

	m_GsmAlertDoc->m_strCom=l_strSelectedCom;
	m_GsmAlertDoc->m_bUseModem=m_bUseModem;
	m_GsmAlertDoc->m_strUnitPhoneNumber=m_strUnitPhoneNumber;
	m_GsmAlertDoc->m_bSavePass=m_bSavePass;
	m_GsmAlertDoc->m_strUnitPass=m_strPass;
	m_GsmAlertDoc->SetModifiedFlag();
	CDialog::OnOK();
}

void CSearchModemDlg::Localize() 
{
	INFO_LOG(g_DbLogger,CString("CSearchModemDlg::Localize"));

	SetWindowText(GetResString(IDS_IDD_SEARCH_MODEM_DIALOG));
	SetDlgItemText( IDC_LBL_COM_PORTS,GetResString(IDS_IDC_LBL_COM_PORTS));
	SetDlgItemText( IDC_LBL_PASSWORD,GetResString(IDS_IDC_LBL_PASSWORD));
	SetDlgItemText(IDOK,GetResString(IDS_OK));
	SetDlgItemText(IDCANCEL,GetResString(IDS_CANCEL));
	SetDlgItemText(IDC_CHK_USE_MODEM,GetResString(IDS_IDC_CHK_USE_MODEM));
	SetDlgItemText(IDC_LBL_UNIT_PHONE_NUMBER,GetResString(IDS_IDC_LBL_UNIT_PHONE_NUMBER));
	SetDlgItemText(IDC_CHK_SAVE_PASS,GetResString(IDS_IDC_CHK_SAVE_PASS));
}

void CSearchModemDlg::OnChkUseModem() 
{
	UpdateData();
	m_txtUnitPhoneNumber.EnableWindow(m_bUseModem);
	m_lblUnitPhoneNumber.EnableWindow(m_bUseModem);
}
