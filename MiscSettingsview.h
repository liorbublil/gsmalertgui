#if !defined(AFX_MISCSETTINGSVIEW_H__C71284CA_478D_4BFE_A757_1B8DB631E485__INCLUDED_)
#define AFX_MISCSETTINGSVIEW_H__C71284CA_478D_4BFE_A757_1B8DB631E485__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MiscSettingsView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMiscSettingsView form view

#include "GsmAlertDoc.h"

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CMainFrame;

class CMiscSettingsView : public CFormView
{
protected:
	CMiscSettingsView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMiscSettingsView)

public:
	enum enmMiscSettingsLastSentCommand
	{
		enmMiscSettingsLastSentCommandNone,
		enmMiscSettingsLastSentCommandSetTime,
		enmMiscSettingsLastSentCommandChangePass,
		enmMiscSettingsLastSentCommandGetUnitName,
		enmMiscSettingsLastSentCommandSetUnitName,
		enmMiscSettingsLastSentCommandGetGateDelay,
		enmMiscSettingsLastSentCommandSetGateDelay,
		enmMiscSettingsLastSentCommandGetSelfSms,
		enmMiscSettingsLastSentCommandSetSelfSms,
		enmMiscSettingsLastSentCommandGetBandSelect,
		enmMiscSettingsLastSentCommandSetBandSelect,
		enmMiscSettingsLastSentCommandGetZoneActiveMode,
		enmMiscSettingsLastSentCommandSetZoneActiveMode,
		enmMiscSettingsLastSentCommandGetVersion,
		enmMiscSettingsLastSentCommandGetReception,
		enmMiscSettingsLastSentCommandGetDebaunce,
		enmMiscSettingsLastSentCommandSetDebaunce,
		enmMiscSettingsLastSentCommandGetRemider,
		enmMiscSettingsLastSentCommandSetRemider,
		enmMiscSettingsLastSentCommandGetTestAlarm,
		enmMiscSettingsLastSentCommandSetTestAlarm,
		enmMiscSettingsLastSentCommandGetSmsLogger,
		enmMiscSettingsLastSentCommandSetSmsLogger,
		enmMiscSettingsLastSentCommandGetInternetParam,
		enmMiscSettingsLastSentCommandSetInternetParam,
		enmMiscSettingsLastSentCommandGetRssiToCloud,
		enmMiscSettingsLastSentCommandSetRssiToCloud,
	};
// Form Data
public:
	//{{AFX_DATA(CMiscSettingsView)
	enum { IDD = IDD_MISC_SETTINGS_FORM };
	CButton	m_cmdSmsToLoggerSet;
	CButton	m_cmdSmsToLoggerGet;
	CEdit	m_txtSmsPass;
	CEdit	m_txtSmsInterval;
	CEdit	m_txtTempDiff;
	CEdit	m_txtDeviceId;
	CEdit	m_txtTestAlarmMsg;
	CDateTimeCtrl	m_dtpTestAlarmTime;
	CComboBox	m_cmbTestAlarmWeekDay;
	CButton	m_rdoDailyTestAlarm;
	CButton	m_rdoWeeklyTestAlarm;
	CButton	m_chkTestAlarm;
	CEdit	m_txtReminderDelay;
	CButton	m_chkReminder;
	CButton	m_cmdReminderGet;
	CButton	m_cmdReminderSet;
	CButton	m_cmdGetReception;
	CButton	m_cmdGetVersion;
	CEdit	m_txtReception;
	CEdit	m_txtVersion;
	CButton	m_cmdSetZoneActiveMode;
	CButton	m_cmdGetZoneActiveMode;
	CButton	m_rdoZoneActiveModeEnPin;
	CButton m_rdoZoneActiveModeRing;
	CButton m_rdoZoneActiveModeSms;
	CButton	m_cmdSetBandSelect;
	CButton	m_cmdGetBandSelect;
	CEdit	m_txtSelfPhoneNum;
	CButton	m_cmdSelfCuSmsSet;
	CButton	m_cmdSelfCuSmsGet;
	CButton	m_chkSelfCuSms;
	CButton	m_cmdGetPcTime;
	CEdit	m_txtConfirmPass;
	CDateTimeCtrl	m_dtpUnitTime;
	CEdit	m_txtGateDelay;
	CEdit	m_txtUnitName;
	CEdit	m_txtNewPass;
	CEdit	m_txtCurrentPass;
	CButton	m_cmdSetUnitName;
	CButton	m_cmdGetUnitName;
	CButton	m_cmdSetUnitPass;
	CButton	m_cmdSetUnitTime;
	CButton	m_cmdSetGateDelay;
	CButton	m_cmdGetGateDelay;
	COleDateTime	m_odtUnitTime;
	CString	m_strNewPass;
	CString	m_strCurrentPass;
	CString	m_strUnitName;
	CString	m_strGateDelay;
	CString	m_strConfirmPass;
	BOOL	m_bSelfCuSmsEn;
	CString	m_strSelfPhoneNum;
	int		m_iZoneActiveMode;
	CString	m_strVersion;
	CString	m_strReception;
	CString	m_strInputDebounce;
	CString	m_strReminderDelay;
	BOOL	m_bReminderEnable;
	BOOL	m_bTestAlarmEnable;
	int		m_iTestAlarmPeriod;
	int		m_iTestAlarmWeekDay;
	COleDateTime m_odtTestAlarmTime;
	CString	m_strTestAlarmMsg;
	CString	m_strDeviceId;
	CString	m_strTempDiff;
	CString	m_strSmsInterval;
	CString	m_strSmsPass;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	CMainFrame *m_pMainFram;
	enmMiscSettingsLastSentCommand m_enmMiscSettingsLastSentCommand;
// Operations
public:
protected:
	void EnableControls(BOOL a_bEnabled=TRUE);
	void SelectiveEnableControls(BOOL a_bEnabled=TRUE);
	void TestAlarmChange();
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmMiscSettingsLastSentCommand &a_enmMiscSettingsLastSentCommand);
	void Localize();
// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMiscSettingsView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CMiscSettingsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	// Generated message map functions
	//{{AFX_MSG(CMiscSettingsView)
	afx_msg void OnCmdSetUnitCurrentTime();
	afx_msg void OnCmdSetUnitTime();
	afx_msg void OnChangeTxtCurrentPass();
	afx_msg void OnCmdSetUnitPass();
	afx_msg void OnCmdGetUnitName();
	afx_msg void OnCmdSetUnitName();
	afx_msg void OnCmdSetGateDelay();
	afx_msg void OnCmdGetGateDelay();
	afx_msg void OnCmdSelfCuSmsGet();
	afx_msg void OnCmdSelfCuSmsSet();
	afx_msg void OnChangeTxtGateDelay();
	afx_msg void OnCmdGetZoneActiveMode();
	afx_msg void OnCmdSetZoneActiveMode();
	afx_msg void OnRdoZoneActiveModEnPin();
	afx_msg void OnRdoZoneActiveModRing();
	afx_msg void OnRdoZoneActiveModSms();
	afx_msg void OnCmdGetVersion();
	afx_msg void OnCmdGetReception();
	afx_msg void OnChangeTxtReminderDelay();
	afx_msg void OnChangeTxtUnitName();
	afx_msg void OnChkSelfCuSms();
	afx_msg void OnChangeTxtSelfPhoneNum();
	afx_msg void OnChkReminder();
	afx_msg void OnCmdReminderSet();
	afx_msg void OnCmdReminderGet();
	afx_msg void OnRdoDailyTestAlarm();
	afx_msg void OnRdoWeelyTestAlarm();
	afx_msg void OnChkTestAlarm();
	afx_msg void OnSelchangeCmbTestAlarmWeekDay();
	afx_msg void OnDatetimechangeDtpTestAlarmTime(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCmdSetTestAlarm();
	afx_msg void OnCmdGetTestAlarm();
	afx_msg void OnChangeTxtTestAlarmMsg();
	afx_msg void OnCmdSmsToLoggerGet();
	afx_msg void OnCmdSmsToLoggerSet();
	afx_msg void OnChangeTxtDeviceId();
	afx_msg void OnChangeTxtTempDiff();
	afx_msg void OnChangeTxtSmsInterval();
	afx_msg void OnChangeTxtSmsPass();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCmdInternetGet();
	afx_msg void OnEnChangeTxtApn();
	afx_msg void OnEnChangeTxtGprsUser();
	afx_msg void OnEnChangeTxtGprsPass();
	afx_msg void OnEnChangeTxtCloudToken();
	afx_msg void OnEnChangeTxtCloudDns();
	CEdit m_txtApn;
	CString m_strApn;
	CEdit m_txtCloudDns;
	CString m_strCloudDns;
	CEdit m_txtCloudToken;
	CString m_strCloudToken;
	CEdit m_txtGprsPass;
	CString m_strGprsPass;
	CEdit m_txtGprsUser;
	CString m_strGprsUser;
	CButton m_chkGprsEn;
	BOOL m_bGprsEn;
	afx_msg void OnBnClickedChkGprsEn();
	afx_msg void OnBnClickedCmdInternetSet();
	CEdit m_txtCloudInterval;
	CString m_strCloudInterval;
	afx_msg void OnEnChangeTxtCloudInterval();
	CButton m_chkUseSsl;
	BOOL m_bUseSsl;
	afx_msg void OnBnClickedChkSslEn();
	afx_msg void OnBnClickedCmdRssiToCloudGet();
	afx_msg void OnBnClickedCmdRssiToCloudSet();
	BOOL m_bRssiToCloud;
	CButton m_cmdRssiToCloudGet;
	CButton m_cmdRssiToCloudSet;
};


#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CMiscSettingsView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MISCSETTINGSVIEW_H__C71284CA_478D_4BFE_A757_1B8DB631E485__INCLUDED_)
#include "afxwin.h"
