// SendQueue.h: interface for the CSendQueue class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SENDQUEUE_H__59554B73_FC45_437D_A4CC_E458982B5390__INCLUDED_)
#define AFX_SENDQUEUE_H__59554B73_FC45_437D_A4CC_E458982B5390__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSendQueueElement : public CObject 
{
public:
	CSendQueueElement();		//constractor
	~CSendQueueElement();		//distructor
	HWND m_hWnd;
	CString m_strCommand;
	bool m_bSentToUnit;
	bool m_bUnitMsg;
};

class CSendQueue: public CTypedPtrList< CObList, CSendQueueElement* >
{
public:
	~CSendQueue();

	// Go to the end of the line
    void qEnqueue(CSendQueueElement* l_newQueueElement );
	void EmptyQueue();
	void qVipEnqueue(CSendQueueElement* l_newQueueElement );
	int qGetCount();
    // Get first element in line
    CSendQueueElement* qDequeue();
	// Get first element in line, but don't remove it
	CSendQueueElement* qLookHead();
};

#endif // !defined(AFX_SENDQUEUE_H__59554B73_FC45_437D_A4CC_E458982B5390__INCLUDED_)
