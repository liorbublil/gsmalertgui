// ZoneList.h: interface for the CZoneList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ZONE_LIST_H__8975565A_F57A_4D94_B62D_491C553056D7__INCLUDED_)
#define AFX_ZONE_LIST_H__8975565A_F57A_4D94_B62D_491C553056D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

enum enmZoneType
{
	enmZoneTypeRegular = 0,
	enmZoneTypePulseChange = 4,
	enmZoneTypePulseDown = 5,
	enmZoneTypePulseUp = 6,
};
class CZoneListElement : public CObject
{
public:
	CZoneListElement();
	~CZoneListElement();
	int m_iLocInMem;
	CString m_strZoneMsgNoToNc;
	CString m_strZoneMsgNcToNo;
	int m_iZoneDelayNoToNc;
	int m_iZoneDelayNcToNo;
	bool m_bZoneActiveNoToNc;
	bool m_bZoneActiveNcToNo;
	enmZoneType m_enmZoneType;
	int m_iZoneCounterGap;
	bool m_bInUnit;
	CString ElementToStr();
	void StrToElement(CString a_strData);
	bool operator==(const CZoneListElement& rhs);
	bool operator!=(const CZoneListElement& rhs);
};

class CZoneList : public CTypedPtrList< CObList, CZoneListElement* > 
{
public:
	DECLARE_SERIAL( CZoneList )

	CZoneList();
	virtual ~CZoneList();
	void Serialize( CArchive& archive );
	
	void DeleteAllList();
	CZoneListElement* AddItemSorted(int a_iLocInMem, enmZoneType a_enmZoneCntrType, CString a_strZoneMsgNoToNc, CString a_strZoneMsgNcToNo, int a_iZoneDelayNoToNc, int a_iZoneDelayNcToNo, bool a_bZoneActiveNoToNc, bool a_bZoneActiveNcToNo, int a_iZoneCntrGap, bool a_bInUnit, bool a_bNoSearch);
	CZoneListElement* GetItemByLocInMem(int a_iLocInMem);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_ZONE_LIST_H__8975565A_F57A_4D94_B62D_491C553056D7__INCLUDED_)


