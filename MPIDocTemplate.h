/*********************************************************
* Multi-Page Interface
* Version: 1.2
* Date: September 2, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* You may freely use and modify this code, but don't remove
* this copyright note.
*
* There is no warranty of any kind, express or implied, for this class.
* The author does not take the responsibility for any damage
* resulting from the use of it.
*
* Let me know if you find this code useful, and
* send me any modifications and bug reports.
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#pragma once

#include "MainFrm.h"

class CMPIChildFrame;

class CMPIDocTemplate : public CMultiDocTemplate
{
	DECLARE_DYNAMIC(CMPIDocTemplate)

public:
	CMPIDocTemplate(UINT nIDResource, CRuntimeClass* pDocClass, CRuntimeClass* pFrameClass);

public:
	// Get child frame from index
	CMPIChildFrame* GetChildFrame(int nIndex);
	// Get index of the child frame
	int FindChildFrame(CMPIChildFrame* pFrame);

	// Create all the application frames
	void CreateAppFrames(CMainFrame *a_pMainFrame);
	CMPIChildFrame* CreateMPIFrame(int nIDResource,CMPIChildFrame::enmMPIChildFrameType a_enmMPIChildFrameType);
	

public:
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszPathName, BOOL bMakeVisible = TRUE);
	virtual CDocument* OpenDocumentFile(LPCTSTR lpszPathName, BOOL bAddToMRU, BOOL bMakeVisible);
	virtual CDocument* CreateNewDocument();
protected:
	virtual ~CMPIDocTemplate();

protected:
	CMapWordToPtr m_mapMenu;
	CMapWordToPtr m_mapAccel;
	CArray<CMPIChildFrame*, CMPIChildFrame*> m_arrChildFrm;

	DECLARE_MESSAGE_MAP()
};

