// PhoneNumList.cpp: implementation of the CPhoneNumList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GsmAlert.h"
#include "PhoneNumList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPhoneNumListElement::CPhoneNumListElement()
{
}

CPhoneNumListElement::~CPhoneNumListElement()
{
}

CPhoneNumList::CPhoneNumList()
{

}

void CPhoneNumListElement::operator=(const CPhoneNumListElement& a_PhoneNumListElement)
{
	m_bInUnit=a_PhoneNumListElement.m_bInUnit;
	m_iLocInMem=a_PhoneNumListElement.m_iLocInMem;
	m_iType=a_PhoneNumListElement.m_iType;
	m_strPhoneName=a_PhoneNumListElement.m_strPhoneName;
	m_strPhoneNum=a_PhoneNumListElement.m_strPhoneNum;
}

void CPhoneNumList::DeleteAllList()
{
	CPhoneNumListElement *l_pElementToRemove;
	
	while(!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove=RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}


CPhoneNumList::~CPhoneNumList()
{
	DeleteAllList();
}

void CPhoneNumList::Serialize( CArchive& archive )
{
	CPhoneNumListElement *l_pListElement;
    //CObject::Serialize( archive );

    // now do the stuff for our specific class
    if( archive.IsStoring() )
	{
		POSITION l_Position;
		int l_iNumbOfPhoneNumbers = GetCount();
        archive << l_iNumbOfPhoneNumbers;
		
		BOOL l_bInUnit;

		l_Position=GetHeadPosition();
		while(l_Position!=NULL)
		{
			l_pListElement=GetNext(l_Position);
			archive << l_pListElement->m_iLocInMem << l_pListElement->m_strPhoneNum <<l_pListElement->m_strPhoneName << l_pListElement->m_iType << l_pListElement->m_bInUnit;
		}
	}
    else
	{
		int l_iListCount;
		int l_iBool;
		DeleteAllList();

		archive >> l_iListCount;
		for(int l_iIndex=0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement=new CPhoneNumListElement;
			archive >> l_pListElement->m_iLocInMem >> l_pListElement->m_strPhoneNum >> l_pListElement->m_strPhoneName >> l_pListElement->m_iType >> l_pListElement->m_bInUnit;
			AddTail(l_pListElement);
		}
	}
}

void CPhoneNumList::AddItemSorted(int a_iLocInMem,CString a_strPhoneNum,CString a_strPhoneName,int a_iType,bool a_bInUnit,bool a_bNoSearch)
{
	CPhoneNumListElement *l_pListElement;
	bool l_bNew=false;
	if(a_bNoSearch)
	{
		l_bNew=true;
		l_pListElement=new CPhoneNumListElement;
	}
	else
	{
		if((l_pListElement=GetItemByLocInMem(a_iLocInMem))==NULL)
		{
			l_bNew=true;
			l_pListElement=new CPhoneNumListElement;
		}
	}
	
	l_pListElement->m_iLocInMem=a_iLocInMem;
	l_pListElement->m_strPhoneNum=a_strPhoneNum;
	l_pListElement->m_strPhoneName=a_strPhoneName;
	l_pListElement->m_bInUnit=a_bInUnit;
	l_pListElement->m_iType=a_iType;
	if(l_bNew)
	{
		AddTail(l_pListElement);
	}
}

void CPhoneNumList::DeleteItem(int a_iLocInMem)
{
	POSITION l_Position;
	CPhoneNumListElement *l_pListElement,*l_pListElementCopy;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iLocInMem==a_iLocInMem)
		{
			//Copy all the next items reversed
			while(l_Position!=NULL)
			{
				l_pListElementCopy=GetNext(l_Position);
				*l_pListElement=*l_pListElementCopy;
				if(l_pListElement->m_iLocInMem>a_iLocInMem)
				{
					--l_pListElement->m_iLocInMem;
				}
				l_pListElement=l_pListElementCopy;
			}
			l_pListElement = RemoveTail();
			delete l_pListElement;
			return;
		}
	}
}

CPhoneNumListElement* CPhoneNumList::GetItemByPhoneNum(CString a_strPhoneNum)
{
	POSITION l_Position;
	CPhoneNumListElement*l_pListElement;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_strPhoneNum==a_strPhoneNum)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CPhoneNumListElement* CPhoneNumList::GetItemByLocInMem(int a_iLocInMem)
{
	POSITION l_Position;
	CPhoneNumListElement *l_pListElement;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iLocInMem==a_iLocInMem)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CPhoneNumListElement* CPhoneNumList::GetItemByPhoneName(CString a_strPhoneName)
{
	POSITION l_Position;
	CPhoneNumListElement *l_pListElement;
	CString l_strFind;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		l_strFind=l_pListElement->m_strPhoneName;
		l_strFind.MakeLower();
		a_strPhoneName.MakeLower();
		if(l_strFind.Find(a_strPhoneName)!=-1)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

int CPhoneNumList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	CPhoneNumListElement *l_pListElement;
	int l_iCounter=0;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_bInUnit==false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(CPhoneNumList,  CObList, 0)