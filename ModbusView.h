#pragma once
#include "afxcmn.h"
#include "afxwin.h"



// CModbusView form view

class CModbusView : public CFormView
{
	DECLARE_DYNCREATE(CModbusView)

protected:
	CModbusView();           // protected constructor used by dynamic creation
	virtual ~CModbusView();

public:
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MODBUS_VIEW_FORM };
#endif
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	enum enmModbusLastSentCommand
	{
		enmModbusLastSentCommandNone,
		enmModbusLastSentCommandGetModbusList,
		enmModbusLastSentCommandSetModbusList,
		enmModbusLastSentCommandGetModbusPort,
		enmModbusLastSentCommandSetModbusPort,
		enmModbusLastSentCommandGetModbusResults,
	};

	// Attributes
public:
protected:
	bool m_bFirstRun;
	CMainFrame *m_pMainFram;
	CLevelAlertView *m_pLevelEventView;
	CString m_strSetString;
	CImageList m_imlModbusView;
	enmModbusLastSentCommand m_enmModbusLastSentCommand;
	// Operations
public:
protected:
	void HandleLastSentCommand();
	int GetModbusItemByIndex(int a_iIndex);
	int AddItemToModbusList(int a_iIndex);
	void ApplyModbusListToScreen();
	void ApplyVarTypeFromRegType(bool a_bForace);
	bool VerifyValidFeilds();
	void ClrearFeilds();
	void DeleteModbusList();
	void EnableControls(BOOL a_bEnabled = TRUE);
	void SendSetCommandToUnit(enmModbusLastSentCommand &a_enmModbusLastSentCommand);

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	// Overrides
	CGsmAlertDoc* GetDocument();
public:
	CListCtrl m_lstModbusRegs;
	virtual void OnInitialUpdate();
	afx_msg void OnBnClickedCmdAddModbusReg();
	CComboBox m_cmbModbusRegType;
	int m_iModbusRegType;
	CComboBox m_cmbModbusVarType;
	int m_iModbusVarType;
	afx_msg void OnCbnSelchangeCmbModbusRegType();
	CEdit m_txtModbusAddr;
	CString m_strModBusAddr;
	CEdit m_txtModbusKey;
	CString m_strModbusKey;
	CEdit m_txtModbusRegName;
	CString m_strModbusRegName;
	CEdit m_txtModbusSlaveId;
	CString m_strModbusSlaveId;
	afx_msg void OnBnClickedCmdRemoveModbusReg();
//	afx_msg void OnLvnItemchangedLstModbusRegs(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnClickLstModbusRegs(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedCmdReplaceModbusReg();
	afx_msg void OnBnClickedCmdGetModbusRegs();
	afx_msg void OnBnClickedCmdSetModbusRegs();
	CComboBox m_cmbModbusRegDecode;
	int m_iModbusRegDecode;

	afx_msg LRESULT OnCommandArrived(WPARAM wParam, LPARAM lParam);
	CButton m_cmdAddModbusReg;
	CButton m_cmdGetModbusReg;
	CButton m_cmdRemoveModbusReg;
	CButton m_cmdReplaceModbusReg;
	CButton m_cmdSetModbusReg;
	CComboBox m_cmbModbusBaudRate;
	int m_iModbusBaudRate;
	CComboBox m_cmbModbusCharSet;
	int m_iModbusCharSet;
	CEdit m_txtModbusMaxBlockSize;
	CString m_strModbusMaxBlockSize;
	afx_msg void OnSelchangeCmbModbusBaudRate();
	afx_msg void OnSelchangeCmbModbusCharSet();
	afx_msg void OnEnChangeTxtModbusMaxBlockSize();
	afx_msg void OnBnClickedCmdQueryModbus();
	CButton m_cmdQueyModbus;
	CListCtrl m_lstModbusQuery;
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CModbusView::GetDocument()
{
	return (CGsmAlertDoc*)m_pDocument;
}
#endif

