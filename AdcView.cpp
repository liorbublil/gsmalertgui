// AdcView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MainFrm.h"
#include "GsmAlertDoc.h"
#include "AdcView.h"
#include "LevelAlertView.h"
#include "Language.h"
#include "global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ONE_WIRE_SENSOR_NUMBER_COL_INDEX 0
#define ONE_WIRE_SENSOR_NAME_COL_INDEX 1
#define ONE_WIRE_TEMPERATURE_COL_INDEX 2
#define ONE_WIRE_DISCONNECT_MSG_COL_INDEX 2
#define ONE_WIRE_RESUME_CONNECT_MSG_COL_INDEX 3

#define ADC_SENSOR_NUMBER_COL_INDEX 0
#define ADC_SENSOR_NAME_COL_INDEX 1
#define ADC_LUT_COL_INDEX 2

#define ADC_LUT_MEASURED_COL_INDEX 0
#define ADC_LUT_ENGINEER_COL_INDEX 1

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}


/////////////////////////////////////////////////////////////////////////////
// CAdcView

IMPLEMENT_DYNCREATE(CAdcView, CFormView)

CAdcView::CAdcView()
	: CFormView(CAdcView::IDD)
	, m_strLut(_T(""))
	, m_strAdcLutEngineer(_T(""))
	, m_strAdcLutMeasured(_T(""))
{
	//{{AFX_DATA_INIT(CAdcView)
	m_bSendVinToCloud = FALSE;
	m_bSendVBatToCloud = FALSE;
	m_bSendVinAlarmToCloud = FALSE;
	m_iOneWire = -1;
	m_iOneWireToProgram = -1;
	m_strOneWireSensorName = _T("");
	m_strDisconnectedMgs = _T("");
	m_strResumeConnectMgs = _T("");
	m_iAdc = -1;
	m_strAdcSensorName = _T("");
	//}}AFX_DATA_INIT
	m_bFirstRun = true;
	m_enmAdcSettingsLastSentCommand = enmAdcSettingsLastSentCommandNone;
}

CAdcView::~CAdcView()
{
}

void CAdcView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdcView)
	DDX_Control(pDX, IDC_TXT_SENSOR_RECUME_CONNECT_MSG, m_txtResumeConnectMgs);
	DDX_Control(pDX, IDC_TXT_SENSOR_DISCONNECT_MSG, m_txtDisconnectedMgs);
	DDX_Control(pDX, IDC_CMD_QUERY_1_WIRE_SENSORS, m_cmdQueryOneWireSensors);
	DDX_Control(pDX, IDC_LST_ONE_WIRE_QUERY, m_lstOneWireQuery);
	DDX_Control(pDX, IDC_TXT_1_WIRE_SENSOR_NAME, m_txtOneWireSensorName);
	DDX_Control(pDX, IDC_CMD_SET_1_WIRE_SENSOR, m_cmdSetOneWireSensor);
	DDX_Control(pDX, IDC_CMD_REMOVE_1_WIRE_SENSOR, m_cmdRemoveOneWireSensor);
	DDX_Control(pDX, IDC_CMD_PROGRAM_1_WIRE, m_cmdProramOneWire);
	DDX_Control(pDX, IDC_CMD_GET_1_WIRE_SENSOR, m_cmdGetOneWireSensor);
	DDX_Control(pDX, IDC_CMD_ADD_1_WIRE_SENSOR, m_cmdAddOneWireSensor);
	DDX_Control(pDX, IDC_CMB_1_WIRE_TO_PRGRM, m_cmbOneWireToProgram);
	DDX_Control(pDX, IDC_CMB_1_WIRE, m_cmbOneWire);
	DDX_Control(pDX, IDC_LST_ONE_WIRE, m_lstOneWire);
	DDX_Control(pDX, IDC_CHK_SEND_VBATT_TO_CLOUD, m_chkSendVBattToCloud);
	DDX_Control(pDX, IDC_CHK_SEND_VIN_TO_CLOUD, m_chkSendVinToCloud);
	DDX_Control(pDX, IDC_CHK_SEND_VIN_ALARM_TO_CLOUD, m_chkSendVinAlarmToCloud);
	DDX_Check(pDX, IDC_CHK_SEND_VIN_TO_CLOUD, m_bSendVinToCloud);
	DDX_Check(pDX, IDC_CHK_SEND_VBATT_TO_CLOUD, m_bSendVBatToCloud);
	DDX_Check(pDX, IDC_CHK_SEND_VIN_ALARM_TO_CLOUD, m_bSendVinAlarmToCloud);
	DDX_CBIndex(pDX, IDC_CMB_1_WIRE, m_iOneWire);
	DDX_CBIndex(pDX, IDC_CMB_1_WIRE_TO_PRGRM, m_iOneWireToProgram);
	DDX_Text(pDX, IDC_TXT_1_WIRE_SENSOR_NAME, m_strOneWireSensorName);
	DDX_Text(pDX, IDC_TXT_SENSOR_DISCONNECT_MSG, m_strDisconnectedMgs);
	DDX_Text(pDX, IDC_TXT_SENSOR_RECUME_CONNECT_MSG, m_strResumeConnectMgs);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_LST_ADC, m_lstAdc);
	DDX_Control(pDX, IDC_CMB_ADC, m_cmbAdc);
	DDX_CBIndex(pDX, IDC_CMB_ADC, m_iAdc);
	DDX_Control(pDX, IDC_TXT_ADC_SENSOR_NAME, m_txtAdcSensorName);
	DDX_Text(pDX, IDC_TXT_ADC_SENSOR_NAME, m_strAdcSensorName);
	DDX_Control(pDX, IDC_TXT_ADC_LUT, m_txtLut);
	DDX_Text(pDX, IDC_TXT_ADC_LUT, m_strLut);
	DDX_Control(pDX, IDC_LST_ADC_LUT, m_lstAdcLut);
	DDX_Control(pDX, IDC_TXT_ADC_LUT_ENGINEER, m_txtAdcLutEngineer);
	DDX_Text(pDX, IDC_TXT_ADC_LUT_ENGINEER, m_strAdcLutEngineer);
	DDX_Control(pDX, IDC_TXT_ADC_LUT_MEASURED, m_txtAdcLutMeasured);
	DDX_Text(pDX, IDC_TXT_ADC_LUT_MEASURED, m_strAdcLutMeasured);
	DDX_Control(pDX, IDC_CMD_ADD_ADC_SENSOR, m_cmdAddAdcSensor);
	DDX_Control(pDX, IDC_CMD_ADD_ADC_LUT, m_cmdAddAdcLut);
	DDX_Control(pDX, IDC_CMD_GET_ADC_SENSOR, m_cmdGetAdcSensors);
	DDX_Control(pDX, IDC_CMD_REMOVE_ADC_LUT, m_cmdRemoveAdcLut);
	DDX_Control(pDX, IDC_CMD_REMOVE_ADC_SENSOR, m_cmdRemoveAdcSensor);
	DDX_Control(pDX, IDC_CMD_SET_ADC_SENSOR, m_cmdSetAdcSensors);
}


BEGIN_MESSAGE_MAP(CAdcView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CAdcView)
	ON_BN_CLICKED(IDC_CHK_SEND_VIN_TO_CLOUD, OnChkSendVinToCloud)
	ON_BN_CLICKED(IDC_CHK_SEND_VBATT_TO_CLOUD, OnChkSendVBatToCloud)
	ON_BN_CLICKED(IDC_CMD_GET_INPUT_VOLTAGE, OnCmdGetInputVoltage)
	ON_BN_CLICKED(IDC_CMD_SET_INPUT_VOLTAGE, OnCmdSetInputVoltage)
	ON_BN_CLICKED(IDC_CMD_ADD_1_WIRE_SENSOR, OnCmdAdd1WireSensor)
	ON_NOTIFY(NM_CLICK, IDC_LST_ONE_WIRE, OnClickLstOneWire)
	ON_BN_CLICKED(IDC_CMD_REMOVE_1_WIRE_SENSOR, OnCmdRemove1WireSensor)
	ON_BN_CLICKED(IDC_CMD_GET_1_WIRE_SENSOR, OnCmdGet1WireSensor)
	ON_BN_CLICKED(IDC_CMD_SET_1_WIRE_SENSOR, OnCmdSet1WireSensor)
	ON_BN_CLICKED(IDC_CMD_PROGRAM_1_WIRE, OnCmdProgram1Wire)
	ON_BN_CLICKED(IDC_CMD_QUERY_1_WIRE_SENSORS, OnCmdQuery1WireSensors)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CMD_ADD_ADC_SENSOR, &CAdcView::OnBnClickedCmdAddAdcSensor)
	ON_BN_CLICKED(IDC_CMD_ADD_ADC_LUT, &CAdcView::OnBnClickedCmdAddAdcLut)
	ON_NOTIFY(NM_CLICK, IDC_LST_ADC, &CAdcView::OnClickLstAdc)
	ON_BN_CLICKED(IDC_CMD_REMOVE_ADC_SENSOR, &CAdcView::OnBnClickedCmdRemoveAdcSensor)
	ON_BN_CLICKED(IDC_CMD_GET_ADC_SENSOR, &CAdcView::OnBnClickedCmdGetAdcSensor)
	ON_BN_CLICKED(IDC_CMD_SET_ADC_SENSOR, &CAdcView::OnBnClickedCmdSetAdcSensor)
	ON_BN_CLICKED(IDC_CMD_REMOVE_ADC_LUT, &CAdcView::OnBnClickedCmdRemoveAdcLut)
	ON_BN_CLICKED(IDC_CHK_SEND_VIN_ALARM_TO_CLOUD, &CAdcView::OnBnClickedChkSendVinAlarmToCloud)
	ON_BN_CLICKED(IDC_CMD_COPY_1_WIRE_SENSOR_CLIPBOARD, &CAdcView::OnBnClickedCmdCopy1WireSensorClipboard)
	ON_BN_CLICKED(IDC_CMD_COPY_ADC_SENSOR_CLIPBOARD, &CAdcView::OnBnClickedCmdCopyAdcSensorClipboard)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdcView diagnostics

#ifdef _DEBUG
void CAdcView::AssertValid() const
{
	CFormView::AssertValid();
}

void CAdcView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CAdcView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CAdcView message handlers

LRESULT CAdcView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	CString *l_pArrivedRespond;

	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CAdcView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,CString("CAdcView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,CString("CAdcView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if(m_enmAdcSettingsLastSentCommand!=enmAdcSettingsLastSentCommandNone)
		{
			m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR | MB_OK);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CAdcView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	l_pArrivedRespond=(CString*)lParam;
	int l_iRespondStartAt=-1;
	int l_iLastRespondStartAt=0;
	CGsmAlertDoc *l_pDoc=GetDocument();
 
	INFO_LOG(g_DbLogger,CString("CAdcView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(UNIT_VOLT_CLOUD_GET_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			CString l_strDataToPars, l_strSendVinToCloud, l_strSendVBatToCloud, l_strSendVinAlarmToCloud;
			CGsmAlertDoc *l_pDoc=GetDocument();

			int l_iDataStartAt=FindInString(*l_pArrivedRespond,UNIT_VOLT_CLOUD_GET_MSG_RESPOND);
			l_iDataStartAt+=(CString(UNIT_VOLT_CLOUD_GET_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			AfxExtractSubString(l_strSendVinToCloud, l_strDataToPars, 0, _T(','));
			AfxExtractSubString(l_strSendVBatToCloud, l_strDataToPars, 1, _T(','));
			AfxExtractSubString(l_strSendVinAlarmToCloud, l_strDataToPars, 1, _T(','));
			

			l_pDoc->m_bSendVinToCloud=atoi(l_strSendVinToCloud);
			l_pDoc->m_bSendVBatToCloud =atoi(l_strSendVBatToCloud);
			l_pDoc->m_bSendVinAlarmToCloud = atoi(l_strSendVinAlarmToCloud);
			
			l_pDoc->SetModifiedFlag();

			m_bSendVinToCloud=l_pDoc->m_bSendVinToCloud;
			m_bSendVBatToCloud = l_pDoc->m_bSendVBatToCloud;
			m_bSendVinAlarmToCloud = l_pDoc->m_bSendVinAlarmToCloud;
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(ONE_WIRE_SENSOR_GET_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			CString l_strSensorNum,l_strSensorName,l_strDataToPars,l_strLevelEvent,l_strDisconnectedMgs,l_strResumeConnectMgs;
			CGsmAlertDoc *l_pDoc=GetDocument();
			
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,ONE_WIRE_SENSOR_GET_MSG_RESPOND);
			l_iDataStartAt+=(CString(ONE_WIRE_SENSOR_GET_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			l_strDataToPars=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			l_pDoc->m_OneWireList.DeleteAllList();
			m_lstOneWire.DeleteAllItems();

			// last argument is the delimiter
			for(int l_iIndex=0; (l_strDataToPars!=CString("")) && AfxExtractSubString(l_strLevelEvent,l_strDataToPars,l_iIndex,_T(';')) ; ++l_iIndex)
			{
				AfxExtractSubString(l_strSensorNum,l_strLevelEvent,0,_T(','));
				AfxExtractSubString(l_strSensorName,l_strLevelEvent,1,_T(','));
				AfxExtractSubString(l_strDisconnectedMgs,l_strLevelEvent,2,_T(','));
				AfxExtractSubString(l_strResumeConnectMgs,l_strLevelEvent,3,_T(','));
				
				l_pDoc->m_OneWireList.AddItemSorted(atoi(l_strSensorNum),l_strSensorName,l_strDisconnectedMgs,l_strResumeConnectMgs,true,true);
				AddItemToOneWireList(atoi(l_strSensorNum),l_strSensorName,l_strDisconnectedMgs,l_strResumeConnectMgs,true,true);
			}
			m_lstOneWire.SortItems(SimpleSortCompareFunc,0);
			this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED,0,0);
			l_pDoc->SetModifiedFlag();
			
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;

		}

		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(ONE_WIRE_SENSOR_QUERY_MSG_RESPOND)+CString(ACK_RESPOND)),l_iLastRespondStartAt))!=-1)
		{
			CString l_strSensorNum,l_strSensorName,l_strTemperature,l_strDataToPars,l_strTmp;
			CGsmAlertDoc *l_pDoc=GetDocument();
			
			
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,ONE_WIRE_SENSOR_QUERY_MSG_RESPOND);
			l_iDataStartAt+=(CString(ONE_WIRE_SENSOR_QUERY_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			l_strDataToPars=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			m_lstOneWireQuery.DeleteAllItems();
			
			// last argument is the delimiter
			for(int l_iIndex=0; (l_strDataToPars!=CString("")) && AfxExtractSubString(l_strTmp,l_strDataToPars,l_iIndex,_T(';')) ; ++l_iIndex)
			{
				AfxExtractSubString(l_strSensorNum,l_strTmp,0,_T(','));
				AfxExtractSubString(l_strSensorName,l_strTmp,1,_T(','));
				AfxExtractSubString(l_strTemperature,l_strTmp,2,_T(','));

				
				
				AddItemToOneWireQueryList(atoi(l_strSensorNum),l_strSensorName,l_strTemperature);
			}
			m_lstOneWireQuery.SortItems(SimpleSortCompareFunc,0);

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
			
		}

		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(ADC_SENSOR_GET_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			CString l_strSensorNum, l_strSensorName, l_strDataToPars, l_strLevelEventId, l_strLut;
			CGsmAlertDoc *l_pDoc = GetDocument();

			int l_iDataStartAt = FindInString(*l_pArrivedRespond, ADC_SENSOR_GET_MSG_RESPOND);
			l_iDataStartAt += (CString(ADC_SENSOR_GET_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			l_pDoc->m_AdcList.DeleteAllList();
			m_lstAdc.DeleteAllItems();

			// last argument is the delimiter
			for (int l_iIndex = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strLevelEventId, l_strDataToPars, l_iIndex, _T(',')); )
			{
				AfxExtractSubString(l_strSensorNum, l_strDataToPars, l_iIndex++, _T(','));
				AfxExtractSubString(l_strSensorName, l_strDataToPars, l_iIndex++, _T(','));
				AfxExtractSubString(l_strLut, l_strDataToPars, l_iIndex++, _T(','));
				AfxExtractSubString(l_strLevelEventId, l_strDataToPars, l_iIndex++, _T(','));
				
				l_pDoc->m_AdcList.AddItemSorted(atoi(l_strSensorNum), l_strSensorName, l_strLut,true, true);
				AddItemToAdcList(atoi(l_strSensorNum));
			}
			m_lstAdc.SortItems(SimpleSortCompareFunc, 0);
			this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);
			l_pDoc->SetModifiedFlag();


			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;

		}
	} while(l_iRespondStartAt!=-1);
	UpdateData(FALSE);

	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,CString("exit CAdcView::OnCommandArrived"));
}

void CAdcView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,CString("CAdcView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	switch(m_enmAdcSettingsLastSentCommand)
	{
		case enmAdcSettingsLastSentCommandInputVoltageGet:
		case enmAdcSettingsLastSentCommandInputVoltageSet:
		case m_enmAdcSettingsLastSentCommandOneWireListGet:
		case m_enmAdcSettingsLastSentCommandOneWireMark:
		case m_enmAdcSettingsLastSentCommandOneWireQuery:
		case m_enmAdcSettingsLastSentCommandAdcListGet:
			m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
			EnableControls();
			return;

		case m_enmAdcSettingsLastSentCommandOneWireListSet:
			{
				CGsmAlertDoc *l_pDoc=GetDocument();
				COneWireListElement*l_ListElement;
				for(int l_i=1;l_i<=l_pDoc->m_OneWireList.GetCount();++l_i)
				{
					l_ListElement=l_pDoc->m_OneWireList.GetItemBySensorNum(l_i);
					l_ListElement->m_bInUnit=true;
					SetOneWireItemImage(l_ListElement->m_iSensorNumber,true);
				}
				m_lstOneWire.SortItems(SimpleSortCompareFunc,0);
				
				m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
				EnableControls();
				l_pDoc->SetModifiedFlag();
				return;
			}
		break;
		case m_enmAdcSettingsLastSentCommandAdcListSet:
		{
			CGsmAlertDoc *l_pDoc = GetDocument();
			CAdcListElement*l_ListElement;
			for (int l_i = 1;l_i <= l_pDoc->m_AdcList.GetCount();++l_i)
			{
				l_ListElement = l_pDoc->m_AdcList.GetItemByPos(l_i);
				l_ListElement->m_bInUnit = true;
				AddItemToAdcList(l_ListElement->m_iSensorNumber);
			}
			m_lstAdc.SortItems(SimpleSortCompareFunc, 0);

			m_enmAdcSettingsLastSentCommand = enmAdcSettingsLastSentCommandNone;
			EnableControls();
			l_pDoc->SetModifiedFlag();
			return;
		}
		break;
	}
}

void CAdcView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger,CString("CAdcView::EnableControls"));

	UpdateData();

	m_chkSendVinToCloud.EnableWindow(a_bEnabled);
	m_chkSendVBattToCloud.EnableWindow(a_bEnabled);
	m_chkSendVinAlarmToCloud.EnableWindow(a_bEnabled);
	m_lstOneWire.EnableWindow(a_bEnabled);
	m_cmbOneWire.EnableWindow(a_bEnabled);
	m_txtOneWireSensorName.EnableWindow(a_bEnabled);
	m_txtDisconnectedMgs.EnableWindow(a_bEnabled);
	m_txtResumeConnectMgs.EnableWindow(a_bEnabled);
	m_cmdAddOneWireSensor.EnableWindow(a_bEnabled);
	m_cmdRemoveOneWireSensor.EnableWindow(a_bEnabled);
	m_cmdGetOneWireSensor.EnableWindow(a_bEnabled);
	m_cmdSetOneWireSensor.EnableWindow(a_bEnabled);
	m_cmdProramOneWire.EnableWindow(a_bEnabled);
	m_cmbOneWireToProgram.EnableWindow(a_bEnabled);

	m_lstOneWireQuery.EnableWindow(a_bEnabled);
	m_cmdQueryOneWireSensors.EnableWindow(a_bEnabled);

	m_lstAdc.EnableWindow(a_bEnabled);
	m_cmbAdc.EnableWindow(a_bEnabled);
	m_txtAdcSensorName.EnableWindow(a_bEnabled);
	m_cmdAddAdcSensor.EnableWindow(a_bEnabled);
	m_cmdRemoveAdcSensor.EnableWindow(a_bEnabled);
	m_cmdGetAdcSensors.EnableWindow(a_bEnabled);
	m_cmdSetAdcSensors.EnableWindow(a_bEnabled);

	m_lstAdcLut.EnableWindow(a_bEnabled);
	m_txtAdcLutEngineer.EnableWindow(a_bEnabled);
	m_txtAdcLutMeasured.EnableWindow(a_bEnabled);
	m_cmdAddAdcLut.EnableWindow(a_bEnabled);
	m_cmdRemoveAdcLut.EnableWindow(a_bEnabled);
}

void CAdcView::SendSetCommandToUnit(enmAdcSettingsLastSentCommand &a_enmAdcSettingsLastSentCommand)
{
	INFO_LOG(g_DbLogger,CString("CAdcView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc=GetDocument();

	l_pNewCommand=new CString();
	
	switch(a_enmAdcSettingsLastSentCommand)
	{
	case enmAdcSettingsLastSentCommandInputVoltageGet:
		l_pNewCommand->Format("%s",UNIT_VOLT_CLOUD_GET_MSG_COMMAND);
		break;
	case enmAdcSettingsLastSentCommandInputVoltageSet:
		l_pNewCommand->Format("%s%s",UNIT_VOLT_CLOUD_SET_MSG_COMMAND,m_strSetString);
		break;
	case m_enmAdcSettingsLastSentCommandOneWireListGet:
		l_pNewCommand->Format("%s",ONE_WIRE_SENSOR_GET_MSG_COMMAND);
		break;
	case m_enmAdcSettingsLastSentCommandOneWireListSet:
		l_pNewCommand->Format("%s",m_strSetString);
		break;
	case m_enmAdcSettingsLastSentCommandOneWireMark:
		l_pNewCommand->Format("%s%s",ONE_WIRE_SENSOR_MARK_MSG_COMMAND,m_strSetString);
		break;
	case m_enmAdcSettingsLastSentCommandOneWireQuery:
		l_pNewCommand->Format("%s",ONE_WIRE_SENSOR_QUERY_MSG_COMMAND);
		break;

	case m_enmAdcSettingsLastSentCommandAdcListGet:
		l_pNewCommand->Format("%s", ADC_SENSOR_GET_MSG_COMMAND);
		break;
	case m_enmAdcSettingsLastSentCommandAdcListSet:
		l_pNewCommand->Format("%s", m_strSetString);
		break;
	}
	
	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandNone;
	}
}

void CAdcView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	INFO_LOG(g_DbLogger,CString("CAdcView::OnInitialUpdate"));
	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,CString("CAdcView::OnInitialUpdate first run"));

		m_bFirstRun=false;

		m_lstOneWire.InsertColumn(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,GetResString(IDS_SENSOR_NUMBER),LVCFMT_LEFT,90);
		m_lstOneWire.InsertColumn(ONE_WIRE_SENSOR_NAME_COL_INDEX,GetResString(IDS_SENSOR_NAME),LVCFMT_LEFT,80);
		m_lstOneWire.InsertColumn(ONE_WIRE_DISCONNECT_MSG_COL_INDEX,GetResString(IDS_DISCONNECT_MSG),LVCFMT_LEFT,170);
		m_lstOneWire.InsertColumn(ONE_WIRE_RESUME_CONNECT_MSG_COL_INDEX,GetResString(IDS_RESUME_CONNECT_MSG),LVCFMT_LEFT,170);		

		m_lstOneWireQuery.InsertColumn(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,GetResString(IDS_SENSOR_NUMBER),LVCFMT_LEFT,90);
		m_lstOneWireQuery.InsertColumn(ONE_WIRE_SENSOR_NAME_COL_INDEX,GetResString(IDS_SENSOR_NAME),LVCFMT_LEFT,80);
		m_lstOneWireQuery.InsertColumn(ONE_WIRE_TEMPERATURE_COL_INDEX,GetResString(IDS_SENSOR_TEMPERATURE),LVCFMT_LEFT,80);


		m_lstAdc.InsertColumn(ADC_SENSOR_NUMBER_COL_INDEX, GetResString(IDS_SENSOR_NUMBER), LVCFMT_LEFT, 90);
		m_lstAdc.InsertColumn(ADC_SENSOR_NAME_COL_INDEX, GetResString(IDS_SENSOR_NAME), LVCFMT_LEFT, 80);
		m_lstAdc.InsertColumn(ADC_LUT_COL_INDEX, GetResString(IDS_LOOK_UP_TABLE), LVCFMT_LEFT, 250);

		m_lstAdcLut.InsertColumn(ADC_LUT_MEASURED_COL_INDEX, GetResString(IDS_ADC_LUT_MEASURED), LVCFMT_LEFT, 70);
		m_lstAdcLut.InsertColumn(ADC_LUT_ENGINEER_COL_INDEX, GetResString(IDS_ADC_LUT_ENGINEER), LVCFMT_LEFT, 100);
		 

		m_imlAdcView.Create(16,16,ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_NOT_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_LOADED),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE( IDI_SEARCH),IMAGE_ICON,16,16,LR_DEFAULTCOLOR);
		m_imlAdcView.Add(l_hIconNotLoaded_16x16);
		m_imlAdcView.Add(l_hIconLoaded_16x16);
		m_imlAdcView.Add(l_hIconSearch_16x16 );
		m_lstOneWire.SetImageList(&m_imlAdcView,LVSIL_SMALL | LVSIL_NORMAL);
		m_lstAdc.SetImageList(&m_imlAdcView, LVSIL_SMALL | LVSIL_NORMAL);
		
		//Set the list to be full row selection and with grid lines
		DWORD l_dwStyle=m_lstOneWire.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstOneWire.SetExtendedStyle(l_dwStyle);
		m_lstOneWireQuery.SetExtendedStyle(l_dwStyle);
		m_lstAdc.SetExtendedStyle(l_dwStyle);
		m_lstAdcLut.SetExtendedStyle(l_dwStyle);
		
		CString l_strTmp;
		int l_iNewIndex;
		for(int l_iIndex=0;l_iIndex<MAX_1_WIRE_SENSORS;++l_iIndex)
		{
			l_strTmp.Format("%d",l_iIndex+1);
			l_iNewIndex = m_cmbOneWire.AddString(l_strTmp);
			m_cmbOneWire.SetItemData(l_iNewIndex,l_iIndex+1);
			l_iNewIndex = m_cmbOneWireToProgram.AddString(l_strTmp);
			m_cmbOneWireToProgram.SetItemData(l_iNewIndex,l_iIndex+1);
		}

		for (int l_iIndex = 0;l_iIndex<MAX_ADC_SENSORS;++l_iIndex)
		{
			l_strTmp.Format("%d", l_iIndex + 1);
			l_iNewIndex = m_cmbAdc.AddString(l_strTmp);
			m_cmbAdc.SetItemData(l_iNewIndex, l_iIndex + 1);
		}

		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);
		m_pLevelEventView=((CMPIChildFrame*)l_pWnd)->m_pLevelAlertView;
		ASSERT_KINDOF(CLevelAlertView ,m_pLevelEventView);

		Localize();
	}
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	m_bSendVBatToCloud=l_pDoc->m_bSendVBatToCloud;
	m_bSendVinToCloud=l_pDoc->m_bSendVinToCloud;
	m_bSendVinAlarmToCloud = l_pDoc->m_bSendVinAlarmToCloud;

	m_lstOneWire.DeleteAllItems();
	POSITION l_Position;
	COneWireListElement *l_pListElement;
	l_Position=l_pDoc->m_OneWireList.GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=l_pDoc->m_OneWireList.GetNext(l_Position);
		AddItemToOneWireList(l_pListElement->m_iSensorNumber,l_pListElement->m_strSensorName,l_pListElement->m_strDisconnectedMgs,l_pListElement->m_strResumeConnectMgs,l_pListElement->m_bInUnit,true);
	}
	m_lstOneWire.SortItems(SimpleSortCompareFunc,0);

	CAdcListElement *l_pAdcListElement;
	l_Position = l_pDoc->m_AdcList.GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pAdcListElement = l_pDoc->m_AdcList.GetNext(l_Position);
		AddItemToAdcList(l_pAdcListElement->m_iSensorNumber);
	}
	m_lstOneWire.SortItems(SimpleSortCompareFunc, 0);

	UpdateData(FALSE);
	EnableControls();
}

void CAdcView::Localize()
{
	INFO_LOG(g_DbLogger,CString("CAdcView::Localize"));

}

void CAdcView::OnChkSendVinToCloud() 
{
	UpdateData();

	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_bSendVinToCloud = m_bSendVinToCloud;
	l_pDoc->m_bSendVBatToCloud = m_bSendVBatToCloud;
	l_pDoc->m_bSendVinAlarmToCloud = m_bSendVinAlarmToCloud;
	l_pDoc->SetModifiedFlag();
}

void CAdcView::OnBnClickedChkSendVinAlarmToCloud()
{
	OnChkSendVinToCloud();
}


void CAdcView::OnChkSendVBatToCloud() 
{
	OnChkSendVinToCloud();
}

void CAdcView::OnCmdGetInputVoltage() 
{
	m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandInputVoltageGet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);
	
	EnableControls(FALSE);
}

void CAdcView::OnCmdSetInputVoltage() 
{
	UpdateData();
	
	m_strSetString.Format("%d,%d,%d",m_bSendVinToCloud,m_bSendVBatToCloud, m_bSendVinAlarmToCloud);

	m_enmAdcSettingsLastSentCommand=enmAdcSettingsLastSentCommandInputVoltageSet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);
	
	EnableControls(FALSE);
}

void CAdcView::OnCmdAdd1WireSensor() 
{
	INFO_LOG(g_DbLogger,CString("CAdcView::OnCmdAdd1WireSensor"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	UpdateData();
	if(m_iOneWire==-1)
	{
		MessageBox(GetResString(IDS_ONE_WIRE_SENSOR_NOT_SELECTED),NULL,MB_ICONERROR);
		return;
	}
	int l_iSensorNum=m_cmbOneWire.GetItemData(m_iOneWire);
	
	CString l_strPhoneNum,l_strMaxPhoneLen;
	CString l_strPhoneName,l_strMaxPhoneNameLen;
	
	l_pDoc->m_OneWireList.AddItemSorted(l_iSensorNum,m_strOneWireSensorName,m_strDisconnectedMgs,m_strResumeConnectMgs,false,false);
	l_pDoc->SetModifiedFlag();
	AddItemToOneWireList(l_iSensorNum,m_strOneWireSensorName,m_strDisconnectedMgs,m_strResumeConnectMgs,false,false);
	m_lstOneWire.SortItems(SimpleSortCompareFunc,0);
	int l_iIndex=GetOneWireItemByNumber(l_iSensorNum);
	m_lstOneWire.EnsureVisible(l_iIndex,false);
	m_strOneWireSensorName="";
	m_strDisconnectedMgs="";
	m_strResumeConnectMgs="";
	m_iOneWire=-1;
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED,0,0);
}

int CAdcView::AddItemToOneWireList(int a_iSensorNum,CString a_strSensorName,CString a_strDisconnectedMgs,CString a_strResumeConnectMgs, bool a_bInUnit,bool a_bNoSearch)
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	INFO_LOG(g_DbLogger,CString("CAdcView::AddItemToOneWireList"));
	
	int l_iImage;
	if(a_bInUnit)
	{
		l_iImage=1;
	}
	else
	{
		l_iImage=0;
	}
	
	CString l_strSensorNum;
	l_strSensorNum.Format("%d",a_iSensorNum);
	
	
	int l_iIndex;
	if(a_bNoSearch)
	{
		l_iIndex=m_lstOneWire.InsertItem(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,l_strSensorNum,l_iImage);
		m_lstOneWire.SetItemData(l_iIndex,a_iSensorNum);
	}
	else
	{
		if((l_iIndex=GetOneWireItemByNumber(a_iSensorNum))==-1)
		{
			l_iIndex=m_lstOneWire.InsertItem(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,l_strSensorNum,l_iImage);
			m_lstOneWire.EnsureVisible(l_iIndex,false);
			m_lstOneWire.SetItemData(l_iIndex,a_iSensorNum);
		}
		else
		{
			m_lstOneWire.DeleteItem(l_iIndex);
			l_iIndex=m_lstOneWire.InsertItem(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,l_strSensorNum,l_iImage);
			m_lstOneWire.EnsureVisible(l_iIndex,false);
			m_lstOneWire.SetItemData(l_iIndex,a_iSensorNum);
		}
	}
	m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_SENSOR_NAME_COL_INDEX,a_strSensorName);
	m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_DISCONNECT_MSG_COL_INDEX,a_strDisconnectedMgs);
	m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_RESUME_CONNECT_MSG_COL_INDEX,a_strResumeConnectMgs);
	
	return l_iIndex;
}


int CAdcView::AddItemToOneWireQueryList(int a_iSensorNum,CString a_strSensorName,CString a_strSensorTemperature)
{
	INFO_LOG(g_DbLogger,CString("CAdcView::AddItemToOneWireQueryList"));
	
	CString l_strSensorNum;
	l_strSensorNum.Format("%d",a_iSensorNum);
	
	
	int l_iIndex;

	l_iIndex=m_lstOneWireQuery.InsertItem(ONE_WIRE_SENSOR_NUMBER_COL_INDEX,l_strSensorNum);
	m_lstOneWireQuery.SetItemData(l_iIndex,a_iSensorNum);
	m_lstOneWireQuery.SetItemText(l_iIndex,ONE_WIRE_SENSOR_NAME_COL_INDEX,a_strSensorName);
	m_lstOneWireQuery.SetItemText(l_iIndex,ONE_WIRE_TEMPERATURE_COL_INDEX,a_strSensorTemperature);
	
	return l_iIndex;
}


void CAdcView::SetOneWireItemImage(int a_iSensorNum,int a_iInUnit)
{
	INFO_LOG(g_DbLogger,CString("CAdcView::SetOneWireItemImage"));
	int l_iImage;
	l_iImage=a_iInUnit;
	
	int l_iIndex;
	if((l_iIndex=GetOneWireItemByNumber(a_iSensorNum))==-1)
	{
		return;
	}
	else
	{
		CString l_strSensorName=m_lstOneWire.GetItemText(l_iIndex,ONE_WIRE_SENSOR_NAME_COL_INDEX);
		CString l_strDisconnectedMgs=m_lstOneWire.GetItemText(l_iIndex,ONE_WIRE_DISCONNECT_MSG_COL_INDEX);
		CString l_strResumeConnectMgs=m_lstOneWire.GetItemText(l_iIndex,ONE_WIRE_RESUME_CONNECT_MSG_COL_INDEX);

		CString l_strSensorNum;
		l_strSensorNum.Format("%d",a_iSensorNum);
		
		m_lstOneWire.DeleteItem(l_iIndex);
		l_iIndex=m_lstOneWire.InsertItem(l_iIndex,l_strSensorNum,l_iImage);
		m_lstOneWire.SetItemData(l_iIndex,a_iSensorNum);
		m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_SENSOR_NAME_COL_INDEX,l_strSensorName);
		m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_DISCONNECT_MSG_COL_INDEX,l_strDisconnectedMgs);
		m_lstOneWire.SetItemText(l_iIndex,ONE_WIRE_RESUME_CONNECT_MSG_COL_INDEX,l_strResumeConnectMgs);

		m_lstOneWire.EnsureVisible(l_iIndex,false);
	}	
}


int CAdcView::GetOneWireItemByNumber(int a_iSensorNum)
{
	INFO_LOG(g_DbLogger,CString("CAdcView::GetOneWireItemByNumber"));
	
	CString l_strExistNum;
	for(int l_iForIndex=0;l_iForIndex<m_lstOneWire.GetItemCount();++l_iForIndex)
	{
		l_strExistNum=m_lstOneWire.GetItemText(l_iForIndex,ONE_WIRE_SENSOR_NUMBER_COL_INDEX);
		if(atoi(l_strExistNum)==a_iSensorNum)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

void CAdcView::OnClickLstOneWire(NMHDR* pNMHDR, LRESULT* pResult) 
{
	INFO_LOG(g_DbLogger,CString("CAdcView::OnClickLstOneWire"));
	POSITION l_pos = m_lstOneWire.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc=GetDocument();
		int l_iItem = m_lstOneWire.GetNextSelectedItem(l_pos);	  
		
		CString l_strIndex= m_lstOneWire.GetItemText(l_iItem,ONE_WIRE_SENSOR_NUMBER_COL_INDEX);;
		int l_iIndex=atoi(LPCTSTR(l_strIndex));
		COneWireListElement *l_pOneWireListElement=l_pDoc->m_OneWireList.GetItemBySensorNum(l_iIndex);
		SetComboBoxFromItemData(m_cmbOneWire,l_pOneWireListElement->m_iSensorNumber);
		UpdateData();
		m_strOneWireSensorName= l_pOneWireListElement->m_strSensorName;
		m_strDisconnectedMgs = l_pOneWireListElement->m_strDisconnectedMgs;
		m_strResumeConnectMgs = l_pOneWireListElement->m_strResumeConnectMgs;

		UpdateData(FALSE);
	}
	if(pResult)
	{
		*pResult = 0;
	}
}

void CAdcView::OnCmdRemove1WireSensor() 
{
	INFO_LOG(g_DbLogger,CString("CAdcView::OnCmdRemove1WireSensor"));
	CGsmAlertDoc *l_pDoc=GetDocument();
	
	POSITION l_pos = m_lstOneWire.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		int l_iItem = m_lstOneWire.GetNextSelectedItem(l_pos);
		CString l_strSensorNum = m_lstOneWire.GetItemText(l_iItem,ONE_WIRE_SENSOR_NUMBER_COL_INDEX);
		int l_iSensorNum = atoi(LPCTSTR(l_strSensorNum));
		l_pDoc->m_OneWireList.DeleteItem(l_iSensorNum);
		m_lstOneWire.DeleteItem(l_iItem);
	}
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED,0,0);
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}

void CAdcView::DeleteOneWireSensorList()
{
	CGsmAlertDoc *l_pDoc=GetDocument();
	l_pDoc->m_OneWireList.DeleteAllList();
	m_lstOneWire.DeleteAllItems();
	l_pDoc->SetModifiedFlag();
}

void CAdcView::OnCmdGet1WireSensor() 
{
	INFO_LOG(g_DbLogger,CString("CAdcView::OnCmdGet1WireSensor"));
	
	UpdateData(FALSE);
	m_enmAdcSettingsLastSentCommand=m_enmAdcSettingsLastSentCommandOneWireListGet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);
	
	EnableControls(FALSE);
}

CString CAdcView::GetOneWireSensorsString()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	COneWireListElement *l_pListElement;
	CString l_strTmp, l_strSensorId, l_strSetRes;

	l_strSetRes = "";

	for (int l_i = 0;l_i<l_pDoc->m_OneWireList.GetCount();++l_i)
	{
		l_pListElement = l_pDoc->m_OneWireList.GetItemByPos(l_i + 1);
		l_strSensorId.Format("%s-%d", ONE_WIRE_SENSOR_ID_STR, l_pListElement->m_iSensorNumber);

		l_strTmp.Format("%d,%s,%s,%s,%s", l_pListElement->m_iSensorNumber, l_pListElement->m_strSensorName, l_pListElement->m_strDisconnectedMgs, l_pListElement->m_strResumeConnectMgs, l_strSensorId);
		if (l_strSetRes != "")	//For the second, third,.. items add semi column
		{
			l_strSetRes += ";";
		}
		l_strSetRes += l_strTmp;
	}
	l_strSetRes = CString(ONE_WIRE_SENSOR_SET_MSG_COMMAND) + l_strSetRes;

	return l_strSetRes;
}

void CAdcView::OnCmdSet1WireSensor() 
{
	INFO_LOG(g_DbLogger,CString("CAdcView::OnCmdSet1WireSensor"));
	
	m_strSetString = GetOneWireSensorsString();
	
	m_enmAdcSettingsLastSentCommand=m_enmAdcSettingsLastSentCommandOneWireListSet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);

	EnableControls(FALSE);
}

void CAdcView::OnCmdProgram1Wire() 
{
	UpdateData();
	if (m_iOneWireToProgram==-1)
	{
		MessageBox(GetResString(IDS_ONE_WIRE_SENSOR_NOT_SELECTED),NULL,MB_ICONERROR | MB_OK);
		return;
	}
	m_strSetString.Format("%d",m_cmbOneWireToProgram.GetItemData(m_iOneWireToProgram));
	m_enmAdcSettingsLastSentCommand=m_enmAdcSettingsLastSentCommandOneWireMark;

	EnableControls(FALSE);

	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);
}

void CAdcView::OnCmdQuery1WireSensors() 
{
	m_enmAdcSettingsLastSentCommand=m_enmAdcSettingsLastSentCommandOneWireQuery;
	
	EnableControls(FALSE);
	
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);
}


void CAdcView::OnBnClickedCmdAddAdcSensor()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnBnClickedCmdAddAdcSensor"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	UpdateData();
	if (m_iAdc== -1)
	{
		MessageBox(GetResString(IDS_ADC_SENSOR_NOT_SELECTED), NULL, MB_ICONERROR);
		return;
	}
	if(m_strLut=="")
	{
		MessageBox(GetResString(IDS_ADC_NO_LUT), NULL, MB_ICONERROR);
		return;
	}
	int l_iSensorNum = m_cmbAdc.GetItemData(m_iAdc);

	l_pDoc->m_AdcList.AddItemSorted(l_iSensorNum, m_strAdcSensorName,m_strLut,false, false);
	l_pDoc->SetModifiedFlag();
	AddItemToAdcList(l_iSensorNum);
	m_lstAdc.SortItems(SimpleSortCompareFunc, 0);
	int l_iIndex = GetAdcItemByNumber(l_iSensorNum);
	m_lstAdc.EnsureVisible(l_iIndex, false);
	m_strAdcSensorName = "";
	m_strLut = "";
	m_iAdc = -1;
	m_lstAdcLut.DeleteAllItems();
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
	
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);
}

int CAdcView::GetAdcItemByNumber(int a_iSensorNum)
{
	INFO_LOG(g_DbLogger, CString("CAdcView::GetAdcItemByNumber"));

	CString l_strExistNum;
	for (int l_iForIndex = 0;l_iForIndex<m_lstAdc.GetItemCount();++l_iForIndex)
	{
		l_strExistNum = m_lstAdc.GetItemText(l_iForIndex, ADC_SENSOR_NUMBER_COL_INDEX);
		if (atoi(l_strExistNum) == a_iSensorNum)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CAdcView::GetAdcLutItemByMeasured(CString a_strLutMeasured)
{
	INFO_LOG(g_DbLogger, CString("CAdcView::GetAdcLutItemByMeasured"));

	CString l_strExistMeasured;
	for (int l_iForIndex = 0;l_iForIndex<m_lstAdc.GetItemCount();++l_iForIndex)
	{
		l_strExistMeasured = m_lstAdcLut.GetItemText(l_iForIndex, ADC_LUT_MEASURED_COL_INDEX);
		if (l_strExistMeasured == a_strLutMeasured)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CAdcView::AddItemToAdcList(int a_iSensorNum)
{
	CGsmAlertDoc *l_pDoc = GetDocument();

	INFO_LOG(g_DbLogger, CString("CAdcView::AddItemToAdcList"));

	CAdcListElement *l_AdcListElement = l_pDoc->m_AdcList.GetItemBySensorNum(a_iSensorNum);
	int l_iImage;
	if (l_AdcListElement->m_bInUnit)
	{
		l_iImage = 1;
	}
	else
	{
		l_iImage = 0;
	}

	CString l_strSensorNum;
	l_strSensorNum.Format("%d", a_iSensorNum);


	int l_iIndex;
	if ((l_iIndex = GetAdcItemByNumber(a_iSensorNum)) == -1)
	{
		l_iIndex = m_lstAdc.InsertItem(ADC_SENSOR_NUMBER_COL_INDEX, l_strSensorNum, l_iImage);
		m_lstAdc.EnsureVisible(l_iIndex, false);
		m_lstAdc.SetItemData(l_iIndex, a_iSensorNum);
	}
	else
	{
		m_lstAdc.DeleteItem(l_iIndex);
		l_iIndex = m_lstAdc.InsertItem(ADC_SENSOR_NUMBER_COL_INDEX, l_strSensorNum, l_iImage);
		m_lstAdc.EnsureVisible(l_iIndex, false);
		m_lstAdc.SetItemData(l_iIndex, a_iSensorNum);
	}
	m_lstAdc.SetItemText(l_iIndex, ADC_SENSOR_NAME_COL_INDEX, l_AdcListElement->m_strSensorName);
	m_lstAdc.SetItemText(l_iIndex, ADC_LUT_COL_INDEX, l_AdcListElement->m_strLut);
	
	return l_iIndex;
}

int CAdcView::AddItemToAdcLutList(CString a_strLutMeasured, CString a_strLutEngineer)
{

	INFO_LOG(g_DbLogger, CString("CAdcView::AddItemToAdcLutList"));

	int l_iIndex;
	if ((l_iIndex = GetAdcLutItemByMeasured(a_strLutMeasured)) != -1)
	{
		m_lstAdc.DeleteItem(l_iIndex);
	}
	l_iIndex = m_lstAdcLut.InsertItem(ADC_LUT_MEASURED_COL_INDEX, a_strLutMeasured);
	m_lstAdcLut.SetItemText(l_iIndex, ADC_LUT_ENGINEER_COL_INDEX, a_strLutEngineer);
	m_lstAdcLut.SetItemData(l_iIndex, (int)(atof(a_strLutMeasured) * 1000));
	m_lstAdcLut.EnsureVisible(l_iIndex, true);

	m_lstAdcLut.SortItems(SimpleSortCompareFunc, 0);

	return l_iIndex;
}

void CAdcView::LutListToTextBox()
{
	UpdateData(TRUE);
	m_strLut = "";
	for (int l_iItem = 0;l_iItem < m_lstAdcLut.GetItemCount();++l_iItem)
	{
		if (m_strLut != "")
		{
			m_strLut += ";";
		}
		m_strLut += m_lstAdcLut.GetItemText(l_iItem, ADC_LUT_MEASURED_COL_INDEX) + CString(";") + m_lstAdcLut.GetItemText(l_iItem, ADC_LUT_ENGINEER_COL_INDEX);
	}

	UpdateData(FALSE);
}

void CAdcView::LutTextBoxToList()
{
	CString l_strAdcLutMeasured, l_strAdcLutEngineer;
	int l_iStart, l_iEndMeasured, l_iEndLut;

	UpdateData(TRUE);
	m_lstAdcLut.DeleteAllItems();

	for (l_iStart = 0; m_strLut.Find(";", l_iStart) != -1;)
	{
		l_iEndMeasured = m_strLut.Find(";", l_iStart);
		l_iEndLut = m_strLut.Find(";", l_iEndMeasured + 1);

		l_strAdcLutMeasured = m_strLut.Mid(l_iStart, l_iEndMeasured - l_iStart);
		if (l_iEndLut == -1)
		{
			l_strAdcLutEngineer = m_strLut.Mid(l_iEndMeasured + 1);
			l_iStart = l_iEndMeasured + 1;
		}
		else
		{
			l_strAdcLutEngineer = m_strLut.Mid(l_iEndMeasured + 1, l_iEndLut - l_iEndMeasured - 1);
			l_iStart = l_iEndLut + 1;
		}
		AddItemToAdcLutList(l_strAdcLutMeasured, l_strAdcLutEngineer);
	}
}



void CAdcView::OnBnClickedCmdAddAdcLut()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnBnClickedCmdAddAdcLut"));
	
	UpdateData();
	if ( (IsNumeric(m_strAdcLutEngineer,true)==0) || (IsNumeric(m_strAdcLutMeasured) == 0)  )
	{
		MessageBox(GetResString(IDS_ADC_NO_LUT), NULL, MB_ICONERROR);
		return;
	}
	AddItemToAdcLutList(m_strAdcLutMeasured, m_strAdcLutEngineer);
	
	m_strAdcLutEngineer = "";
	m_strAdcLutMeasured = "";
	UpdateData(FALSE);
	LutListToTextBox();
}


void CAdcView::OnClickLstAdc(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: Add your control notification handler code here

	INFO_LOG(g_DbLogger, CString("CAdcView::OnClickLstAdc"));
	POSITION l_pos = m_lstAdc.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		CGsmAlertDoc *l_pDoc = GetDocument();
		int l_iItem = m_lstAdc.GetNextSelectedItem(l_pos);

		CString l_strIndex = m_lstAdc.GetItemText(l_iItem, ADC_SENSOR_NUMBER_COL_INDEX);;
		int l_iIndex = atoi(LPCTSTR(l_strIndex));
		CAdcListElement *l_pAdcListElement = l_pDoc->m_AdcList.GetItemBySensorNum(l_iIndex);
		SetComboBoxFromItemData(m_cmbAdc, l_pAdcListElement->m_iSensorNumber);
		UpdateData();
		m_strAdcSensorName = l_pAdcListElement->m_strSensorName;
		m_strLut= l_pAdcListElement->m_strLut;
		UpdateData(FALSE);

		LutTextBoxToList();

		
	}

	*pResult = 0;
}


void CAdcView::OnBnClickedCmdRemoveAdcSensor()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnBnClickedCmdRemoveAdcSensor"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	POSITION l_pos = m_lstAdc.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		int l_iItem = m_lstAdc.GetNextSelectedItem(l_pos);
		CString l_strSensorNum = m_lstAdc.GetItemText(l_iItem, ADC_SENSOR_NUMBER_COL_INDEX);
		int l_iSensorNum = atoi(LPCTSTR(l_strSensorNum));
		l_pDoc->m_AdcList.DeleteItem(l_iSensorNum);
		m_lstAdc.DeleteItem(l_iItem);
	}
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);
	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}


void CAdcView::OnBnClickedCmdGetAdcSensor()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnBnClickedCmdGetAdcSensor"));

	UpdateData(FALSE);
	m_enmAdcSettingsLastSentCommand = m_enmAdcSettingsLastSentCommandAdcListGet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);

	EnableControls(FALSE);
}

CString CAdcView::GetAdcSensorsString()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	CAdcListElement *l_pListElement;
	CString l_strTmp, l_strSensorId, l_strSetString;

	l_strSetString = "";

	for (int l_i = 0;l_i < l_pDoc->m_AdcList.GetCount();++l_i)
	{
		l_pListElement = l_pDoc->m_AdcList.GetItemByPos(l_i + 1);

		l_strSensorId.Format("%s-%d", ADC_SENSOR_ID_STR, l_pListElement->m_iSensorNumber);

		l_strTmp.Format("%d,%s,%s,%s", l_pListElement->m_iSensorNumber, l_pListElement->m_strSensorName, l_pListElement->m_strLut, l_strSensorId);
		if (l_strSetString != "")	//For the second, third,.. items add semi column
		{
			l_strSetString += ",";
		}
		l_strSetString += l_strTmp;
	}
	l_strSetString = CString(ADC_SENSOR_SET_MSG_COMMAND) + l_strSetString;

	return l_strSetString;
}

void CAdcView::OnBnClickedCmdSetAdcSensor()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnBnClickedCmdSetAdcSensor"));

	m_strSetString = GetAdcSensorsString();

	m_enmAdcSettingsLastSentCommand = m_enmAdcSettingsLastSentCommandAdcListSet;
	SendSetCommandToUnit(m_enmAdcSettingsLastSentCommand);

	EnableControls(FALSE);
}


void CAdcView::OnBnClickedCmdRemoveAdcLut()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::OnCmdRemove1WireSensor"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	POSITION l_pos = m_lstAdcLut.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	else
	{
		int l_iItem = m_lstOneWire.GetNextSelectedItem(l_pos);
		m_lstAdcLut.DeleteItem(l_iItem);
	}
	LutListToTextBox();
}


void CAdcView::OnBnClickedCmdCopy1WireSensorClipboard()
{
	CopyStringToClipbard(GetOneWireSensorsString(), GetSafeHwnd());
}


void CAdcView::OnBnClickedCmdCopyAdcSensorClipboard()
{
	CopyStringToClipbard(GetAdcSensorsString(), GetSafeHwnd());
}
