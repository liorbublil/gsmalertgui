#if !defined(AFX_INPUTDIALOG_H__3511F29F_D962_4372_8D53_F55315FD65D5__INCLUDED_)
#define AFX_INPUTDIALOG_H__3511F29F_D962_4372_8D53_F55315FD65D5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputDialog dialog

#define DIALOG_TITLE _T("Input Dialog")
#define DIALOG_MESSAGE _T("")

class CInputDialog : public CDialog
{
// Construction
public:

	typedef enum InputDialogFlagsEnum
	{
		NO_FLAGS		=	0,
		PASSWORD_LETTERS=	ES_PASSWORD,
		RIGHT_TO_LEFT	=	WS_EX_RTLREADING,
		RIGHT_ALIGNMENT	=	WS_EX_RIGHT
	}InputDialogFlags;

	CInputDialog(DWORD ai_dwFlags =NO_FLAGS, CString ai_strTitle=DIALOG_TITLE, CString ai_strMessage = DIALOG_MESSAGE, 
			     CString ai_strDefault=_T(""), CWnd* pParent = NULL);


	int InputBox(char* str_aoInputText);
	void SetFlags(DWORD ai_dwflags);
	void SetDefaultInputString(CString a_strDefaultInput);
	void SetTitle(LPCTSTR ai_strTiltle);
	void SetMessageText(LPCTSTR ai_strMessage);
	CString GetInputString();

// Dialog Data
	//{{AFX_DATA(CInputDialog)
	enum {	IDD = IDD_INPUTBOX_DIALOG};
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strTitle;
	CString m_strMessage;
	CString	m_strInput;
	DWORD m_dwFlags;

	// Generated message map functions
	//{{AFX_MSG(CInputDialog)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTDIALOG_H__3511F29F_D962_4372_8D53_F55315FD65D5__INCLUDED_)

