// LogView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MainFrm.h"
#include "LogView.h"
#include "Language.h"
#include "global.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TIME_COL_INDEX 0
#define LOG_DETAILES_COL_INDEX 1

#define GET_LOG_DATA_STEP 30
#define LOG_ROW_LEN 117
#define LOG_TIME_LEN 10

/////////////////////////////////////////////////////////////////////////////
// CLogView

IMPLEMENT_DYNCREATE(CLogView, CFormView)

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

CLogView::CLogView()
	: CFormView(CLogView::IDD)
{
	//{{AFX_DATA_INIT(CLogView)
	m_strLastLogEntries = _T("");
	//}}AFX_DATA_INIT
	m_bFirstRun=true;
	m_bStopDownload=false;
	m_bOnLastLogEntriesReq=false;
	m_iAmountOfLogDataInUnit=0;
}

CLogView::~CLogView()
{
	INFO_LOG(g_DbLogger,"CLogView::~CLogView()");
}

void CLogView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogView)
	DDX_Control(pDX, IDC_CMD_DEL_LOG, m_cmdDelLog);
	DDX_Control(pDX, IDC_CMD_EXPORT_LOG, m_cmdExportLog);
	DDX_Control(pDX, IDC_TXT_LAST_LOG_ENTRIES, m_txtLastLogEntries);
	DDX_Control(pDX, IDC_CMD_LAST_LOG_ENTRIES, m_cmdLastLogEntries);
	DDX_Control(pDX, IDC_LBL_LAST_LOG_ENTRIES, m_lblLastLogEntries);
	DDX_Control(pDX, IDC_CMD_STOP, m_cmdStop);
	DDX_Control(pDX, IDC_PRGS_LOG_DOWNLOAD, m_prgsLogDownload);
	DDX_Control(pDX, IDC_CMD_GET_LOG, m_cmdGetLog);
	DDX_Control(pDX, IDC_LOG, m_lblLog);
	DDX_Control(pDX, IDC_LST_LOG, m_lstLog);
	DDX_Text(pDX, IDC_TXT_LAST_LOG_ENTRIES, m_strLastLogEntries);
	DDV_MaxChars(pDX, m_strLastLogEntries, 5);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLogView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	//{{AFX_MSG_MAP(CLogView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_CMD_GET_LOG, OnCmdGetLog)
	ON_BN_CLICKED(IDC_CMD_STOP, OnCmdStop)
	ON_BN_CLICKED(IDC_CMD_LAST_LOG_ENTRIES, OnCmdLastLogEntries)
	ON_BN_CLICKED(IDC_CMD_EXPORT_LOG, OnCmdExportLog)
	ON_BN_CLICKED(IDC_CMD_DEL_LOG, OnCmdDelLog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogView diagnostics

#ifdef _DEBUG
void CLogView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLogView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CGsmAlertDoc* CLogView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLogView message handlers

LRESULT CLogView::OnCommandArrived (WPARAM wParam,LPARAM lParam)
{
	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CLogView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
		m_prgsLogDownload.ShowWindow(SW_HIDE);
		m_cmdStop.EnableWindow(FALSE);
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger,CString("CLogView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger,CString("CLogView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if(m_enmLogViewLastSentCommand!=enmLogViewLastSentCommandNone)
		{
			m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
			CString l_strError=GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError,NULL,MB_ICONERROR + MB_OK);
			m_prgsLogDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
		}
		return 0;
	}
	else if(wParam!=COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger,CString("CLogView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	CString *l_pArrivedRespond;
	l_pArrivedRespond=(CString*)lParam;
	int l_iRespondStartAt;
	int l_iLastRespondStartAt=0;

	INFO_LOG(g_DbLogger,CString("CLogView::OnCommandArrived l_pArrivedRespond:")+ *l_pArrivedRespond);
	
	do
	{
		if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_LOG_LEN_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_LOG_LEN_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_LOG_LEN_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
			m_iAmountOfLogDataInUnit=atoi(LPCTSTR(l_pArrivedRespond->Mid(l_iDataStartAt)));

			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
		else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,LPCTSTR(CString(GET_LOG_DATA_MSG_RESPOND)+CString(ACK_RESPOND)) ,l_iLastRespondStartAt))!=-1)
		{
			int l_iDataStartAt=FindInString(*l_pArrivedRespond,GET_LOG_DATA_MSG_RESPOND);
			l_iDataStartAt+=(CString(GET_LOG_DATA_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();

			CString l_strLogData;
			CString l_strLogRow,l_strTime,l_strLogContent;
			int l_iCounter;
			time_t l_tmtTmp;
			CTime l_tmZeroDate(2000,1,1,0,0,0,0),l_tmLogTime;
			
			l_strLogData=l_pArrivedRespond->Mid(l_iDataStartAt);
			
			int l_iGetRowLine;
			for(l_iGetRowLine=0,l_iCounter=1;l_iGetRowLine<l_strLogData.GetLength();l_iGetRowLine+=LOG_ROW_LEN,++l_iCounter)
			{
				l_strLogRow=l_strLogData.Mid(l_iGetRowLine,LOG_ROW_LEN);
				l_iDataStartAt=l_strLogRow.Find(LOG_ROW_MSG_RESPOND);

				l_strTime=l_strLogRow.Mid(l_iDataStartAt + strlen(LOG_ROW_MSG_RESPOND));
				l_iDataStartAt=l_strTime.Find(",");
				l_strLogContent = l_strTime.Mid(l_iDataStartAt+1);
				l_strTime= l_strTime.Left(l_iDataStartAt);

				l_strTime.TrimLeft();
				l_strTime.TrimRight();
				l_strLogContent.TrimLeft();
				l_strLogContent.TrimRight();
				l_iDataStartAt=l_strLogContent.Find(">>");
				l_strLogContent=l_strLogContent.Left(l_iDataStartAt);

				l_tmtTmp = (time_t)atol((LPCTSTR)l_strTime);
				l_tmLogTime=l_tmZeroDate + l_tmtTmp;
				AddToLogLst(l_tmLogTime,l_strLogContent);
				
				m_prgsLogDownload.SetPos(m_iLastFromIndex+l_iCounter);
			}
			//m_lstLog.SortItems(LogListSortCompareFunc,0);		//Sort the list according to the date and time
			
			l_iLastRespondStartAt=l_iRespondStartAt+1;
			continue;
		}
	} while(l_iRespondStartAt!=-1);

	if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ACK_RESPOND))!=-1)
	{
		HandleLastSentCommand();
	}
	else if((l_iRespondStartAt=FindInString(*l_pArrivedRespond,ERROR_RESPOND))!=-1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
		m_prgsLogDownload.ShowWindow(SW_HIDE);
		m_cmdStop.EnableWindow(FALSE);
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger,CString("exit CLogView::OnCommandArrived"));

	return 0;
}

void CLogView::OnSize(UINT nType, int cx, int cy) 
{
	INFO_LOG(g_DbLogger,CString("CLogView::OnSize"));
	RECT l_rctLogLbl;
	RECT l_rctLogLst;
	RECT l_rctLogDownloadPrgs;
	RECT l_rctStopCmd;
	RECT l_rctGetLogCmd;
	RECT l_rctLastLogEntriesCmd;
	RECT l_rctExportLogCmd;
	RECT l_rctDelLogCmd;
	RECT l_rctLastLogEntriesLbl;
	RECT l_rctLastLogEntriesTxt;


	int l_iTop;
	int l_iLeft;
	if(IsWindow(m_lstLog.GetSafeHwnd()))
	{
		m_cmdGetLog.GetWindowRect(&l_rctGetLogCmd);
		m_lblLog.GetWindowRect(&l_rctLogLbl);
		m_prgsLogDownload.GetWindowRect(&l_rctLogDownloadPrgs);
		m_cmdStop.GetWindowRect(&l_rctStopCmd);
		m_lblLastLogEntries.GetWindowRect(&l_rctLastLogEntriesLbl);
		m_txtLastLogEntries.GetWindowRect(&l_rctLastLogEntriesTxt);
		m_cmdLastLogEntries.GetWindowRect(&l_rctLastLogEntriesCmd);
		m_cmdExportLog.GetWindowRect(&l_rctExportLogCmd);
		m_cmdDelLog.GetWindowRect(&l_rctDelLogCmd);

		l_iTop=0;
		l_iLeft=5;

		m_lblLog.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);

		l_iTop+=l_rctLogLbl.bottom-l_rctLogLbl.top+5;

		int l_iHeight=cy-l_rctLogLbl.bottom+l_rctLogLbl.top-l_rctGetLogCmd.bottom + l_rctGetLogCmd.top -l_rctLogDownloadPrgs.bottom + l_rctLogDownloadPrgs.top - 5;
		
		m_lstLog.SetWindowPos(NULL,l_iLeft,l_iTop,cx-2*l_iLeft,l_iHeight,0);
		m_lstLog.GetWindowRect(&l_rctLogLst);

		l_iTop=l_iHeight + l_rctLogLbl.bottom-l_rctLogLbl.top+5;

		m_prgsLogDownload.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);

		l_iTop+=l_rctLogDownloadPrgs.bottom-l_rctLogDownloadPrgs.top;
		m_cmdGetLog.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctGetLogCmd.right-l_rctGetLogCmd.left + 5;

		m_cmdStop.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctStopCmd.right-l_rctStopCmd.left + 5;

		m_lblLastLogEntries.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctLastLogEntriesLbl.right-l_rctLastLogEntriesLbl.left + 5;

		m_txtLastLogEntries.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctLastLogEntriesTxt.right-l_rctLastLogEntriesTxt.left + 5;

		m_cmdLastLogEntries.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctLastLogEntriesCmd.right-l_rctLastLogEntriesCmd.left + 5;

		m_cmdExportLog.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctExportLogCmd.right-l_rctExportLogCmd.left + 5;

		m_cmdDelLog.SetWindowPos(NULL,l_iLeft,l_iTop,0,0,SWP_NOSIZE);
		l_iLeft+=l_rctDelLogCmd.right-l_rctDelLogCmd.left + 5;
	}
	CFormView::OnSize(nType, cx, cy);	
}

void CLogView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();

	INFO_LOG(g_DbLogger,CString("CLogView::OnInitialUpdate"));

	if(m_bFirstRun)
	{
		INFO_LOG(g_DbLogger,CString("CLogView::OnInitialUpdate first run"));

		m_bFirstRun=false;
		
		CWnd *l_pWnd=GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame,l_pWnd);
		m_pMainFram=(CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame,m_pMainFram);

		m_lstLog.InsertColumn(TIME_COL_INDEX,GetResString(IDS_TIME_COL_INDEX),LVCFMT_LEFT,200);
		m_lstLog.InsertColumn(LOG_DETAILES_COL_INDEX,GetResString(IDS_LOG_DETAILES_COL_INDEX),LVCFMT_LEFT,500);
		
		//Set the list to be full row selection and with grid lines
		DWORD  l_dwStyle=m_lstLog.GetExtendedStyle();
		l_dwStyle|=LVS_EX_FULLROWSELECT;
		l_dwStyle|=LVS_EX_GRIDLINES;
		m_lstLog.SetExtendedStyle(l_dwStyle);
		Localize();
	}
	m_lstLog.DeleteAllItems();
}

void CLogView::Localize() 
{
	INFO_LOG(g_DbLogger,"CLogView::Localize()");

	SetDlgItemText( IDC_LOG,GetResString(IDS_IDC_LOG));
	SetDlgItemText( IDC_CMD_GET_LOG,GetResString(IDS_IDC_CMD_GET_LOG));
	SetDlgItemText( IDC_CMD_LAST_LOG_ENTRIES,GetResString(IDS_IDC_CMD_LAST_LOG_ENTRIES));
	SetDlgItemText( IDC_LBL_LAST_LOG_ENTRIES,GetResString(IDS_IDC_LBL_LAST_LOG_ENTRIES));
	SetDlgItemText( IDC_CMD_EXPORT_LOG,GetResString(IDS_IDC_CMD_EXPORT_LOG));
}

void CLogView::OnCmdGetLog() 
{
	INFO_LOG(g_DbLogger,CString("CLogView::OnCmdGetLog"));

	m_prgsLogDownload.SetRange(0,0);
	m_prgsLogDownload.SetPos(0);
	m_lstLog.DeleteAllItems();

	m_bStopDownload=false;
	m_bOnLastLogEntriesReq=false;

	m_enmLogViewLastSentCommand=enmLogViewLastSentCommandGetLogLen;
	SendSetCommandToUnit(m_enmLogViewLastSentCommand);
	
	m_prgsLogDownload.ShowWindow(SW_SHOW);
	m_cmdStop.EnableWindow();
	EnableControls(FALSE);
}

void CLogView::SendSetCommandToUnit(enmLogViewLastSentCommand &a_enmLogViewLastSentCommand)
{
	INFO_LOG(g_DbLogger,CString("CLogView::SendSetCommandToUnit"));
	CString *l_pNewCommand;

	l_pNewCommand=new CString();
	
	switch(a_enmLogViewLastSentCommand)
	{
	case enmLogViewLastSentCommandGetLogLen:
		l_pNewCommand->Format("%s",GET_LOG_LEN_MSG_COMMAND);
		break;
	case enmLogViewLastSentCommandGetLogData:
		l_pNewCommand->Format("%s=%d,%d",GET_LOG_DATA_MSG_COMMAND,m_iLastFromIndex,m_iLastToIndex);
		break;
	case enmLogViewLastSentCommandDelLog:
		l_pNewCommand->Format("%s",DEL_LOG_FILES_MSG_COMMAND);
		break;
	}

	if(*l_pNewCommand!="")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND,(WPARAM)GetSafeHwnd(),(LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
	}
}

void CLogView::EnableControls(BOOL a_bEnabled)
{
	INFO_LOG(g_DbLogger,CString("CLogView::EnableControls"));
	m_cmdGetLog.EnableWindow(a_bEnabled);
	m_txtLastLogEntries.EnableWindow(a_bEnabled);
	m_cmdLastLogEntries.EnableWindow(a_bEnabled);
	m_cmdExportLog.EnableWindow(a_bEnabled);
	m_cmdDelLog.EnableWindow(a_bEnabled);
}

void CLogView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger,CString("CLogView::HandleLastSentCommand"));
	
	switch(m_enmLogViewLastSentCommand)
	{
	case enmLogViewLastSentCommandGetLogLen:
		if((m_bStopDownload) || (m_iAmountOfLogDataInUnit==0))
		{
			m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
			m_prgsLogDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			return;
		}

		if(m_bOnLastLogEntriesReq)
		{
			UpdateData();
			int l_iLastLogEntries=atoi(LPCTSTR(m_strLastLogEntries));
			if(m_iAmountOfLogDataInUnit - l_iLastLogEntries +1<1)
			{
				m_iLastFromIndex=1;
			}
			else
			{
				m_iLastFromIndex=m_iAmountOfLogDataInUnit - l_iLastLogEntries +1;
			}
		}
		else
		{
			m_iLastFromIndex=1;
		}
		m_prgsLogDownload.SetRange(m_iLastFromIndex-1,m_iAmountOfLogDataInUnit+1);
		m_prgsLogDownload.SetPos(m_iLastFromIndex);

		//Check the To index
		if(m_iLastFromIndex+GET_LOG_DATA_STEP-1 < m_iAmountOfLogDataInUnit)
		{
			m_iLastToIndex= m_iLastFromIndex + GET_LOG_DATA_STEP-1;
		}
		else
		{
			m_iLastToIndex=m_iAmountOfLogDataInUnit;
		}

		m_enmLogViewLastSentCommand=enmLogViewLastSentCommandGetLogData;
		SendSetCommandToUnit(m_enmLogViewLastSentCommand);
		break;
	case enmLogViewLastSentCommandGetLogData:
		if(m_bStopDownload)
		{
			m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
			m_prgsLogDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			return;
		}
		m_prgsLogDownload.SetPos(m_iLastToIndex+1);
		m_iLastFromIndex = m_iLastToIndex + 1;
		if(m_iLastFromIndex+GET_LOG_DATA_STEP-1 < m_iAmountOfLogDataInUnit)
		{
			m_iLastToIndex=m_iLastFromIndex + GET_LOG_DATA_STEP - 1;
		}
		else
		{
			m_iLastToIndex=m_iAmountOfLogDataInUnit;
		}
		if(m_iLastFromIndex<=m_iAmountOfLogDataInUnit)
		{
			SendSetCommandToUnit(m_enmLogViewLastSentCommand);
		}
		else
		{
			m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
			m_prgsLogDownload.ShowWindow(SW_HIDE);
			m_cmdStop.EnableWindow(FALSE);
			EnableControls();
			return;
		}
		break;
	case enmLogViewLastSentCommandDelLog:
		MessageBox(GetResString(IDS_LOG_HAVE_BEEN_DELETED),NULL,MB_OK | MB_ICONINFORMATION);
		m_enmLogViewLastSentCommand=enmLogViewLastSentCommandNone;
		m_prgsLogDownload.ShowWindow(SW_HIDE);
		m_cmdStop.EnableWindow(FALSE);
		EnableControls();
		return;
	}
}

void CLogView::OnCmdStop() 
{
	INFO_LOG(g_DbLogger,CString("CLogView::OnCmdStop"));
	m_bStopDownload=true;
}

void CLogView::AddToLogLst(CTime a_tmLogTime,CString a_strLogContent)
{
	DWORD l_dwTime=DWORD(a_tmLogTime.GetTime());
	int l_iIndex;

	/*
	//Do not add log entries with the same date and time
	for(l_iIndex = 0 ;l_iIndex<m_lstLog.GetItemCount();++l_iIndex)
	{
		//Check if the item already exist
		if(m_lstLog.GetItemData(l_iIndex)==l_dwTime)
		{
			return;
		}
	}
	*/
	_daylight=0;		//Must ignore day light saving
	l_iIndex=m_lstLog.InsertItem(TIME_COL_INDEX,a_tmLogTime.Format("%H:%M:%S    %d/%m/%Y"));
	m_lstLog.SetItemText(l_iIndex,LOG_DETAILES_COL_INDEX,a_strLogContent);
	m_lstLog.SetItemData(l_iIndex,l_dwTime);
}

void CLogView::OnCmdLastLogEntries() 
{
	INFO_LOG(g_DbLogger,CString("CLogView::OnCmdLastLogEntries"));

	UpdateData();
	int l_iLastLogEntries=atoi(LPCTSTR(m_strLastLogEntries));
	if(l_iLastLogEntries<=0)
	{
		MessageBox(GetResString(IDS_NO_LAST_ENTRIES_INSERTED),NULL,MB_ICONERROR);
		return;
	}

	m_prgsLogDownload.SetRange(0,0);
	m_prgsLogDownload.SetPos(0);
	m_lstLog.DeleteAllItems();

	m_bStopDownload=false;
	m_bOnLastLogEntriesReq=true;

	m_enmLogViewLastSentCommand=enmLogViewLastSentCommandGetLogLen;
	SendSetCommandToUnit(m_enmLogViewLastSentCommand);
	
	m_prgsLogDownload.ShowWindow(SW_SHOW);
	m_cmdStop.EnableWindow();
	EnableControls(FALSE);
}

void CLogView::OnCmdExportLog() 
{
	/*
	INFO_LOG(g_DbLogger,CString("CLogView::OnCmdExportLog"));
	CDatabase database;
	CString l_strDriver = "MICROSOFT EXCEL DRIVER (*.xls)"; // exactly the same name as in the ODBC-Manager
	CString l_strExcelFile;					// Filename and path for the file to be created
	CString l_strSql;

	CGsmAlertDoc *l_pDoc=GetDocument();

	TRY
	{
		// Get the file name to export the excel file
		CFileDialog dlgFile(FALSE);
		CString l_strLoadFilter;
		CString l_strFilter;
		CString l_strTitle=GetResString(IDS_EXCEL_EXPORT);

		VERIFY(l_strLoadFilter.LoadString(IDS_EXCEL_FILE));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.xls");
		l_strFilter+= (TCHAR)'\0';   // last string

		VERIFY(l_strLoadFilter.LoadString(AFX_IDS_ALLFILTER));
		l_strFilter += l_strLoadFilter;
		l_strFilter+= (TCHAR)'\0';   // next string please
		l_strFilter+= _T("*.*");
		l_strFilter+= (TCHAR)'\0';   // last string
		dlgFile.m_ofn.nMaxCustFilter++;

		dlgFile.m_ofn.lpstrFilter = l_strFilter;
		dlgFile.m_ofn.lpstrTitle = l_strTitle;
		dlgFile.m_ofn.lpstrFile = l_strExcelFile.GetBuffer(_MAX_PATH);

		int nResult = dlgFile.DoModal();
		l_strExcelFile.ReleaseBuffer();
		
		if(nResult!=IDOK)
		{
			return;
		}

		CFileFind l_FileFinder;

		if(l_FileFinder.FindFile(l_strExcelFile))
		{
			if(!DeleteFile(l_strExcelFile))
			{
				MessageBox(GetResString(IDS_ERR_DELETE_FILE),NULL,MB_OK | MB_ICONERROR);
				return;
			}
		}

		// Build the creation string for access without DSN

		l_strSql.Format("DRIVER={%s};DSN='';FIRSTROWHASNAMES=1;READONLY=FALSE;CREATE_DB=\"%s\";DBQ=%s", l_strDriver,l_strExcelFile,l_strExcelFile);

		// Create the database (i.e. Excel sheet)
		if( database.OpenEx(l_strSql,CDatabase::noOdbcDialog))
		{
			m_prgsLogDownload.SetRange(0,m_lstLog.GetItemCount()-1);
			m_prgsLogDownload.SetPos(0);
			m_prgsLogDownload.ShowWindow(SW_SHOW);

			//Create the call log table
			CString l_strSql;
			l_strSql.Format("CREATE TABLE tblLog (\"Log Time\" CHAR(25), \"Log Content\" CHAR(%d))",LOG_ROW_LEN - LOG_TIME_LEN - 1);
			database.ExecuteSQL(l_strSql);

			CString l_strTmpTime,l_strTmpContent;
			// Insert data
			for(int l_i=0;l_i<m_lstLog.GetItemCount();++l_i)
			{
				m_prgsLogDownload.SetPos(l_i);

				l_strTmpTime=m_lstLog.GetItemText(l_i,TIME_COL_INDEX);
				l_strTmpContent=m_lstLog.GetItemText(l_i,LOG_DETAILES_COL_INDEX);
				l_strSql.Format("INSERT INTO \"tblLog\" (\"Log Time\" ,\"Log Content\") VALUES ('%s','%s')",l_strTmpTime,l_strTmpContent);
				database.ExecuteSQL(l_strSql);
			}
			
		}
		
		// Close database
		database.Close();
		m_prgsLogDownload.ShowWindow(SW_HIDE);
	}
	CATCH_ALL(e)
	{
		TCHAR   l_szCause[255];
		e->GetErrorMessage(&(l_szCause[1]),254);
		
		l_szCause[0]='\n';
		MessageBox(GetResString(IDS_ERR_WHILE_EXPORT_EXCEL_FILE) + l_szCause,NULL,MB_OK | MB_ICONERROR);

		m_prgsLogDownload.ShowWindow(SW_HIDE);
	}
	END_CATCH_ALL;
	*/
}

void CLogView::OnCmdDelLog() 
{
	INFO_LOG(g_DbLogger,CString("CLogView::OnCmdDelLog"));

	if(MessageBox(GetResString(IDS_DEL_LOG_QUEST),NULL,MB_YESNO | MB_ICONQUESTION)==IDNO)
	{
		return;
	}
	
	m_enmLogViewLastSentCommand=enmLogViewLastSentCommandDelLog;
	SendSetCommandToUnit(m_enmLogViewLastSentCommand);
	
	m_prgsLogDownload.ShowWindow(SW_SHOW);
	m_cmdStop.EnableWindow();
	EnableControls(FALSE);
}
