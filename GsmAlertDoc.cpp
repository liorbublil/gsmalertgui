// GsmAlertDoc.cpp : implementation of the CGsmAlertDoc class
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "Language.h"
#include "GsmAlertDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertDoc

IMPLEMENT_DYNCREATE(CGsmAlertDoc, CDocument)

BEGIN_MESSAGE_MAP(CGsmAlertDoc, CDocument)
	//{{AFX_MSG_MAP(CGsmAlertDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_FILE_SAVE, OnFileSave)
	ON_COMMAND(ID_FILE_SAVE_AS, OnFileSaveAs)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertDoc construction/destruction

CGsmAlertDoc::CGsmAlertDoc()
{
}

CGsmAlertDoc::~CGsmAlertDoc()
{
}

BOOL CGsmAlertDoc::OnNewDocument()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertDoc::OnNewDocument"));

	if (!CDocument::OnNewDocument())
		return FALSE;

	//CMiscSettingsView defaults
	m_strUnitPass=DEFAULT_UNIT_PASS_DEF;
	m_bSavePass=false;
	m_iGateDelay=DEFAULT_GATE_DELAY_DEF;
	m_iBandSelect=GSM900_DCS1800_DEF;
	m_iZoneActiveMode= DEFAULT_ZONE_ACTIVE_MODE;
	m_iInputDebounce=INPUT_DEBOUNCE_DEF;
	
	m_bReminderEnabled=REMINDER_ENABLE_DEF;
	m_iReminderDelay=REMINDER_DELAY_DEF;
	
	m_bTestAlarmEnabled=TEST_ALARM_ENABLE_DEF;
	if (TEST_ALARM_PERIOD_DEF==TEST_ALARM_PERIOD_DAILY)
	{
		m_iTestAlarmWeekDay=7;
	}
	else
	{
		m_iTestAlarmWeekDay=TEST_ALARM_WEEK_DAY_DEF;
	}
	m_iTestAlarmTime=TEST_ALARM_TIME_DEF;
	m_strTestAlarmMsg=TEST_ALARM_MSG_DEF;

	m_strSelfPhoneNum="";
	m_bSelfCuSmsEn=FALSE;

	m_iSmsLoggerDeviceId=DEVICE_ID_DEF;
	m_fSmsLoggerTempDiff=TEMP_DIFF_DEF;
	m_iSmsLoggerInterval=SMS_LOGGER_INTERVAL_DEF;
	m_strSmsLoggerPass=SMS_LOGGER_PASS_DEF;

	m_strCloudDns = CLOUD_DNS_DEF;
	m_bGprsEn = FALSE;
	m_bUseSsl = FALSE;
	m_strApn = APN_DEF;
	m_strGprsUser = GPRS_USER_DEF;
	m_strGprsPass = GPRS_PASS_DEF;
	m_strCloudToken = CLOUD_TOKEN_DEF;
	m_iCloudInterval = CLOUD_INTERVAL_DEF;

	m_bRssiToCloud = FALSE;

	//CAdcView defaults
	m_bSendVinToCloud=TRUE;
	m_bSendVBatToCloud=TRUE;
	m_bSendVinAlarmToCloud = FALSE;

	m_OneWireList.DeleteAllList();
	m_AdcList.DeleteAllList();

	//Level Event view
	m_LevelEventList.DeleteAllList();

	//Modbus view
	m_ModbusList.DeleteAllList();
	m_iModbusBaudRate = MODBUS_BAUD_RATE_DEF;
	m_strModbusCharSet = MODBUS_CHAR_SET_DEF;
	m_iModbusMaxBlockSize = MODBUS_MAX_BLOCK_SIZE_DEF;

	//WirelessSensors view
	m_bRfActive = FALSE;
	m_WirelessList.DeleteAllList();

	//CPhoneListView defaults
	m_PhoneNumList.DeleteAllList();
	m_iMaxPhoneNumAmount=MAX_PHONE_NUM_IN_MEM_DEF;
	m_iMaxZonesAmount=MAX_ZONES_IN_MEM_DEF;
	m_iMaxOutputsAmount=MAX_OUTPUT_IN_MEM_DEF;

	//CZoneListView defaults
	m_ZoneList.DeleteAllList();
	CString l_strZoneMsgNoToNc,l_strZoneMsgNcToNo;
	for(int l_i=1;l_i<=m_iMaxZonesAmount;++l_i)
	{
		l_strZoneMsgNoToNc.Format(DEFAULT_ZONE_MSG_NO_TO_NC,l_i);
		l_strZoneMsgNcToNo.Format(DEFAULT_ZONE_MSG_NC_TO_NO,l_i);
		m_ZoneList.AddItemSorted(l_i,enmZoneTypeRegular, l_strZoneMsgNoToNc,l_strZoneMsgNcToNo,DEFAULT_ZONE_DELAY,DEFAULT_ZONE_DELAY, DEFAULT_ZONE_CNTR_GAP,true,true,false,true);
	}

	m_OutputList.DeleteAllList();
	m_OutputList.AddItemSorted(1,DEFAULT_OUTPUT_1_STATE,DEFAULT_OUTPUT_1_STATE_PARAM ,false,true);
	for(int l_i=2;l_i<=m_iMaxOutputsAmount;++l_i)
	{
		m_OutputList.AddItemSorted(l_i,DEFAULT_OUTPUT_STATE,DEFAULT_OUTPUT_STATE_PARAM,false,true);
	}

	//CSearchModemDlg defaults
	m_strCom="";
	m_bUseModem=false;
	
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertDoc serialization

void CGsmAlertDoc::Serialize(CArchive& ar)
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertDoc::Serialize"));
	
	if (ar.IsStoring())
	{
		//File header
		ar<<CString("GSM_ALERT_FV6");

		//The working COM port
		ar<<m_strCom<<(int)m_bUseModem<<m_strUnitPhoneNumber;

		//CMiscSettingsView variables
		ar<<m_strUnitName;
		if(m_bSavePass)
		{
			ar<<m_strUnitPass;
		}
		else
		{
			ar<<CString(DEFAULT_UNIT_PASS_DEF);
		}
		ar<<(int)m_bSavePass<<m_iGateDelay<<m_strSelfPhoneNum<<(int)m_bSelfCuSmsEn<<m_iBandSelect<<m_iZoneActiveMode<<m_iInputDebounce;
		ar<<(int)m_bReminderEnabled<<m_iReminderDelay;//Write the reminder vars
		ar<<(int)m_bTestAlarmEnabled<<m_iTestAlarmWeekDay<<m_iTestAlarmTime<<m_strTestAlarmMsg;		//Write the Alarm test vars

		//Read the SMS logger parameters
		ar<<m_iSmsLoggerDeviceId<<m_fSmsLoggerTempDiff<<m_iSmsLoggerInterval<<m_strSmsLoggerPass;	//Write the SMS logger parameters

		//Read the internet parameters
		ar << m_bGprsEn << m_strApn << m_strGprsUser << m_strGprsPass << m_strCloudToken << m_strCloudDns << m_iCloudInterval << m_bUseSsl << m_bRssiToCloud;

		//CPhoneListView
		ar<<m_iMaxPhoneNumAmount;
		m_PhoneNumList.Serialize(ar);

		//CZoneListView
		m_ZoneList.Serialize(ar);
		m_OutputList.Serialize(ar);

		//CAdcView
		ar << m_bSendVinToCloud << m_bSendVBatToCloud << m_bSendVinAlarmToCloud;
		m_OneWireList.Serialize(ar);
		m_AdcList.Serialize(ar);

		//CWirelessSenseView
		ar << m_bRfActive;
		m_WirelessList.Serialize(ar);

		//CModbusView
		ar << m_iModbusBaudRate << m_strModbusCharSet << m_iModbusMaxBlockSize;
		m_ModbusList.Serialize(ar);

		//CLevelEventView
		m_LevelEventList.Serialize(ar);
	}
	else
	{
		CString l_strCheck;
		int l_iTmp;
		int l_iVer;
		ar>>l_strCheck;

		//Check the file header
		if(l_strCheck.Left(9)!="GSM_ALERT")
		{
			AfxMessageBox(IDS_CANNOT_OPEN_FILE);
			CFileException  *l_Exception=new CFileException();
			throw l_Exception;
		}
		l_iVer = atoi(l_strCheck.Mid(12));
		if (l_iVer < 6)
		{
			AfxMessageBox(IDS_CANNOT_OPEN_FILE);
			CFileException  *l_Exception = new CFileException();
			throw l_Exception;
		}

		//The working COM port
		ar>>m_strCom>>l_iTmp>>m_strUnitPhoneNumber;
		m_bUseModem=(bool)l_iTmp;

		//CMiscSettingsView variables
		ar>>m_strUnitName>>m_strUnitPass>>l_iTmp>>m_iGateDelay;
		m_bSavePass=(bool)l_iTmp;
		ar>>m_strSelfPhoneNum>>l_iTmp>>m_iBandSelect>>m_iZoneActiveMode;
		m_bSelfCuSmsEn=(bool)l_iTmp;

		ar>>m_iInputDebounce;
		
		//Read the reminder vars
		ar>>l_iTmp>>m_iReminderDelay;
		m_bReminderEnabled=(bool)l_iTmp;
		
		//Read the Alarm test vars
		ar>>l_iTmp>>m_iTestAlarmWeekDay>>m_iTestAlarmTime>>m_strTestAlarmMsg;
		m_bTestAlarmEnabled=(bool)l_iTmp;
		
		//Read the SMS logger parameters
		ar>>m_iSmsLoggerDeviceId>>m_fSmsLoggerTempDiff>>m_iSmsLoggerInterval>>m_strSmsLoggerPass;

		//Read the internet parameters
		ar >> m_bGprsEn >> m_strApn >> m_strGprsUser >> m_strGprsPass >> m_strCloudToken >> m_strCloudDns >> m_iCloudInterval >> m_bUseSsl >> m_bRssiToCloud;

		//CPhoneListView
		ar>>m_iMaxPhoneNumAmount;
		m_PhoneNumList.Serialize(ar);

		//CZoneListView
		m_ZoneList.Serialize(ar);
		m_OutputList.Serialize(ar);

		//CAdcView
		ar >> m_bSendVinToCloud >> m_bSendVBatToCloud >> m_bSendVinAlarmToCloud;
		m_OneWireList.Serialize(ar);
		m_AdcList.Serialize(ar);

		//CWirelessSenseView
		ar >> m_bRfActive;
		m_WirelessList.Serialize(ar);

		//CModbusView
		ar >> m_iModbusBaudRate >> m_strModbusCharSet >> m_iModbusMaxBlockSize;
		m_ModbusList.Serialize(ar);

		//CLevelEventView
		m_LevelEventList.Serialize(ar);
	}
}

void CGsmAlertDoc::OnFileSave()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertDoc::OnFileSave"));

	CDocument::OnFileSave();

	g_pApp->WriteProfileString(REG_SETTINGS_SECTION,REG_LAST_SET_FILE_ENTRY,m_strPathName);
	//Update the views
	UpdateAllViews(NULL);
}

void CGsmAlertDoc::OnFileSaveAs()
{
	INFO_LOG(g_DbLogger,CString("CGsmAlertDoc::OnFileSaveAs"));

	CDocument::OnFileSaveAs();

	g_pApp->WriteProfileString(REG_SETTINGS_SECTION,REG_LAST_SET_FILE_ENTRY,m_strPathName);
	//Update the views
	UpdateAllViews(NULL);
}

CString CGsmAlertDoc::GetTypeNameFromNum(int a_iTypeNum)
{
	switch(a_iTypeNum)
	{
	case CMB_TYPE_MASTER_DEF:
		return GetResString(IDS_TYPE_MASTER);
	case CMB_TYPE_SECOND_DEF:
		return GetResString(IDS_TYPE_SECONDARY);
	case CMB_TYPE_MACHINE_DEF:
		return GetResString(IDS_TYPE_MACHINE);
	}
	return "";
}

CString CGsmAlertDoc::GetOutputStateNameFromNum(int a_iOutputStateNum)
{
	switch(a_iOutputStateNum)
	{
	case CMB_OUTPUT_STATE_HIGTH_LOW:
		return GetResString(IDS_OUTPUT_STATE_HIGHT_LOW);
	case CMB_OUTPUT_STATE_BY_REQ:
		return GetResString(IDS_OUTPUT_STATE_BY_REQ);
	case CMB_OUTPUT_STATE_ZONE_DETECT :
		return GetResString(IDS_OUTPUT_STATE_ZONE_DETECT);
	case CMB_OUTPUT_STATE_ALARM:
		return GetResString(IDS_OUTPUT_STATE_ALARM);
	case CMB_OUTPUT_STATE_BUZZER:
		return GetResString(IDS_OUTPUT_STATE_BUZZER);
	}
	return "";
}

CString CGsmAlertDoc::GetInputTypeNameFromNum(enmZoneType a_iInputType)
{
	switch (a_iInputType)
	{

	case enmZoneTypeRegular:
		return GetResString(IDS_INPUT_TYPE_REGULAR);
	case enmZoneTypePulseChange:
		return GetResString(IDS_INPUT_TYPE_PULSE_CHANGE);
	case enmZoneTypePulseDown:
		return GetResString(IDS_INPUT_TYPE_PULSE_DOWN);
	case enmZoneTypePulseUp:
		return GetResString(IDS_INPUT_TYPE_PULSE_UP);
	}
	return "";
}


/////////////////////////////////////////////////////////////////////////////
// CGsmAlertDoc diagnostics

#ifdef _DEBUG
void CGsmAlertDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGsmAlertDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertDoc commands
