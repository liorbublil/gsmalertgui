// ChangeLanguage.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "ChangeLanguage.h"
#include "Language.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChangeLanguage dialog


CChangeLanguage::CChangeLanguage(CWnd* pParent /*=NULL*/)
	: CDialog(CChangeLanguage::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChangeLanguage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CChangeLanguage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChangeLanguage)
	DDX_Control(pDX, IDC_CMB_LANGUAGES, m_cmbLanguage);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChangeLanguage, CDialog)
	//{{AFX_MSG_MAP(CChangeLanguage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChangeLanguage message handlers

BOOL CChangeLanguage::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString l_strLanguagePath=g_pApp->GetProfileString(REG_SETTINGS_SECTION,REG_LANGUAGE);

	CLanguage* l_pLanguage;
	bool l_bFound=false;
	POSITION l_pos = _listLanguages.GetHeadPosition();
	int l_iNewIndex;
	while (l_pos)
	{
		l_pLanguage= _listLanguages.GetNext( l_pos );
		l_iNewIndex=m_cmbLanguage.AddString(l_pLanguage->sDisplayName);
		if(l_strLanguagePath==l_pLanguage->sLangDLLFullPath)
		{
			l_bFound=true;
			m_cmbLanguage.SetCurSel(l_iNewIndex);
		}
	}
	if(!l_bFound)
	{
		m_cmbLanguage.SetCurSel(0);		//Set to the default language
	}

	Localize();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChangeLanguage::OnOK() 
{
	CString l_strLanguage;
	CLanguage* l_pLanguage;

	g_pApp->GetProfileString(REG_SETTINGS_SECTION,REG_LANGUAGE);
	m_cmbLanguage.GetLBText(m_cmbLanguage.GetCurSel(),l_strLanguage);

	POSITION l_pos = _listLanguages.GetHeadPosition();
	while (l_pos)
	{
		l_pLanguage= _listLanguages.GetNext( l_pos );
		if(l_strLanguage==l_pLanguage->sDisplayName)
		{
			g_pApp->WriteProfileString(REG_SETTINGS_SECTION,REG_LANGUAGE,l_pLanguage->sLangDLLFullPath);
			break;
		}
	}

	CDialog::OnOK();
}

void CChangeLanguage::Localize()
{
	INFO_LOG(g_DbLogger,CString("CChangeLanguage::Localize"));

	SetWindowText(GetResString(IDS_IDD_CHANGE_LANG_DIAL));
	SetDlgItemText(IDOK,GetResString(IDS_OK));
	SetDlgItemText(IDCANCEL,GetResString(IDS_CANCEL));
}