#if !defined(AFX_LOGVIEW_H__B7AB8FB0_7860_4A4A_8496_AA97528A7F7A__INCLUDED_)
#define AFX_LOGVIEW_H__B7AB8FB0_7860_4A4A_8496_AA97528A7F7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// LogView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLogView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CLogView : public CFormView
{
protected:
	CLogView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CLogView)
public:
	enum enmLogViewLastSentCommand
	{
		enmLogViewLastSentCommandNone,
		enmLogViewLastSentCommandGetLogLen,
		enmLogViewLastSentCommandGetLogData,
		enmLogViewLastSentCommandDelLog,
	};

// Form Data
public:
	//{{AFX_DATA(CLogView)
	enum { IDD = IDD_LOG_FORM };
	CButton	m_cmdDelLog;
	CButton	m_cmdExportLog;
	CEdit	m_txtLastLogEntries;
	CButton	m_cmdLastLogEntries;
	CStatic	m_lblLastLogEntries;
	CButton	m_cmdStop;
	CProgressCtrl	m_prgsLogDownload;
	CButton	m_cmdGetLog;
	CStatic	m_lblLog;
	CListCtrl	m_lstLog;
	CButton	m_framLog;
	CString	m_strLastLogEntries;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	CMainFrame *m_pMainFram;
	enmLogViewLastSentCommand m_enmLogViewLastSentCommand;
	bool m_bStopDownload;
	bool m_bOnLastLogEntriesReq;
	int m_iHashFailedTimes;
	int m_iLastFromIndex;
	int m_iLastToIndex;
	bool m_bHashOK;
	int m_iAmountOfLogDataInUnit;
// Operations
public:
protected:
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmLogViewLastSentCommand &a_enmLogViewLastSentCommand);
	void Localize() ;
	void EnableControls(BOOL a_bEnabled=TRUE);
	void AddToLogLst(CTime a_tmLogTime,CString a_strLogContent);
// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CLogView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CLogView)
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnExportDb();
	afx_msg void OnCmdGetLog();
	afx_msg void OnCmdStop();
	afx_msg void OnCmdLastLogEntries();
	afx_msg void OnCmdExportLog();
	afx_msg void OnCmdDelLog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CLogView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGVIEW_H__B7AB8FB0_7860_4A4A_8496_AA97528A7F7A__INCLUDED_)
