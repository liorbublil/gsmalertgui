/*********************************************************
* Multi-Page Interface
* Version: 1.2
* Date: September 2, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#include "stdafx.h"
#include "GsmAlert.h"
#include "MPIDocTemplate.h"
#include "MPIChildFrame.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


IMPLEMENT_DYNAMIC(CMPIDocTemplate, CMultiDocTemplate)

CMPIDocTemplate::CMPIDocTemplate(UINT nIDResource, CRuntimeClass* pDocClass, CRuntimeClass* pFrameClass)
	: CMultiDocTemplate(nIDResource, pDocClass, pFrameClass, NULL)
{
}

CMPIDocTemplate::~CMPIDocTemplate()
{
	WORD nID;	// destroy shared menus and accelerators
	POSITION pos = m_mapMenu.GetStartPosition();
	while (pos)
	{
		HMENU hMenu;
		m_mapMenu.GetNextAssoc(pos, nID, (void*&)hMenu);
		DestroyMenu(hMenu);
	}
	pos = m_mapAccel.GetStartPosition();
	while (pos)
	{
		HACCEL hAccel;
		m_mapAccel.GetNextAssoc(pos, nID, (void*&)hAccel);
		DestroyAcceleratorTable(hAccel);
	}
}

BEGIN_MESSAGE_MAP(CMPIDocTemplate, CMultiDocTemplate)
END_MESSAGE_MAP()

CDocument* CMPIDocTemplate::OpenDocumentFile(LPCTSTR lpszPathName, BOOL bAddToMRU, BOOL bMakeVisible)
{
	return OpenDocumentFile(lpszPathName, bMakeVisible);
}

CDocument* CMPIDocTemplate::OpenDocumentFile(LPCTSTR lpszPathName, BOOL bMakeVisible)
{
	// don't create new document
	// NOTE: no support for serialization
	CDocument *l_pDoc;
	POSITION pos = GetFirstDocPosition();
	l_pDoc=GetNextDoc(pos);

	if (!l_pDoc->SaveModified())
		return NULL;        // leave the original one

	if (lpszPathName == NULL)
	{
		// create a new document
		SetDefaultTitle(l_pDoc);

		// avoid creating temporary compound file when starting up invisible
		if (!bMakeVisible)
			l_pDoc->m_bEmbedded = TRUE;

		if (!l_pDoc->OnNewDocument())
		{
			// user has been alerted to what failed in OnNewDocument
			TRACE0("CDocument::OnNewDocument returned FALSE.\n");
			return NULL;
		}
	}
	else
	{
		CWaitCursor wait;

		// open an existing document
		BOOL bWasModified = l_pDoc->IsModified();
		l_pDoc->SetModifiedFlag(FALSE);  // not dirty for open

		if (!l_pDoc->OnOpenDocument(lpszPathName))
		{
			return NULL;
		}
		l_pDoc->SetPathName(lpszPathName);
	}
	for(int l_i=0;l_i<m_arrChildFrm.GetSize();++l_i)
	{
		InitialUpdateFrame(m_arrChildFrm[l_i], l_pDoc, TRUE);
	}
	if(lpszPathName)
	{
		//Write the new file in the registry as the last worked file
		g_pApp->WriteProfileString(REG_SETTINGS_SECTION,REG_LAST_SET_FILE_ENTRY,lpszPathName);
	}
	return l_pDoc;
}

CDocument* CMPIDocTemplate::CreateNewDocument()
{
	CDocument* l_pDoc = CMultiDocTemplate::CreateNewDocument();
	if (l_pDoc)
	{
		SetDefaultTitle(l_pDoc);
		if (l_pDoc->OnNewDocument())
		{
			//pDoc
			return l_pDoc;
		}
		delete l_pDoc;
	}
	return NULL;
}

CMPIChildFrame* CMPIDocTemplate::CreateMPIFrame(int nIDResource,CMPIChildFrame::enmMPIChildFrameType a_enmMPIChildFrameType)
{
	POSITION pos = GetFirstDocPosition();
	CDocument* pDoc = GetNextDoc(pos);

	// initialize the extended create context
	CCreateContext context;
	context.m_pCurrentFrame = NULL;
	context.m_pCurrentDoc = pDoc;
	context.m_pNewViewClass = NULL;
	context.m_pNewDocTemplate = this;

	// create and load child frame
	CMPIChildFrame* pFrame = (CMPIChildFrame*)m_pFrameClass->CreateObject();
	ASSERT_KINDOF(CMPIChildFrame, pFrame);
	pFrame->SetChildType(a_enmMPIChildFrameType);
	if (!pFrame->LoadFrame(m_nIDResource, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL, &context))
		return NULL;

	if (pFrame)
	{
		// load frame's title from resources
		CString strTitle;
		strTitle.LoadString(nIDResource);
		pFrame->SetTitle(strTitle);

		// display the window
		InitialUpdateFrame(pFrame, pDoc, TRUE);

		m_arrChildFrm.Add(pFrame);

		return pFrame;
	}

	return NULL;
}

//Create the application frames
void CMPIDocTemplate::CreateAppFrames(CMainFrame *a_pMainFrame)
{
	a_pMainFrame->m_pSettingsFrame= CreateMPIFrame(IDR_SETTINGS,CMPIChildFrame::enmMPIChildFrameTypeSettings);
	a_pMainFrame->m_pLogFrame= CreateMPIFrame(IDR_LOG,CMPIChildFrame::enmMPIChildFrameTypeLog);
}

int CMPIDocTemplate::FindChildFrame(CMPIChildFrame *pFrame)
{
	for (int i=0; i<m_arrChildFrm.GetSize(); i++)
	{
		if (m_arrChildFrm[i] == pFrame)
			return i;
	}
	return -1;
}

CMPIChildFrame* CMPIDocTemplate::GetChildFrame(int nIndex)
{
	return m_arrChildFrm[nIndex];
}