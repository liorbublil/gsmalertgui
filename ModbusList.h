#pragma once

// CModbusList command target

class CModbusListElement : public CObject
{
public:
	CModbusListElement();
	~CModbusListElement();
	void operator=(const CModbusListElement& a_ModbusListElement);   // = operator

	int m_iIndex;				//The index of the register in the list
	CString m_strRegName;		//The name that the user gives to the item
	int m_iSlaveId;				//The ModBus slave ID that this register is corelative to
	int m_iRegType;				//The Type in the Modbus memory map (holding register, register bit)
	int m_iVarType;				//The register type (UINT, INT, LONG, ULONG, etc...)
	int m_iRegAddr;				//The register address
	int m_iRegDecoding;			//The register decoding (LSWF or MSWF)
	CString m_strModbusKey;		//The register key for the cloud ID

	bool m_bInUnit;
};

class CModbusList : public CTypedPtrList< CObList, CModbusListElement* >
{
public:
	DECLARE_SERIAL(CModbusList)
	CModbusList();
	virtual ~CModbusList();

	void Serialize(CArchive& archive);

	void DeleteAllList();
	void AddItemSorted(int a_iIndex, CString a_strRegName, int a_iSlaveId, int a_iRegType, int a_iVarType, int a_iRegAddr, int a_iRegDecoding, CString a_strModbusKey, bool a_bInUnit, bool a_bNoSearch);
	void DeleteItem(int a_iIndex);
	CModbusListElement* GetItemByIndex(int a_iIndex);
	CModbusListElement* GetItemByPos(int a_iPos);
	CModbusListElement* GetItemByKey(CString a_strKey);
};
