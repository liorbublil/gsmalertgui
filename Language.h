#ifndef _LANGUAGE_H
#define _LANGUAGE_H


#include "stdafx.h"
#include "Afxtempl.h"

class CLanguage 
{
public:
	CString sLangDLLFullPath;
	CString sDisplayName;
};

extern CList <CLanguage*, CLanguage*> _listLanguages;


CString GetResString(UINT uStringID);

void LanguagesInit(const CString& rstrLangDir, bool bReInit = false);
void LanguagesRelease();
bool LoadLangLibDefault();
bool LoadLangLib( CString& strLangDLLPath);
HINSTANCE GetCurrentLangHandle();


#endif //_LANGUAGE_H