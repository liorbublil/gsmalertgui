// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "MPIChildFrame.h"
#include "MPIDocTemplate.h"
#include "MainFrm.h"
#include "SearchModemDlg.h"
#include "ChangeLanguage.h"
#include "Language.h"
#include "InputDialog.h"
#include "global.h"
#include "MD5.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define TIMER_ID_COM_TIME_OUT 0
#define TIMER_ID_COM_PING 1

#define COM_TIME_OUT_MS 5000
#define COM_TIME_OUT_EX_MS 30000
#define COM_TIME_OUT_CONN_MS 120000
#define COM_PING_INTERVAL 15000


/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	ON_UPDATE_COMMAND_UI_RANGE(ID_MPI_SETTINGS, ID_MPI_LOG,OnMPIUpdate)
    ON_COMMAND_RANGE(ID_MPI_SETTINGS, ID_MPI_LOG, OnMPICommand)
	ON_MESSAGE(UM_COMMAND_ARRIVED,OnCommandArrived)
	ON_MESSAGE(UM_SEND_COMMAND,OnSendCommand)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_COMMAND(ID_LOAD_SEARCH_MODEM_DLG, OnLoadSearchModemDlg)
	ON_COMMAND(ID_CHANGE_LANGUAGE, OnChangeLanguage)
	ON_COMMAND(ID_CONNECT_TO_UNIT, OnConnectToUnit)
	ON_COMMAND(ID_DISCONNECT_FROM_UNIT, OnDisconnectFromUnit)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	m_pLogFrame=NULL;
	m_pSettingsFrame=NULL;
	m_pComThread=NULL;
	m_hComPort=(void*)-1;
	m_enmMainFrameUnitMessage=MFUM_NONE;
	m_bClearToSendToUnit=true;
	m_bConnectedToUnit=false;
	m_bConnectingToUnit=false;
	m_enmUsedTimeOut=UTO_REGULAR;
	m_strEncr=ENCR_DEF;
}

CMainFrame::~CMainFrame()
{
	INFO_LOG(g_DbLogger,"CMainFrame::~CMainFrame()");

	if(m_hComPort!=(void*)-1) //if the COM is open, close it
		CloseHandle(m_hComPort);
	if(m_pComThread)
	{
		m_pComThread->PostThreadMessage(WM_QUIT,NULL,NULL);
		while(WaitForSingleObject(m_pComThread->m_hThread,1000)==WAIT_TIMEOUT)
			Sleep(500);
		m_pComThread=NULL;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.Create(this, AFX_IDW_TOOLBAR) ||
        !m_wndToolBar.LoadToolBar(IDR_MAINFRAME, AILS_NEW))
    {
        TRACE0("Failed to create toolbar\n");
        return -1;      // fail to create
    }

    if (!m_wndMPIBar.Create(this, AFX_IDW_TOOLBAR+6) ||
        !m_wndMPIBar.LoadToolBar(IDR_MPI_BAR, AILS_NEW))
    {
        TRACE0("Failed to create MPI toolbar\n");
        return -1;      // fail to create
    }

	if (!m_wndReBar.Create(this, RBS_BANDBORDERS,
		WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | CBRS_ALIGN_TOP)
		//|| !m_wndReBar.AddBar(&m_wndMenuBar)
		|| !m_wndReBar.AddBar(&m_wndToolBar, NULL, (CBitmap*)NULL, RBBS_BREAK)
  		|| !m_wndReBar.AddBar(&m_wndMPIBar, NULL, (CBitmap*)NULL, RBBS_BREAK))
	{
        TRACE0("Failed to create rebar\n");
        return -1;      // fail to create
    }

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}
	return 0;
}

void CMainFrame::OnUpdateFrameTitle(BOOL bAddToTitle)
{
	CMDIFrameWnd::OnUpdateFrameTitle(FALSE);
}

void CMainFrame::UpdateMenu(HMENU hMenu)
{
	SetMenu(NULL);
}

void CMainFrame::OnConnectToUnit()
{
	if(!OpenComPort())
	{
		m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_UNIT_INIT_FAIL));
		return;
	}
	m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_HIDDEN);
	m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_HIDDEN);
	m_wndToolBar.RedrawWindow();
	//Send password command to the unit
	CString *l_strCommand=new CString();
	CGsmAlertDoc *l_pDoc=GetGsmAlertDoc();
	if(l_pDoc->m_bUseModem)
	{
		*l_strCommand=MODEM_INIT_COMMAND;
		m_enmMainFrameUnitMessage=MFUM_MODEM_INIT;
	}
	else
	{
		if(m_strEncr==CString(""))
		{
			l_strCommand->Format("%s=%s",PASS_MSG_COMMAND,l_pDoc->m_strUnitPass);
			
			//If the user is changing the password in on the screen and we are connected,
			//we don't want that the working password will disconnect, so we need to save
			//the password in a separate variable
			l_pDoc->m_strTempUnitPass=l_pDoc->m_strUnitPass;
			
			m_enmMainFrameUnitMessage=MFUM_SET_PASS;
		}
		else
		{
			
			unsigned char* l_pTmp=(unsigned char*)m_strEncrRand.GetBuffer(16);
			for(int l_i=0;l_i<15;++l_i)
			{
				l_pTmp[l_i]=rand() % 0x100;
			}
			MD5ReplaceDigits(l_pTmp,15);
			m_strEncrRand.ReleaseBuffer(15);
			
			l_strCommand->Format("%s=%s",ENCR_MSG_COMMAND,m_strEncrRand);
			
			m_enmMainFrameUnitMessage=MFUM_GET_ENCR;
		}
	}
	OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
	m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_CONNECTING_TO_UNIT));
	m_bConnectingToUnit=true;
}

void CMainFrame::OnDisconnectFromUnit()
{
	//If message is being sent, don't disconnect
	if (m_SendQueue.qLookHead())
	{
		MessageBox(GetResString(IDS_UNIT_IS_IN_SESSION),NULL,MB_ICONERROR);
		return;
	}
	//Send password command to the unit
	CString *l_strCommand=new CString();
	l_strCommand->Format("%s",DISCONNECT_MSG_COMMAND);
	m_enmMainFrameUnitMessage=MFUM_EXIT_SET_MODE;
	OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
	m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTING_FROM_UNIT));

	m_wndToolBar.SetButtonStyle(DISCONNECT_TOOL_BAR_INDEX,TBBS_HIDDEN);
	m_wndToolBar.RedrawWindow();
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

void CMainFrame::OnMPIUpdate(CCmdUI* pCmdUI)
{
	INFO_LOG(g_DbLogger,"CMainFrame::OnMPIUpdate()");
	pCmdUI->Enable();

	CMPIChildFrame* pFrame = (CMPIChildFrame*)MDIGetActive();

	if (pFrame)
	{
		POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
		CMPIDocTemplate* pDocTemplate =
						 (CMPIDocTemplate*)AfxGetApp()->
						 GetNextDocTemplate(pos);

		ASSERT_KINDOF(CMPIDocTemplate, pDocTemplate);

		int nIndex = pDocTemplate->FindChildFrame(pFrame);

		pCmdUI->SetCheck(nIndex == (int)pCmdUI->m_nID -
		   ID_MPI_SETTINGS);
	}
}

void CMainFrame::OnMPICommand(UINT nID)
{
	INFO_LOG(g_DbLogger,"CMainFrame::OnMPICommand()");

	POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
	CMPIDocTemplate* pDocTemplate = (CMPIDocTemplate*)AfxGetApp()->GetNextDocTemplate(pos);

	ASSERT_KINDOF(CMPIDocTemplate, pDocTemplate);

	CMPIChildFrame* pFrame = pDocTemplate->
							GetChildFrame(nID - ID_MPI_SETTINGS);

	ASSERT_KINDOF(CMPIChildFrame, pFrame);
	MDIActivate(pFrame);
}

void CMainFrame::CloseComPort()
{
	if(m_hComPort!=(void*)-1) //if the COM is open, close it
		CloseHandle(m_hComPort);
	if(m_pComThread)
	{
		KillTimer(TIMER_ID_COM_PING);
		KillTimer(TIMER_ID_COM_TIME_OUT);
		m_pComThread->PostThreadMessage(WM_QUIT,NULL,NULL);
		while(WaitForSingleObject(m_pComThread->m_hThread,1000)==WAIT_TIMEOUT)
			Sleep(500);
		m_pComThread=NULL;
		m_hComPort=(void*)-1;
	}
}

bool CMainFrame::OpenComPort() 
{
	INFO_LOG(g_DbLogger,"CMainFrame::OpenComPort()");

	CGsmAlertDoc *l_pDoc=GetGsmAlertDoc();
	
	//openning the COM
	m_hComPort = CreateFile(l_pDoc->m_strCom, GENERIC_READ | GENERIC_WRITE,0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	//if fail to open the COM msg an error and exit the program
	if(int(m_hComPort)==-1)
	{
		if (l_pDoc->m_strCom!="")
			MessageBox(GetResString(IDS_CANNOT_OPEN_COM)+l_pDoc->m_strCom,NULL,MB_ICONERROR);
		else
			MessageBox(GetResString(IDS_SELECT_COM_PORT),NULL,MB_ICONERROR);
		return false;
	}

	DCB l_oDcb;
	memset(&l_oDcb,0,sizeof(DCB));
	l_oDcb.BaudRate=CBR_115200;
	l_oDcb.DCBlength=sizeof(DCB);
	l_oDcb.fBinary =TRUE;
	l_oDcb.ByteSize =8;
	l_oDcb.StopBits =ONESTOPBIT;
	l_oDcb.fDtrControl = TRUE;
	l_oDcb.fRtsControl = TRUE;
	if(!SetCommState(m_hComPort,&l_oDcb))
	{
		CloseHandle(m_hComPort);
		m_hComPort=(void*)-1;
		MessageBox(CString("Cannot configure ")+l_pDoc->m_strCom);
		return false;
	}
	COMMTIMEOUTS l_oComTimeOuts;
	memset(&l_oComTimeOuts,0,sizeof(COMMTIMEOUTS));
	l_oComTimeOuts.ReadIntervalTimeout =1;
	l_oComTimeOuts.ReadTotalTimeoutConstant=1;
	
	if(!SetCommTimeouts(m_hComPort,&l_oComTimeOuts))
	{
		CloseHandle(m_hComPort);
		m_hComPort=(void*)-1;
		MessageBox(CString("Cannot configure ")+l_pDoc->m_strCom);
		return false;
	}
	
	m_oComTrdVars.l_hComPort=&m_hComPort;
	m_oComTrdVars.m_hWnd=GetSafeHwnd();
	SetTimer(TIMER_ID_COM_PING,COM_PING_INTERVAL,NULL);
	m_pComThread= AfxBeginThread(ComThread,&m_oComTrdVars);		//Begin the com thread

	m_bClearToSendToUnit=true;
	m_SendQueue.EmptyQueue();
	return true;
}

void CMainFrame::OnLoadSearchModemDlg()
{
	INFO_LOG(g_DbLogger,"CMainFrame::OnLoadSearchModemDlg()");

	if((m_bConnectedToUnit)||(m_bConnectingToUnit))
	{
		MessageBox(GetResString(IDS_DISCONNECT_FROM_UNIT_MSG),NULL,MB_ICONERROR);
		return;
	}
	CSearchModemDlg l_SearchModemDlg(GetGsmAlertDoc());
	l_SearchModemDlg.DoModal();
}

void CMainFrame::OnChangeLanguage()
{
	INFO_LOG(g_DbLogger,"CMainFrame::OnChangeLanguage()");

	CChangeLanguage l_ChangeLanguage;

	int l_iAnswer;
	if((l_iAnswer=l_ChangeLanguage.DoModal())==IDOK)
	{
		MessageBox(GetResString(IDS_RESTART_NEEDED));
	}
}

LRESULT CMainFrame::OnCommandArrived(WPARAM wParam,LPARAM lParam)
{
	TRACE("MainFrm OnCommandArrived\n");
	if(wParam==COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger,CString("CMainFrame::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		return 0;
	}
	
	if(wParam==COMMAND_MESSAGE_MODEM_RESPOND)
	{
		TRACE("MainFrm COMMAND_MESSAGE_MODEM_RESPOND\n");
		//Get the incoming data to the all data string
		CString l_strTmp;
		char *l_strArrivedData;
		CComTrdDataInVars *l_pComTrdDataInVars=(CComTrdDataInVars*)lParam;
		l_strArrivedData=l_strTmp.GetBufferSetLength(l_pComTrdDataInVars->m_iDataLen+1);
		memcpy(l_strArrivedData,(void*)l_pComTrdDataInVars->m_pData,l_pComTrdDataInVars->m_iDataLen);
		//l_strTmp.ReleaseBuffer(l_pComTrdDataInVars->m_iDataLen);
		l_strTmp.ReleaseBufferSetLength(l_pComTrdDataInVars->m_iDataLen);
		m_strIncommingData+=l_strTmp;
		delete l_pComTrdDataInVars->m_pData;
		delete l_pComTrdDataInVars;
	}
	else if(wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		m_strIncommingData="";
	}

	INFO_LOG(g_DbLogger,CString("CMainFrame::OnCommandArrived m_strIncommingData:") + m_strIncommingData);

	bool l_bContinue;	//indicate if to loop again in order to see if there is another messages in the string

	CSendQueueElement *l_pSendQueueElement;
	do
	{
		l_bContinue=false;
		//Reset the "ping" timer		
		KillTimer(TIMER_ID_COM_PING);
		SetTimer(TIMER_ID_COM_PING,COM_PING_INTERVAL,NULL);

		if(m_SendQueue.IsEmpty())	//If the queue is not Empty
		{
			int l_iLastMessage;
			if((l_iLastMessage=FindInString(m_strIncommingData,END_OF_MSG_COMMAND))!=-1)
			{
				CString l_strMessageToHandle=m_strIncommingData.Left(l_iLastMessage+strlen(END_OF_BUSRT_RESPOND));
				INFO_LOG(g_DbLogger,CString("CMainFrame::OnCommandArrived l_strMessageToHandle:") + l_strMessageToHandle);

				m_strIncommingData=m_strIncommingData.Mid(l_iLastMessage+strlen(END_OF_BUSRT_RESPOND));
				l_bContinue=true;
			}
		}
		else
		{
			//Reset the time out timer
			KillTimer(TIMER_ID_COM_TIME_OUT);
			switch(m_enmUsedTimeOut)
			{
			case UTO_REGULAR:
				SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_MS,NULL);
				break;
			case UTO_EXTENDED:
				SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_EX_MS,NULL);
				break;
			case UTO_CONNECTION:
				SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_CONN_MS,NULL);
				break;
			}
			l_pSendQueueElement=m_SendQueue.qLookHead();
			//If there is an ACK reply
			if(((FindInString(m_strIncommingData,END_OF_MSG_COMMAND)!=-1) && (l_pSendQueueElement->m_bUnitMsg)) ||(((FindInString(m_strIncommingData,MODEM_ACK_RESPOND)!=-1) || (FindInString(m_strIncommingData,MODEM_ERROR_RESPOND)!=-1)||(FindInString(m_strIncommingData,MODEM_BUSY_RESPOND)!=-1)||(FindInString(m_strIncommingData,MODEM_CONNECT_RESPOND)!=-1)||(FindInString(m_strIncommingData,MODEM_NO_CARRIER_RESPOND)!=-1)) && (l_pSendQueueElement->m_bUnitMsg==false)))
			{
				INFO_LOG(g_DbLogger,CString("CMainFrame::OnCommandArrived found respond in m_strIncommingData to AT command"));

				int l_iRespondPlaceInString;
				CString l_strLastMessage;
				CString l_strRespond;

				m_bClearToSendToUnit=true;
				
				if(l_pSendQueueElement->m_bUnitMsg)
				{
					l_strRespond=CString(END_OF_MSG_COMMAND);
					l_iRespondPlaceInString=FindInString(m_strIncommingData,END_OF_MSG_COMMAND);

					l_strLastMessage=m_strIncommingData.Left(l_iRespondPlaceInString);
					m_strIncommingData=m_strIncommingData.Mid(l_iRespondPlaceInString+l_strRespond.GetLength());
				}
				else
				{
					if(FindInString(m_strIncommingData,MODEM_ACK_RESPOND)!=-1)
					{
						l_strRespond=CString(MODEM_ACK_RESPOND);
					}
					else if(FindInString(m_strIncommingData,MODEM_ERROR_RESPOND)!=-1)
					{
						l_strRespond=CString(MODEM_ERROR_RESPOND);
					}
					else if(FindInString(m_strIncommingData,MODEM_BUSY_RESPOND)!=-1)
					{
						l_strRespond=CString(MODEM_BUSY_RESPOND);
					}
					else if(FindInString(m_strIncommingData,MODEM_CONNECT_RESPOND)!=-1)
					{
						l_strRespond=CString(MODEM_CONNECT_RESPOND);
					}
					else if(FindInString(m_strIncommingData,MODEM_NO_CARRIER_RESPOND)!=-1)
					{
						l_strRespond=CString(MODEM_NO_CARRIER_RESPOND);
					}
					
					l_iRespondPlaceInString=FindInString(m_strIncommingData,l_strRespond);

					l_strLastMessage=m_strIncommingData.Left(l_iRespondPlaceInString + l_strRespond.GetLength());
					m_strIncommingData=m_strIncommingData.Mid(l_iRespondPlaceInString + l_strRespond.GetLength());
				}
				//Stop the time out timer
				KillTimer(TIMER_ID_COM_TIME_OUT);
				l_pSendQueueElement=m_SendQueue.qDequeue();

				INFO_LOG(g_DbLogger,CString("CMainFrame::OnCommandArrived m_strIncommingData:") + m_strIncommingData);

				if(l_pSendQueueElement->m_hWnd==GetSafeHwnd())
				{
					HandleIncommingMsg(l_strLastMessage);
				}
				else
				{
					CString *l_pLastMessage=new CString();
					*l_pLastMessage=l_strLastMessage;
					::PostMessage(l_pSendQueueElement->m_hWnd,UM_COMMAND_ARRIVED,COMMAND_MESSAGE_MODEM_RESPOND,(LPARAM)l_pLastMessage);
				}
				delete l_pSendQueueElement;
				OnSendCommand(NULL,NULL);	//Check if there is another message waiting to be sent
				l_bContinue=true;
			}
			//The only message that should be sent in the middle of the request is get phone numbers list,
			//because it's a very long time to retrieve this message
		}
	}while(l_bContinue);
	INFO_LOG(g_DbLogger,CString("exit CMainFrame::OnCommandArrived"));
	return 0;
}

LRESULT CMainFrame::OnSendCommand(WPARAM wParam,LPARAM lParam)
{
	INFO_LOG(g_DbLogger,CString("CMainFrame::OnSendCommand"));

	CSendQueueElement *l_pSendQueueElement;
	CString *l_pCommand;

	if(wParam)		//If there is a new command to send
	{
		//Put the command in the queue
		l_pSendQueueElement=new CSendQueueElement();
		l_pSendQueueElement->m_hWnd=(HWND)wParam;
		l_pCommand=(CString*)lParam;
		//If the command is a modem command and not a unit message, 
		//don't add the unit message prefix and suffix
		if((l_pCommand->Left(2)==MODEM_COMMAND_PREFIX) || (*l_pCommand==MODEM_DISCONNECT_COMMAND))
		{
			l_pSendQueueElement->m_strCommand=*l_pCommand;
			l_pSendQueueElement->m_bUnitMsg=false;//Not a unit message, It's a modem message
		}
		else
		{
			l_pSendQueueElement->m_strCommand=START_OF_MSG_COMMAND + *l_pCommand + END_OF_MSG_COMMAND;
		}
		m_SendQueue.qEnqueue(l_pSendQueueElement);
		delete l_pCommand;
	}

	l_pSendQueueElement=m_SendQueue.qLookHead();
	if(l_pSendQueueElement)
	{
		if((!(l_pSendQueueElement->m_bSentToUnit))&&(m_bClearToSendToUnit))
		{
			if(((m_pComThread)&&(m_bConnectedToUnit))||((m_pComThread)&&(l_pSendQueueElement->m_hWnd==GetSafeHwnd())))
			{
				m_bClearToSendToUnit=false;
				l_pSendQueueElement->m_bSentToUnit=true;
				l_pCommand=new CString(l_pSendQueueElement->m_strCommand);			
				m_pComThread->PostThreadMessage(UM_SEND_COMMAND,NULL,(LPARAM)l_pCommand);
				if((FindInString(*l_pCommand,SET_PHONE_LIST_MSG_COMMAND)!=-1) || (FindInString(*l_pCommand, MODBUS_GET_RESULTS_MSG_COMMAND) != -1))
				{
					SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_EX_MS,NULL);
					m_enmUsedTimeOut=UTO_EXTENDED;
				}
				else if(FindInString(*l_pCommand,MODEM_DIAL_COMMAND)!=-1)
				{
					SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_CONN_MS,NULL);
					m_enmUsedTimeOut=UTO_CONNECTION;
				}
				else
				{
					SetTimer(TIMER_ID_COM_TIME_OUT,COM_TIME_OUT_MS,NULL);
					m_enmUsedTimeOut=UTO_REGULAR;
				}
			}
			else
			{
				::PostMessage(l_pSendQueueElement->m_hWnd,UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);
				l_pSendQueueElement=m_SendQueue.qDequeue();
				delete l_pSendQueueElement;
			}
		}
	}
	INFO_LOG(g_DbLogger,CString("exit CMainFrame::OnSendCommand"));
	return 0;
}

void CMainFrame::HandleIncommingMsg(CString a_strIncommingMsg)
{
	CGsmAlertDoc *l_pDoc=GetGsmAlertDoc();
	CString *l_strCommand;
	
	INFO_LOG(g_DbLogger,CString("CMainFrame::HandleIncommingMsg a_strIncommingMsg:") + a_strIncommingMsg);

	if(FindInString(a_strIncommingMsg,MODEM_ACK_RESPOND)!=-1)
	{
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_MODEM_INIT:
			{
				l_strCommand=new CString();
				*l_strCommand=MODEM_CHECK_CREG_COMMAND;
				m_enmMainFrameUnitMessage=MFUM_MODEM_CHECK_CREG;
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
				m_iCRegCheckFailed=0;
				break;
			}
		case MFUM_MODEM_CHECK_CREG:
			{
				if((FindInString(a_strIncommingMsg,MODEM_CREG_RESPOND_REG)!=-1) || (FindInString(a_strIncommingMsg,MODEM_CREG_RESPOND_ROAMING)!=-1))
				{
					l_strCommand=new CString();
					l_strCommand->Format("%s%s\r",MODEM_DIAL_COMMAND,l_pDoc->m_strUnitPhoneNumber);
					m_enmMainFrameUnitMessage=MFUM_MODEM_DIAL;
					OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
				}
				else
				{
					++m_iCRegCheckFailed;
					if(m_iCRegCheckFailed>5)
					{
						m_bConnectedToUnit=false;
						m_bConnectingToUnit=false;
						m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
						m_enmMainFrameUnitMessage=MFUM_NONE;
						m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
						m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
						m_wndToolBar.RedrawWindow();
						CloseComPort();
						m_SendQueue.EmptyQueue();
						MessageBox(GetResString(IDS_MODEM_NOT_REGISTER),NULL,MB_ICONERROR);
					}
					else
					{
						Sleep(1000);
						l_strCommand=new CString();
						*l_strCommand=MODEM_CHECK_CREG_COMMAND;
						m_enmMainFrameUnitMessage=MFUM_MODEM_CHECK_CREG;
						OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
					}
				}
				break;
			}
		case MFUM_MODEM_DISCONNECT:
			{
				l_strCommand=new CString();
				*l_strCommand=MODEM_HANGUP_COMMAND;
				m_enmMainFrameUnitMessage=MFUM_MODEM_HANGUP_CALL;
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
				break;
			}
		case MFUM_MODEM_HANGUP_CALL:
			{
				m_bConnectedToUnit=false;
				m_bConnectingToUnit=false;
				m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
				m_enmMainFrameUnitMessage=MFUM_NONE;
				m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.RedrawWindow();
				CloseComPort();
				m_SendQueue.EmptyQueue();
			}
		}
	}
	else if(FindInString(a_strIncommingMsg,MODEM_CONNECT_RESPOND)!=-1)
	{
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_MODEM_DIAL:
			l_strCommand=new CString;
			if(m_strEncr=="")
			{
				l_strCommand->Format("%s=%s",PASS_MSG_COMMAND,l_pDoc->m_strUnitPass);
				
				//If the user is changing the password in on the screen and we are connected,
				//we don't want that the working password will disconnect, so we need to save
				//the password in a separate variable
				l_pDoc->m_strTempUnitPass=l_pDoc->m_strUnitPass;
				m_enmMainFrameUnitMessage=MFUM_SET_PASS;
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
			}
			else
			{
				unsigned char* l_pTmp=(unsigned char*)m_strEncrRand.GetBuffer(16);
				for(int l_i=0;l_i<15;++l_i)
				{
					l_pTmp[l_i]=rand() % 0x100;
				}
				MD5ReplaceDigits(l_pTmp,15);
				m_strEncrRand.ReleaseBuffer(15);
				
				l_strCommand->Format("%s=%s",ENCR_MSG_COMMAND,m_strEncrRand);
				
				m_enmMainFrameUnitMessage=MFUM_GET_ENCR;
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
			}
			break;
		}
	}
	else if(FindInString(a_strIncommingMsg,MODEM_ERROR_RESPOND)!=-1)
	{
		m_bConnectedToUnit=false;
		m_bConnectingToUnit=false;
		m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
		m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.RedrawWindow();
		CloseComPort();
		m_SendQueue.EmptyQueue();
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_MODEM_INIT:
			MessageBox(GetResString(IDS_MODEM_INIT_FAILD),NULL,MB_ICONERROR);
			break;
		case MFUM_MODEM_CHECK_CREG:
			MessageBox(GetResString(IDS_MODEM_CREG_FAILD),NULL,MB_ICONERROR);
			break;
		case MFUM_MODEM_DIAL:
			MessageBox(GetResString(IDS_MODEM_DIAL_FAILD),NULL,MB_ICONERROR);
			break;
		}
		m_enmMainFrameUnitMessage=MFUM_NONE;
	}
	else if(FindInString(a_strIncommingMsg,MODEM_NO_CARRIER_RESPOND)!=-1)
	{
		m_bConnectedToUnit=false;
		m_bConnectingToUnit=false;
		m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
		m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.RedrawWindow();
		CloseComPort();
		m_SendQueue.EmptyQueue();
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_MODEM_DIAL:
			MessageBox(GetResString(IDS_MODEM_DIAL_FAILD),NULL,MB_ICONERROR);
			break;
		}
		m_enmMainFrameUnitMessage=MFUM_NONE;
	}
	else if(FindInString(a_strIncommingMsg,ACK_RESPOND)!=-1)
	{
		int l_iRespondStartAt;
		int l_iLastRespondStartAt=0;
		do
		{
			CString tt1(ENCR_MSG_RESPOND);
			CString tt2(ACK_RESPOND);
				
			if((l_iRespondStartAt=FindInString(a_strIncommingMsg, (tt1+ tt2).GetBuffer()/*(CString(ENCR_MSG_RESPOND)+CString(ACK_RESPOND)).GetBuffer()*/ ,l_iLastRespondStartAt))!=-1)
			{
				int l_iDataStartAt=FindInString(a_strIncommingMsg,ENCR_MSG_RESPOND);
				l_iDataStartAt+=(CString(ENCR_MSG_RESPOND)+CString(ACK_RESPOND)+CString(",")).GetLength();
				m_strEncrData=a_strIncommingMsg.Mid(l_iDataStartAt);
				
				l_iLastRespondStartAt=l_iRespondStartAt+1;
				continue;
			}
		} while(l_iRespondStartAt!=-1);
		
		MD5_CTX l_Md5Cntxt;
		CString l_strEncrRes;
		unsigned char* l_pTmp=(unsigned char*)l_strEncrRes.GetBuffer(16);
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_GET_ENCR:
			MD5Init(&l_Md5Cntxt);
			MD5Update(&l_Md5Cntxt, (unsigned char*)m_strEncrRand.GetBuffer(5000), m_strEncrRand.GetLength());
			MD5Update(&l_Md5Cntxt, (unsigned char*)m_strEncr.GetBuffer(5000), m_strEncr.GetLength());
			MD5Final(l_pTmp, &l_Md5Cntxt);
			
			//ReplaceDigits(l_pTmp,15);
			l_strEncrRes.ReleaseBuffer(15);
			if (l_strEncrRes==m_strEncrData)
			{
				l_strCommand=new CString;
				l_strCommand->Format("%s=%s",PASS_MSG_COMMAND,l_pDoc->m_strUnitPass);
				
				//If the user is changing the password in on the screen and we are connected,
				//we don't want that the working password will disconnect, so we need to save
				//the password in a separate variable
				l_pDoc->m_strTempUnitPass=l_pDoc->m_strUnitPass;
				m_enmMainFrameUnitMessage=MFUM_SET_PASS;
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
			}
			else
			{
				if(l_pDoc->m_bUseModem)
				{
					m_bConnectedToUnit=false;
					m_bConnectingToUnit=false;
					m_enmMainFrameUnitMessage=MFUM_MODEM_DISCONNECT;
					l_strCommand=new CString;
					l_strCommand->Format("%s",MODEM_DISCONNECT_COMMAND);
					OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
					MessageBox(GetResString(IDS_INCORRECT_PASS),NULL,MB_ICONERROR | MB_OK);
				}
				else
				{
					//Send the unit is gone message to the other windows
					m_pSettingsFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);
					m_pLogFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);
					
					m_bConnectedToUnit=false;
					m_bConnectingToUnit=false;
					MessageBox(GetResString(IDS_INCORRECT_PASS),NULL,MB_ICONERROR | MB_OK);
					m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
					m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
					m_wndToolBar.RedrawWindow();
					
					m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
					m_enmMainFrameUnitMessage=MFUM_NONE;
					
					CloseComPort();
				}
			}
			break;
		case MFUM_SET_PASS:
			if(m_bConnectingToUnit)	//If the unit is back to online
			{
				m_bConnectingToUnit=false;
				m_wndToolBar.SetButtonStyle(DISCONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.RedrawWindow();
				m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_CONNECTED_TO_UNIT));
				if (MessageBox(GetResString(IDS_GET_DATA_ON_CON), NULL, MB_YESNO | MB_ICONQUESTION) == IDNO)
				{
					l_pDoc->m_bGetDataOnCon = false;
				}
				else
				{
					l_pDoc->m_bGetDataOnCon = true;
				}
				//Send that unit has found to the other windows
				m_pSettingsFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_FOUND,NULL);
				m_pLogFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_FOUND,NULL);
			}
			m_bConnectedToUnit=true;
			m_enmMainFrameUnitMessage=MFUM_NONE;
			break;
		case MFUM_EXIT_SET_MODE:
			if(l_pDoc->m_bUseModem)
			{
				m_bConnectedToUnit=false;
				m_bConnectingToUnit=false;
				m_enmMainFrameUnitMessage=MFUM_MODEM_DISCONNECT;
				l_strCommand=new CString;
				l_strCommand->Format("%s",MODEM_DISCONNECT_COMMAND);
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
			}
			else
			{
				m_bConnectedToUnit=false;
				m_bConnectingToUnit=false;
				m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
				m_enmMainFrameUnitMessage=MFUM_NONE;
				m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.RedrawWindow();
				CloseComPort();
				m_SendQueue.EmptyQueue();
			}
			break;
		}
	}
	else if(FindInString(a_strIncommingMsg,ERROR_RESPOND)!=-1)
	{
		switch(m_enmMainFrameUnitMessage)
		{
		case MFUM_SET_PASS:
		case MFUM_GET_ENCR:
			if(l_pDoc->m_bUseModem)
			{
				m_bConnectedToUnit=false;
				m_bConnectingToUnit=false;
				m_enmMainFrameUnitMessage=MFUM_MODEM_DISCONNECT;
				l_strCommand=new CString;
				l_strCommand->Format("%s",MODEM_DISCONNECT_COMMAND);
				OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
				MessageBox(GetResString(IDS_INCORRECT_PASS),NULL,MB_ICONERROR | MB_OK);
			}
			else
			{
				//Send the unit is gone message to the other windows
				m_pSettingsFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);
				m_pLogFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);

				m_bConnectedToUnit=false;
				m_bConnectingToUnit=false;
				MessageBox(GetResString(IDS_INCORRECT_PASS),NULL,MB_ICONERROR | MB_OK);
				m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
				m_wndToolBar.RedrawWindow();

				m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
				m_enmMainFrameUnitMessage=MFUM_NONE;
				
				CloseComPort();
			}
			break;
		}
	}
}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	CSendQueueElement *l_pSendQueueElement;
	CString *l_strCommand;

	switch(nIDEvent)
	{
	case TIMER_ID_COM_TIME_OUT:
		INFO_LOG(g_DbLogger,CString("CMainFrame::OnTimer TIMER_ID_COM_TIME_OUT"));

		KillTimer(TIMER_ID_COM_TIME_OUT);
		KillTimer(TIMER_ID_COM_PING);
		m_bClearToSendToUnit=true;
		
		//Send the unit is gone message to the other windows
		m_pSettingsFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);
		m_pLogFrame->PostMessage(UM_COMMAND_ARRIVED,COMMAND_MESSAGE_UNIT_NOT_CONNECTED,NULL);

		m_bConnectedToUnit=false;
		m_bConnectingToUnit=false;
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED),NULL,MB_ICONERROR | MB_OK);
		m_wndToolBar.SetButtonStyle(CONNECT_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.SetButtonStyle(SEARCH_MODEM_TOOL_BAR_INDEX,TBBS_BUTTON);
		m_wndToolBar.RedrawWindow();

		m_wndStatusBar.SetPaneText(1,GetResString(IDS_UNIT_STATUS_MSG_DISCONNECTED_FROM_UNIT));
		m_enmMainFrameUnitMessage=MFUM_NONE;
		
		if((l_pSendQueueElement=m_SendQueue.qDequeue())!=NULL)
		{
			//post time out message
			::PostMessage(l_pSendQueueElement->m_hWnd,UM_COMMAND_ARRIVED,COMMAND_MESSAGE_TIME_OUT,NULL);
			delete l_pSendQueueElement;
			//OnSendCommand(NULL,NULL);	//Check if there is another message waiting to be sent
		}
		CloseComPort();
		m_SendQueue.EmptyQueue();
		
		break;
	case TIMER_ID_COM_PING:
		if(m_enmMainFrameUnitMessage==MFUM_NONE)
		{
			INFO_LOG(g_DbLogger,CString("CMainFrame::OnTimer TIMER_ID_COM_PING"));
			l_strCommand=new CString();
			CGsmAlertDoc *l_pDoc=GetGsmAlertDoc();
			l_strCommand->Format("%s=%s",PASS_MSG_COMMAND,l_pDoc->m_strTempUnitPass);
			m_enmMainFrameUnitMessage=MFUM_SET_PASS;
			OnSendCommand((WPARAM)GetSafeHwnd(),(LPARAM)l_strCommand);
		}
		break;
	}
	CMDIFrameWnd::OnTimer(nIDEvent);
}

CGsmAlertDoc* CMainFrame::GetGsmAlertDoc()
{
	POSITION pos = AfxGetApp()->GetFirstDocTemplatePosition();
	CMPIDocTemplate* pDocTemplate = (CMPIDocTemplate*)AfxGetApp()->GetNextDocTemplate(pos);
	if(!pDocTemplate)
		return NULL;
	pos = pDocTemplate->GetFirstDocPosition();
	CGsmAlertDoc *l_pDoc=(CGsmAlertDoc*)pDocTemplate->GetNextDoc(pos);
	return l_pDoc;
}

void CMainFrame::OnClose() 
{
	if((m_bConnectedToUnit)||(m_bConnectingToUnit))
	{
		if (MessageBox(GetResString(IDS_DISCONNECT_FROM_UNIT_MSG), NULL, MB_YESNO) == IDNO)
		{
			return;
		}
	}
	CMDIFrameWnd::OnClose();
}
