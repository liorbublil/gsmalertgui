// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__0F3B4BCA_5EBF_40DB_BE97_F4B66422A78C__INCLUDED_)
#define AFX_MAINFRM_H__0F3B4BCA_5EBF_40DB_BE97_F4B66422A78C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MenuBar.h"
#include "AlphaToolBar.h"
#include "ListToolBar.h"
#include "ComThread.h"
#include "SendQueue.h"
#include "GsmAlertDoc.h"

//#define ENCR_DEF "parts232uQfKhturCuckhk02"
#define ENCR_DEF ""

#define SEARCH_MODEM_TOOL_BAR_INDEX 5
#define CONNECT_TOOL_BAR_INDEX 7
#define DISCONNECT_TOOL_BAR_INDEX 8

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	enum enmMainFrameUnitMessage
	{
		MFUM_NONE,
		MFUM_MODEM_INIT,
		MFUM_MODEM_CHECK_CREG,
		MFUM_MODEM_DIAL,
		MFUM_SET_PASS,
		MFUM_EXIT_SET_MODE,
		MFUM_MODEM_DISCONNECT,
		MFUM_MODEM_HANGUP_CALL,
		MFUM_GET_ENCR
	};
	enum enmUsedTimeOut
	{
		UTO_REGULAR,
		UTO_EXTENDED,
		UTO_CONNECTION
	};
	CMainFrame();

// Attributes
public:
protected:
	enmMainFrameUnitMessage m_enmMainFrameUnitMessage;
// Operations
public:
	void UpdateMenu(HMENU hMenu);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Attributes
public:
	// control bar embedded members
	CStatusBar  m_wndStatusBar;
	CAlphaToolBar	m_wndToolBar;
	CListToolBar	m_wndMPIBar;
	CReBar			m_wndReBar;
	CMPIChildFrame *m_pSettingsFrame;
	CMPIChildFrame *m_pLogFrame;
	CWinThread *m_pComThread;
	CComTrdVars m_oComTrdVars;
	bool m_bConnectedToUnit;
	bool m_bConnectingToUnit;
	int m_iCRegCheckFailed;
	CString m_strModemPass;
	//Encryption data
	CString m_strEncr;
	CString m_strEncrData;
	CString m_strEncrRand;
protected:
	HANDLE m_hComPort;
	CSendQueue m_SendQueue;
	CString m_strIncommingData;
	bool m_bClearToSendToUnit;
	enmUsedTimeOut m_enmUsedTimeOut;
//Operators
public:
	bool OpenComPort();
	void CloseComPort();
	CGsmAlertDoc* GetGsmAlertDoc();
protected:
	void HandleIncommingMsg(CString a_strIncommingMsg);

// Generated message map functions
public:
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);
protected:
	afx_msg LRESULT OnCommandArrived(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnSendCommand(WPARAM wParam,LPARAM lParam);

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLoadSearchModemDlg();
	afx_msg void OnChangeLanguage();
	afx_msg void OnConnectToUnit();
	afx_msg void OnDisconnectFromUnit();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg void OnMPIUpdate(CCmdUI* pCmdUI);
	afx_msg void OnMPICommand(UINT nID);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__0F3B4BCA_5EBF_40DB_BE97_F4B66422A78C__INCLUDED_)
