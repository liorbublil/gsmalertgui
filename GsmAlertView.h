// GsmAlertView.h : interface of the CGsmAlertView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GSM_ALERT_VIEW_H__0A793D21_DF5D_433F_9BF2_22A4DA62557E__INCLUDED_)
#define AFX_GSM_ALERT_VIEW_H__0A793D21_DF5D_433F_9BF2_22A4DA62557E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CGsmAlertView : public CFormView
{
protected: // create from serialization only
	CGsmAlertView();
	DECLARE_DYNCREATE(CGsmAlertView)

public:
	//{{AFX_DATA(CGsmAlertView)
	enum { IDD = IDD_GSM_ALERT_FORM };
	//}}AFX_DATA

// Attributes
public:
	CGsmAlertDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmAlertView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGsmAlertView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGsmAlertView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GsmAlertView.cpp
inline CGsmAlertDoc* CGsmAlertView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSM_ALERT_VIEW_H__0A793D21_DF5D_433F_9BF2_22A4DA62557E__INCLUDED_)
