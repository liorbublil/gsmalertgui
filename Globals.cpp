#include "stdafx.h"
#include "Afxtempl.h"
#include "resource.h"
#include "ComThread.h"
//#include <odbcinst.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "Globals.h"

bool SetComboBoxFromItemData(CComboBox &a_cmbToSet,DWORD a_iItemData)
{
	int l_iIndex;
	DWORD hh;
	for(l_iIndex=0;l_iIndex<a_cmbToSet.GetCount();++l_iIndex)
	{
		if((hh=a_cmbToSet.GetItemData(l_iIndex))==a_iItemData)
		{
			a_cmbToSet.SetCurSel(l_iIndex);
			return true;
		}
	}
	return false;
}

bool SetComboBoxFromItemText(CComboBox &a_cmbToSet, CString a_strItemText)
{
	int l_iIndex;
	CString hh;
	for (l_iIndex = 0;l_iIndex<a_cmbToSet.GetCount();++l_iIndex)
	{
		a_cmbToSet.GetLBText(l_iIndex, hh);
		if (hh == a_strItemText)
		{
			a_cmbToSet.SetCurSel(l_iIndex);
			return true;
		}
	}
	return false;
}

int GetNumAsIntFromString(CString &a_strNum)
{
	int l_iRespond=0;
	for(int l_i=0;l_i<a_strNum.GetLength();++l_i)
	{
		if((a_strNum.GetAt(l_i)>='0')&&(a_strNum.GetAt(l_i)<='9'))
		{
			l_iRespond*=10;
			l_iRespond+=a_strNum.GetAt(l_i)-'0';
		}
		else
		{
			return l_iRespond;
		}
	}
	return l_iRespond;
}

CString GetNumAsStringFromString(CString &a_strNum)
{
	CString l_strRespond;
	for(int l_i=0;l_i<a_strNum.GetLength();++l_i)
	{
		if((a_strNum.GetAt(l_i)>='0')&&(a_strNum.GetAt(l_i)<='9'))
		{
			l_strRespond+=a_strNum.GetAt(l_i);
		}
		else
		{
			return l_strRespond;
		}
	}
	return l_strRespond;
}

CString GetStringFromInvertedCommasString(CString &a_strNum)
{
	int l_iStartAt=a_strNum.Find("\"");
	int l_iEndAt=a_strNum.Find("\"",l_iStartAt+1);
	if(l_iStartAt==-1)
	{
		return "";
	}
	if(l_iEndAt==-1)
	{
		return a_strNum.Mid(l_iStartAt+1);
	}
	return a_strNum.Mid(l_iStartAt+1,l_iEndAt-l_iStartAt-1);
}

bool IsPhoneNumOK(CString a_strPhoneNum)
{
	if(a_strPhoneNum=="")
	{
		return false;
	}

	int l_i;
	if(a_strPhoneNum[0]=='+')
	{
		l_i=1;
	}
	else
	{
		l_i=0;
	}
	for(;l_i<a_strPhoneNum.GetLength();++l_i)
	{
		if((a_strPhoneNum.GetAt(l_i)<'0')||(a_strPhoneNum.GetAt(l_i)>'9'))
		{
			return false;
		}
	}
	return true;
}

bool IsNumeric(CString a_strNum,bool a_bNegativeAlowed)
{
	bool l_bFoundDot=false;
	if(a_strNum=="")
	{
		return false;
	}
	int l_i;
	if((a_bNegativeAlowed==true) &&(a_strNum[0]=='-'))
	{
		l_i=1;
	}
	else
	{
		l_i=0;
	}

	for(;l_i<a_strNum.GetLength();++l_i)
	{
		if((a_strNum.GetAt(l_i)<'0')||(a_strNum.GetAt(l_i)>'9'))
		{
			if(a_strNum.GetAt(l_i)=='.')
			{
				if(!l_bFoundDot)
				{
					l_bFoundDot=true;
					continue;
				}

			}
			return false;
		}
	}
	return true;
}

bool IsDate(CString a_strDate)
{
	//Look for the ":" in the string
	int l_iCulomnPlace=a_strDate.Find(":");
	if(l_iCulomnPlace==-1)
	{
		return false;
	}

	//Check if the format is XX:XX
	if(((a_strDate.Mid(l_iCulomnPlace+1)).GetLength()!=2)||((a_strDate.Left(l_iCulomnPlace)).GetLength()!=2))
	{
		return false;
	}

	if(!IsNumeric((a_strDate.Mid(l_iCulomnPlace+1)))||(!IsNumeric(a_strDate.Left(l_iCulomnPlace))))
	{
		return false;
	}
	if((atoi(a_strDate.Mid(l_iCulomnPlace+1))>59)||(atoi(a_strDate.Mid(l_iCulomnPlace+1))<0)||(atoi(a_strDate.Left(l_iCulomnPlace))>23)||(atoi(a_strDate.Left(l_iCulomnPlace))<0))
	{
		return false;
	}
	
	return true;
}

//The functions assume that the date string is OK (can be checked with IsDate
void GetDateVars(CString a_strDate,int &a_iHoure,int &a_iMinute)
{
	if(a_strDate=="")
	{
		a_iHoure=0;
		a_iMinute=0;
		return;
	}
	//Look for the ":" in the string
	int l_iCulomnPlace=a_strDate.Find(":");

	a_iHoure=atoi(LPCTSTR(a_strDate.Left(l_iCulomnPlace)));
	a_iMinute=atoi(LPCTSTR(a_strDate.Mid(l_iCulomnPlace+1)));
}

void ChangeListItemIcon(CListCtrl &a_lst,int a_iIndex,int a_iIcon,int l_iColNum)
{
	CStringArray l_arr;
	int l_iItemData;
	int l_i;

	l_iItemData=a_lst.GetItemData(a_iIndex);

	for(l_i=0;l_i<=l_iColNum;++l_i)
	{
		l_arr.InsertAt(l_i,a_lst.GetItemText(a_iIndex,l_i));
	}
	a_lst.DeleteItem(a_iIndex);
	int l_iIndex=a_lst.InsertItem(a_iIndex,l_arr[0],a_iIcon);
	a_lst.SetItemData(l_iIndex,l_iItemData);
	for(l_i=1;l_i<=l_iColNum;++l_i)
	{
		a_lst.SetItemText(l_iIndex,l_i,l_arr[l_i]);
	}
}

// Get the name of the Excel-ODBC driver
CString GetExcelDriver()
{/*
	char szBuf[2001];
	WORD cbBufMax = 2000;
	WORD cbBufOut;
	char *pszBuf = szBuf;
	CString sDriver;

	// Get the names of the installed drivers ("odbcinst.h" has to be included )
   if(!SQLGetInstalledDrivers(szBuf,cbBufMax,& cbBufOut))
		return "";
	
	// Search for the driver...
	do
	{
		if( strstr( pszBuf, "Excel" ) != 0 )
		{
			// Found !
			sDriver = CString( pszBuf );
			break;
		}
		pszBuf = strchr( pszBuf, '\0' ) + 1;
	}
	while( pszBuf[1] != '\0' );

	return sDriver;
	*/
	return CString("");
}

// Get the name of the Excel-ODBC driver
CString GetAccessDriver()
{/*
	char szBuf[2001];
	WORD cbBufMax = 2000;
	WORD cbBufOut;
	char *pszBuf = szBuf;
	CString sDriver;

	// Get the names of the installed drivers ("odbcinst.h" has to be included )
   if(!SQLGetInstalledDrivers(szBuf,cbBufMax,& cbBufOut))
		return "";
	
	// Search for the driver...
	do
	{
		if( strstr( pszBuf, "Access" ) != 0 )
		{
			// Found !
			sDriver = CString( pszBuf );
			break;
		}
		pszBuf = strchr( pszBuf, '\0' ) + 1;
	}
	while( pszBuf[1] != '\0' );

	return sDriver;
	*/
	return CString("");
}

int FindInString(const CString &a_str1,const char *a_str2,int a_iStartAt)
{
	int l_iStrLen1=a_str1.GetLength();
	int l_iStrLen2=strlen(a_str2);
	int l_i2;
	bool l_bFound;
	for(int l_i=a_iStartAt;l_i<l_iStrLen1;++l_i)
	{
		if(a_str1[l_i]==a_str2[0])
		{
			l_bFound=true;
			for(l_i2=1;l_i2<l_iStrLen2;++l_i2)
			{
				if(l_i+l_i2>=l_iStrLen1)
				{
					return -1;
				}
				if(a_str1[l_i+l_i2]!=a_str2[l_i2])
				{
					l_bFound=false;
					break;
				}
			}
			if(l_bFound)
			{
				return l_i;
			}
		}
	}
	return -1;
}

int GetTimedStrAsSec(CString a_strTime)
{
    int l_iSec = 0;
    CString l_strTmp = a_strTime;
    if(l_strTmp=="")    //Don't allow empty string
	{
        return -1;
	}
    //Get the hours from the string
    int l_iEnd = l_strTmp.Find("H");
    if(l_iEnd==-1)
	{
        l_iEnd = l_strTmp.Find("h");
	}
    //If found house string
    if(l_iEnd!=-1)
	{
        if(IsNumeric(l_strTmp.Left(l_iEnd))==false)
		{
            return -1;
		}
        l_iSec = l_iSec + (atoi(LPCTSTR(l_strTmp.Left(l_iEnd))) * 3600);
        l_strTmp = l_strTmp.Mid(l_iEnd+1);
	}
    //Get the minutes from the string
    l_iEnd = l_strTmp.Find("M");
    if(l_iEnd==-1)
	{
        l_iEnd = l_strTmp.Find("m");
	}
    //If found minutes string
    if(l_iEnd!=-1)
	{
        if(IsNumeric(l_strTmp.Left(l_iEnd))==false)
		{
            return -1;
		}
        l_iSec = l_iSec + (atoi(LPCTSTR(l_strTmp.Left(l_iEnd))) * 60);
        l_strTmp = l_strTmp.Mid(l_iEnd+1);
	}

    //Get the seconds from the string
    l_iEnd = l_strTmp.Find("S");
    if(l_iEnd==-1)
	{
        l_iEnd = l_strTmp.Find("s");
	}
    //If found seconds string
    if(l_iEnd!=-1)
	{
        if(IsNumeric(l_strTmp.Left(l_iEnd))==false)
		{
            return -1;
		}
        l_iSec = l_iSec + (atoi(LPCTSTR(l_strTmp.Left(l_iEnd))));
        l_strTmp = l_strTmp.Mid(l_iEnd+1);
	}
    if((l_iSec==0) || (l_strTmp!=""))
	{
        return -1;
	}

    return l_iSec;
}

CString GetSecAsTimedStr(int a_iSec)
{
    CString l_strTime = "";
	CString l_strTmp;
    if ((a_iSec / 3600) != 0)	//Get the hours
	{
		l_strTmp.Format("%dH",(a_iSec / 3600));
        l_strTime = l_strTime + l_strTmp;
	}

    a_iSec = a_iSec % 3600;
    if ((a_iSec / 60) != 0)   //Get the minutes
	{
		l_strTmp.Format("%dM" , (a_iSec / 60));
        l_strTime = l_strTime + l_strTmp;
	}

    a_iSec = a_iSec % 60;
    if(a_iSec != 0)	//Get the minutes
	{
		l_strTmp.Format("%dS" , (a_iSec));
        l_strTime = l_strTime + l_strTmp;
	}

    if(l_strTime=="")
	{
        l_strTime = "0S";
	}
        
    return l_strTime;
}

void CopyStringToClipbard(CString a_strData,HWND a_Owner)
{
	LPTSTR  lptstrCopy;
	if (!OpenClipboard(a_Owner))
	{
		AfxMessageBox(_T("Cannot open the Clipboard"));
		return;
	}
	// Remove the current Clipboard contents
	if (!EmptyClipboard())
	{
		AfxMessageBox(_T("Cannot empty the Clipboard"));
		return;
	}
	a_strData = CString(START_OF_MSG_COMMAND) + a_strData;
	// Get the currently selected data
	unsigned int strSize = a_strData.GetLength();//get your string lenght
	HGLOBAL hGlob = GlobalAlloc(GMEM_MOVEABLE, (strSize + 1) * sizeof(TCHAR));//allocate the memory object with GMEM_MOVEABLE 
	lptstrCopy = (LPTSTR)GlobalLock(hGlob);
	memcpy(lptstrCopy, a_strData.GetBuffer(), strSize * sizeof(TCHAR)); //copy the text data
	lptstrCopy[strSize] = (TCHAR)0;//the null terminator
	GlobalUnlock(hGlob);

	// For the appropriate data formats...
	if (::SetClipboardData(CF_TEXT, hGlob) == NULL)
	{
		CString msg;
		msg.Format(_T("Unable to set Clipboard data, error: %d"), GetLastError());
		AfxMessageBox(msg);
		CloseClipboard();
		GlobalFree(hGlob);
		return;
	}
	CloseClipboard();
}