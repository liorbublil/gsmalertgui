#if !defined(AFX_LEVELEVENTS_H__CA1B3AF3_AFC6_427B_9285_A6DD5E1B424E__INCLUDED_)
#define AFX_LEVELEVENTS_H__CA1B3AF3_AFC6_427B_9285_A6DD5E1B424E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LevelEvents.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLevelEvents form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CLevelEvents : public CFormView
{
protected:
	CLevelEvents();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CLevelEvents)

// Form Data
public:
	//{{AFX_DATA(CLevelEvents)
	enum { IDD = IDD_LEVEL_EVENTS_FORM };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLevelEvents)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CLevelEvents();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(CLevelEvents)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEVELEVENTS_H__CA1B3AF3_AFC6_427B_9285_A6DD5E1B424E__INCLUDED_)
