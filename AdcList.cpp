// AdcList.cpp: implementation of the CAdcList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gsmalert.h"
#include "AdcList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAdcListElement::CAdcListElement()
{
}

CAdcListElement::~CAdcListElement()
{
}

void CAdcListElement::operator=(const CAdcListElement& a_AdcListElement)
{
	m_bInUnit = a_AdcListElement.m_bInUnit;
	m_iSensorNumber = a_AdcListElement.m_iSensorNumber;
	m_strSensorName = a_AdcListElement.m_strSensorName;
	m_strLut = a_AdcListElement.m_strLut;
}

CAdcList::CAdcList()
{

}

void CAdcList::DeleteAllList()
{
	CAdcListElement *l_pElementToRemove;

	while (!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove = RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}


CAdcList::~CAdcList()
{
	DeleteAllList();
}

void CAdcList::Serialize(CArchive& archive)
{
	CAdcListElement *l_pListElement;
	//CObject::Serialize( archive );

	// now do the stuff for our specific class
	if (archive.IsStoring())
	{
		POSITION l_Position;

		archive << GetCount();

		l_Position = GetHeadPosition();
		while (l_Position != NULL)
		{
			l_pListElement = GetNext(l_Position);
			archive << l_pListElement->m_iSensorNumber << l_pListElement->m_strSensorName<< l_pListElement->m_strLut << l_pListElement->m_bInUnit;
		}
	}
	else
	{
		int l_iListCount;
		DeleteAllList();

		archive >> l_iListCount;
		for (int l_iIndex = 0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement = new CAdcListElement;
			archive >> l_pListElement->m_iSensorNumber >> l_pListElement->m_strSensorName>> l_pListElement->m_strLut >> l_pListElement->m_bInUnit;
			AddTail(l_pListElement);
		}
	}
}

void CAdcList::AddItemSorted(int a_iSensorNum, CString a_strSensorName, CString a_strLut, bool a_bInUnit, bool a_bNoSearch)
{
	CAdcListElement *l_pListElement;
	bool l_bNew = false;
	if (a_bNoSearch)
	{
		l_bNew = true;
		l_pListElement = new CAdcListElement;
	}
	else
	{
		if ((l_pListElement = GetItemBySensorNum(a_iSensorNum)) == NULL)
		{
			l_bNew = true;
			l_pListElement = new CAdcListElement;
		}
	}

	l_pListElement->m_iSensorNumber = a_iSensorNum;
	l_pListElement->m_strSensorName = a_strSensorName;
	if(a_strLut!=CString(""))	//If the look up table is empty, preserve the old one
	{
		l_pListElement->m_strLut = a_strLut;
	}
	l_pListElement->m_bInUnit = a_bInUnit;

	if (l_bNew)
	{
		AddTail(l_pListElement);
	}
}

void CAdcList::DeleteItem(int a_iSensorNum)
{
	POSITION l_Position;
	CAdcListElement *l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetAt(l_Position);
		if (l_pListElement->m_iSensorNumber == a_iSensorNum)
		{
			RemoveAt(l_Position);
			delete l_pListElement;
			return;
		}
		GetNext(l_Position);
	}
}

CAdcListElement* CAdcList::GetItemBySensorNum(int a_iSensorNum)
{
	POSITION l_Position;
	CAdcListElement*l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_iSensorNumber == a_iSensorNum)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CAdcListElement* CAdcList::GetItemByPos(int a_iPos)
{
	POSITION l_Position;
	CAdcListElement*l_pListElement=NULL;
	int l_iPos;
	
	for (l_Position = GetHeadPosition(), l_iPos = 0;(l_iPos < a_iPos) &&  (l_Position != NULL);++l_iPos)
	{
		l_pListElement = GetNext(l_Position);
	}
	return l_pListElement;
}

int CAdcList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	CAdcListElement *l_pListElement;
	int l_iCounter = 0;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_bInUnit == false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(CAdcList, CObList, 0)
