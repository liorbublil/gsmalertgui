// LevelEventList.cpp: implementation of the CLevelEventList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "gsmalert.h"
#include "LevelEventList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CLevelEventListElement::CLevelEventListElement()
{
	m_odtRepeatAlert.SetTime(0,0,0);
	m_odtHighDelay.SetTime(0,0,0);
	m_odtLowDelay.SetTime(0,0,0);
}

CLevelEventListElement::~CLevelEventListElement()
{
}

void CLevelEventListElement::operator=(const CLevelEventListElement& a_LevelEventListElement)
{
	m_iIndex=a_LevelEventListElement.m_iIndex;
	m_iSensorType=a_LevelEventListElement.m_iSensorType;
	m_strSensorNum=a_LevelEventListElement.m_strSensorNum;
	m_strSensorId=a_LevelEventListElement.m_strSensorId;
	m_dLowHys=a_LevelEventListElement.m_dLowHys;
	m_dHighHys=a_LevelEventListElement.m_dHighHys;
	m_strLowMsg=a_LevelEventListElement.m_strLowMsg;
	m_strHighMsg=a_LevelEventListElement.m_strHighMsg;
	m_iNormalRange=a_LevelEventListElement.m_iNormalRange;
	m_odtRepeatAlert=a_LevelEventListElement.m_odtRepeatAlert;
	m_bInUnit=a_LevelEventListElement.m_bInUnit;
}

CLevelEventList::CLevelEventList()
{

}

void CLevelEventList::DeleteAllList()
{
	CLevelEventListElement *l_pElementToRemove;
	
	while(!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove=RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}


CLevelEventList::~CLevelEventList()
{
	DeleteAllList();
}

void CLevelEventList::Serialize( CArchive& archive )
{
	CLevelEventListElement *l_pListElement;
    //CObject::Serialize( archive );
	
    // now do the stuff for our specific class
    if( archive.IsStoring() )
	{
		POSITION l_Position;
		
        archive << GetCount();
		
		l_Position=GetHeadPosition();
		while(l_Position!=NULL)
		{
			l_pListElement=GetNext(l_Position);
			archive << l_pListElement->m_iIndex<< l_pListElement->m_iSensorType<<l_pListElement->m_strSensorNum<<  l_pListElement->m_strSensorId<< l_pListElement->m_dLowHys<< l_pListElement->m_dHighHys << l_pListElement->m_strLowMsg << l_pListElement->m_strHighMsg << l_pListElement->m_iNormalRange << l_pListElement->m_odtRepeatAlert<< l_pListElement->m_bInUnit;
		}
	}
    else
	{
		int l_iListCount;
		int l_iBool;
		DeleteAllList();
		
		archive >> l_iListCount;
		for(int l_iIndex=0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement=new CLevelEventListElement ;
			archive >> l_pListElement->m_iIndex>> l_pListElement->m_iSensorType>>l_pListElement->m_strSensorNum>>  l_pListElement->m_strSensorId>> l_pListElement->m_dLowHys>> l_pListElement->m_dHighHys >> l_pListElement->m_strLowMsg >> l_pListElement->m_strHighMsg >> l_pListElement->m_iNormalRange >> l_pListElement->m_odtRepeatAlert>> l_pListElement->m_bInUnit;
			
			AddTail(l_pListElement);
		}
	}
}

void CLevelEventList::AddItemSorted(int a_iIndex,int a_iSensorType, CString a_strSensorNum, CString a_strSensorId,double a_dLowHys,double a_dHighHys,CString a_strLowMsg,CString a_strHighMsg,int a_iNormalRange,COleDateTime a_odtRepeatAlert,COleDateTime a_odtLowDelay,COleDateTime a_odtHighDelay,CString a_strCloudAlarmKey, CString a_strLevelEventKey, bool a_bInUnit)
{	
	CLevelEventListElement *l_pListElement;
	bool l_bNew=false;
	if((l_pListElement=GetItemByIndex(a_iIndex))==NULL)
	{
		l_bNew=true;
		l_pListElement=new CLevelEventListElement;
	}
	
	l_pListElement->m_iIndex=a_iIndex;
	l_pListElement->m_iSensorType=a_iSensorType;
	l_pListElement->m_strSensorNum=a_strSensorNum;
	l_pListElement->m_strSensorId=a_strSensorId;
	l_pListElement->m_dLowHys=a_dLowHys;
	l_pListElement->m_dHighHys=a_dHighHys;
	l_pListElement->m_strLowMsg=a_strLowMsg;
	l_pListElement->m_strHighMsg=a_strHighMsg;
	l_pListElement->m_iNormalRange=a_iNormalRange;
	l_pListElement->m_odtRepeatAlert=a_odtRepeatAlert;
	l_pListElement->m_odtLowDelay=a_odtLowDelay;
	l_pListElement->m_odtHighDelay=a_odtHighDelay;
	l_pListElement->m_strCloudAlarmKey = a_strCloudAlarmKey;
	l_pListElement->m_strLevelEventKey = a_strLevelEventKey;
	l_pListElement->m_bInUnit=a_bInUnit;
	if(l_bNew)
	{
		AddTail(l_pListElement);
	}
}

void CLevelEventList::DeleteItem(int a_iIndex)
{
	POSITION l_Position;
	CLevelEventListElement *l_pListElement, *l_pListElementCopy;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iIndex==a_iIndex)
		{
			//Copy all the next items reversed
			while(l_Position!=NULL)
			{
				l_pListElementCopy=GetNext(l_Position);
				*l_pListElement=*l_pListElementCopy;
				if(l_pListElement->m_iIndex>a_iIndex)
				{
					--l_pListElement->m_iIndex;
				}
				l_pListElement=l_pListElementCopy;
			}
			l_pListElement = RemoveTail();
			delete l_pListElement;
			return;
		}
	}
}

CLevelEventListElement* CLevelEventList::GetItemByIndex(int a_iIndex)
{
	POSITION l_Position;
	CLevelEventListElement*l_pListElement;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iIndex==a_iIndex)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

int CLevelEventList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	CLevelEventListElement *l_pListElement;
	int l_iCounter=0;
	
	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_bInUnit==false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(CLevelEventList,  CObList, 0)
