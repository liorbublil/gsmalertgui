; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CSpPhoneListView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "gto.h"
LastPage=0

ClassCount=15
Class1=CChangeLanguage
Class2=CChannelImeiView
Class3=CGtoApp
Class4=CAboutDlg
Class5=CGtoDoc
Class6=CGtoView
Class7=CInputDialog
Class8=CLogView
Class9=CMainFrame
Class10=CMiscSettingsView
Class11=CMPIChildFrame
Class12=CPhoneListView
Class13=CSearchModemDlg
Class14=CSpPhoneListView
Class15=CStatusView

ResourceCount=15
Resource1=IDR_MPI_BAR (English (U.S.))
Resource2=IDD_SEARCH_MODEM_DIALOG (English (U.S.))
Resource3=IDD_INPUTBOX_DIALOG (English (U.S.))
Resource4=IDD_STATUS_FORM (English (U.S.))
Resource5=IDR_MAINFRAME (English (U.S.))
Resource6=IDD_CHANNEL_IMEI_FORM (English (U.S.))
Resource7=IDD_LOG_FORM (English (U.S.))
Resource8=IDD_PHONE_LIST_FORM (English (U.S.))
Resource9=IDD_GSM_ALERT_FORM (English (U.S.))
Resource10=IDD_GTO_FORM (English (U.S.))
Resource11=IDD_ABOUTBOX (English (U.S.))
Resource12=IDD_MISC_SETTINGS_FORM (English (U.S.))
Resource13=IDR_SETTINGS_TABS (English (U.S.))
Resource14=IDD_SP_PHONE_LIST_FORM (English (U.S.))
Resource15=IDD_CHANGE_LANG_DIAL

[CLS:CChangeLanguage]
Type=0
BaseClass=CDialog
HeaderFile=ChangeLanguage.h
ImplementationFile=ChangeLanguage.cpp

[CLS:CChannelImeiView]
Type=0
BaseClass=CFormView
HeaderFile=channelimeiview.h
ImplementationFile=ChannelImeiView.cpp

[CLS:CGtoApp]
Type=0
BaseClass=CWinApp
HeaderFile=gto.h
ImplementationFile=gto.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=gto.cpp
ImplementationFile=gto.cpp

[CLS:CGtoDoc]
Type=0
BaseClass=CDocument
HeaderFile=gtoDoc.h
ImplementationFile=gtoDoc.cpp

[CLS:CGtoView]
Type=0
BaseClass=CFormView
HeaderFile=gtoView.h
ImplementationFile=gtoView.cpp

[CLS:CInputDialog]
Type=0
BaseClass=CDialog
HeaderFile=InputDialog.h
ImplementationFile=InputDialog.cpp

[CLS:CLogView]
Type=0
BaseClass=CFormView
HeaderFile=LogView.h
ImplementationFile=LogView.cpp
Filter=D
VirtualFilter=VWC
LastObject=IDC_CMD_DEL_LOG

[CLS:CMainFrame]
Type=0
BaseClass=CMDIFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CMiscSettingsView]
Type=0
BaseClass=CFormView
HeaderFile=MiscSettingsview.h
ImplementationFile=MiscSettingsView.cpp
Filter=D
VirtualFilter=VWC
LastObject=IDC_CMD_GET_BAND_SELECT

[CLS:CMPIChildFrame]
Type=0
BaseClass=CMDIChildWnd
HeaderFile=MPIChildFrame.h
ImplementationFile=MPIChildFrame.cpp

[CLS:CPhoneListView]
Type=0
BaseClass=CFormView
HeaderFile=phonelistview.h
ImplementationFile=phonelistview.cpp
Filter=D
VirtualFilter=VWC
LastObject=IDC_TXT_PHONE_NAME

[CLS:CSearchModemDlg]
Type=0
BaseClass=CDialog
HeaderFile=SearchModemDlg.h
ImplementationFile=SearchModemDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_TXT_PASSWORD

[CLS:CSpPhoneListView]
Type=0
BaseClass=CFormView
HeaderFile=SpPhoneListView.h
ImplementationFile=SpPhoneListView.cpp
Filter=D
VirtualFilter=VWC
LastObject=IDC_TXT_ZONE_DELAY

[CLS:CStatusView]
Type=0
BaseClass=CFormView
HeaderFile=StatusView.h
ImplementationFile=StatusView.cpp

[DLG:IDD_CHANGE_LANG_DIAL]
Type=1
Class=CChangeLanguage
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_CMB_LANGUAGES,combobox,1344340227

[DLG:IDD_CHANNEL_IMEI_FORM]
Type=1
Class=CChannelImeiView

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg

[DLG:IDD_GTO_FORM]
Type=1
Class=CGtoView

[DLG:IDD_INPUTBOX_DIALOG]
Type=1
Class=CInputDialog

[DLG:IDD_LOG_FORM]
Type=1
Class=CLogView

[DLG:IDD_MISC_SETTINGS_FORM]
Type=1
Class=CMiscSettingsView

[DLG:IDD_PHONE_LIST_FORM]
Type=1
Class=CPhoneListView

[DLG:IDD_SEARCH_MODEM_DIALOG]
Type=1
Class=CSearchModemDlg

[DLG:IDD_SP_PHONE_LIST_FORM]
Type=1
Class=CSpPhoneListView

[DLG:IDD_STATUS_FORM]
Type=1
Class=CStatusView

[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_LOAD_SEARCH_MODEM_DLG
Command6=ID_CONNECT_TO_UNIT
Command7=ID_DISCONNECT_FROM_UNIT
Command8=ID_CHANGE_LANGUAGE
CommandCount=8

[TB:IDR_MPI_BAR (English (U.S.))]
Type=1
Class=?
Command1=ID_MPI_SETTINGS
Command2=ID_MPI_LOG
CommandCount=2

[TB:IDR_SETTINGS_TABS (English (U.S.))]
Type=1
Class=?
Command1=ID_TAB_PHONE_LIST
Command2=ID_TAB_SP_PHONE_LIST
Command3=ID_TAB_MISC_SETTINGS
CommandCount=3

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_AUTO_OPEN_LAST_FILE
Command6=ID_LOAD_SEARCH_MODEM_DLG
Command7=ID_APP_EXIT
Command8=ID_VIEW_TOOLBAR
Command9=ID_VIEW_STATUS_BAR
Command10=ID_APP_ABOUT
CommandCount=10

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_STATIC,static,1342177294
Control2=IDC_LBL_VERSION,static,1342308481
Control3=IDC_STATIC,static,1342308353
Control4=IDOK,button,1342373889

[DLG:IDD_GTO_FORM (English (U.S.))]
Type=1
Class=?
ControlCount=0

[DLG:IDD_INPUTBOX_DIALOG (English (U.S.))]
Type=1
Class=?
ControlCount=4
Control1=IDC_INPUT_EDIT,edit,1350631552
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_MESSAGE,static,1342308352

[DLG:IDD_CHANNEL_IMEI_FORM (English (U.S.))]
Type=1
Class=?
ControlCount=10
Control1=IDC_TXT_INDEX,edit,1350639744
Control2=IDC_TXT_PHONE_NUM,edit,1350639744
Control3=IDC_CMD_SET,button,1342242816
Control4=IDC_CMD_REFRESH,button,1342242816
Control5=IDC_TXT_IMEI,edit,1350641792
Control6=IDC_LST_IMEI_SET,SysListView32,1350631429
Control7=IDC_LBL_INDEX,static,1342308352
Control8=IDC_LBL_PHONE_NUM,static,1342308352
Control9=IDC_LBL_IMEI,static,1342308352
Control10=IDC_FRAM_IMEI_SN,button,1342177287

[DLG:IDD_PHONE_LIST_FORM (English (U.S.))]
Type=1
Class=CPhoneListView
ControlCount=21
Control1=IDC_TXT_PHONE_NUM,edit,1350631552
Control2=IDC_GET_PHONE_NUM,button,1342254848
Control3=IDC_CMD_ADD,button,1342242816
Control4=IDC_CMD_DOWNLOAD_ALL_PHONES_TO_UNIT,button,1342254848
Control5=IDC_CMD_DELETE_PHONE_NUM,button,1342254848
Control6=IDC_CMD_STOP,button,1476472576
Control7=IDC_CMD_EXCEL_EXPORT,button,1342254848
Control8=IDC_CMD_EXCEL_IMPORT,button,1342254848
Control9=IDC_LST_PHONE_NUM,SysListView32,1350631437
Control10=IDC_FRAM_PHONE_NUM,button,1342177287
Control11=IDC_LBL_PHONE_NUM,static,1342308352
Control12=IDC_PRGS_PHONE_DOWNLOAD,msctls_progress32,1082130432
Control13=IDC_LBL_USED_PHONE,static,1342308352
Control14=IDC_CMD_QUICK_COMPARE,button,1342254848
Control15=IDC_TXT_PHONE_NAME,edit,1350631552
Control16=IDC_LBL_PHONE_NAME,static,1342308352
Control17=IDC_CMD_SEARCH_PHONE_NUM,button,1342242816
Control18=IDC_CMD_SEARCH_PHONE_NAME,button,1342242816
Control19=IDC_CMD_REPLACE,button,1342242816
Control20=IDC_LBL_TYPE,static,1342308352
Control21=IDC_CMB_TYPE,combobox,1344339971

[DLG:IDD_MISC_SETTINGS_FORM (English (U.S.))]
Type=1
Class=CMiscSettingsView
ControlCount=30
Control1=IDC_DTP_UNIT_TIME,SysDateTimePick32,1342242857
Control2=IDC_FRAM_UNIT_TIME,button,1342177287
Control3=IDC_CMD_SET_UNIT_TIME,button,1342242816
Control4=IDC_CMD_GET_PC_TIME,button,1342242816
Control5=IDC_FRAM_UNIT_PASS,button,1342177287
Control6=IDC_CMD_SET_UNIT_PASS,button,1342251008
Control7=IDC_TXT_CONFIRM_PASS,edit,1350631584
Control8=IDC_LBL_CONFIRM_PASS,static,1342308352
Control9=IDC_TXT_NEW_PASS,edit,1350631584
Control10=IDC_LBL_NEW_PASS,static,1342308352
Control11=IDC_FRAM_UNIT_NAME,button,1342177287
Control12=IDC_CMD_SET_UNIT_NAME,button,1342251008
Control13=IDC_TXT_UNIT_NAME,edit,1350631552
Control14=IDC_CMD_GET_UNIT_NAME,button,1342251008
Control15=IDC_FRAM_GATE_DELAY,button,1342177287
Control16=IDC_CMD_SET_GATE_DELAY,button,1342251008
Control17=IDC_TXT_GATE_DELAY,edit,1350639744
Control18=IDC_CMD_GET_GATE_DELAY,button,1342251008
Control19=IDC_TXT_PASSWORD,edit,1350631584
Control20=IDC_LBL_PASSWORD,static,1342308352
Control21=IDC_FRAM_SELF_CU_SMS,button,1342177287
Control22=IDC_CMD_SELF_CU_SMS_SET,button,1342251008
Control23=IDC_TXT_SELF_PHONE_NUM,edit,1350639744
Control24=IDC_CMD_SELF_CU_SMS_GET,button,1342251008
Control25=IDC_CHK_SELF_CU_SMS,button,1342242819
Control26=IDC_LBL_SELF_PHONE_NUM,static,1342308352
Control27=IDC_FRAM_BAND_SELECT,button,1342177287
Control28=IDC_CMD_SET_BAND_SELECT,button,1342251008
Control29=IDC_CMD_GET_BAND_SELECT,button,1342251008
Control30=IDC_CMB_BAND_SELECT,combobox,1344339971

[DLG:IDD_STATUS_FORM (English (U.S.))]
Type=1
Class=?
ControlCount=5
Control1=IDC_CMD_REFRESH,button,1342242816
Control2=IDC_CMD_TURN_OFF,button,1342242816
Control3=IDC_CMD_RESTART,button,1342242816
Control4=IDC_LST_STATUS,SysListView32,1350631429
Control5=IDC_FRAM_CHANNEL_STATUS,button,1342177287

[DLG:IDD_SEARCH_MODEM_DIALOG (English (U.S.))]
Type=1
Class=CSearchModemDlg
ControlCount=10
Control1=IDC_COM_PORTS,combobox,1344339971
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_CHK_USE_MODEM,button,1342242819
Control5=IDC_LBL_UNIT_PHONE_NUMBER,static,1342308352
Control6=IDC_TXT_UNIT_PHONE_NUMBER,edit,1350631552
Control7=IDC_LBL_COM_PORTS,static,1342308352
Control8=IDC_TXT_PASSWORD,edit,1350631584
Control9=IDC_LBL_PASSWORD,static,1342308352
Control10=IDC_CHK_SAVE_PASS,button,1342242819

[DLG:IDD_LOG_FORM (English (U.S.))]
Type=1
Class=CLogView
ControlCount=10
Control1=IDC_LST_LOG,SysListView32,1350631429
Control2=IDC_LOG,static,1342308352
Control3=IDC_CMD_GET_LOG,button,1342251008
Control4=IDC_PRGS_LOG_DOWNLOAD,msctls_progress32,1082130432
Control5=IDC_CMD_STOP,button,1476468736
Control6=IDC_TXT_LAST_LOG_ENTRIES,edit,1350639744
Control7=IDC_LBL_LAST_LOG_ENTRIES,static,1342308352
Control8=IDC_CMD_LAST_LOG_ENTRIES,button,1342251008
Control9=IDC_CMD_EXPORT_LOG,button,1342251008
Control10=IDC_CMD_DEL_LOG,button,1342251008

[DLG:IDD_SP_PHONE_LIST_FORM (English (U.S.))]
Type=1
Class=CSpPhoneListView
ControlCount=22
Control1=IDC_TXT_LOC_IN_MEM,edit,1350631552
Control2=IDC_TXT_ZONE_NAME,edit,1350631552
Control3=IDC_CMD_SET,button,1342242816
Control4=IDC_CMD_DELETE_SP_PHONE_NUM,button,1342254848
Control5=IDC_GET_SP_PHONE_NUM,button,1342254848
Control6=IDC_CMD_DOWNLOAD_TO_UNIT,button,1342254848
Control7=IDC_LST_ZONES,SysListView32,1350631429
Control8=IDC_FRAM_SP_PHONE_NUM,button,1342177287
Control9=IDC_LBL_LOC_IN_MEM,static,1342308352
Control10=IDC_LBL_ZONE_NAME,static,1342308352
Control11=IDC_PRGS_PHONE_DOWNLOAD,msctls_progress32,1082130432
Control12=IDC_LBL_USED_SP_PHONE,static,1342308352
Control13=IDC_CMD_STOP,button,1476472576
Control14=IDC_CMD_DOWNLOAD_MODIFIED_SP_PHONES_TO_UNIT,button,1342254848
Control15=IDC_CMD_QUICK_COMPARE,button,1342254848
Control16=IDC_CMD_EXCEL_EXPORT,button,1342254848
Control17=IDC_CMD_EXCEL_IMPORT,button,1342254848
Control18=IDC_TXT_ZONE_DELAY,edit,1350631552
Control19=IDC_LBL_ZONE_DELAY,static,1342308352
Control20=IDC_CMD_SEARCH_SP_INDEX,button,1342242816
Control21=IDC_CMD_SEARCH_SP_PHONE_NUM,button,1342242816
Control22=IDC_CHK_ZONE_ACTIVE,button,1342242851

[DLG:IDD_GSM_ALERT_FORM (English (U.S.))]
Type=1
Class=?
ControlCount=0

