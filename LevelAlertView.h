#if !defined(AFX_LEVELALERTVIEW_H__52B6D346_AE45_41C1_8B62_4850675F4AA2__INCLUDED_)
#define AFX_LEVELALERTVIEW_H__52B6D346_AE45_41C1_8B62_4850675F4AA2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LevelAlertView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CLevelAlertView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

class CLevelAlertView : public CFormView
{
protected:
	CLevelAlertView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CLevelAlertView)
public:
	enum enmLevelEventLastSentCommand
	{
		enmLevelEventLastSentCommandNone,
		enmLevelEventLastSentCommandLevelEventsGet,
		enmLevelEventLastSentCommandLevelEventsSet,
	};	
// Form Data
public:
	//{{AFX_DATA(CLevelAlertView)
	enum { IDD = IDD_LEVEL_ALERT_FORM };
	CDateTimeCtrl	m_dtpHighDelay;
	CDateTimeCtrl	m_dtpLowDelay;
	CButton	m_cmdReplaceLevelEvent;
	CEdit	m_txtLowMsg;
	CEdit	m_txtLowHys;
	CEdit	m_txtHighMsg;
	CEdit	m_txtHighHys;
	CDateTimeCtrl	m_dtpRepeatAlert;
	CComboBox	m_cmbNormalRange;
	CButton	m_cmdSetLevelEvents;
	CButton	m_cmdRemoveLevelEvent;
	CButton	m_cmdGetLevelEvents;
	CButton	m_cmdAddLevelEvent;
	CListCtrl	m_lstSensorsForLevelEvent;
	CListCtrl	m_lstLevelEvent;
	int		m_iNormlRange;
	COleDateTime m_odtRepeatAlert;
	CString	m_strHighHys;
	CString	m_strHighMsg;
	CString	m_strLowHys;
	CString	m_strLowMsg;
	COleDateTime	m_odtLowDelay;
	COleDateTime	m_odtHighDelay;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	CMainFrame *m_pMainFram;
	CString m_strSetString;
	CImageList m_imlLevelEventView;
	enmLevelEventLastSentCommand m_enmLevelEventLastSentCommand;
// Operations
public:
protected:
	void Localize();
	void EnableControls(BOOL a_bEnabled=TRUE);
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmLevelEventLastSentCommand &a_enmLevelEventLastSentCommand);
	int AddItemToLevelEventList(int a_iIndex);
	int AddItemToSensorsForLevelEventList(CString a_strSensorType,int a_iSensorType,CString a_strSensorNum,CString a_strSensorName);
	int GetLevelEventItemByIndex(int a_iIndex);
	void ApplyLevelEventListToScreen();
	void DeleteLevelEventList();
	void SetLevelEventItemImage(int a_iSensorNum,int a_iInUnit);
	CString GetLevelAlertsString();
	void ApplyLevelEvent(int a_iIndex);

// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLevelAlertView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CLevelAlertView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnUpdateSensorsList(WPARAM wParam,LPARAM lParam);
	// Generated message map functions
	//{{AFX_MSG(CLevelAlertView)
	afx_msg void OnCmdAddLevelEvent();
	afx_msg void OnCmdRemoveLevelEvent();
	afx_msg void OnCmdReplaceLevelEvent();
	afx_msg void OnClickLstLevelEvents(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCmdGetLevelEvents();
	afx_msg void OnCmdSetLevelEvents();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeTxtLevelAlertLowHys();
	CEdit m_txtAlarmKey;
	CString m_strAlarmKey;
	afx_msg void OnBnClickedCmdCopyLevelEventsClipboard();
	CEdit m_txtLevelEventKey;
	CString m_strLevelEventKey;
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CLevelAlertView::GetDocument()
{ return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEVELALERTVIEW_H__52B6D346_AE45_41C1_8B62_4850675F4AA2__INCLUDED_)
#include "afxwin.h"
