// GsmAlert.h : main header file for the GSM ALERT application
//

#if !defined(AFX_GSM_ALERT_H__6534954A_8315_4E47_B182_CC37093AB565__INCLUDED_)
#define AFX_GSM_ALERT_H__6534954A_8315_4E47_B182_CC37093AB565__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "PhoneListView.h"
#include "MiscSettingsView.h"
#include "MPIChildFrame.h"
#include "Globals.h"
#include "logger.h"
//#include <afxdb.h>

#define REG_PATH "GsmAlert"
#define REG_SETTINGS_SECTION "Settings"
#define REG_LAST_SET_FILE_ENTRY "LastFile"
#define REG_AUTO_OPEN_LAST_FILE "AutoOpen"
#define REG_LANGUAGE "Language"
#define REG_COM "Com"

#define MAX_PASS_LEN_DEF 8

/////////////////////////////////////////////////////////////////////////////
// CGsmAlertApp:
// See GsmAlert.cpp for the implementation of this class
//

class CGsmAlertApp : public CWinApp
{
public:
	CGsmAlertApp();
	virtual ~CGsmAlertApp();
//Variables
public:
	CString m_strLanguageFilePath;
	int m_iAutoOpenLastFile;
protected:
	CFileLogHandler *m_pDbFileLogHandler;
//Functions
public:
	CString GetAppVersion();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGsmAlertApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL
	
// Implementation
	//{{AFX_MSG(CGsmAlertApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNew();
	afx_msg void OnFileOpen();
	afx_msg void OnAutoOpenLastFile();
	afx_msg void OnUpdateAutoOpenLastFile(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
extern CGsmAlertApp *g_pApp;
extern CLogger g_DbLogger;

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GSM_ALERT_H__6534954A_8315_4E47_B182_CC37093AB565__INCLUDED_)
