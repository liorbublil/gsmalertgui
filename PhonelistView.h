#if !defined(AFX_PHONELIST_VIEW_H__EAEA64ED_38AB_4255_AD53_9A06112368D7__INCLUDED_)
#define AFX_PHONELIST_VIEW_H__EAEA64ED_38AB_4255_AD53_9A06112368D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PhoneListView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPhoneListView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "GsmAlertDoc.h"

class CMainFrame;

class CPhoneListView : public CFormView
{
protected:
	CPhoneListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPhoneListView)
public:
	enum enmPhoneListLastSentCommand
	{
		enmPhoneListLastSentCommandNone,
		enmPhoneListLastSentCommandPhoneNumAsk,
		enmPhoneListLastSentCommandPhoneNumSetAll,
		enmPhoneListLastSentCommandQucikCompare
	};

// Form Data
public:
	//{{AFX_DATA(CPhoneListView)
	enum { IDD = IDD_PHONE_LIST_FORM };
	CComboBox	m_cmbType;
	CStatic	m_lblType;
	CButton	m_cmdReplace;
	CButton	m_cmdSearchPhoneNum;
	CButton	m_cmdSearchPhoneName;
	CEdit	m_txtPhoneName;
	CStatic	m_lblPhoneName;
	CButton	m_cmdExcelImport;
	CButton	m_cmdExcelExport;
	CButton	m_cmdStop;
	CStatic	m_lblUsedPhones;
	CButton	m_cmdDeletePhoneNum;
	CProgressCtrl	m_prgsPhoneDownload;
	CButton	m_cmdDownloadAllPhonesToUnit;
	CButton	m_cmdAdd;
	CButton	m_cmdQuickCompare;
	CEdit	m_txtPhoneNum;
	CStatic	m_lblPhoneNum;
	CButton	m_framPhoneNum;
	CButton	m_cmdGetPhoneNum;
	CListCtrl	m_lstPhoneNum;
	CString	m_strPhoneNum;
	CString	m_strUsedPhones;
	CString	m_strPhoneName;
	int		m_iType;
	//}}AFX_DATA

// Attributes
public:
protected:
	bool m_bFirstRun;
	bool m_bStopDownload;
	CMainFrame *m_pMainFram;
	enmPhoneListLastSentCommand m_enmPhoneListLastSentCommand;
	CImageList m_imlPhoneNum;
	POSITION m_posLastSentNum;
	int m_iTotalPrgrsPos;
	int m_iCurrentPrgrsPos;
	bool m_bHashOK;
	bool m_bQuickCompareOverDataInTheUnit;
	int m_iHashFailedTimes;
	int m_iLastAmoungOfData;
	CString m_strLastRecordsData;
// Operations
public:
protected:
	void HandleLastSentCommand();
	void SendSetCommandToUnit(enmPhoneListLastSentCommand &a_enmPhoneListLastSentCommand);
	int GetPhoneNumItemByLocInMem(int a_iLocInMem);
	int GetPhoneNumItemByPhoneNum(CString a_strPhoneNum);
	//Add item to the phone list and return the index
	int AddItemToPhoneList(int a_iLocInMem,CString a_strPhoneNum,CString a_strPhoneName,int a_iType,bool a_bInUnit,bool a_bNoSearch);
	void SetItemImage(int a_iLocInMem,int a_iInUnit);
	void EnableControls(BOOL a_bEnabled=TRUE);
	void Localize();
	int GetPhoneListCount();
	void DeletePhoneList();
	void BlinkItem(int a_iLocInMem,bool a_bInUnit);
// Overrides
	CGsmAlertDoc* GetDocument();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPhoneListView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CPhoneListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	// Generated message map functions
	//{{AFX_MSG(CPhoneListView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetPhoneNum();
	afx_msg void OnClickLstPhoneNum(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCmdAdd();
	afx_msg void OnCmdDownloadAllPhonesToUnit();
	afx_msg void OnCmdDownloadModifiedPhonesToUnit();
	afx_msg void OnCmdDeletePhoneNum();
	afx_msg void OnCmdStop();
	afx_msg void OnCmdExcelExport();
	afx_msg void OnCmdExcelImport();
	afx_msg void OnCmdQuickCompare();
	afx_msg void OnCmdSearchPhoneNum();
	afx_msg void OnCmdSearchPhoneName();
	afx_msg void OnCmdReplace();
	afx_msg void OnChangeTxtPhoneName();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RearView.cpp
inline CGsmAlertDoc* CPhoneListView::GetDocument()
   { return (CGsmAlertDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PHONESETTINGS_VIEW_H__EAEA64ED_38AB_4255_AD53_9A06112368D7__INCLUDED_)
