// ZoneList.cpp: implementation of the CZoneList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GsmAlert.h"
#include "ZoneList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CZoneListElement::CZoneListElement()
{
	m_bZoneActiveNoToNc=true;
	m_bZoneActiveNcToNo=true;
	m_enmZoneType = enmZoneTypeRegular;
	m_iZoneCounterGap = 100;
}

CZoneListElement::~CZoneListElement()
{
}


bool CZoneListElement::operator==(const CZoneListElement& rhs)
{
	if (m_iLocInMem != rhs.m_iLocInMem)
	{
		return false;
	}
	if (m_strZoneMsgNoToNc != rhs.m_strZoneMsgNoToNc)
	{
		return false;
	}
	if (m_strZoneMsgNcToNo != rhs.m_strZoneMsgNcToNo)
	{
		return false;
	}
	if (m_iZoneDelayNoToNc != rhs.m_iZoneDelayNoToNc)
	{
		return false;
	}
	if (m_iZoneDelayNcToNo != rhs.m_iZoneDelayNcToNo)
	{
		return false;
	}
	if (m_bZoneActiveNoToNc != rhs.m_bZoneActiveNoToNc)
	{
		return false;
	}
	if (m_bZoneActiveNcToNo != rhs.m_bZoneActiveNcToNo)
	{
		return false;
	}
	if (m_enmZoneType != rhs.m_enmZoneType)
	{
		return false;
	}
	if (m_iZoneCounterGap != rhs.m_enmZoneType)
	{
		return false;
	}
	
	return true;
}

bool CZoneListElement::operator!=(const CZoneListElement& rhs) { return !operator==(rhs); }

CString CZoneListElement::ElementToStr()
{
	CString l_strData;

	l_strData.Format("%s,%s,%d,%d,%d,%d,%d,%d",m_strZoneMsgNoToNc,m_strZoneMsgNcToNo,m_iZoneDelayNoToNc,m_iZoneDelayNcToNo,m_bZoneActiveNoToNc,m_bZoneActiveNcToNo, m_enmZoneType, m_iZoneCounterGap);
	
	return l_strData;
}

void CZoneListElement::StrToElement(CString a_strData)
{
	CString l_strZoneDelayNoToNc, l_strZoneDelayNcToNo, l_strZoneActiveNoToNc, l_strZoneActiveNcToNo, l_strZoneType, l_strZoneCntrGap;

	AfxExtractSubString(m_strZoneMsgNoToNc, a_strData, 0, _T(','));
	AfxExtractSubString(m_strZoneMsgNcToNo, a_strData, 1, _T(','));
	AfxExtractSubString(l_strZoneDelayNoToNc, a_strData, 2, _T(','));
	AfxExtractSubString(l_strZoneDelayNcToNo, a_strData, 3, _T(','));
	AfxExtractSubString(l_strZoneActiveNoToNc, a_strData, 4, _T(','));
	AfxExtractSubString(l_strZoneActiveNcToNo, a_strData, 5, _T(','));
	AfxExtractSubString(l_strZoneType, a_strData, 6, _T(','));
	AfxExtractSubString(l_strZoneCntrGap, a_strData, 7, _T(','));


	m_iZoneDelayNoToNc = atoi(LPCTSTR(l_strZoneDelayNoToNc));
	m_iZoneDelayNcToNo = atoi(LPCTSTR(l_strZoneDelayNcToNo));
	m_bZoneActiveNoToNc = (bool)atoi(LPCTSTR(l_strZoneActiveNoToNc));
	m_bZoneActiveNcToNo = (bool)atoi(LPCTSTR(l_strZoneActiveNcToNo));
	m_enmZoneType = (enmZoneType)atoi(LPCTSTR(l_strZoneType));
	m_iZoneCounterGap = atoi(LPCTSTR(l_strZoneCntrGap));
}

CZoneList::CZoneList()
{
}

CZoneList::~CZoneList()
{
	DeleteAllList();
}

void CZoneList::DeleteAllList()
{
	CZoneListElement *l_pElementToRemove;
	
	while(!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove=RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}

void CZoneList::Serialize( CArchive& archive )
{
	CZoneListElement *l_pListElement;
    //CObject::Serialize( archive );

    // now do the stuff for our specific class
    if( archive.IsStoring() )
	{
		POSITION l_Position;

        archive << GetCount();
		
		l_Position=GetHeadPosition();
		while(l_Position!=NULL)
		{
			l_pListElement=GetNext(l_Position);

			archive << l_pListElement->m_iLocInMem << l_pListElement->m_enmZoneType << l_pListElement->m_strZoneMsgNoToNc << l_pListElement->m_strZoneMsgNcToNo << l_pListElement->m_iZoneDelayNoToNc << l_pListElement->m_iZoneDelayNcToNo << l_pListElement->m_bZoneActiveNoToNc << l_pListElement->m_bZoneActiveNcToNo << l_pListElement->m_iZoneCounterGap << l_pListElement->m_bInUnit;
		}
	}
    else
	{
		int l_iListCount;
		int l_iZoneType, l_bZoneActiveNoToNc, l_bZoneActiveNcToNo ,l_iBoolInUnit;
		
		DeleteAllList();

		archive >> l_iListCount;
		for(int l_iIndex=0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement=new CZoneListElement;
			archive >> l_pListElement->m_iLocInMem >> l_iZoneType >> l_pListElement->m_strZoneMsgNoToNc >> l_pListElement->m_strZoneMsgNcToNo >> l_pListElement->m_iZoneDelayNoToNc >> l_pListElement->m_iZoneDelayNcToNo >> l_pListElement->m_bZoneActiveNoToNc >> l_pListElement->m_bZoneActiveNcToNo >> l_pListElement->m_iZoneCounterGap >> l_pListElement->m_bInUnit;

			l_pListElement->m_enmZoneType = (enmZoneType)l_iZoneType;
			AddTail(l_pListElement);
		}
	}
}

CZoneListElement* CZoneList::AddItemSorted(int a_iLocInMem,enmZoneType a_enmZoneCntrType, CString a_strZoneMsgNoToNc,CString a_strZoneMsgNcToNo,int a_iZoneDelayNoToNc,int a_iZoneDelayNcToNo,bool a_bZoneActiveNoToNc,bool a_bZoneActiveNcToNo,int a_iZoneCntrGap, bool a_bInUnit,bool a_bNoSearch)
{
	CZoneListElement *l_pListElement;
	bool l_bNew=false;
	if(a_bNoSearch)
	{
		l_bNew=true;
		l_pListElement=new CZoneListElement;
	}
	else
	{
		if((l_pListElement=GetItemByLocInMem(a_iLocInMem))==NULL)
		{
			l_bNew=true;
			l_pListElement=new CZoneListElement;
		}
	}
	
	l_pListElement->m_iLocInMem=a_iLocInMem;
	l_pListElement->m_enmZoneType = a_enmZoneCntrType;
	l_pListElement->m_strZoneMsgNoToNc=a_strZoneMsgNoToNc;
	l_pListElement->m_strZoneMsgNcToNo=a_strZoneMsgNcToNo;
	l_pListElement->m_iZoneDelayNoToNc=a_iZoneDelayNoToNc;
	l_pListElement->m_iZoneDelayNcToNo=a_iZoneDelayNcToNo;
	l_pListElement->m_bZoneActiveNoToNc=a_bZoneActiveNoToNc;
	l_pListElement->m_bZoneActiveNcToNo=a_bZoneActiveNcToNo;
	l_pListElement->m_iZoneCounterGap = a_iZoneCntrGap;
	l_pListElement->m_bInUnit=a_bInUnit;

	if(l_bNew)
	{
		AddTail(l_pListElement);
	}
	return l_pListElement;
}

CZoneListElement* CZoneList::GetItemByLocInMem(int a_iLocInMem)
{
	POSITION l_Position;
	CZoneListElement *l_pListElement;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_iLocInMem==a_iLocInMem)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

int CZoneList::GetAmountOfNotInUnit()
{
	POSITION l_Position;
	CZoneListElement *l_pListElement;
	int l_iCounter=0;

	l_Position=GetHeadPosition();
	while(l_Position!=NULL)
	{
		l_pListElement=GetNext(l_Position);
		if(l_pListElement->m_bInUnit==false)
		{
			++l_iCounter;
		}
	}
	return l_iCounter;
}

IMPLEMENT_SERIAL(CZoneList,  CObList, 0)