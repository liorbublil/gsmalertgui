#ifndef GLOBALS_H
#define GLOBALS_H

#include "stdafx.h"
#include "Afxtempl.h"

#define MAX_PHONE_NUM_LEN_DEF 20

bool SetComboBoxFromItemData(CComboBox &a_cmbToSet,DWORD a_iItemData);
bool SetComboBoxFromItemText(CComboBox &a_cmbToSet, CString a_strItemText);
int GetNumAsIntFromString(CString &a_strNum);
CString GetNumAsStringFromString(CString &a_strNum);
CString GetStringFromInvertedCommasString(CString &a_strNum);
bool IsPhoneNumOK(CString a_strPhoneNum);
bool IsNumeric(CString a_strNum,bool a_bNegativeAlowed=false);
bool IsDate(CString a_strDate);
void ChangeListItemIcon(CListCtrl &a_lst,int a_iIndex,int a_iIcon,int l_iColNum);
void GetDateVars(CString a_strDate,int &a_iHoure,int &a_iMinute);
CString GetExcelDriver();
CString GetAccessDriver();
int FindInString(const CString &a_str,const char *a_str2,int a_iStartAt=0);
int GetTimedStrAsSec(CString a_strTime);
CString GetSecAsTimedStr(int a_iSec);
void CopyStringToClipbard(CString a_strData, HWND a_Owner);
#endif //GLOBALS_H