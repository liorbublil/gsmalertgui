/*********************************************************
* Multi-Page Interface
* Version: 1.2
* Date: September 2, 2003
* Author: Michal Mecinski
* E-mail: mimec@mimec.w.pl
* WWW: http://www.mimec.w.pl
*
* You may freely use and modify this code, but don't remove
* this copyright note.
*
* There is no warranty of any kind, express or implied, for this class.
* The author does not take the responsibility for any damage
* resulting from the use of it.
*
* Let me know if you find this code useful, and
* send me any modifications and bug reports.
*
* Copyright (C) 2003 by Michal Mecinski
*********************************************************/

#pragma once

#include "MPITabCtrl.h"

class CMPITabWnd : public CSplitterWnd
{
public:
	CMPITabWnd();
	virtual ~CMPITabWnd();

public:
	// Show the given page
	void SetPage(int nPage);

	// Load tabs from toolbar resource
	int CreateTabs(int nResourceID);

public:
	virtual void RecalcLayout();
protected:
	virtual int HitTest(CPoint pt) const;
	virtual void DrawAllSplitBars(CDC* pDC, int cxInside, int cyInside);

protected:
	CMPITabCtrl m_wndTabCtrl;
	int m_nCurPage;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSelChange(NMHDR* pNMHDR, LRESULT* pResult);
};

