// ModbusView.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "ModbusView.h"
#include "MainFrm.h"
#include "LevelAlertView.h"
#include "GsmAlertDoc.h"
#include "Language.h"
#include "global.h"

#define MODBUS_INDEX_COL_INDEX 0
#define MODBUS_REG_NAME_COL_INDEX 1
#define MODBUS_SLAVE_ID_COL_INDEX 2
#define MODBUS_REG_TYPE_COL_INDEX 3
#define MODBUS_REG_ADDR_COL_INDEX 4
#define MODBUS_VAR_TYPE_COL_INDEX 5
#define MODBUS_REG_DECODE_COL_INDEX 6
#define MODBUS_KEY_COL_INDEX 7

#define MODBUS_REG_TYPE_COIL_STAT 1
#define MODBUS_REG_TYPE_INPUT_STAT 2
#define MODBUS_REG_TYPE_HOLDING_REG 3
#define MODBUS_REG_TYPE_INPUT_REG 4

#define MODBUS_VAR_TYPE_UINT 0
#define MODBUS_VAR_TYPE_INT 1
#define MODBUS_VAR_TYPE_ULNG 2
#define MODBUS_VAR_TYPE_LNG 3
#define MODBUS_VAR_TYPE_FLOAT 6
#define MODBUS_VAR_TYPE_DOUBLE 7
#define MODBUS_VAR_TYPE_BIT 8

#define MODBUS_REG_DECODE_LSWF 0
#define MODBUS_REG_DECODE_MSWF 1

#define MODBUS_QUERY_REG_NAME_COL_INDEX 0
#define MODBUS_QUERY_KEY_COL_INDEX 1
#define MODBUS_QUERY_STATUS_COL_INDEX 2

static int CALLBACK SimpleSortCompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	if (lParam1<lParam2)
	{
		return -1;
	}
	else
	{
		return 1;
	}
}

// CModbusView

IMPLEMENT_DYNCREATE(CModbusView, CFormView)

CModbusView::CModbusView()
	: CFormView(IDD_MODBUS_VIEW_FORM)
	, m_iModbusRegType(-1)
	, m_iModbusVarType(-1)
	, m_strModBusAddr(_T(""))
	, m_strModbusKey(_T(""))
	, m_strModbusRegName(_T(""))
	, m_strModbusSlaveId(_T(""))
	, m_iModbusRegDecode(-1)
	, m_iModbusBaudRate(-1)
	, m_iModbusCharSet(-1)
	, m_strModbusMaxBlockSize(_T(""))
{
	m_bFirstRun = true;
}

CModbusView::~CModbusView()
{
}

void CModbusView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LST_MODBUS_REGS, m_lstModbusRegs);
	DDX_Control(pDX, IDC_CMB_MODBUS_REG_TYPE, m_cmbModbusRegType);
	DDX_CBIndex(pDX, IDC_CMB_MODBUS_REG_TYPE, m_iModbusRegType);
	DDX_Control(pDX, IDC_CMB_MODBUS_VAR_TYPE, m_cmbModbusVarType);
	DDX_CBIndex(pDX, IDC_CMB_MODBUS_VAR_TYPE, m_iModbusVarType);
	DDX_Control(pDX, IDC_TXT_MODBUS_ADDRESS, m_txtModbusAddr);
	DDX_Text(pDX, IDC_TXT_MODBUS_ADDRESS, m_strModBusAddr);
	DDV_MaxChars(pDX, m_strModBusAddr, 4);
	DDX_Control(pDX, IDC_TXT_MODBUS_KEY, m_txtModbusKey);
	DDX_Text(pDX, IDC_TXT_MODBUS_KEY, m_strModbusKey);
	DDX_Control(pDX, IDC_TXT_MODBUS_REG_NAME, m_txtModbusRegName);
	DDX_Text(pDX, IDC_TXT_MODBUS_REG_NAME, m_strModbusRegName);
	DDX_Control(pDX, IDC_TXT_MODBUS_SLAVE_ID, m_txtModbusSlaveId);
	DDX_Text(pDX, IDC_TXT_MODBUS_SLAVE_ID, m_strModbusSlaveId);
	DDX_Control(pDX, IDC_CMB_MODBUS_REG_DECODE, m_cmbModbusRegDecode);
	DDX_CBIndex(pDX, IDC_CMB_MODBUS_REG_DECODE, m_iModbusRegDecode);
	DDX_Control(pDX, IDC_CMD_ADD_MODBUS_REG, m_cmdAddModbusReg);
	DDX_Control(pDX, IDC_CMD_GET_MODBUS_REGS, m_cmdGetModbusReg);
	DDX_Control(pDX, IDC_CMD_REMOVE_MODBUS_REG, m_cmdRemoveModbusReg);
	DDX_Control(pDX, IDC_CMD_REPLACE_MODBUS_REG, m_cmdReplaceModbusReg);
	DDX_Control(pDX, IDC_CMD_SET_MODBUS_REGS, m_cmdSetModbusReg);
	DDX_Control(pDX, IDC_CMB_MODBUS_BAUD_RATE, m_cmbModbusBaudRate);
	DDX_CBIndex(pDX, IDC_CMB_MODBUS_BAUD_RATE, m_iModbusBaudRate);
	DDX_Control(pDX, IDC_CMB_MODBUS_CHAR_SET, m_cmbModbusCharSet);
	DDX_CBIndex(pDX, IDC_CMB_MODBUS_CHAR_SET, m_iModbusCharSet);
	DDX_Control(pDX, IDC_TXT_MODBUS_MAX_BLOCK_SIZE, m_txtModbusMaxBlockSize);
	DDX_Text(pDX, IDC_TXT_MODBUS_MAX_BLOCK_SIZE, m_strModbusMaxBlockSize);
	DDX_Control(pDX, IDC_CMD_QUERY_MODBUS, m_cmdQueyModbus);
	DDX_Control(pDX, IDC_LST_MODBUS_QUERY, m_lstModbusQuery);
}

BEGIN_MESSAGE_MAP(CModbusView, CFormView)
	ON_MESSAGE(UM_COMMAND_ARRIVED, OnCommandArrived)
	ON_BN_CLICKED(IDC_CMD_ADD_MODBUS_REG, &CModbusView::OnBnClickedCmdAddModbusReg)
	ON_CBN_SELCHANGE(IDC_CMB_MODBUS_REG_TYPE, &CModbusView::OnCbnSelchangeCmbModbusRegType)
	ON_BN_CLICKED(IDC_CMD_REMOVE_MODBUS_REG, &CModbusView::OnBnClickedCmdRemoveModbusReg)
//	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LST_MODBUS_REGS, &CModbusView::OnLvnItemchangedLstModbusRegs)
ON_NOTIFY(NM_CLICK, IDC_LST_MODBUS_REGS, &CModbusView::OnClickLstModbusRegs)
ON_BN_CLICKED(IDC_CMD_REPLACE_MODBUS_REG, &CModbusView::OnBnClickedCmdReplaceModbusReg)
ON_BN_CLICKED(IDC_CMD_GET_MODBUS_REGS, &CModbusView::OnBnClickedCmdGetModbusRegs)
ON_BN_CLICKED(IDC_CMD_SET_MODBUS_REGS, &CModbusView::OnBnClickedCmdSetModbusRegs)
ON_CBN_SELCHANGE(IDC_CMB_MODBUS_BAUD_RATE, &CModbusView::OnSelchangeCmbModbusBaudRate)
ON_CBN_SELCHANGE(IDC_CMB_MODBUS_CHAR_SET, &CModbusView::OnSelchangeCmbModbusCharSet)
ON_EN_CHANGE(IDC_TXT_MODBUS_MAX_BLOCK_SIZE, &CModbusView::OnEnChangeTxtModbusMaxBlockSize)
ON_BN_CLICKED(IDC_CMD_QUERY_MODBUS, &CModbusView::OnBnClickedCmdQueryModbus)
END_MESSAGE_MAP()


// CModbusView diagnostics

#ifdef _DEBUG
void CModbusView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CModbusView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif


CGsmAlertDoc* CModbusView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGsmAlertDoc)));
	return (CGsmAlertDoc*)m_pDocument;
}
#endif //_DEBUG

// CModbusView message handlers

LRESULT CModbusView::OnCommandArrived(WPARAM wParam, LPARAM lParam)
{
	CString *l_pArrivedRespond;

	if (wParam == COMMAND_MESSAGE_TIME_OUT)	//If the command is time out command
	{
		INFO_LOG(g_DbLogger, CString("CModbusView::OnCommandArrived wParam==COMMAND_MESSAGE_TIME_OUT"));
		m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
		EnableControls();
		MessageBox(GetResString(IDS_UNIT_NOT_RESPONDED), NULL, MB_ICONERROR | MB_OK);
		return 0;
	}
	else if (wParam == COMMAND_MESSAGE_UNIT_FOUND)
	{
		INFO_LOG(g_DbLogger, CString("CModbusView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_FOUND"));
		return 0;
	}
	else if (wParam == COMMAND_MESSAGE_UNIT_NOT_CONNECTED)
	{
		INFO_LOG(g_DbLogger, CString("CAdcView::OnCommandArrived wParam==COMMAND_MESSAGE_UNIT_NOT_CONNECTED"));
		if (m_enmModbusLastSentCommand != enmModbusLastSentCommandNone)
		{
			m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
			CString l_strError = GetResString(IDS_CONNECTION_ERROR);
			MessageBox(l_strError, NULL, MB_ICONERROR | MB_OK);
			EnableControls();
		}
		return 0;
	}
	else if (wParam != COMMAND_MESSAGE_MODEM_RESPOND)
	{
		INFO_LOG(g_DbLogger, CString("CAdcView::OnCommandArrived wParam!=COMMAND_MESSAGE_MODEM_RESPOND"));
		return 0;
	}

	l_pArrivedRespond = (CString*)lParam;
	int l_iRespondStartAt = -1;
	int l_iLastRespondStartAt = 0;
	CGsmAlertDoc *l_pDoc = GetDocument();

	INFO_LOG(g_DbLogger, CString("CModbusView::OnCommandArrived l_pArrivedRespond:") + *l_pArrivedRespond);

	do
	{
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(MODBUS_GET_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{			
			CString l_strIndex, l_strRegName, l_strSlaveId, l_strRegType, l_strVarType, l_strRegAddr, l_strRegDecode, l_strKey, l_strDataToPars, l_strModbusRegData;
			CGsmAlertDoc *l_pDoc = GetDocument();

			int l_iDataStartAt = FindInString(*l_pArrivedRespond, MODBUS_GET_MSG_RESPOND);
			l_iDataStartAt += (CString(MODBUS_GET_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			l_pDoc->m_ModbusList.DeleteAllList();
			m_lstModbusRegs.DeleteAllItems();

			// last argument is the delimiter
			for (int l_iIndex = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strModbusRegData, l_strDataToPars, l_iIndex, _T(';')); ++l_iIndex)
			{
				AfxExtractSubString(l_strIndex, l_strModbusRegData, 0, _T(','));
				AfxExtractSubString(l_strRegName, l_strModbusRegData, 1, _T(','));
				AfxExtractSubString(l_strSlaveId, l_strModbusRegData, 2, _T(','));
				AfxExtractSubString(l_strRegType, l_strModbusRegData, 3, _T(','));
				AfxExtractSubString(l_strVarType, l_strModbusRegData, 4, _T(','));
				AfxExtractSubString(l_strRegAddr, l_strModbusRegData, 5, _T(','));
				AfxExtractSubString(l_strRegDecode, l_strModbusRegData, 6, _T(','));
				AfxExtractSubString(l_strKey, l_strModbusRegData, 7, _T(','));

				l_pDoc->m_ModbusList.AddItemSorted(atoi(l_strIndex), l_strRegName, atoi(l_strSlaveId), atoi(l_strRegType), atoi(l_strVarType), atoi(l_strRegAddr), atoi(l_strRegDecode), l_strKey, true, false);
			}
			ApplyModbusListToScreen();
			m_lstModbusRegs.SortItems(SimpleSortCompareFunc, 0);
			this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);
			l_pDoc->SetModifiedFlag();


			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;

		}
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(MODBUS_PORT_GET_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			CGsmAlertDoc *l_pDoc = GetDocument();
			CString l_strDataToPars, l_strBaudRate, l_strCharSet;

			int l_iDataStartAt = FindInString(*l_pArrivedRespond, MODBUS_PORT_GET_MSG_RESPOND);
			l_iDataStartAt += (CString(MODBUS_PORT_GET_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			// last argument is the delimiter
			AfxExtractSubString(l_strBaudRate, l_strDataToPars, 0, _T(','));
			SetComboBoxFromItemText(m_cmbModbusBaudRate, l_strBaudRate);
			AfxExtractSubString(l_strCharSet, l_strDataToPars, 1, _T(','));
			SetComboBoxFromItemText(m_cmbModbusCharSet, l_strCharSet);
			UpdateData();
			AfxExtractSubString(m_strModbusMaxBlockSize, l_strDataToPars, 2, _T(','));
			
			l_pDoc->SetModifiedFlag();


			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;

		}
		if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, LPCTSTR(CString(MODBUS_GET_RESULTS_MSG_RESPOND) + CString(ACK_RESPOND)), l_iLastRespondStartAt)) != -1)
		{
			CString l_strDataToPars, l_strModbusRegRes, l_strRegName, l_strKey, l_strRes;
			int l_iNewItem;

			int l_iDataStartAt = FindInString(*l_pArrivedRespond, MODBUS_GET_RESULTS_MSG_RESPOND);
			l_iDataStartAt += (CString(MODBUS_GET_RESULTS_MSG_RESPOND) + CString(ACK_RESPOND) + CString(",")).GetLength();
			l_strDataToPars = l_pArrivedRespond->Mid(l_iDataStartAt);

			m_lstModbusQuery.DeleteAllItems();

			// last argument is the delimiter
			for (int l_iIndex = 0; (l_strDataToPars != CString("")) && AfxExtractSubString(l_strModbusRegRes, l_strDataToPars, l_iIndex, _T(';')); ++l_iIndex)
			{
				AfxExtractSubString(l_strRegName, l_strModbusRegRes, 0, _T(','));
				AfxExtractSubString(l_strKey, l_strModbusRegRes, 1, _T(','));
				AfxExtractSubString(l_strRes, l_strModbusRegRes, 2, _T(','));
				
				l_iNewItem = m_lstModbusQuery.InsertItem(MODBUS_QUERY_REG_NAME_COL_INDEX, l_strRegName);
				m_lstModbusQuery.SetItemText(l_iNewItem, MODBUS_QUERY_KEY_COL_INDEX, l_strKey);
				m_lstModbusQuery.SetItemText(l_iNewItem, MODBUS_QUERY_STATUS_COL_INDEX, l_strRes);
			}

			l_iLastRespondStartAt = l_iRespondStartAt + 1;
			continue;

		}
	} while (l_iRespondStartAt != -1);
	UpdateData(FALSE);

	if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, ACK_RESPOND)) != -1)
	{
		HandleLastSentCommand();
	}
	else if ((l_iRespondStartAt = FindInString(*l_pArrivedRespond, ERROR_RESPOND)) != -1)
	{
		MessageBox(GetResString(IDS_ERROR_OCCURRED));
		m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
		EnableControls();
	}
	delete l_pArrivedRespond;
	INFO_LOG(g_DbLogger, CString("exit CModbusView::OnCommandArrived"));
	return 1;
}

void CModbusView::HandleLastSentCommand()
{
	INFO_LOG(g_DbLogger, CString("CAdcView::HandleLastSentCommand"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	switch (m_enmModbusLastSentCommand)
	{
	case enmModbusLastSentCommandGetModbusList:
		m_enmModbusLastSentCommand = enmModbusLastSentCommandGetModbusPort;
		SendSetCommandToUnit(m_enmModbusLastSentCommand);
		break;
	case enmModbusLastSentCommandGetModbusPort:
		m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
		EnableControls();
		break;
	case enmModbusLastSentCommandSetModbusList:
	{
		CGsmAlertDoc *l_pDoc = GetDocument();
		CModbusListElement *l_ListElement;
		for (int l_i = 1;l_i <= l_pDoc->m_ModbusList.GetCount();++l_i)
		{
			l_ListElement = l_pDoc->m_ModbusList.GetItemByIndex(l_i);
			l_ListElement->m_bInUnit = true;
		}
		ApplyModbusListToScreen();


		CString l_strTmp;
		m_cmbModbusBaudRate.GetLBText(m_iModbusBaudRate, l_strTmp);		//Get the baudrate
		m_strSetString = l_strTmp;

		m_cmbModbusCharSet.GetLBText(m_iModbusCharSet, l_strTmp);		//Get the baudrate
		m_strSetString += CString(",") + l_strTmp + CString(",") + m_strModbusMaxBlockSize;

		m_enmModbusLastSentCommand = enmModbusLastSentCommandSetModbusPort;
		SendSetCommandToUnit(m_enmModbusLastSentCommand);
		break;
	}
	case enmModbusLastSentCommandSetModbusPort:
		m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
		EnableControls();
		l_pDoc->SetModifiedFlag();
		break;
	case enmModbusLastSentCommandGetModbusResults:
		m_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
		EnableControls();
		break;
	}
}

void CModbusView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

	INFO_LOG(g_DbLogger, CString("CModbusView::OnInitialUpdate"));
	if (m_bFirstRun)
	{
		INFO_LOG(g_DbLogger, CString("CModbusView::OnInitialUpdate first run"));

		m_bFirstRun = false;
	

		m_lstModbusRegs.InsertColumn(MODBUS_INDEX_COL_INDEX, GetResString(IDS_INDEX), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_REG_NAME_COL_INDEX, GetResString(IDS_MODBUS_REG_NAME), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_SLAVE_ID_COL_INDEX, GetResString(IDS_SLAVE_ID), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_REG_TYPE_COL_INDEX, GetResString(IDS_REG_TYPE), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_REG_ADDR_COL_INDEX, GetResString(IDS_MODBUS_REG_ADDR), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_VAR_TYPE_COL_INDEX, GetResString(IDS_MODBUS_VAR_TYPE), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_REG_DECODE_COL_INDEX, GetResString(IDS_MODBUS_REG_DECODE), LVCFMT_LEFT, 120);
		m_lstModbusRegs.InsertColumn(MODBUS_KEY_COL_INDEX, GetResString(IDS_CLOUD_KEY), LVCFMT_LEFT, 120);

		m_imlModbusView.Create(16, 16, ILC_MASK | ILC_COLOR32, 0, 0);
		HICON l_hIconNotLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_NOT_LOADED), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconLoaded_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LOADED), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		HICON l_hIconSearch_16x16 = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_SEARCH), IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
		m_imlModbusView.Add(l_hIconNotLoaded_16x16);
		m_imlModbusView.Add(l_hIconLoaded_16x16);
		m_imlModbusView.Add(l_hIconSearch_16x16);
		m_lstModbusRegs.SetImageList(&m_imlModbusView, LVSIL_SMALL | LVSIL_NORMAL);

		//Set the list to be full row selection and with grid lines
		DWORD l_dwStyle = m_lstModbusRegs.GetExtendedStyle();
		l_dwStyle |= LVS_EX_FULLROWSELECT;
		l_dwStyle |= LVS_EX_GRIDLINES;
		m_lstModbusRegs.SetExtendedStyle(l_dwStyle);

		int l_iNewIndex = m_cmbModbusRegType.AddString(GetResString(IDS_MODBUS_REG_TYPE_COIL_STAT));
		m_cmbModbusRegType.SetItemData(l_iNewIndex, MODBUS_REG_TYPE_COIL_STAT);
		l_iNewIndex = m_cmbModbusRegType.AddString(GetResString(IDS_MODBUS_REG_TYPE_INPUT_STAT));
		m_cmbModbusRegType.SetItemData(l_iNewIndex, MODBUS_REG_TYPE_INPUT_STAT);
		l_iNewIndex = m_cmbModbusRegType.AddString(GetResString(IDS_MODBUS_REG_TYPE_HOLDING_REG));
		m_cmbModbusRegType.SetItemData(l_iNewIndex, MODBUS_REG_TYPE_HOLDING_REG);
		l_iNewIndex = m_cmbModbusRegType.AddString(GetResString(IDS_MODBUS_REG_TYPE_INPUT_REGISTER));
		m_cmbModbusRegType.SetItemData(l_iNewIndex, MODBUS_REG_TYPE_INPUT_REG);

		//Add the register decoding
		l_iNewIndex = m_cmbModbusRegDecode.AddString(GetResString(IDS_MODBUS_REG_DECODE_LSWF));
		m_cmbModbusRegDecode.SetItemData(l_iNewIndex, MODBUS_REG_DECODE_LSWF);
		l_iNewIndex = m_cmbModbusRegDecode.AddString(GetResString(IDS_MODBUS_REG_DECODE_MSWF));
		m_cmbModbusRegDecode.SetItemData(l_iNewIndex, MODBUS_REG_DECODE_MSWF);
		
		//Add the Baud rates for the port
		CString l_strTmp;
		l_strTmp.Format("%d", BAUD_RATE_300);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_300);
		l_strTmp.Format("%d", BAUD_RATE_1200);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_1200);
		l_strTmp.Format("%d", BAUD_RATE_2400);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_2400);
		l_strTmp.Format("%d", BAUD_RATE_4800);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_4800);
		l_strTmp.Format("%d", BAUD_RATE_9600);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_9600);
		l_strTmp.Format("%d", BAUD_RATE_19200);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_19200);
		l_strTmp.Format("%d", BAUD_RATE_38400);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_38400);
		l_strTmp.Format("%d", BAUD_RATE_57600);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_57600);
		l_strTmp.Format("%d", BAUD_RATE_115200);
		l_iNewIndex = m_cmbModbusBaudRate.AddString(l_strTmp);
		m_cmbModbusBaudRate.SetItemData(l_iNewIndex, BAUD_RATE_115200);

		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8N1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8N1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8N2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8N2);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8E1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8E1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8E2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8E2);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8O1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8O1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_8O2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_8O2);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7N1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7N1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7N2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7N2);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7E1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7E1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7E2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7E2);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7O1);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7O1);
		l_iNewIndex = m_cmbModbusCharSet.AddString(CHAR_SET_STR_7O2);
		m_cmbModbusCharSet.SetItemData(l_iNewIndex, CHAR_SET_INT_7O2);

		//Add the modbus query colomns
		m_lstModbusQuery.InsertColumn(MODBUS_QUERY_REG_NAME_COL_INDEX, GetResString(IDS_MODBUS_REG_NAME), LVCFMT_LEFT, 120);
		m_lstModbusQuery.InsertColumn(MODBUS_QUERY_KEY_COL_INDEX, GetResString(IDS_CLOUD_KEY), LVCFMT_LEFT, 120);
		m_lstModbusQuery.InsertColumn(MODBUS_QUERY_STATUS_COL_INDEX, GetResString(IDS_MODBUS_STATUS), LVCFMT_LEFT, 120);
		
		//Set the list to be full row selection and with grid lines
		m_lstModbusQuery.SetExtendedStyle(l_dwStyle);


		CWnd *l_pWnd = GetParentFrame();
		ASSERT_KINDOF(CMPIChildFrame, l_pWnd);
		m_pMainFram = (CMainFrame*)((CMPIChildFrame*)l_pWnd)->GetMDIFrame();
		ASSERT_KINDOF(CMainFrame, m_pMainFram);
		m_pLevelEventView = ((CMPIChildFrame*)l_pWnd)->m_pLevelAlertView;
		ASSERT_KINDOF(CLevelAlertView, m_pLevelEventView);

		//Localize();
	}
	CGsmAlertDoc *l_pDoc = GetDocument();

	ApplyModbusListToScreen();
	m_lstModbusRegs.SortItems(SimpleSortCompareFunc, 0);
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);

	CString l_strTmp;
	l_strTmp.Format("%d", l_pDoc->m_iModbusBaudRate);
	SetComboBoxFromItemText(m_cmbModbusBaudRate, l_strTmp);

	SetComboBoxFromItemText(m_cmbModbusCharSet, l_pDoc->m_strModbusCharSet);

	UpdateData(TRUE);
	m_strModbusMaxBlockSize.Format("%d", l_pDoc->m_iModbusMaxBlockSize);

	ClrearFeilds();	//Clear the fields after add them to the list


	/*OnUpdateSensorsList(0, 0);

	UpdateData(FALSE);
	EnableControls();
	*/
}

bool CModbusView::VerifyValidFeilds()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::VerifyValidFeilds"));

	UpdateData();
	CGsmAlertDoc *l_pDoc = GetDocument();
	int l_iIndex = l_pDoc->m_ModbusList.GetCount() + 1;

	if (l_iIndex>MAX_LEVEL_EVENTS_ALLOWED)
	{
		MessageBox(GetResString(IDS_LOC_IN_MEM_TO_BIG), NULL, MB_ICONERROR);
		return false;
	}

	if (m_strModbusRegName == "")
	{
		MessageBox(GetResString(IDS_MODBUS_REG_NAME_NOT_OK), NULL, MB_ICONERROR);
		return false;
	}

	if (!IsNumeric(m_strModbusSlaveId, true))
	{
		MessageBox(GetResString(IDS_MODBUS_SLAVE_ID_NOT_OK), NULL, MB_ICONERROR);
		return false;
	}
	if ((atoi(m_strModbusSlaveId)<1) || (atoi(m_strModbusSlaveId)>250))
	{
		MessageBox(GetResString(IDS_MODBUS_SLAVE_ID_NOT_OK), NULL, MB_ICONERROR);
		return false;
	}

	if (!IsNumeric(m_strModBusAddr, true))
	{
		MessageBox(GetResString(IDS_MODBUS_REG_ADDR_NOT_OK), NULL, MB_ICONERROR);
		return false;
	}

	if (m_iModbusRegType == -1)
	{
		MessageBox(GetResString(IDS_REG_TYPE_NOT_SELECTED), NULL, MB_ICONERROR);
		return false;
	}
	if (m_iModbusVarType == -1)
	{
		MessageBox(GetResString(IDS_MODBUS_VAR_TYPE_NOT_SELECTED), NULL, MB_ICONERROR);
		return false;
	}

	if (m_strModbusKey == "")
	{
		MessageBox(GetResString(IDS_CLOUD_KEY_NOT_OK), NULL, MB_ICONERROR);
		return false;
	}

	return true;
}

void CModbusView::ClrearFeilds()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::ClrearFeilds"));

	m_strModbusRegName = "";
	m_strModbusSlaveId = "";
	m_strModBusAddr = "";
	m_iModbusRegType = -1;
	m_iModbusVarType = -1;
	m_iModbusRegDecode= -1;
	m_strModbusKey = "";
	UpdateData(FALSE);
}

void CModbusView::OnBnClickedCmdAddModbusReg()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::OnBnClickedCmdAddModbusReg"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	if (!VerifyValidFeilds())
	{
		return;
	}

	int l_iIndex = l_pDoc->m_ModbusList.GetCount() + 1;
	int l_iRegType = m_cmbModbusRegType.GetItemData(m_iModbusRegType);
	int l_iVarType = m_cmbModbusVarType.GetItemData(m_iModbusVarType);
	int l_iRegDecode= m_cmbModbusRegDecode.GetItemData(m_iModbusRegDecode);
	l_pDoc->m_ModbusList.AddItemSorted(l_iIndex, m_strModbusRegName, atoi(m_strModbusSlaveId), l_iRegType, l_iVarType, atoi(m_strModBusAddr), l_iRegDecode,m_strModbusKey, false, false);
	l_pDoc->SetModifiedFlag();
	AddItemToModbusList(l_iIndex);
	m_lstModbusRegs.SortItems(SimpleSortCompareFunc, 0);
	l_iIndex = GetModbusItemByIndex(l_iIndex);
	m_lstModbusRegs.EnsureVisible(l_iIndex, false);
	
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);

	ClrearFeilds();	//Clear the fields after add them to the list
}

int CModbusView::GetModbusItemByIndex(int a_iIndex)
{
	INFO_LOG(g_DbLogger, CString("CModbusView::GetModbusItemByIndex"));

	CString l_strExistIndex;
	for (int l_iForIndex = 0;l_iForIndex<m_lstModbusRegs.GetItemCount();++l_iForIndex)
	{
		l_strExistIndex = m_lstModbusRegs.GetItemText(l_iForIndex, MODBUS_INDEX_COL_INDEX);
		if (atoi(l_strExistIndex) == a_iIndex)
		{
			return l_iForIndex;
		}
	}
	return -1;
}

int CModbusView::AddItemToModbusList(int a_iIndex)
{
	CGsmAlertDoc *l_pDoc = GetDocument();

	INFO_LOG(g_DbLogger, CString("CModbusView::AddItemToModbusList"));

	CModbusListElement *l_ModbusListElement = l_pDoc->m_ModbusList.GetItemByIndex(a_iIndex);

	int l_iImage;
	if (l_ModbusListElement->m_bInUnit)
	{
		l_iImage = 1;
	}
	else
	{
		l_iImage = 0;
	}

	CString l_strIndex;
	l_strIndex.Format("%d", a_iIndex);


	int l_iIndex;
	if ((l_iIndex = GetModbusItemByIndex(a_iIndex)) == -1)
	{
		l_iIndex = m_lstModbusRegs.InsertItem(MODBUS_INDEX_COL_INDEX, l_strIndex, l_iImage);
		m_lstModbusRegs.EnsureVisible(l_iIndex, false);
		m_lstModbusRegs.SetItemData(l_iIndex, a_iIndex);
	}
	else
	{
		m_lstModbusRegs.DeleteItem(l_iIndex);
		l_iIndex = m_lstModbusRegs.InsertItem(MODBUS_INDEX_COL_INDEX, l_strIndex, l_iImage);
		m_lstModbusRegs.EnsureVisible(l_iIndex, false);
		m_lstModbusRegs.SetItemData(l_iIndex, a_iIndex);
	}
	
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_REG_NAME_COL_INDEX, l_ModbusListElement->m_strRegName);
	CString l_strSlaveId;
	l_strSlaveId.Format("%d", l_ModbusListElement->m_iSlaveId);
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_SLAVE_ID_COL_INDEX, l_strSlaveId);

	CString m_strRegType;
	switch (l_ModbusListElement->m_iRegType)
	{
	case MODBUS_REG_TYPE_COIL_STAT:
		m_strRegType = GetResString(IDS_MODBUS_REG_TYPE_COIL_STAT);
		break;
	case MODBUS_REG_TYPE_INPUT_STAT:
		m_strRegType = GetResString(IDS_MODBUS_REG_TYPE_INPUT_STAT);
		break;
	case MODBUS_REG_TYPE_HOLDING_REG:
		m_strRegType = GetResString(IDS_MODBUS_REG_TYPE_HOLDING_REG);
		break;
	case MODBUS_REG_TYPE_INPUT_REG:
		m_strRegType = GetResString(IDS_MODBUS_REG_TYPE_INPUT_REGISTER);
		break;
	default:
		break;
	}
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_REG_TYPE_COL_INDEX, m_strRegType);

	CString l_strRegAddr;
	l_strRegAddr.Format("%d", l_ModbusListElement->m_iRegAddr);
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_REG_ADDR_COL_INDEX, l_strRegAddr);

	CString m_strVarType;
	switch (l_ModbusListElement->m_iVarType)
	{
	case MODBUS_VAR_TYPE_BIT:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_BIT);
		break;

	case MODBUS_VAR_TYPE_UINT:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_UNIT);
		break;
	case MODBUS_VAR_TYPE_INT:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_INT);
		break;
	case MODBUS_VAR_TYPE_ULNG:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_ULNG);
		break;
	case MODBUS_VAR_TYPE_LNG:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_LNG);
		break;
	case MODBUS_VAR_TYPE_FLOAT:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_FLOAT);
		break;
	case MODBUS_VAR_TYPE_DOUBLE:
		m_strVarType = GetResString(IDS_MODBUS_VAR_TYPE_DBL);
		break;
	default:
		break;
	}
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_VAR_TYPE_COL_INDEX, m_strVarType);

	CString m_strRegDecode;
	switch (l_ModbusListElement->m_iRegDecoding)
	{
	case MODBUS_REG_DECODE_LSWF:
		m_strRegDecode = GetResString(IDS_MODBUS_REG_DECODE_LSWF);
		break;
	case MODBUS_REG_DECODE_MSWF:
		m_strRegDecode = GetResString(IDS_MODBUS_REG_DECODE_MSWF);
		break;
	}
	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_REG_DECODE_COL_INDEX, m_strRegDecode);

	m_lstModbusRegs.SetItemText(l_iIndex, MODBUS_KEY_COL_INDEX, l_ModbusListElement->m_strModbusKey);

	return l_iIndex;
}

void CModbusView::ApplyModbusListToScreen()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::ApplyModbusListToScreen"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	POSITION l_Position = l_pDoc->m_ModbusList.GetHeadPosition();
	CLevelEventListElement *l_pListElement;
	CString l_strSensorNum;
	m_lstModbusRegs.DeleteAllItems();
	//Go over all the level events list and add them to the level event list control
	while (l_Position != NULL)
	{
		l_pListElement = l_pDoc->m_LevelEventList.GetNext(l_Position);
		AddItemToModbusList(l_pListElement->m_iIndex);
	}
	m_lstModbusRegs.SortItems(SimpleSortCompareFunc, 0);
}

void CModbusView::ApplyVarTypeFromRegType(bool a_bForace)
{
	if (!a_bForace)
	{
		int l_iOldRegType = m_iModbusRegType;
		UpdateData();

		if (l_iOldRegType == m_iModbusRegType)
		{
			return;
		}
	}

	int l_iRegType = m_cmbModbusRegType.GetItemData(m_iModbusRegType);
	m_cmbModbusVarType.ResetContent();

	int l_iNewIndex;

	switch (l_iRegType)
	{
	case MODBUS_REG_TYPE_COIL_STAT:
	case MODBUS_REG_TYPE_INPUT_STAT:
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_BIT));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_BIT);
		break;

	case MODBUS_REG_TYPE_HOLDING_REG:
	case MODBUS_REG_TYPE_INPUT_REG:
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_UNIT));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_UINT);
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_INT));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_INT);
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_ULNG));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_ULNG);
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_LNG));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_LNG);
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_FLOAT));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_FLOAT);
		l_iNewIndex = m_cmbModbusVarType.AddString(GetResString(IDS_MODBUS_VAR_TYPE_DBL));
		m_cmbModbusVarType.SetItemData(l_iNewIndex, MODBUS_VAR_TYPE_DOUBLE);
		break;

	}
	m_iModbusVarType = -1;
	UpdateData(FALSE);
}

void CModbusView::OnCbnSelchangeCmbModbusRegType()
{
	ApplyVarTypeFromRegType(false);
}


void CModbusView::OnBnClickedCmdRemoveModbusReg()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::OnBnClickedCmdRemoveModbusReg"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	POSITION l_pos = m_lstModbusRegs.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}

	CString l_strIndex;
	int l_iIndex;
	int l_iItem = m_lstModbusRegs.GetNextSelectedItem(l_pos);
	l_strIndex = m_lstModbusRegs.GetItemText(l_iItem, MODBUS_INDEX_COL_INDEX);
	l_iIndex = atoi(LPCTSTR(l_strIndex));
	l_pDoc->m_ModbusList.DeleteItem(l_iIndex);
	ApplyModbusListToScreen();
	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);

	UpdateData(FALSE);
	l_pDoc->SetModifiedFlag();
}

void CModbusView::OnClickLstModbusRegs(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	INFO_LOG(g_DbLogger, CString("CModbusView::OnClickLstModbusRegs"));

	if (pResult)
	{
		*pResult = 0;
	}

	POSITION l_pos = m_lstModbusRegs.GetFirstSelectedItemPosition();
	if (l_pos == NULL)
	{
		return;
	}
	
	CGsmAlertDoc *l_pDoc = GetDocument();
	int l_iItem = m_lstModbusRegs.GetNextSelectedItem(l_pos);

	int l_iIndex = atoi(m_lstModbusRegs.GetItemText(l_iItem, MODBUS_INDEX_COL_INDEX));
	CModbusListElement *l_pModbusListElement = l_pDoc->m_ModbusList.GetItemByIndex(l_iIndex);

	//First set teh combo box because the direction is from the controller to the variable
	SetComboBoxFromItemData(m_cmbModbusRegType, l_pModbusListElement->m_iRegType);
	UpdateData();
	ApplyVarTypeFromRegType(true);
	SetComboBoxFromItemData(m_cmbModbusVarType, l_pModbusListElement->m_iVarType);
	SetComboBoxFromItemData(m_cmbModbusRegDecode, l_pModbusListElement->m_iRegDecoding);
	UpdateData();

	m_strModbusRegName = l_pModbusListElement->m_strRegName;
	m_strModbusSlaveId.Format("%d",l_pModbusListElement->m_iSlaveId);
	m_strModBusAddr.Format("%d", l_pModbusListElement->m_iRegAddr);
	m_strModbusKey= l_pModbusListElement->m_strModbusKey;

	UpdateData(FALSE);	
}


void CModbusView::OnBnClickedCmdReplaceModbusReg()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::OnBnClickedCmdReplaceModbusReg"));
	CGsmAlertDoc *l_pDoc = GetDocument();

	if (!VerifyValidFeilds())
	{
		return;
	}

	POSITION l_posModBus = m_lstModbusRegs.GetFirstSelectedItemPosition();
	if (l_posModBus == NULL)
	{
		MessageBox(GetResString(IDS_MODBUS_NOT_SELECTED), NULL, MB_ICONERROR);
		return;
	}
	//Get the level event index from the selected list item
	int l_iModbusItem = m_lstModbusRegs.GetNextSelectedItem(l_posModBus);
	int l_iIndex = atoi(m_lstModbusRegs.GetItemText(l_iModbusItem, MODBUS_INDEX_COL_INDEX));

	int l_iRegType = m_cmbModbusRegType.GetItemData(m_iModbusRegType);
	int l_iVarType = m_cmbModbusVarType.GetItemData(m_iModbusVarType);
	int l_iRegDecode = m_cmbModbusRegDecode.GetItemData(m_iModbusRegDecode);
	l_pDoc->m_ModbusList.AddItemSorted(l_iIndex, m_strModbusRegName, atoi(m_strModbusSlaveId), l_iRegType, l_iVarType, atoi(m_strModBusAddr), l_iRegDecode, m_strModbusKey, false, false);
	l_pDoc->SetModifiedFlag();
	ApplyModbusListToScreen();
	m_lstModbusRegs.SortItems(SimpleSortCompareFunc, 0);
	l_iIndex = GetModbusItemByIndex(l_iIndex);
	m_lstModbusRegs.EnsureVisible(l_iIndex, false);

	this->m_pLevelEventView->PostMessage(UM_SENSORS_LIST_CHANGED, 0, 0);

	ClrearFeilds();	//Clear the fields after add them to the list
}


void CModbusView::OnBnClickedCmdGetModbusRegs()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::OnBnClickedCmdGetModbusRegs"));
	if (!(m_pMainFram->m_bConnectedToUnit))
	{
		MessageBox(GetResString(IDS_CONNECTION_ERROR), NULL, MB_ICONERROR);
		return;
	}
	CGsmAlertDoc *l_pDoc = GetDocument();

	DeleteModbusList();

	UpdateData(FALSE);
	m_enmModbusLastSentCommand = enmModbusLastSentCommandGetModbusList;
	SendSetCommandToUnit(m_enmModbusLastSentCommand);

	EnableControls(FALSE);
}

void CModbusView::DeleteModbusList()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	l_pDoc->m_ModbusList.DeleteAllList();
	m_lstModbusRegs.DeleteAllItems();
	l_pDoc->SetModifiedFlag();
}

void CModbusView::EnableControls(BOOL a_bEnabled)
{
	m_lstModbusRegs.EnableWindow(a_bEnabled);
	m_txtModbusRegName.EnableWindow(a_bEnabled);
	m_txtModbusSlaveId.EnableWindow(a_bEnabled);
	m_cmbModbusRegType.EnableWindow(a_bEnabled);
	m_txtModbusAddr.EnableWindow(a_bEnabled);
	m_cmbModbusVarType.EnableWindow(a_bEnabled);
	m_txtModbusKey.EnableWindow(a_bEnabled);
	m_cmbModbusRegDecode.EnableWindow(a_bEnabled);
	
	m_cmdAddModbusReg.EnableWindow(a_bEnabled);
	m_cmdGetModbusReg.EnableWindow(a_bEnabled);
	m_cmdRemoveModbusReg.EnableWindow(a_bEnabled);
	m_cmdReplaceModbusReg.EnableWindow(a_bEnabled);
	m_cmdSetModbusReg.EnableWindow(a_bEnabled);

	m_cmbModbusBaudRate.EnableWindow(a_bEnabled);
	m_cmbModbusCharSet.EnableWindow(a_bEnabled);
	m_txtModbusMaxBlockSize.EnableWindow(a_bEnabled);

	m_cmdQueyModbus.EnableWindow(a_bEnabled);
}

void CModbusView::SendSetCommandToUnit(enmModbusLastSentCommand &a_enmModbusLastSentCommand)
{
	INFO_LOG(g_DbLogger, CString("CModbusView::SendSetCommandToUnit"));
	CString *l_pNewCommand;
	CGsmAlertDoc *l_pDoc = GetDocument();

	l_pNewCommand = new CString();

	switch (a_enmModbusLastSentCommand)
	{
	case enmModbusLastSentCommandGetModbusList:
		l_pNewCommand->Format("%s", MODBUS_GET_MSG_COMMAND);
		break;
	case enmModbusLastSentCommandSetModbusList:
		l_pNewCommand->Format("%s%s", MODBUS_SET_MSG_COMMAND, m_strSetString);
		break;
	case enmModbusLastSentCommandSetModbusPort:
		l_pNewCommand->Format("%s%s", MODBUS_PORT_SET_MSG_COMMAND, m_strSetString);
		break;
	case enmModbusLastSentCommandGetModbusPort:
		l_pNewCommand->Format("%s", MODBUS_PORT_GET_MSG_COMMAND);
		break;
	case enmModbusLastSentCommandGetModbusResults:
		l_pNewCommand->Format("%s", MODBUS_GET_RESULTS_MSG_COMMAND);
		break;
	}
	if (*l_pNewCommand != "")
	{
		m_pMainFram->PostMessage(UM_SEND_COMMAND, (WPARAM)GetSafeHwnd(), (LPARAM)l_pNewCommand);
	}
	else
	{
		delete l_pNewCommand;
		a_enmModbusLastSentCommand = enmModbusLastSentCommandNone;
	}
}


void CModbusView::OnBnClickedCmdSetModbusRegs()
{
	INFO_LOG(g_DbLogger, CString("CModbusView::OnBnClickedCmdSetModbusRegs"));

	CGsmAlertDoc *l_pDoc = GetDocument();
	CModbusListElement *l_pListElement;
	CString l_strTmp, l_strLevelEventId;

	m_strSetString = "";

	for (int l_i = 0;l_i<l_pDoc->m_ModbusList.GetCount();++l_i)
	{
		l_pListElement = l_pDoc->m_ModbusList.GetItemByIndex(l_i + 1);

		l_strLevelEventId.Format("%s-%s", MODBUS_SENSOR_ID_STR, l_pListElement->m_strModbusKey);
		l_strTmp.Format("%d,%s,%d,%d,%d,%d,%d,%s,%s", l_pListElement->m_iIndex, l_pListElement->m_strRegName, l_pListElement->m_iSlaveId, l_pListElement->m_iRegType, l_pListElement->m_iVarType, l_pListElement->m_iRegAddr, l_pListElement->m_iRegDecoding, l_pListElement->m_strModbusKey, l_strLevelEventId);
		if (m_strSetString != "")	//For the second, third,.. items add semi column
		{
			m_strSetString += ";";
		}
		m_strSetString += l_strTmp;
	}

	m_enmModbusLastSentCommand= enmModbusLastSentCommandSetModbusList;
	SendSetCommandToUnit(m_enmModbusLastSentCommand);

	EnableControls(FALSE);
}

void CModbusView::OnSelchangeCmbModbusBaudRate()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	UpdateData();
	CString l_strTmp;
	m_cmbModbusBaudRate.GetLBText(m_iModbusBaudRate, l_strTmp);
	l_pDoc->m_iModbusBaudRate = atoi(l_strTmp);
	l_pDoc->SetModifiedFlag();
}


void CModbusView::OnSelchangeCmbModbusCharSet()
{
	CGsmAlertDoc *l_pDoc = GetDocument();
	UpdateData();
	m_cmbModbusCharSet.GetLBText(m_iModbusCharSet, l_pDoc->m_strModbusCharSet);
	l_pDoc->SetModifiedFlag();
}


void CModbusView::OnEnChangeTxtModbusMaxBlockSize()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CFormView::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here

	CGsmAlertDoc *l_pDoc = GetDocument();
	UpdateData();
	l_pDoc->m_iModbusMaxBlockSize = atoi((LPCSTR)m_strModbusMaxBlockSize);
	l_pDoc->SetModifiedFlag();
}


void CModbusView::OnBnClickedCmdQueryModbus()
{
	m_enmModbusLastSentCommand = enmModbusLastSentCommandGetModbusResults;
	SendSetCommandToUnit(m_enmModbusLastSentCommand);

	EnableControls(FALSE);
}
