// ModbusList.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "ModbusList.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CModbusListElement::CModbusListElement()
{
}

CModbusListElement::~CModbusListElement()
{
}

void CModbusListElement::operator=(const CModbusListElement& a_ModbusListElement)
{
	m_iIndex = a_ModbusListElement.m_iIndex;
	m_strRegName = a_ModbusListElement.m_strRegName;
	m_iSlaveId = a_ModbusListElement.m_iSlaveId;
	m_iRegType = a_ModbusListElement.m_iRegType;
	m_iVarType = a_ModbusListElement.m_iVarType;
	m_iRegAddr = a_ModbusListElement.m_iRegAddr;
	m_iRegDecoding = a_ModbusListElement.m_iRegDecoding;
	m_strModbusKey = a_ModbusListElement.m_strModbusKey;

	m_bInUnit = a_ModbusListElement.m_bInUnit;
}


// CModbusList
CModbusList::CModbusList()
{
}

CModbusList::~CModbusList()
{
	DeleteAllList();
}


// CModbusList member functions
void CModbusList::DeleteAllList()
{
	CModbusListElement *l_pElementToRemove;

	while (!IsEmpty())	//loop on all the elements from queue
	{
		l_pElementToRemove = RemoveHead();
		delete l_pElementToRemove;					//delete the element
	}
}

void CModbusList::Serialize(CArchive& archive)
{
	CModbusListElement *l_pListElement;
	//CObject::Serialize( archive );

	// now do the stuff for our specific class
	if (archive.IsStoring())
	{
		POSITION l_Position;

		archive << GetCount();

		l_Position = GetHeadPosition();
		while (l_Position != NULL)
		{
			l_pListElement = GetNext(l_Position);
			archive << l_pListElement->m_iIndex << l_pListElement->m_strRegName << l_pListElement->m_iSlaveId << l_pListElement->m_iRegType << l_pListElement->m_iVarType << l_pListElement->m_iRegAddr << l_pListElement->m_iRegDecoding <<  l_pListElement->m_strModbusKey << l_pListElement->m_bInUnit;
		}
	}
	else
	{
		int l_iListCount;
		int l_iBool;
		DeleteAllList();

		archive >> l_iListCount;
		for (int l_iIndex = 0;l_iIndex<l_iListCount;++l_iIndex)
		{
			l_pListElement = new CModbusListElement;
			archive >> l_pListElement->m_iIndex >> l_pListElement->m_strRegName >> l_pListElement->m_iSlaveId >> l_pListElement->m_iRegType >> l_pListElement->m_iVarType >> l_pListElement->m_iRegAddr >> l_pListElement->m_iRegDecoding >> l_pListElement->m_strModbusKey >> l_pListElement->m_bInUnit;

			AddTail(l_pListElement);
		}
	}
}



void CModbusList::AddItemSorted(int a_iIndex, CString a_strRegName, int a_iSlaveId, int a_iRegType, int a_iVarType, int a_iRegAddr,int a_iRegDecoding, CString a_strModbusKey, bool a_bInUnit, bool a_bNoSearch)
{
	CModbusListElement *l_pListElement;
	bool l_bNew = false;
	if (a_bNoSearch)
	{
		l_bNew = true;
		l_pListElement = new CModbusListElement;
	}
	else
	{
		if ((l_pListElement = GetItemByIndex(a_iIndex)) == NULL)
		{
			l_bNew = true;
			l_pListElement = new CModbusListElement;
		}
	}

	l_pListElement->m_iIndex = a_iIndex;
	l_pListElement->m_strRegName = a_strRegName;
	l_pListElement->m_iSlaveId = a_iSlaveId;
	l_pListElement->m_iRegType = a_iRegType;
	l_pListElement->m_iVarType = a_iVarType;
	l_pListElement->m_iRegAddr = a_iRegAddr;
	l_pListElement->m_iRegDecoding = a_iRegDecoding;
	l_pListElement->m_strModbusKey = a_strModbusKey;
	l_pListElement->m_bInUnit = a_bInUnit;

	if (l_bNew)
	{
		AddTail(l_pListElement);
	}
}

void CModbusList::DeleteItem(int a_iIndex)
{
	POSITION l_Position;
	CModbusListElement *l_pListElement, *l_pListElementCopy;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_iIndex == a_iIndex)
		{
			//Copy all the next items reversed
			while (l_Position != NULL)
			{
				l_pListElementCopy = GetNext(l_Position);
				*l_pListElement = *l_pListElementCopy;
				if (l_pListElement->m_iIndex>a_iIndex)
				{
					--l_pListElement->m_iIndex;
				}
				l_pListElement = l_pListElementCopy;
			}
			l_pListElement = RemoveTail();
			delete l_pListElement;
			return;
		}
	}
}

CModbusListElement* CModbusList::GetItemByIndex(int a_iIndex)
{
	POSITION l_Position;
	CModbusListElement*l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_iIndex == a_iIndex)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CModbusListElement* CModbusList::GetItemByKey(CString a_strKey)
{
	POSITION l_Position;
	CModbusListElement*l_pListElement;

	l_Position = GetHeadPosition();
	while (l_Position != NULL)
	{
		l_pListElement = GetNext(l_Position);
		if (l_pListElement->m_strModbusKey== a_strKey)
		{
			return l_pListElement;
		}
	}
	return NULL;
}

CModbusListElement* CModbusList::GetItemByPos(int a_iPos)
{
	POSITION l_Position;
	CModbusListElement*l_pListElement = NULL;
	int l_iPos;

	for (l_Position = GetHeadPosition(), l_iPos = 0;(l_iPos < a_iPos) && (l_Position != NULL);++l_iPos)
	{
		l_pListElement = GetNext(l_Position);
	}
	return l_pListElement;
}


IMPLEMENT_SERIAL(CModbusList, CObList, 0)
