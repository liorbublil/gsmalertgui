// LevelEventList.h: interface for the CLevelEventList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEVELEVENTLIST_H__F3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
#define AFX_LEVELEVENTLIST_H__F3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLevelEventListElement : public CObject
{
public:
	CLevelEventListElement();
	~CLevelEventListElement();
	void operator=(const CLevelEventListElement& a_LevelEventListElement);   // = operator


	int m_iIndex;
	int m_iSensorType;
	CString m_strSensorNum;
	CString m_strSensorId;
	double m_dLowHys;
	double m_dHighHys;
	CString m_strLowMsg;
	CString m_strHighMsg;
	int m_iNormalRange;
	COleDateTime m_odtRepeatAlert;
	COleDateTime m_odtHighDelay;
	COleDateTime m_odtLowDelay;
	CString m_strCloudAlarmKey;
	CString m_strLevelEventKey;
	bool m_bInUnit;
};

class CLevelEventList : public CTypedPtrList< CObList, CLevelEventListElement* > 
{
public:
	DECLARE_SERIAL( CLevelEventList )

	CLevelEventList();
	virtual ~CLevelEventList();
	void Serialize( CArchive& archive );
	
	void DeleteAllList();
	void AddItemSorted(int a_iIndex,int a_iSensorType, CString a_strSensorNum, CString a_strSensorId,double a_dLowHys,double a_dHighHys,CString a_strLowMsg,CString a_strHighMsg,int a_iNormalRange,COleDateTime a_odtRepeatAlert,COleDateTime a_odtLowDelay,COleDateTime a_odtHighDelay, CString a_strCloudAlarmKey, CString a_strLevelEventKey, bool a_bInUnit);

	void DeleteItem(int a_iIndex);
	CLevelEventListElement* GetItemByIndex(int a_iIndex);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_LEVELEVENTLIST_H__F3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
