// InputDialog.cpp : implementation file
//

#include "stdafx.h"
#include "GsmAlert.h"
#include "InputDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputDialog dialog
CInputDialog::CInputDialog(DWORD ai_dwFlags /*=NO_FLAGS*/, CString ai_strTitle /*=DIALOG_TITLE*/, CString ai_strMessage /*= DIALOG_MESSAGE*/, 
						   CString ai_strDefault/*_T("")*/, CWnd* pParent /*= NULL*/)
						   : CDialog(CInputDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInputDialog)
	//}}AFX_DATA_INIT
	SetFlags(ai_dwFlags);
	SetDefaultInputString(ai_strDefault);
	SetTitle(ai_strTitle);
	SetMessageText(ai_strMessage);	
}

void CInputDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInputDialog)
	DDX_Text(pDX, IDC_INPUT_EDIT, m_strInput);
	//}}AFX_DATA_MAP
}

void CInputDialog::SetDefaultInputString(CString a_strDefaultInput)
{
	m_strInput=a_strDefaultInput;
}

void CInputDialog::SetFlags(DWORD ai_dwFlags)
{
	m_dwFlags=ai_dwFlags;
}

CString CInputDialog::GetInputString()
{
	return m_strInput;
}

BEGIN_MESSAGE_MAP(CInputDialog, CDialog)
	//{{AFX_MSG_MAP(CInputDialog)
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputDialog message handlers


void CInputDialog::SetTitle(LPCTSTR ai_strTiltle)
{
	m_strTitle=ai_strTiltle;
}

void CInputDialog::SetMessageText(LPCTSTR ai_strMessage)
{
	m_strMessage=ai_strMessage;
}

int CInputDialog::InputBox(char* str_aoInputText)
{
	int l_iResult;
	l_iResult = DoModal();

	if (l_iResult==IDOK)
		strcpy(str_aoInputText, (LPCTSTR)GetInputString());

	return l_iResult;
}



void CInputDialog::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	SetWindowText(m_strTitle);
	CEdit* l_pInputEditCtrl = (CEdit*)GetDlgItem(IDC_INPUT_EDIT);

	DWORD l_dwFlagsToAdd =		(RIGHT_ALIGNMENT|RIGHT_TO_LEFT) & m_dwFlags;
	DWORD l_dwFlagsToRemove =	(RIGHT_ALIGNMENT|RIGHT_TO_LEFT) & (~m_dwFlags);

	// update the styles:
	ModifyStyleEx(l_dwFlagsToRemove, l_dwFlagsToAdd);
	l_pInputEditCtrl->ModifyStyleEx(l_dwFlagsToRemove, l_dwFlagsToAdd);

	if(m_dwFlags & PASSWORD_LETTERS)
	{
		l_pInputEditCtrl->SetPasswordChar('*');
	}
	
}

void CInputDialog::OnPaint() 
{
	CDialog::OnPaint();
	CRect l_rect;
	CStatic* l_pMessageCtrl = ((CStatic*)GetDlgItem(IDC_MESSAGE));//->GetClientRect(&l_rect);
	
	CPaintDC MessageDC(l_pMessageCtrl);

	l_pMessageCtrl->GetClientRect(&l_rect);

	UINT l_uiFlags=0;
	if (m_dwFlags&RIGHT_TO_LEFT)
		l_uiFlags |= DT_RTLREADING;
	if (m_dwFlags&RIGHT_ALIGNMENT)
		l_uiFlags |= DT_RIGHT;

	MessageDC.SetBkMode(TRANSPARENT);
	MessageDC.DrawText(m_strMessage, &l_rect, l_uiFlags);
	//l_pMessageCtrl->Invalidate();
	// Do not call CDialog::OnPaint() for painting messages
}
