# Microsoft Developer Studio Project File - Name="GsmAlert" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=GsmAlert - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "GsmAlert.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "GsmAlert.mak" CFG="GsmAlert - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "GsmAlert - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "GsmAlert - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/GsmAlert", PYAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "GsmAlert - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40d /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40d /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 version.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "GsmAlert - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40d /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40d /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 version.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "GsmAlert - Win32 Release"
# Name "GsmAlert - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AdcView.cpp
# End Source File
# Begin Source File

SOURCE=.\AlphaImageList.cpp
# End Source File
# Begin Source File

SOURCE=.\AlphaToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\BarSplitWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\ChangeLanguage.cpp
# End Source File
# Begin Source File

SOURCE=.\ComThread.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\DualSplitWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\Globals.cpp

!IF  "$(CFG)" == "GsmAlert - Win32 Release"

!ELSEIF  "$(CFG)" == "GsmAlert - Win32 Debug"

# ADD CPP /Yu

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\GsmAlert.cpp
# End Source File
# Begin Source File

SOURCE=.\GsmAlert.rc
# End Source File
# Begin Source File

SOURCE=.\GsmAlertDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\GsmAlertView.cpp
# End Source File
# Begin Source File

SOURCE=.\InputDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\Language.cpp
# End Source File
# Begin Source File

SOURCE=.\LevelAlertView.cpp
# End Source File
# Begin Source File

SOURCE=.\LevelEventList.cpp
# End Source File
# Begin Source File

SOURCE=.\ListToolBar.cpp
# End Source File
# Begin Source File

SOURCE=.\logger.cpp
# End Source File
# Begin Source File

SOURCE=.\LogView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MD5C.C
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\MDDRIVER.C
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\MenuBar.cpp
# End Source File
# Begin Source File

SOURCE=.\MiscSettingsView.cpp
# End Source File
# Begin Source File

SOURCE=.\MPIChildFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\MPIDocTemplate.cpp
# End Source File
# Begin Source File

SOURCE=.\MPITabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\MPITabWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\OneWireList.cpp
# End Source File
# Begin Source File

SOURCE=.\OutputList.cpp
# End Source File
# Begin Source File

SOURCE=.\PhonelistView.cpp
# End Source File
# Begin Source File

SOURCE=.\PhoneNumList.cpp
# End Source File
# Begin Source File

SOURCE=.\SearchModemDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SendQueue.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\ZoneList.cpp
# End Source File
# Begin Source File

SOURCE=.\ZoneListView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AdcView.h
# End Source File
# Begin Source File

SOURCE=.\AlphaImageList.h
# End Source File
# Begin Source File

SOURCE=.\AlphaToolBar.h
# End Source File
# Begin Source File

SOURCE=.\BarSplitWnd.h
# End Source File
# Begin Source File

SOURCE=.\ChangeLanguage.h
# End Source File
# Begin Source File

SOURCE=.\ComThread.h
# End Source File
# Begin Source File

SOURCE=.\DualSplitWnd.h
# End Source File
# Begin Source File

SOURCE=.\global.h
# End Source File
# Begin Source File

SOURCE=.\Globals.h
# End Source File
# Begin Source File

SOURCE=.\GsmAlert.h
# End Source File
# Begin Source File

SOURCE=.\GsmAlertDoc.h
# End Source File
# Begin Source File

SOURCE=.\GsmAlertView.h
# End Source File
# Begin Source File

SOURCE=.\InputDialog.h
# End Source File
# Begin Source File

SOURCE=.\Language.h
# End Source File
# Begin Source File

SOURCE=.\LevelAlertView.h
# End Source File
# Begin Source File

SOURCE=.\LevelEventList.h
# End Source File
# Begin Source File

SOURCE=.\ListToolBar.h
# End Source File
# Begin Source File

SOURCE=.\logger.h
# End Source File
# Begin Source File

SOURCE=.\LogView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MD5.H
# End Source File
# Begin Source File

SOURCE=.\MenuBar.h
# End Source File
# Begin Source File

SOURCE=.\MiscSettingsview.h
# End Source File
# Begin Source File

SOURCE=.\MPIChildFrame.h
# End Source File
# Begin Source File

SOURCE=.\MPIDocTemplate.h
# End Source File
# Begin Source File

SOURCE=.\MPITabCtrl.h
# End Source File
# Begin Source File

SOURCE=.\MPITabWnd.h
# End Source File
# Begin Source File

SOURCE=.\OneWireList.h
# End Source File
# Begin Source File

SOURCE=.\OutputList.h
# End Source File
# Begin Source File

SOURCE=.\PhonelistView.h
# End Source File
# Begin Source File

SOURCE=.\PhoneNumList.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SearchModemDlg.h
# End Source File
# Begin Source File

SOURCE=.\SendQueue.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\ZoneList.h
# End Source File
# Begin Source File

SOURCE=.\ZoneListView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\AVG.ico
# End Source File
# Begin Source File

SOURCE=.\res\Aviem.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Bisk.ico
# End Source File
# Begin Source File

SOURCE=.\res\cell_acc.ico
# End Source File
# Begin Source File

SOURCE=.\res\error.ICO
# End Source File
# Begin Source File

SOURCE=.\res\EXPLORER.ICO
# End Source File
# Begin Source File

SOURCE=.\res\GateTel.ico
# End Source File
# Begin Source File

SOURCE=.\res\GsmAlert.ico
# End Source File
# Begin Source File

SOURCE=.\res\GsmAlert.rc2
# End Source File
# Begin Source File

SOURCE=.\res\GsmAlertDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_cell.ico
# End Source File
# Begin Source File

SOURCE=.\res\idr_phon.ico
# End Source File
# Begin Source File

SOURCE=.\res\loaded.ico
# End Source File
# Begin Source File

SOURCE=.\res\LogoAVG.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoBisk.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoCellAccess.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoGateTel.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoPartsUnited.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoRotem.bmp
# End Source File
# Begin Source File

SOURCE=.\res\LogoTador.bmp
# End Source File
# Begin Source File

SOURCE=.\mdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\mpi_bar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\not_load.ico
# End Source File
# Begin Source File

SOURCE=.\res\Offline.ICO
# End Source File
# Begin Source File

SOURCE=.\res\OK.ico
# End Source File
# Begin Source File

SOURCE=.\res\on_call.ICO
# End Source File
# Begin Source File

SOURCE=.\res\PartsUnited.ico
# End Source File
# Begin Source File

SOURCE=.\res\restart.ICO
# End Source File
# Begin Source File

SOURCE=.\res\Rotem.ico
# End Source File
# Begin Source File

SOURCE=.\res\Settings_Tabs.bmp
# End Source File
# Begin Source File

SOURCE=.\res\shut_down.ICO
# End Source File
# Begin Source File

SOURCE=.\res\SINEWAVE.ICO
# End Source File
# Begin Source File

SOURCE=.\res\tador.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\warning.ICO
# End Source File
# End Group
# End Target
# End Project
