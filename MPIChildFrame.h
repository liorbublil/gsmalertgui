
#pragma once

class CDualSplitWnd;
class CStatusView;
class CLogView;
class CChannelImeiView;
class CPhoneListView;
class CMiscSettingsView;
class CZoneListView;
class CAdcView;
class CLevelAlertView;
class CModbusView;
class CWirelessSenseView;

// don't use these constants directly
// use the MPI_ macros from MPIDocTemplate.h
#define MPIT_VIEW		0
#define MPIT_HSPLIT		1
#define MPIT_VSPLIT		2
#define MPIT_TABS		3
#define MPIT_END		100

#define MPIS_BAR_LEFT	-1
#define MPIS_BAR_RIGHT	-2
#define MPIS_BAR_TOP	-1
#define MPIS_BAR_BOTTOM	-2

#define MPIBS_AUTO		-1


class CMPIChildFrame : public CMDIChildWnd
{
	DECLARE_DYNCREATE(CMPIChildFrame)
public:
	CMPIChildFrame();
	virtual ~CMPIChildFrame();

public:
	enum enmMPIChildFrameType
	{
		enmMPIChildFrameTypeNoType,
		enmMPIChildFrameTypeLog,
		enmMPIChildFrameTypeSettings
	};
	
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void ActivateFrame(int nCmdShow = -1);
	virtual void OnUpdateFrameMenu(BOOL bActivate, CWnd* pActivateWnd, HMENU hMenuAlt);
	virtual bool SetChildType(enmMPIChildFrameType a_enmNewType);
protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
public:
	virtual void OnUpdateFrameTitle(BOOL bAddToTitle);

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	// Recursive function for creating splitters and views
	BOOL CreateSettingsClient(CCreateContext* pContext);
	BOOL CreateStatusClient(CCreateContext* pContext);	

protected:
	// Automatically created splitter windows
	CTypedPtrList<CPtrList, CSplitterWnd*> m_listSplitters;

protected:
	afx_msg LRESULT OnCommandArrived (WPARAM wParam,LPARAM lParam);
	//{{AFX_MSG(CMPIChildFrame)
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
protected:
	enmMPIChildFrameType m_enmMPIChildFrameType;
public:
	CLogView *m_pLogView;
	CPhoneListView *m_pPhoneListView;
	CMiscSettingsView *m_pMiscSettingsView;
	CZoneListView *m_pZoneListView;
	CAdcView *m_pAdcView;
	CWirelessSenseView *m_pWirelessSenseView;
	CModbusView	*m_pModbusView;
	CLevelAlertView	*m_pLevelAlertView;
};