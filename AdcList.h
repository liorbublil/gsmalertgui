// OneWireList.h: interface for the COneWireList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ADCLIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
#define AFX_ADCLIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAdcListElement : public CObject
{
public:
	CAdcListElement();
	~CAdcListElement();
	void operator=(const CAdcListElement& a_AdcListElement);   // = operator

	int m_iSensorNumber;
	CString m_strSensorName;
	CString m_strLut;
	bool m_bInUnit;
};

class CAdcList : public CTypedPtrList< CObList, CAdcListElement* >
{
public:
	DECLARE_SERIAL(CAdcList)

	CAdcList();
	virtual ~CAdcList();
	void Serialize(CArchive& archive);

	void DeleteAllList();
	void AddItemSorted(int a_iSensorNum, CString a_strSensorName, CString a_strLut, bool a_bInUnit, bool a_bNoSearch);
	void DeleteItem(int a_iSensorNum);
	CAdcListElement* GetItemBySensorNum(int a_iSensorNum);
	CAdcListElement* GetItemByPos(int a_iPos);
	int GetAmountOfNotInUnit();
};

#endif // !defined(AFX_ADCLIST_H__D3B08F81_42E7_4973_A1E8_7E941838532A__INCLUDED_)
