#if !defined(AFX_SEARCHMODEMDLG_H__A444BA49_8F91_40AF_A5A1_C6CD09D8C552__INCLUDED_)
#define AFX_SEARCHMODEMDLG_H__A444BA49_8F91_40AF_A5A1_C6CD09D8C552__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GsmAlertDoc.h"
// SearchModemDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSearchModemDlg dialog

class CSearchModemDlg : public CDialog
{
// Construction
public:
	CSearchModemDlg(CGsmAlertDoc *a_GsmAlertDoc,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSearchModemDlg)
	enum { IDD = IDD_SEARCH_MODEM_DIALOG };
	CStatic	m_lblUnitPhoneNumber;
	CEdit	m_txtUnitPhoneNumber;
	CComboBox	m_ctrlComPorts;
	BOOL	m_bUseModem;
	CString	m_strUnitPhoneNumber;
	BOOL	m_bSavePass;
	CString	m_strPass;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSearchModemDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CGsmAlertDoc *m_GsmAlertDoc;
	void Localize();
	// Generated message map functions
	//{{AFX_MSG(CSearchModemDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnChkUseModem();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#define MAX_COM_PORTS 100

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SEARCHMODEMDLG_H__A444BA49_8F91_40AF_A5A1_C6CD09D8C552__INCLUDED_)
